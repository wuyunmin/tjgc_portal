package com.ruoyi.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.http.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Administrator
 */
public class TencentEmailUtils {
    private static final Logger log = LoggerFactory.getLogger(TencentEmailUtils.class);

    private static final String CORP_ID ="wm63897d5fb624b4e6";

    private static final String LINK_ID ="cipMzkfqop6IiIckbqfnJst9ywdp8988jHu97NBCljzO0do7oby2R6n6lBQtl4m3";

    private static final String SSO_ID ="ypmTYFIxjsMNZZzosnBMDgWEAgQOn4Qwg9kLcRIhP58UvGG847j0FWp_jOXgDrkK";

    private static final String EMILE_NUM_ID ="c1yw51F_jK5HHrZOVqSGJxPZvTlUqzjcvcb3QHcKXqxfpcUTrXCtfFpXF6BwKeXp";

    private static final String SYSTEM_ID="iE7FjHlTPGhLlHYc3bSz2r7lCvwfRWOPvC6xbh47l7P3l4LQuixkH5wBasJMOafG";


    public static String tencentEmailSsoIn(String userId){
        log.info("获取腾讯单点登陆地址 {}",userId);
        String accessToken = getAccessToken(SSO_ID);
        String url ="https://api.exmail.qq.com/cgi-bin/service/get_login_url";
        StringBuffer paramStrBuffer = new StringBuffer();
        paramStrBuffer.append("access_token=").append(accessToken).append("&userid=").append(userId);
        String returnStr = HttpUtils.sendGet(url, paramStrBuffer.toString());
        JSONObject jsonObject = JSON.parseObject(returnStr);
        log.info(jsonObject.toString());
        return jsonObject.getString("login_url");
    }

    /**
     *        StringBuffer stringBuffer = new StringBuffer();
     *        stringBuffer.append("access_token=").append(accessToken)
     *                 .append("&begin_date=").append(format1)
     *                 .append("&end_date=").append(format2)
     *                 .append("&userid=").append(userId);
     *       System.out.println(append.toString());
     *
     *
     *
     *         HashMap<String, Object> param = new HashMap<>(4);
     *         param.put("access_token", accessToken);
     *         param.put("begin_date", parse);
     *         param.put("end_date", parse1);
     *         param.put("userid", userId);
     *         JSONObject paramsObj = new JSONObject(param);
     * @param userId
     * @return
     * @throws Exception
     */
    public static boolean tencentEmailLogin(String userId)throws Exception{
        String rightCode = "0";
        String accessToken = getAccessToken(SYSTEM_ID);
        String url = "https://api.exmail.qq.com/cgi-bin/log/login";

        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        Date parse = ft.parse("2020-12-01");
        Date parse1 = ft.parse("2020-12-07");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("access_token=").append(accessToken)
                .append("&begin_date=").append(parse)
                .append("&end_date=").append(parse1)
                .append("&userid=").append(userId);
        System.out.println(stringBuffer.toString());
        String  returnStr = HttpUtils.sendPost(url, stringBuffer.toString());
        JSONObject jsonObject = JSON.parseObject(returnStr);
        log.info("jsonObject :  {}", jsonObject);
        if (rightCode.equals(jsonObject.getString("errcode"))) {
            JSONArray list = jsonObject.getJSONArray("list");
            int size = list.size();
            return size > 0;
        } else {
            throw new Exception(jsonObject.getString("errmsg"));
        }
    }

    public static Integer tencentEmailNum(String userId) throws Exception {
        long startLong = System.currentTimeMillis();
        String rightCode ="0";
        log.info("获取腾讯邮箱未读数量： {}", userId);
        String accessToken = getAccessToken(EMILE_NUM_ID);
        String url = "https://api.exmail.qq.com/cgi-bin/mail/newcount";
        StringBuffer paramStrBuffer = new StringBuffer();
        paramStrBuffer.append("access_token=").append(accessToken).append("&userid=").append(userId);
        String returnStr = HttpUtils.sendGet(url, paramStrBuffer.toString());
        JSONObject jsonObject = JSON.parseObject(returnStr);
        long endLong = System.currentTimeMillis();
        log.info("获取邮箱待办数量耗时：{}", (endLong - startLong));
        if (rightCode.equals(jsonObject.getString("errcode"))) {
            return jsonObject.getInteger("count");
        } else {
            String errMsg = jsonObject.getString("errmsg");
            log.error(errMsg);
            throw new Exception(errMsg);
        }
    }

    private static String getAccessToken(String corpSecret){
        String  url ="https://api.exmail.qq.com/cgi-bin/gettoken";
        StringBuffer paramStrBuffer = new StringBuffer();
        paramStrBuffer.append("corpid=").append(CORP_ID).append("&corpsecret=").append(corpSecret);
        String returnStr = HttpUtils.sendGet(url, paramStrBuffer.toString());
        JSONObject jsonObject = JSON.parseObject(returnStr);
        String access_token = jsonObject.getString("access_token");
        log.info("access_token : {}",access_token);
        return access_token;
    }
}
