/**
 * ArrayOfSmsReportListHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ruoyi.common.utils.sms.tempuri.holders;

import com.ruoyi.common.utils.sms.tempuri.SmsReportGroup;

public final class ArrayOfSmsReportListHolder implements javax.xml.rpc.holders.Holder {
    public SmsReportGroup[] value;

    public ArrayOfSmsReportListHolder() {
    }

    public ArrayOfSmsReportListHolder(SmsReportGroup[] value) {
        this.value = value;
    }

}
