package com.ruoyi.common.utils.sms.util;

/**
 * @author villwang
 * @date 2020/07/09
 **/
public class MD5 {
    static final byte[] a = new byte[]{-128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    private long[] b = new long[4];
    private long[] c = new long[2];
    private byte[] d = new byte[64];
    public String digestHexStr;
    private byte[] e = new byte[16];

    public String getMD5ofStr(String s) {
        this.a();
        this.a(s.getBytes(), s.length());
        this.b();
        this.digestHexStr = "";

        for(int i = 0; i < 16; ++i) {
            this.digestHexStr = this.digestHexStr + byteHEX(this.e[i]);
        }

        return this.digestHexStr;
    }

    public byte[] getMD5(byte[] abyte0) {
        this.a();
        this.a(abyte0, abyte0.length);
        this.b();
        return this.e;
    }

    public MD5() {
        this.a();
    }

    private void a() {
        this.c[0] = 0L;
        this.c[1] = 0L;
        this.b[0] = 1732584193L;
        this.b[1] = 4023233417L;
        this.b[2] = 2562383102L;
        this.b[3] = 271733878L;
    }

    private static long a(long l, long l1, long l2) {
        return l & l1 | ~l & l2;
    }

    private static long b(long l, long l1, long l2) {
        return l & l2 | l1 & ~l2;
    }

    private static long c(long l, long l1, long l2) {
        return l ^ l1 ^ l2;
    }

    private static long d(long l, long l1, long l2) {
        return l1 ^ (l | ~l2);
    }

    private long a(long l, long l1, long l2, long l3, long l4, long l5, long l6) {
        return (long)((int)(l += a(l1, l2, l3) + l4 + l6) << (int)l5 | (int)l >>> (int)(32L - l5)) + l1;
    }

    private long b(long l, long l1, long l2, long l3, long l4, long l5, long l6) {
        return (long)((int)(l += b(l1, l2, l3) + l4 + l6) << (int)l5 | (int)l >>> (int)(32L - l5)) + l1;
    }

    private long c(long l, long l1, long l2, long l3, long l4, long l5, long l6) {
        return (long)((int)(l += c(l1, l2, l3) + l4 + l6) << (int)l5 | (int)l >>> (int)(32L - l5)) + l1;
    }

    private long d(long l, long l1, long l2, long l3, long l4, long l5, long l6) {
        return (long)((int)(l += d(l1, l2, l3) + l4 + l6) << (int)l5 | (int)l >>> (int)(32L - l5)) + l1;
    }

    private void a(byte[] abyte0, int i) {
        byte[] abyte1 = new byte[64];
        int k = (int)(this.c[0] >>> 3) & 63;
        if ((this.c[0] += (long)(i << 3)) < (long)(i << 3)) {
            ++this.c[1];
        }

        this.c[1] += (long)(i >>> 29);
        int l = 64 - k;
        int j;
        if (i >= l) {
            a(this.d, abyte0, k, 0, l);
            this.a(this.d);

            for(j = l; j + 63 < i; j += 64) {
                a(abyte1, abyte0, 0, j, 64);
                this.a(abyte1);
            }

            k = 0;
        } else {
            j = 0;
        }

        a(this.d, abyte0, k, j, i - j);
    }

    private void b() {
        byte[] abyte0;
        a((byte[])(abyte0 = new byte[8]), (long[])this.c, 8);
        int i;
        int j = (i = (int)(this.c[0] >>> 3) & 63) >= 56 ? 120 - i : 56 - i;
        this.a(a, j);
        this.a(abyte0, 8);
        a((byte[])this.e, (long[])this.b, 16);
    }

    private static void a(byte[] abyte0, byte[] abyte1, int i, int j, int k) {
        for(int l = 0; l < k; ++l) {
            abyte0[i + l] = abyte1[j + l];
        }

    }

    private void a(byte[] abyte0) {
        long l = this.b[0];
        long l1 = this.b[1];
        long l2 = this.b[2];
        long l3 = this.b[3];
        long[] al;
        a((long[])(al = new long[16]), (byte[])abyte0, 64);
        l = this.a(l, l1, l2, l3, al[0], 7L, 3614090360L);
        l3 = this.a(l3, l, l1, l2, al[1], 12L, 3905402710L);
        l2 = this.a(l2, l3, l, l1, al[2], 17L, 606105819L);
        l1 = this.a(l1, l2, l3, l, al[3], 22L, 3250441966L);
        l = this.a(l, l1, l2, l3, al[4], 7L, 4118548399L);
        l3 = this.a(l3, l, l1, l2, al[5], 12L, 1200080426L);
        l2 = this.a(l2, l3, l, l1, al[6], 17L, 2821735955L);
        l1 = this.a(l1, l2, l3, l, al[7], 22L, 4249261313L);
        l = this.a(l, l1, l2, l3, al[8], 7L, 1770035416L);
        l3 = this.a(l3, l, l1, l2, al[9], 12L, 2336552879L);
        l2 = this.a(l2, l3, l, l1, al[10], 17L, 4294925233L);
        l1 = this.a(l1, l2, l3, l, al[11], 22L, 2304563134L);
        l = this.a(l, l1, l2, l3, al[12], 7L, 1804603682L);
        l3 = this.a(l3, l, l1, l2, al[13], 12L, 4254626195L);
        l2 = this.a(l2, l3, l, l1, al[14], 17L, 2792965006L);
        l1 = this.a(l1, l2, l3, l, al[15], 22L, 1236535329L);
        l = this.b(l, l1, l2, l3, al[1], 5L, 4129170786L);
        l3 = this.b(l3, l, l1, l2, al[6], 9L, 3225465664L);
        l2 = this.b(l2, l3, l, l1, al[11], 14L, 643717713L);
        l1 = this.b(l1, l2, l3, l, al[0], 20L, 3921069994L);
        l = this.b(l, l1, l2, l3, al[5], 5L, 3593408605L);
        l3 = this.b(l3, l, l1, l2, al[10], 9L, 38016083L);
        l2 = this.b(l2, l3, l, l1, al[15], 14L, 3634488961L);
        l1 = this.b(l1, l2, l3, l, al[4], 20L, 3889429448L);
        l = this.b(l, l1, l2, l3, al[9], 5L, 568446438L);
        l3 = this.b(l3, l, l1, l2, al[14], 9L, 3275163606L);
        l2 = this.b(l2, l3, l, l1, al[3], 14L, 4107603335L);
        l1 = this.b(l1, l2, l3, l, al[8], 20L, 1163531501L);
        l = this.b(l, l1, l2, l3, al[13], 5L, 2850285829L);
        l3 = this.b(l3, l, l1, l2, al[2], 9L, 4243563512L);
        l2 = this.b(l2, l3, l, l1, al[7], 14L, 1735328473L);
        l1 = this.b(l1, l2, l3, l, al[12], 20L, 2368359562L);
        l = this.c(l, l1, l2, l3, al[5], 4L, 4294588738L);
        l3 = this.c(l3, l, l1, l2, al[8], 11L, 2272392833L);
        l2 = this.c(l2, l3, l, l1, al[11], 16L, 1839030562L);
        l1 = this.c(l1, l2, l3, l, al[14], 23L, 4259657740L);
        l = this.c(l, l1, l2, l3, al[1], 4L, 2763975236L);
        l3 = this.c(l3, l, l1, l2, al[4], 11L, 1272893353L);
        l2 = this.c(l2, l3, l, l1, al[7], 16L, 4139469664L);
        l1 = this.c(l1, l2, l3, l, al[10], 23L, 3200236656L);
        l = this.c(l, l1, l2, l3, al[13], 4L, 681279174L);
        l3 = this.c(l3, l, l1, l2, al[0], 11L, 3936430074L);
        l2 = this.c(l2, l3, l, l1, al[3], 16L, 3572445317L);
        l1 = this.c(l1, l2, l3, l, al[6], 23L, 76029189L);
        l = this.c(l, l1, l2, l3, al[9], 4L, 3654602809L);
        l3 = this.c(l3, l, l1, l2, al[12], 11L, 3873151461L);
        l2 = this.c(l2, l3, l, l1, al[15], 16L, 530742520L);
        l1 = this.c(l1, l2, l3, l, al[2], 23L, 3299628645L);
        l = this.d(l, l1, l2, l3, al[0], 6L, 4096336452L);
        l3 = this.d(l3, l, l1, l2, al[7], 10L, 1126891415L);
        l2 = this.d(l2, l3, l, l1, al[14], 15L, 2878612391L);
        l1 = this.d(l1, l2, l3, l, al[5], 21L, 4237533241L);
        l = this.d(l, l1, l2, l3, al[12], 6L, 1700485571L);
        l3 = this.d(l3, l, l1, l2, al[3], 10L, 2399980690L);
        l2 = this.d(l2, l3, l, l1, al[10], 15L, 4293915773L);
        l1 = this.d(l1, l2, l3, l, al[1], 21L, 2240044497L);
        l = this.d(l, l1, l2, l3, al[8], 6L, 1873313359L);
        l3 = this.d(l3, l, l1, l2, al[15], 10L, 4264355552L);
        l2 = this.d(l2, l3, l, l1, al[6], 15L, 2734768916L);
        l1 = this.d(l1, l2, l3, l, al[13], 21L, 1309151649L);
        l = this.d(l, l1, l2, l3, al[4], 6L, 4149444226L);
        l3 = this.d(l3, l, l1, l2, al[11], 10L, 3174756917L);
        l2 = this.d(l2, l3, l, l1, al[2], 15L, 718787259L);
        l1 = this.d(l1, l2, l3, l, al[9], 21L, 3951481745L);
        this.b[0] += l;
        this.b[1] += l1;
        this.b[2] += l2;
        this.b[3] += l3;
    }

    private static void a(byte[] abyte0, long[] al, int i) {
        int j = 0;

        for(int k = 0; k < i; k += 4) {
            abyte0[k] = (byte)((int)(al[j] & 255L));
            abyte0[k + 1] = (byte)((int)(al[j] >>> 8 & 255L));
            abyte0[k + 2] = (byte)((int)(al[j] >>> 16 & 255L));
            abyte0[k + 3] = (byte)((int)(al[j] >>> 24 & 255L));
            ++j;
        }

    }

    private static void a(long[] al, byte[] abyte0, int i) {
        int j = 0;

        for(int k = 0; k < i; k += 4) {
            al[j] = b2iu(abyte0[k]) | b2iu(abyte0[k + 1]) << 8 | b2iu(abyte0[k + 2]) << 16 | b2iu(abyte0[k + 3]) << 24;
            ++j;
        }

    }

    public static long b2iu(byte byte0) {
        return (long)(byte0 >= 0 ? byte0 : byte0 & 255);
    }

    public static String byteHEX(byte byte0) {
        char[] ac = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] ac1;
        (ac1 = new char[2])[0] = ac[byte0 >>> 4 & 15];
        ac1[1] = ac[byte0 & 15];
        String s = null;
        s = new String(ac1);
        return s;
    }
}
