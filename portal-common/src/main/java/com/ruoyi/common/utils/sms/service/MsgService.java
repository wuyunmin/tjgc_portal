package com.ruoyi.common.utils.sms.service;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author villwang
 * @date 2020/07/08
 **/
public interface MsgService {
    /**
     * @return java.lang.String
     * @describe 获取token
     * @params [] content 内容  nums 手机号码集合
     * @author VillWang
     * @date 2020/5/21
     * @other
     */
    public Map<String ,Object> sendMsg(String phone, String content);

    public void SendMsg(ArrayList<String> strings, String content);

    public void SendMsg2(String strings, String content);
}
