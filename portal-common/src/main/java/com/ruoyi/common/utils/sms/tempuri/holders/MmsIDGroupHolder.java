/**
 * MmsIDGroupHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ruoyi.common.utils.sms.tempuri.holders;

import com.ruoyi.common.utils.sms.tempuri.MmsIDGroup;

public final class MmsIDGroupHolder implements javax.xml.rpc.holders.Holder {
    public MmsIDGroup value;

    public MmsIDGroupHolder() {
    }

    public MmsIDGroupHolder(MmsIDGroup value) {
        this.value = value;
    }

}
