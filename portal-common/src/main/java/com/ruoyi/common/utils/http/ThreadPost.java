package com.ruoyi.common.utils.http;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ruoyi.common.utils.sms.util.JSONUtils;

import java.util.HashMap;
import java.util.Map;

public class ThreadPost  {

    private static Logger logger = LoggerFactory.getLogger(ThreadPost.class);
    /**
     *
     * 发送数据  protal 使用
     *
     * 当发送失败时根据配置次数重试
     * @param
     * @param
     * @param paramMap 请求内容
     * @return 返回 null/"" 时成功,否则返回失败内容
     */
    public static Map<String, Object> postRequestResult(String postUrl,Map<String, String> paramMap, String authorization){
        String url = null;
        int i = 0;
        while (true){
            String requestString=null;
            logger.debug("发送前——————————————————-"+postUrl+"___"+paramMap);
            requestString = HttpLocalUtils.sendGet2(postUrl,paramMap);
            logger.debug("发送后——————————————————-"+requestString);
            // 发送后返回信息
            try {
                if(StringUtils.isNotBlank(requestString)){
                    Map<String, Object> stringObjectMap = JSONUtils.parseMap(requestString);
                    if( "".equals(stringObjectMap.get("Message"))||stringObjectMap.get("Message")==null){
                        Thread.sleep(5000);
                    } else {
                        return (Map<String, Object>) stringObjectMap.get("Result");
                    }
                }
            } catch (Exception e){
                System.out.println(e.getMessage());
            }
            i ++;
            if(i > 3) {
                break;
            }
            // 大于尝试次数退出
        }
        return null;
    }
    /**
     *
     * 发送数据  protal 使用
     *
     * 当发送失败时根据配置次数重试
     * @param
     * @param
     * @param
     * @return 返回 null/"" 时成功,否则返回失败内容
     */
    public static String postRequestResult2(String postUrl,String json, String authorization){
        String url = null;
        int i = 0;
            String requestString=null;
            logger.debug("发送前——————————————————-"+postUrl+"___"+json);
            requestString = HttpLocalUtils.sendHttpPost(postUrl,json,null);
            logger.debug("发送后——————————————————-"+requestString);
            // 发送后返回信息
            try {
                if(StringUtils.isNotBlank(requestString)){
                        return requestString;
                }
            } catch (Exception e){
                System.out.println(e.getMessage());
            }

            // 大于尝试次数退出
        return null;
    }

    public String getToken(String postUrl, String paramMap, String authorization) {
       return  null;
    }
    /**
     *
     * 发送数据  protal 使用
     *
     * 当发送失败时根据配置次数重试
     * @param
     * @param
     * @param
     * @return 返回 null/"" 时成功,否则返回失败内容
     */
    public static Map<String, Object> doPost2RequestResult(String postUrl,String json, String authorization){
        String url = null;
        int i = 0;
        while (true){
            String requestString=null;
            logger.debug("发送前——————————————————-"+postUrl+"___"+json);
            requestString = HttpLocalUtils.doPost2(postUrl,json,"");
            logger.debug("发送后——————————————————-"+requestString);
            // 发送后返回信息
            try {
                if(StringUtils.isNotBlank(requestString)){
                    Map<String, Object> stringObjectMap = JSONUtils.parseMap(requestString);
                    if( "".equals(stringObjectMap.get("SmsIDList"))||stringObjectMap.get("SmsIDList")==null){
                        Thread.sleep(5000);
                    } else {
                        return stringObjectMap;
                    }
                }
            } catch (Exception e){
                System.out.println(e.getMessage());
            }
            i ++;
            if(i > 3) {
                break;
            }
            // 大于尝试次数退出
        }
        return null;
    }

    /**
     *
     * 发送数据  protal 使用
     *
     * 当发送失败时根据配置次数重试
     * @param
     * @param
     * @param
     * @return 返回 null/"" 时成功,否则返回失败内容
     */
    public static Map<String, Object> doPost2Request(String postUrl, String json, String authorization){
        String url = null;
        int i = 0;
        while (true){
            String requestString=null;
            logger.debug("发送前——————————————————-"+postUrl+"___"+json);
            requestString = HttpLocalUtils.doPost2(postUrl, json,null);
            logger.debug("发送后——————————————————-"+requestString);
            // 发送后返回信息
            try {
                if(StringUtils.isNotBlank(requestString)){
                    Map<String, Object> stringObjectMap = JSONUtils.parseMap(requestString);
                    
                    if( "".equals(stringObjectMap.get("code"))||stringObjectMap.get("code")==null|| (!"200".equals(stringObjectMap.get("code")) && Integer.parseInt(stringObjectMap.get("code").toString())!=200)){
                        Thread.sleep(5000);
                    } else {
                        return stringObjectMap;
                    }
                }
            } catch (Exception e){
                logger.error("请求失败，详细信息："+e);
                System.out.println(e.getMessage());
            }
            i ++;
            if(i > 3) {
                break;
            }
            // 大于尝试次数退出
        }
        return null;
    }


}
