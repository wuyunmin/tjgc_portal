/**
 * SmsIDGroupHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ruoyi.common.utils.sms.tempuri.holders;

import com.ruoyi.common.utils.sms.tempuri.SmsIDGroup;

public final class SmsIDGroupHolder implements javax.xml.rpc.holders.Holder {
    public SmsIDGroup value;

    public SmsIDGroupHolder() {
    }

    public SmsIDGroupHolder(SmsIDGroup value) {
        this.value = value;
    }

}
