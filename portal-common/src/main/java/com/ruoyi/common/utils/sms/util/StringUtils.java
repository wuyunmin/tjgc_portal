package com.ruoyi.common.utils.sms.util;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * @author villwang
 * @date 2020/07/09
 **/
public class StringUtils {
    public StringUtils() {
    }

    public static final String replace(String line, String oldString, String newString) {
        if (line == null) {
            return null;
        } else {
            int i = 0;
            if ((i = line.indexOf(oldString, i)) < 0) {
                return line;
            } else {
                char[] line2 = line.toCharArray();
                char[] newString2 = newString.toCharArray();
                int oLength = oldString.length();
                StringBuffer buf = new StringBuffer(line2.length);
                buf.append(line2, 0, i).append(newString2);
                i += oLength;

                int j;
                for(j = i; (i = line.indexOf(oldString, i)) > 0; j = i) {
                    buf.append(line2, j, i - j).append(newString2);
                    i += oLength;
                }

                buf.append(line2, j, line2.length - j);
                return buf.toString();
            }
        }
    }

    public static final String replace(String line, String oldString, String newString, int[] count) {
        if (line == null) {
            return null;
        } else {
            int i = 0;
            if ((i = line.indexOf(oldString, i)) < 0) {
                return line;
            } else {
                int counter = 0;
                 counter = counter + 1;
                char[] line2 = line.toCharArray();
                char[] newString2 = newString.toCharArray();
                int oLength = oldString.length();
                StringBuffer buf = new StringBuffer(line2.length);
                buf.append(line2, 0, i).append(newString2);
                i += oLength;

                int j;
                for(j = i; (i = line.indexOf(oldString, i)) > 0; j = i) {
                    ++counter;
                    buf.append(line2, j, i - j).append(newString2);
                    i += oLength;
                }

                buf.append(line2, j, line2.length - j);
                count[0] = counter;
                return buf.toString();
            }
        }
    }

    public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        } else {
            return !"".equals(str);
        }
    }

    public static String getTaskStatue(long statue) {
        if (0L == statue) {
            return "0- 正在上传";
        } else if (1L == statue) {
            return "1- 待发送";
        } else if (2L == statue) {
            return "2- 审核中";
        } else if (3L == statue) {
            return "3- 审核失败";
        } else if (4L == statue) {
            return "4- 正在发送";
        } else if (5L == statue) {
            return "5- 余额不足";
        } else if (6L == statue) {
            return "6- 强制停止";
        } else if (7L == statue) {
            return "7- 发送完成";
        } else if (8L == statue) {
            return "8- 用户审核";
        } else if (9L == statue) {
            return "9- 用户审核2";
        } else {
            return 10L == statue ? "10- 用户审核失败" : "其他错误：" + statue;
        }
    }
}
