package com.ruoyi.common.utils.sms;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.config.SmsConfig;
import com.ruoyi.common.utils.sms.bean.DataObjectBean;
import com.ruoyi.common.utils.sms.bean.msmResultBean;
import com.ruoyi.common.utils.sms.factory.DataObjectFactory;
import com.ruoyi.common.utils.sms.tempuri.MobileListGroup;
import com.ruoyi.common.utils.sms.tempuri.MobsetApiSoap;
import com.ruoyi.common.utils.sms.tempuri.holders.ArrayOfSmsIDListHolder;
import com.ruoyi.common.utils.sms.util.MD5;
import com.ruoyi.common.utils.sms.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.rpc.holders.LongHolder;
import javax.xml.rpc.holders.StringHolder;

@Slf4j
public class sms_Send {
	private static long corpID;
	private static String loginName;
	private static String password;
	private static String serverIP;
    private static	URL url;
	private static String timeStamp = "1105203910";
	private static String addNum;
	private static String timer;
	private static long longSms = 0;
	private static MobileListGroup[] mobileList;
	private static StringHolder errMsg;
	private static ArrayOfSmsIDListHolder smsIDList;
	private static LongHolder count ;
	
	public static msmResultBean SendMsg(String mobiles, String content){
		log.info("构建短信 信息");
		Date now = new Date(); 
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMddHHmmss");//���Է�����޸����ڸ�ʽ
		timeStamp = dateFormat.format(now);
		errMsg = new StringHolder();
		smsIDList = new ArrayOfSmsIDListHolder();
		count = new LongHolder();
		msmResultBean msmBean = new msmResultBean();
		DataObjectBean bean = DataObjectFactory.getInstance();
		corpID= Long.parseLong(SmsConfig.cordId);
		loginName = SmsConfig.userName;
		password = SmsConfig.passwd;
		serverIP = SmsConfig.serverIP;

		log.debug(" corpID:{}",corpID);
		log.debug(" loginName:{}",loginName);
		log.debug("serverIP :{}",serverIP);
		if (log.isDebugEnabled()) {
			log.debug(" password:{}",password);
		}

		try
		{
			url=new URL(serverIP);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.info("构建短信url 异常");
			log.info(e.getMessage(),e);

		}
        MobsetApiSoap mobset = DataObjectFactory.getMobsetApi(url);
		String [] mobileArray= StringUtils.replace(mobiles, "；", ";").split(";");
		mobileList = new MobileListGroup[mobileArray.length];
		
		for (int i = 0;i<mobileList.length;i++) {
			mobileList[i] = new MobileListGroup();
			mobileList[i].setMobile(mobileArray[i]);
		}
		MD5 md5 = new MD5();
		password = md5.getMD5ofStr(corpID+password+timeStamp);
		
		try {
			log.info("开始发送短信....");
			mobset.sms_Send(corpID, loginName, password, timeStamp, addNum, timer, longSms, mobileList, content, count, errMsg, smsIDList);
			msmBean.setErrMsg(errMsg);
			msmBean.setMobileList(mobileList);
			msmBean.setSmsIDList(smsIDList);
			log.info("短信发送结果: {}", JSON.toJSONString(msmBean));
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.info("短信发送 异常");
			log.info(e.getMessage(),e);
		}


		return msmBean;
	}
}
