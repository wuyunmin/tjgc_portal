package com.ruoyi.common.utils.sms.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ruoyi.common.config.SmsConfig;
import com.ruoyi.common.utils.http.ThreadPost;
import com.ruoyi.common.utils.sms.sms_Send;
import com.ruoyi.common.utils.sms.bean.DataObjectBean;
import com.ruoyi.common.utils.sms.bean.msmResultBean;
import com.ruoyi.common.utils.sms.factory.DataObjectFactory;
import com.ruoyi.common.utils.sms.service.MsgService;
import com.ruoyi.common.utils.sms.tempuri.MobileListGroup;
import com.ruoyi.common.utils.sms.tempuri.MobsetApiSoap;
import com.ruoyi.common.utils.sms.tempuri.holders.ArrayOfSmsIDListHolder;
import com.ruoyi.common.utils.sms.util.JSONUtils;
import com.ruoyi.common.utils.sms.util.MD5;

import javax.xml.rpc.holders.LongHolder;
import javax.xml.rpc.holders.StringHolder;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author villwang
 * @date 2020/07/08
 **/
@Service
@Slf4j
public class MsgServiceImpl implements MsgService{
    private Long corpID;
    private String loginName;
    private String password;
    private String url;
    private static URL urls;
    private static String timeStamp = "1105203910";
    private static String addNum;
    private static String timer = "2012-11-05 20:39:10";
    private static long longSms = 0L;
    private static MobileListGroup[] mobileList;
    private static StringHolder errMsg;
    private static ArrayOfSmsIDListHolder smsIDList;
    private static LongHolder count;

    public MsgServiceImpl() {
    }

    public  Map<String ,Object> sendMsg(String phone, String content) {
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYYMMddHHmmss");
        timeStamp = dateFormat.format(now);
        MD5 md5 = new MD5();
        String SecretKey = md5.getMD5ofStr(corpID + password + timeStamp);
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put("Methods","SmsSend");
        map.put("CorpID",corpID);
        map.put("LoginName",loginName);
        map.put("Password",password);
        map.put("SecretKey",SecretKey);
        map.put("TimeStamp",timeStamp);
        map.put("PhoneNumbers",phone);
        map.put("LongSms","0");
        map.put("Content","测试");
        String json=JSONUtils.toJson(map);
        //1.获取数据
        Map<String, Object> requestString= ThreadPost.doPost2RequestResult(url,json,null);
        if(requestString==null||requestString.isEmpty()){
        }else {
            System.out.println(requestString.toString());
        }
        return null;
    }

    public  void SendMsg(ArrayList<String> strings, String content) {
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMddHHmmss");
        timeStamp = dateFormat.format(now);
        errMsg = new StringHolder();
        smsIDList = new ArrayOfSmsIDListHolder();
        count = new LongHolder();
        msmResultBean msmBean = new msmResultBean();
        DataObjectBean bean = DataObjectFactory.getInstance();
        corpID = new Long(SmsConfig.cordId);
        loginName = SmsConfig.userName;
        password = SmsConfig.passwd;
        try {
            urls = new URL(SmsConfig.serverIP);
        } catch (Exception var11) {
            var11.printStackTrace();
        }
        MobsetApiSoap mobset = DataObjectFactory.getMobsetApi(urls);
        // String[] mobileArray = StringUtils.replace(mobiles, "；", ";").split(";");
        mobileList = new MobileListGroup[strings.size()];
        for(int i = 0; i < mobileList.length; ++i) {
            mobileList[i] = new MobileListGroup();
            mobileList[i].setMobile(strings.get(i));
        }
        MD5 md5 = new MD5();
        password = md5.getMD5ofStr(corpID + password + timeStamp);
        try {
            mobset.sms_Send(corpID, loginName, password, timeStamp, addNum, timer, longSms, mobileList, content, count, errMsg, smsIDList);
            msmBean.setErrMsg(errMsg);
            msmBean.setMobileList(mobileList);
            msmBean.setSmsIDList(smsIDList);
        } catch (RemoteException var10) {
            var10.printStackTrace();
        }
        //return msmBean;
    }
    /**
     *@描述  短信调用接口
     *@参数  strings 手机号，  多个手机号已 ; 隔开  content 短信内容
     *@返回值
     *@创建人 villwang
     *@创建时间
     *@修改人和其它信息
     */
    public  void SendMsg2(String strings, String content) {
        log.info("准备发送短信");
        sms_Send.SendMsg(strings, content);
    }
}
