/**
 * MobsetApi.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ruoyi.common.utils.sms.tempuri;

public interface MobsetApi extends javax.xml.rpc.Service {
    public String getMobsetApiSoapAddress();

    public MobsetApiSoap getMobsetApiSoap() throws javax.xml.rpc.ServiceException;

    public MobsetApiSoap getMobsetApiSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
