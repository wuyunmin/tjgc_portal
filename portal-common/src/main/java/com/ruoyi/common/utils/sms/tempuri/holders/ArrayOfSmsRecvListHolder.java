/**
 * ArrayOfSmsRecvListHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ruoyi.common.utils.sms.tempuri.holders;

import com.ruoyi.common.utils.sms.tempuri.SmsRecvGroup;

public final class ArrayOfSmsRecvListHolder implements javax.xml.rpc.holders.Holder {
    public SmsRecvGroup[] value;

    public ArrayOfSmsRecvListHolder() {
    }

    public ArrayOfSmsRecvListHolder(SmsRecvGroup[] value) {
        this.value = value;
    }

}
