/**
 * SmsRecvGroupHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ruoyi.common.utils.sms.tempuri.holders;

import com.ruoyi.common.utils.sms.tempuri.SmsRecvGroup;

public final class SmsRecvGroupHolder implements javax.xml.rpc.holders.Holder {
    public SmsRecvGroup value;

    public SmsRecvGroupHolder() {
    }

    public SmsRecvGroupHolder(SmsRecvGroup value) {
        this.value = value;
    }

}
