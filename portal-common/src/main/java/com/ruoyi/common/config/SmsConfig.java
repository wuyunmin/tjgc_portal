package com.ruoyi.common.config;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author villwang
 * @date 2020/07/10
 **/
@Component
public class SmsConfig {
    public static String cordId;

    public static String userName;

    public static String passwd;

    public static String serverIP;

    public String getCordId() {
        return cordId;
    }
    @Value("${msg.CorpID}")
    public void setCordId(String cordId) {
        this.cordId = cordId;
    }

    public String getUserName() {
        return userName;
    }
    @Value("${msg.LoginName}")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPasswd() {
        return passwd;
    }
    @Value("${msg.Password}")
    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getServerIP() {
        return serverIP;
    }
    @Value("${msg.url}")
    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }
}
