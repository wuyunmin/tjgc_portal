package com.ruoyi.common.core.domain.entity;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excel.ColumnType;
import com.ruoyi.common.annotation.Excel.Type;
import com.ruoyi.common.annotation.Excels;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户对象 sys_user
 * 
 * @author ruoyi
 */
public class SysUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 员工编号 */
    @Excel(name = "员工编号")
    private String userNo;

    /** 姓名 */
    @Excel(name = "姓名")
    private String nickName;

    @Excel(name = "部门")
    private String deptName;

    /** 在职状态 */
    @Excel(name = "在职状态")
    private String incumbencyContent;

    /** 人员类别 */
    @Excel(name = "人员类别")
    private String personTypeContent;

    /** 入职日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入职日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date entryDate;

    /** 离职日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "离职日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date quitDate;

    /** 是否项目负责人 */
    @Excel(name = "是否项目负责人",readConverterExp="1=是,0=否,''=否")
    private String judgeProjectLeader;

    /** 所属项目负责人 */
    @Excel(name = "所属项目负责人")
    private String projectLeader;

    /** 帐号状态（0正常 1停用） */
    @Excel(name = "帐号状态", dictType = "sys_user_status")
    private String status;

    @Excel(name = "系统名称")
    private String systemName;

    @Excel(name = "持有方式")
    private String holderType;

    @Excel(name = "账号")
    private String userAccount;

    @Excel(name = "账号状态")
    private String userStatus;

    @Excel(name = "最后登陆时间")
    private String lastLogonTime;

    private String jobNo;




    
    /** ID */
    //@Excel(name = "序号", cellType = ColumnType.NUMERIC, prompt = "id")
    private Long id;

    /** 用户全局ID */
   // @Excel(name = "用户序号", cellType = ColumnType.STRING, prompt = "用户编号")
    private String userId;

    /** 部门ID */
    //@Excel(name = "部门编号", type = Type.IMPORT)
    private String deptId;

    /** 用户账号 */
    //@Excel(name = "登录名称")
    private String userName;
    




    /** 用户邮箱 */
   //@Excel(name = "用户邮箱")
    private String email;

    /** 手机号码 */
    //@Excel(name = "手机号码")
    private String phonenumber;

    /** 用户性别 */
    //@Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    /** 用户头像 */
    private String avatar;

    /** 密码 */
    private String password;

    /** 项目负责人全局ID */
    //@Excel(name = "项目负责人全局ID")
    private String projectLeaderId;

    /** 领款经办人全局ID */
    //@Excel(name = "领款经办人全局ID")
    private String projectPayeeId;

    /** 用户类型（00系统用户） */
   // @Excel(name = "用户类型", readConverterExp = "0=0系统用户")
    private String userType;

    /** 证件类型 */
    //@Excel(name = "证件类型")
    private String typeContent;

    /** 证件号码 */
   // @Excel(name = "证件号码")
    private String idNum;



    /** 主管单位名称 */
    //@Excel(name = "主管单位名称")
    private String competentOrg;



    /** 在职状态编码 */
   // @Excel(name = "在职状态编码")
    private String incumbencyState;

    /** 主职 */
    //@Excel(name = "主职")
    private String partType;

    /** 员工编号有效性编码 */
   // @Excel(name = "员工编号有效性编码")
    private String numberEffectiveness;

    /** 员工编号有效性文本 */
    //@Excel(name = "员工编号有效性文本")
    private String isvalidContent;

    /** 人事资料有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    //@Excel(name = "人事资料有效期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dataValid;

    /** 人员类别ID */
    //@Excel(name = "人员类别ID")
    private String personnelType;



    /** HR中员工ID */
    //@Excel(name = "HR中员工ID")
    private String userIdCode;

    /** HR部门ID */
    //@Excel(name = "HR部门ID")
    private String deptIdCode;



    /** 是否项目领款经办人 */
    //@Excel(name = "是否项目领款经办人")
    private String judgeProjectPayee;



    /** hr的项目领款经办人编号 */
    //@Excel(name = "hr的项目领款经办人编号")
    private String projectPayee;

    /** 盐加密 */
    private String salt;



    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 强制 */
    private String forceFlag;

    /** 预设关闭时间 **/
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date forceEndDate;

    /** 最后登录IP */
    //@Excel(name = "最后登录IP", type = Type.EXPORT)
    private String loginIp;

    /** 最后登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "最后登录时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
    private Date loginDate;

    /**
     * 快速查询
     */
    private String quickQuery;

    /** 部门对象 */
    @Excels({
        @Excel(name = "部门名称", targetAttr = "deptName", type = Type.EXPORT),
        @Excel(name = "部门负责人", targetAttr = "leader", type = Type.EXPORT)
    })
    private SysDept dept;
    
    private String hrPhonenumber;

    /** 角色对象 */
    private List<SysRole> roles;
    
    private String portalRole;

    /** 角色组 */
    private Long[] roleIds;

    /** 岗位组 */
    private Long[] postIds;

    private List<Long> deptNode;

    private String accountStatus;

    private String incumbencyStateTjec;

    private String personnelTypeTjec;
    

    
    private String projectPayeeName;
    
    private String projectLeaderNo;

    private List<String> incumbencyContentList;

    private List<String> personTypeContentList;


    private List<String> portalRoleList;

    private List<String> statusList;


    private List<String> competentOrgList;

    public List<String> getIncumbencyContentList() {
        return incumbencyContentList;
    }

    public void setIncumbencyContentList(List<String> incumbencyContentList) {
        this.incumbencyContentList = incumbencyContentList;
    }

    public List<String> getPersonTypeContentList() {
        return personTypeContentList;
    }

    public void setPersonTypeContentList(List<String> personTypeContentList) {
        this.personTypeContentList = personTypeContentList;
    }

    public List<String> getPortalRoleList() {
        return portalRoleList;
    }

    public void setPortalRoleList(List<String> portalRoleList) {
        this.portalRoleList = portalRoleList;
    }

    public List<String> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<String> statusList) {
        this.statusList = statusList;
    }

    public String getHolderType() {
		return holderType;
	}

	public void setHolderType(String holderType) {
		this.holderType = holderType;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public String getLastLogonTime() {
		return lastLogonTime;
	}

	public void setLastLogonTime(String lastLogonTime) {
		this.lastLogonTime = lastLogonTime;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getProjectLeaderNo() {
		return projectLeaderNo;
	}

	public void setProjectLeaderNo(String projectLeaderNo) {
		this.projectLeaderNo = projectLeaderNo;
	}

	public SysUser()
    {

    }

    public SysUser(Long id)
    {
        this.id = id;
    }


    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public boolean isAdmin()
    {
        return isAdmin(this.id);
    }

    public static boolean isAdmin(Long id)
    {
        return id != null && 1L == id;
    }

    public String getDeptId()
    {
        return deptId;
    }

    public void setDeptId(String deptId)
    {
        this.deptId = deptId;
    }

    @Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
    public String getNickName()
    {
        return nickName;
    }

    public void setNickName(String nickName)
    {
        this.nickName = nickName;
    }

    @NotBlank(message = "用户账号不能为空")
    @Size(min = 0, max = 30, message = "用户账号长度不能超过30个字符")
    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    public String getPhonenumber()
    {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber)
    {
        this.phonenumber = phonenumber;
    }

    public String getSex()
    {
        return sex;
    }

    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public String getAvatar()
    {
        return avatar;
    }

    public void setAvatar(String avatar)
    {
        this.avatar = avatar;
    }

	@JsonIgnore
    @JsonProperty
    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getSalt()
    {
        return salt;
    }

    public void setSalt(String salt)
    {
        this.salt = salt;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getLoginIp()
    {
        return loginIp;
    }

    public void setLoginIp(String loginIp)
    {
        this.loginIp = loginIp;
    }

    public Date getLoginDate()
    {
        return loginDate;
    }

    public void setLoginDate(Date loginDate)
    {
        this.loginDate = loginDate;
    }

    public SysDept getDept()
    {
        return dept;
    }

    public void setDept(SysDept dept)
    {
        this.dept = dept;
    }

    public List<SysRole> getRoles()
    {
        return roles;
    }

    public void setRoles(List<SysRole> roles)
    {
        this.roles = roles;
    }

    public Long[] getRoleIds()
    {
        return roleIds;
    }

    public void setRoleIds(Long[] roleIds)
    {
        this.roleIds = roleIds;
    }

    public Long[] getPostIds()
    {
        return postIds;
    }

    public void setPostIds(Long[] postIds)
    {
        this.postIds = postIds;
    }

    public String getProjectLeaderId() {
        return projectLeaderId;
    }

    public void setProjectLeaderId(String projectLeaderId) {
        this.projectLeaderId = projectLeaderId;
    }

    public String getProjectPayeeId() {
        return projectPayeeId;
    }

    public void setProjectPayeeId(String projectPayeeId) {
        this.projectPayeeId = projectPayeeId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getTypeContent() {
        return typeContent;
    }

    public void setTypeContent(String typeContent) {
        this.typeContent = typeContent;
    }

    public String getIdNum() {
        return idNum;
    }

    public void setIdNum(String idNum) {
        this.idNum = idNum;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public Date getQuitDate() {
        return quitDate;
    }

    public void setQuitDate(Date quitDate) {
        this.quitDate = quitDate;
    }

    public String getCompetentOrg() {
        return competentOrg;
    }

    public void setCompetentOrg(String competentOrg) {
        this.competentOrg = competentOrg;
    }

    public String getIncumbencyContent() {
        return incumbencyContent;
    }

    public void setIncumbencyContent(String incumbencyContent) {
        this.incumbencyContent = incumbencyContent;
    }

    public String getIncumbencyState() {
        return incumbencyState;
    }

    public void setIncumbencyState(String incumbencyState) {
        this.incumbencyState = incumbencyState;
    }

    public String getPartType() {
        return partType;
    }

    public void setPartType(String partType) {
        this.partType = partType;
    }

    public String getNumberEffectiveness() {
        return numberEffectiveness;
    }

    public void setNumberEffectiveness(String numberEffectiveness) {
        this.numberEffectiveness = numberEffectiveness;
    }

    public String getIsvalidContent() {
        return isvalidContent;
    }

    public void setIsvalidContent(String isvalidContent) {
        this.isvalidContent = isvalidContent;
    }

    public Date getDataValid() {
        return dataValid;
    }

    public void setDataValid(Date dataValid) {
        this.dataValid = dataValid;
    }

    public String getPersonnelType() {
        return personnelType;
    }

    public void setPersonnelType(String personnelType) {
        this.personnelType = personnelType;
    }

    public String getPersonTypeContent() {
        return personTypeContent;
    }

    public void setPersonTypeContent(String personTypeContent) {
        this.personTypeContent = personTypeContent;
    }

    public String getUserIdCode() {
        return userIdCode;
    }

    public void setUserIdCode(String userIdCode) {
        this.userIdCode = userIdCode;
    }

    public String getDeptIdCode() {
        return deptIdCode;
    }

    public void setDeptIdCode(String deptIdCode) {
        this.deptIdCode = deptIdCode;
    }

    public String getJudgeProjectLeader() {
        return judgeProjectLeader;
    }

    public void setJudgeProjectLeader(String judgeProjectLeader) {
        this.judgeProjectLeader = judgeProjectLeader;
    }

    public String getJudgeProjectPayee() {
        return judgeProjectPayee;
    }

    public void setJudgeProjectPayee(String judgeProjectPayee) {
        this.judgeProjectPayee = judgeProjectPayee;
    }

    public String getProjectLeader() {
        return projectLeader;
    }

    public void setProjectLeader(String projectLeader) {
        this.projectLeader = projectLeader;
    }

    public String getProjectPayee() {
        return projectPayee;
    }

    public void setProjectPayee(String projectPayee) {
        this.projectPayee = projectPayee;
    }

    public String getForceFlag() {
        return forceFlag;
    }

    public void setForceFlag(String forceFlag) {
        this.forceFlag = forceFlag;
    }

    public List<Long> getDeptNode() {
        return deptNode;
    }

    public void setDeptNode(List<Long> deptNode) {
        this.deptNode = deptNode;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getIncumbencyStateTjec() {
        return incumbencyStateTjec;
    }

    public void setIncumbencyStateTjec(String incumbencyStateTjec) {
        this.incumbencyStateTjec = incumbencyStateTjec;
    }

    public String getPersonnelTypeTjec() {
        return personnelTypeTjec;
    }

    public void setPersonnelTypeTjec(String personnelTypeTjec) {
        this.personnelTypeTjec = personnelTypeTjec;
    }

    public Date getForceEndDate() {
        return forceEndDate;
    }

    public void setForceEndDate(Date forceEndDate) {
        this.forceEndDate = forceEndDate;
    }

    public String getQuickQuery() {
        return quickQuery;
    }

    public void setQuickQuery(String quickQuery) {
        this.quickQuery = quickQuery;
    }

    public List<String> getCompetentOrgList() {
        return competentOrgList;
    }

    public void setCompetentOrgList(List<String> competentOrgList) {
        this.competentOrgList = competentOrgList;
    }

    //    @Override
//    public String toString() {
//        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
//                .append("id", getId())
//                .append("userId", getUserId())
//                .append("deptId", getDeptId())
//                .append("userNo", getUserNo())
//                .append("nickName", getNickName())
//                .append("projectLeaderId", getProjectLeaderId())
//                .append("projectPayeeId", getProjectPayeeId())
//                .append("phonenumber", getPhonenumber())
//                .append("email", getEmail())
//                .append("userType", getUserType())
//                .append("typeContent", getTypeContent())
//                .append("idNum", getIdNum())
//                .append("sex", getSex())
//                .append("entryDate", getEntryDate())
//                .append("quitDate", getQuitDate())
//                .append("competentOrg", getCompetentOrg())
//                .append("incumbencyContent", getIncumbencyContent())
//                .append("incumbencyState", getIncumbencyState())
//                .append("partType", getPartType())
//                .append("numberEffectiveness", getNumberEffectiveness())
//                .append("isvalidContent", getIsvalidContent())
//                .append("dataValid", getDataValid())
//                .append("personnelType", getPersonnelType())
//                .append("personTypeContent", getPersonTypeContent())
//                .append("userIdCode", getUserIdCode())
//                .append("deptIdCode", getDeptIdCode())
//                .append("judgeProjectLeader", getJudgeProjectLeader())
//                .append("judgeProjectPayee", getJudgeProjectPayee())
//                .append("projectLeader", getProjectLeader())
//                .append("projectPayee", getProjectPayee())
//                .append("avatar", getAvatar())
//                .append("password", getPassword())
//                .append("status", getStatus())
//                .append("delFlag", getDelFlag())
//                .append("forceFlag", getForceFlag())
//                .append("loginIp", getLoginIp())
//                .append("loginDate", getLoginDate())
//                .append("createBy", getCreateBy())
//                .append("createTime", getCreateTime())
//                .append("updateBy", getUpdateBy())
//                .append("updateTime", getUpdateTime())
//                .append("remark", getRemark())
//                .toString();
//    }

    @Override
    public String toString() {
        return "SysUser{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", deptId='" + deptId + '\'' +
                ", userName='" + userName + '\'' +
                ", userNo='" + userNo + '\'' +
                ", nickName='" + nickName + '\'' +
                ", email='" + email + '\'' +
                ", phonenumber='" + phonenumber + '\'' +
                ", sex='" + sex + '\'' +
                ", avatar='" + avatar + '\'' +
                ", password='" + password + '\'' +
                ", projectLeaderId='" + projectLeaderId + '\'' +
                ", projectPayeeId='" + projectPayeeId + '\'' +
                ", userType='" + userType + '\'' +
                ", typeContent='" + typeContent + '\'' +
                ", idNum='" + idNum + '\'' +
                ", entryDate=" + entryDate +
                ", quitDate=" + quitDate +
                ", competentOrg='" + competentOrg + '\'' +
                ", incumbencyContent='" + incumbencyContent + '\'' +
                ", incumbencyState=" + incumbencyState +
                ", partType=" + partType +
                ", numberEffectiveness='" + numberEffectiveness + '\'' +
                ", isvalidContent='" + isvalidContent + '\'' +
                ", dataValid=" + dataValid +
                ", personnelType='" + personnelType + '\'' +
                ", personTypeContent='" + personTypeContent + '\'' +
                ", userIdCode='" + userIdCode + '\'' +
                ", deptIdCode='" + deptIdCode + '\'' +
                ", judgeProjectLeader='" + judgeProjectLeader + '\'' +
                ", judgeProjectPayee='" + judgeProjectPayee + '\'' +
                ", projectLeader='" + projectLeader + '\'' +
                ", projectPayee='" + projectPayee + '\'' +
                ", salt='" + salt + '\'' +
                ", status='" + status + '\'' +
                ", delFlag='" + delFlag + '\'' +
                ", forceFlag='" + forceFlag + '\'' +
                ", loginIp='" + loginIp + '\'' +
                ", loginDate=" + loginDate +
                ", dept=" + dept +
                ", roles=" + roles +
                ", roleIds=" + Arrays.toString(roleIds) +
                ", postIds=" + Arrays.toString(postIds) +
                ", deptNode=" + deptNode +
                ", accountStatus='" + accountStatus + '\'' +
                ", incumbencyStateTjec='" + incumbencyStateTjec + '\'' +
                ", personnelTypeTjec='" + personnelTypeTjec + '\'' +
                ", projectPayeeName='" + projectPayeeName + '\'' +
                ", projectLeaderNo='" + projectLeaderNo + '\'' +
                ", quickQuery='" + quickQuery + '\'' +
                ", hrPhonenumber='" + hrPhonenumber + '\'' +
                '}';
    }
	
	public void setPortalRole(String portalRole) {
		this.portalRole = portalRole;
	}
	
	public String getPortalRole() {
		return portalRole;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getProjectPayeeName() {
		return projectPayeeName;
	}

	public void setProjectPayeeName(String projectPayeeName) {
		this.projectPayeeName = projectPayeeName;
	}

	public String getHrPhonenumber() {
		return hrPhonenumber;
	}

	public void setHrPhonenumber(String hrPhonenumber) {
		this.hrPhonenumber = hrPhonenumber;
	}

	public String getJobNo() {
		return jobNo;
	}

	public void setJobNo(String jobNo) {
		this.jobNo = jobNo;
	}
}
