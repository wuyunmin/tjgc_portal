package com.ruoyi.common.core.domain.entity;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 部门表 sys_dept
 * 
 * @author ruoyi
 */
public class SysDept extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private Long id;

    /** 部门ID */
    private Long deptId;

    /** 父部门ID */
    private Long parentId;

    /** 祖级列表 */
    private String ancestors;

    /** 部门名称 */
    private String deptName;

    /** 显示顺序 */
    private String orderNum;

    /** 负责人 */
    private String deptAllContent;

    private Integer deptGrade;

    private String deptParentCode;

    private String deptParentIdCode;

    private String deptInchargeLeader;

    private String deptLeader;

    private String deptOaOrgCode;

    private String deptIdCode;

    private String deptCode;

    private String deptEndMark;

    private String deptType;

    private String deptInchargeLeaderId;

    private String deptLeaderId;

    /** 部门状态:0正常,1停用 */
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 父部门名称 */
    private String parentName;


    private String leader;
    
    /** 子部门 */
    private List<SysDept> children = new ArrayList<SysDept>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDeptId()
    {
        return deptId;
    }

    public void setDeptId(Long deptId)
    {
        this.deptId = deptId;
    }

    public Long getParentId()
    {
        return parentId;
    }

    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public String getAncestors()
    {
        return ancestors;
    }

    public void setAncestors(String ancestors)
    {
        this.ancestors = ancestors;
    }

    @NotBlank(message = "部门名称不能为空")
    @Size(min = 0, max = 30, message = "部门名称长度不能超过30个字符")
    public String getDeptName()
    {
        return deptName;
    }

    public void setDeptName(String deptName)
    {
        this.deptName = deptName;
    }

    @NotBlank(message = "显示顺序不能为空")
    public String getOrderNum()
    {
        return orderNum;
    }

    public void setOrderNum(String orderNum)
    {
        this.orderNum = orderNum;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDeptAllContent() {
        return deptAllContent;
    }

    public void setDeptAllContent(String deptAllContent) {
        this.deptAllContent = deptAllContent;
    }

    public Integer getDeptGrade() {
        return deptGrade;
    }

    public void setDeptGrade(Integer deptGrade) {
        this.deptGrade = deptGrade;
    }

    public String getDeptParentCode() {
        return deptParentCode;
    }

    public void setDeptParentCode(String deptParentCode) {
        this.deptParentCode = deptParentCode;
    }

    public String getDeptParentIdCode() {
        return deptParentIdCode;
    }

    public void setDeptParentIdCode(String deptParentIdCode) {
        this.deptParentIdCode = deptParentIdCode;
    }

    public String getDeptInchargeLeader() {
        return deptInchargeLeader;
    }

    public void setDeptInchargeLeader(String deptInchargeLeader) {
        this.deptInchargeLeader = deptInchargeLeader;
    }

    public String getDeptLeader() {
        return deptLeader;
    }

    public void setDeptLeader(String deptLeader) {
        this.deptLeader = deptLeader;
    }

    public String getDeptOaOrgCode() {
        return deptOaOrgCode;
    }

    public void setDeptOaOrgCode(String deptOaOrgCode) {
        this.deptOaOrgCode = deptOaOrgCode;
    }

    public String getDeptIdCode() {
        return deptIdCode;
    }

    public void setDeptIdCode(String deptIdCode) {
        this.deptIdCode = deptIdCode;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptEndMark() {
        return deptEndMark;
    }

    public void setDeptEndMark(String deptEndMark) {
        this.deptEndMark = deptEndMark;
    }

    public String getDeptType() {
        return deptType;
    }

    public void setDeptType(String deptType) {
        this.deptType = deptType;
    }

    public String getDeptInchargeLeaderId() {
        return deptInchargeLeaderId;
    }

    public void setDeptInchargeLeaderId(String deptInchargeLeaderId) {
        this.deptInchargeLeaderId = deptInchargeLeaderId;
    }

    public String getDeptLeaderId() {
        return deptLeaderId;
    }

    public void setDeptLeaderId(String deptLeaderId) {
        this.deptLeaderId = deptLeaderId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public List<SysDept> getChildren()
    {
        return children;
    }

    public void setChildren(List<SysDept> children)
    {
        this.children = children;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("deptId", getDeptId())
            .append("parentId", getParentId())
            .append("ancestors", getAncestors())
            .append("deptName", getDeptName())
            .append("orderNum", getOrderNum())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
