/*
Navicat MySQL Data Transfer

Source Server         : m-华东演示-石药local
Source Server Version : 80022
Source Host           : 127.0.0.1:13306
Source Database       : cloudpivot

Target Server Type    : MYSQL
Target Server Version : 80022
File Encoding         : 65001

Date: 2020-12-15 11:03:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_sso_system
-- ----------------------------
DROP TABLE IF EXISTS `sys_sso_system`;
CREATE TABLE `sys_sso_system` (
  `id` varchar(36) NOT NULL,
  `creater` varchar(200) DEFAULT '' COMMENT '创建人',
  `createdTime` datetime DEFAULT NULL COMMENT '创建时间',
  `modifier` varchar(200) DEFAULT '' COMMENT '修改人',
  `modifiedTime` datetime DEFAULT NULL COMMENT '修改时间',
  `sys_name` varchar(200) DEFAULT '' COMMENT '系统名称',
  `sys_code` varchar(200) DEFAULT '' COMMENT '系统编码',
  `sys_secret` varchar(200) DEFAULT '' COMMENT '系统密钥',
  `sys_url` varchar(200) DEFAULT '' COMMENT '系统登陆回调地址',
  `sys_describe` mediumtext COMMENT '系统描述',
  `sys_logouturl` varchar(200) DEFAULT '' COMMENT '系统注销回调地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SSO应用系统';
