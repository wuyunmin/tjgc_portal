/*
Navicat MySQL Data Transfer

Source Server         : m-华东演示-石药local
Source Server Version : 80022
Source Host           : 127.0.0.1:13306
Source Database       : cloudpivot

Target Server Type    : MYSQL
Target Server Version : 80022
File Encoding         : 65001

Date: 2020-12-15 11:03:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_sso_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_sso_log`;
CREATE TABLE `sys_sso_log` (
  `id` varchar(36) NOT NULL,
  `sys_code` varchar(200) DEFAULT '' COMMENT '系统编码',
  `sys_loginuser` varchar(200) DEFAULT '' COMMENT '系统登陆用户',
  `sys_logouttime` datetime DEFAULT NULL COMMENT '系统登出时间',
  `sys_logintime` datetime DEFAULT NULL COMMENT '系统登陆时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SSO登陆日志';
