package com.ruoyi.web.controller.system;

import java.util.List;
import java.util.Map;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysContactOur;
import com.ruoyi.system.service.ISysContactOurService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 联系我们Controller
 * 
 * @author pangyongfeng
 * @date 2021-03-09
 */
@RestController
@RequestMapping("/system/our")
public class SysContactOurController extends BaseController
{
    @Autowired
    private ISysContactOurService sysContactOurService;

    /**
     * 查询联系我们列表
     */
    @PreAuthorize("@ss.hasPermi('system:our:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysContactOur sysContactOur)
    {
        startPage();
        List<SysContactOur> list = sysContactOurService.selectSysContactOurList(sysContactOur);
        return getDataTable(list);
    }

    /**
     * 导出联系我们列表
     */
    @PreAuthorize("@ss.hasPermi('system:our:export')")
    @Log(title = "联系我们", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysContactOur sysContactOur)
    {
        List<SysContactOur> list = sysContactOurService.selectSysContactOurList(sysContactOur);
        ExcelUtil<SysContactOur> util = new ExcelUtil<SysContactOur>(SysContactOur.class);
        return util.exportExcel(list, "our");
    }

    /**
     * 获取联系我们详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:our:query')")
    @GetMapping(value = "/{guid}")
    public AjaxResult getInfo(@PathVariable("guid") String guid)
    {
        return AjaxResult.success(sysContactOurService.selectSysContactOurByGuid(guid));
    }

    /**
     * 新增联系我们
     */
    @PreAuthorize("@ss.hasPermi('system:our:add')")
    @Log(title = "联系我们", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysContactOur sysContactOur)
    {
        return toAjax(sysContactOurService.insertSysContactOur(sysContactOur));
    }

    /**
     * 修改联系我们
     */
    @PreAuthorize("@ss.hasPermi('system:our:edit')")
    @Log(title = "联系我们", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysContactOur sysContactOur)
    {
        return toAjax(sysContactOurService.updateSysContactOur(sysContactOur));
    }

    /**
     * 删除联系我们
     */
    @PreAuthorize("@ss.hasPermi('system:our:remove')")
    @Log(title = "联系我们", businessType = BusinessType.DELETE)
	@DeleteMapping("/{guids}")
    public AjaxResult remove(@PathVariable String[] guids)
    {
        return toAjax(sysContactOurService.deleteSysContactOurByGuids(guids));
    }
    
    /**
     * 查询联系我们列表
     */
    @GetMapping("/getContactOur")
    public AjaxResult getContactOur()
    {
        return AjaxResult.success(sysContactOurService.getContactOur());
    }
}
