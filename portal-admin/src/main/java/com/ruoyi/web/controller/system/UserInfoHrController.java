package com.ruoyi.web.controller.system;

import com.ruoyi.system.service.ISyncSysDeptService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SyncSysUser;
import com.ruoyi.system.service.ISyncSysUserService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.annotation.Resource;
import java.util.List;

/**
 * hr用户Controller
 * 
 * @author tjgc
 * @date 2021-04-12
 */
@RestController
@RequestMapping("/system/hr")
public class UserInfoHrController extends BaseController
{
    @Resource
    private ISyncSysUserService syncSysUserService;

    @Resource
    private ISyncSysDeptService syncSysDeptService;

    /**
     * 查询部门同步中间列表
     */
    @PreAuthorize("@ss.hasPermi('system:hr:list')")
    @GetMapping("/list")
    public TableDataInfo list(SyncSysUser syncSysUser)
    {
        startPage();
        List<SyncSysUser> list = syncSysUserService.selectSyncSysUserListForHR(syncSysUser);
        return getDataTable(list);
    }

    /**
     * 查询部门同步中间列表
     */
    @PreAuthorize("@ss.hasPermi('system:hr:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(syncSysUserService.selectUserInfoHrById(id));
    }

    /**
     * 导出部门同步中间列表
     */
    @PreAuthorize("@ss.hasPermi('system:hr:export')")
    @Log(title = "部门同步中间", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SyncSysUser syncSysUser)
    {
        List<SyncSysUser> list = syncSysUserService.selectSyncSysUserList(syncSysUser);
        ExcelUtil<SyncSysUser> util = new ExcelUtil<SyncSysUser>(SyncSysUser.class);
        return util.exportExcel(list, "user");
    }


    /**
     * 新增部门同步中间
     */
    @PreAuthorize("@ss.hasPermi('system:hr:add')")
    @Log(title = "部门同步中间", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SyncSysUser syncSysUser)
    {
        return toAjax(syncSysUserService.insertSyncSysUser(syncSysUser));
    }

    /**
     * 修改部门同步中间
     */
    @PreAuthorize("@ss.hasPermi('system:hr:edit')")
    @Log(title = "部门同步中间", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SyncSysUser syncSysUser)
    {
        return toAjax(syncSysUserService.updateSyncSysUser(syncSysUser));
    }

    /**
     * 删除部门同步中间
     */
    @PreAuthorize("@ss.hasPermi('system:hr:remove')")
    @Log(title = "部门同步中间", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(syncSysUserService.deleteSyncSysUserByIds(ids));
    }

    /**
     * 新增部门同步中间
     */
    @RequestMapping("/selectDept")
    public AjaxResult selectDept(String id)
    {
        return AjaxResult.success(syncSysDeptService.selectDeptByDeptId(id));
    }

    @RequestMapping("/selectUser")
    public AjaxResult selectUser(String id)
    {
        return AjaxResult.success(syncSysUserService.selectUserByUserID(id));
    }
}
