package com.ruoyi.web.controller.backstage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Administrator
 */
@RequestMapping("/external")
@RestController
public class ExternalController extends BaseController {

    @Resource
    private ISysUserService userService;

    @RequestMapping("/user/getUserStatus")
    @ResponseBody
    public JSONObject getUserStatus(@RequestBody String json) {
        /**
         * 0 开启
         */
        String open ="0";
        /**
         * 1 离职待关闭
         */
        String opening ="1";
        logger.info("获取用户状态参数值： {}", json);
        JSONObject result = new JSONObject();
        JSONObject jsonObject = JSON.parseObject(json);
        Object userNoObj = jsonObject.get("userNo");
        if (userNoObj!=null){
            result.put("code", HttpStatus.SUCCESS);
            result.put("message","成功");
            String status = null;
            try {
                status = userService.selectUserStatusByUserNo(userNoObj.toString());
            } catch (Exception e) {
                logger.info(e.getMessage());
                if ("user is null".equals(e.getMessage())){
                    result.put("code", HttpStatus.SUCCESS);
                    result.put("message","用户不存在");
                    logger.error("获取用户状态参数,用户不存在");
                }else {
                    result.put("code", HttpStatus.ERROR);
                    result.put("message","系统错误");
                    logger.error("获取用户状态参数,系统错误");
                }
            }
            if (open.equals(status)||opening.equals(status)){
                /**
                 * 0开启 1 关闭
                 */
                result.put("data","0");
            }else {
                result.put("data","1");
            }

        }else {
            result.put("code", HttpStatus.ERROR);
            result.put("message","参数格式错误");
            logger.error("获取用户状态参数缺少用户工号");
        }
        return result;
    }
}
