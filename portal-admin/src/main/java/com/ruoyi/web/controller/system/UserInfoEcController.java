package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.UserInfoEc;
import com.ruoyi.system.service.IUserInfoEcService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 领款用户信息Controller
 * 
 * @author tjgc
 * @date 2021-01-28
 */
@RestController
@RequestMapping("/system/ec")
public class UserInfoEcController extends BaseController
{
    @Autowired
    private IUserInfoEcService userInfoEcService;

    /**
     * 查询领款用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:ec:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserInfoEc userInfoEc)
    {
        startPage();
        List<UserInfoEc> list = userInfoEcService.selectUserInfoEcList(userInfoEc);
        return getDataTable(list);
    }

    /**
     * 导出领款用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:ec:export')")
    @Log(title = "领款用户信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UserInfoEc userInfoEc)
    {
        List<UserInfoEc> list = userInfoEcService.selectUserInfoEcList(userInfoEc);
        ExcelUtil<UserInfoEc> util = new ExcelUtil<UserInfoEc>(UserInfoEc.class);
        return util.exportExcel(list, "ec");
    }

    /**
     * 获取领款用户信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:ec:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(userInfoEcService.selectUserInfoEcById(id));
    }

    /**
     * 新增领款用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:ec:add')")
    @Log(title = "领款用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserInfoEc userInfoEc)
    {
        return toAjax(userInfoEcService.insertUserInfoEc(userInfoEc));
    }

    /**
     * 修改领款用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:ec:edit')")
    @Log(title = "领款用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserInfoEc userInfoEc)
    {
        return toAjax(userInfoEcService.updateUserInfoEc(userInfoEc));
    }

    /**
     * 删除领款用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:ec:remove')")
    @Log(title = "领款用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(userInfoEcService.deleteUserInfoEcByIds(ids));
    }
}
