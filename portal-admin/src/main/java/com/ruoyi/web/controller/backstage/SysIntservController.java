package com.ruoyi.web.controller.backstage;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysIntserv;
import com.ruoyi.system.service.ISysIntservService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 综合服务配合Controller
 * 
 * @author tjgc
 * @date 2020-12-29
 */
@RestController
@RequestMapping("/system/intserv")
public class SysIntservController extends BaseController
{
    @Autowired
    private ISysIntservService sysIntservService;

    /**
     * 查询综合服务配合列表
     */
    @PreAuthorize("@ss.hasPermi('system:intserv:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysIntserv sysIntserv)
    {
        startPage();
        List<SysIntserv> list = sysIntservService.selectSysIntservList(sysIntserv);
        return getDataTable(list);
    }

    /**
     * 查询微信二维码列表
     */
    @GetMapping("/weChatList")
    public AjaxResult weChatList()
    {
        List<SysIntserv> resList = new ArrayList<SysIntserv>();
        return AjaxResult.success(resList);
    }

    /**
     * 查询常用链接列表
     */
    @GetMapping("/linkList")
    public AjaxResult linkList()
    {
        return AjaxResult.success(sysIntservService.selectLinkList());
    }

    /**
     * 导出综合服务配合列表
     */
    @PreAuthorize("@ss.hasPermi('system:intserv:export')")
    @Log(title = "综合服务配合", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysIntserv sysIntserv)
    {
        List<SysIntserv> list = sysIntservService.selectSysIntservList(sysIntserv);
        ExcelUtil<SysIntserv> util = new ExcelUtil<SysIntserv>(SysIntserv.class);
        return util.exportExcel(list, "intserv");
    }

    /**
     * 获取综合服务配合详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:intserv:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(sysIntservService.selectSysIntservById(id));
    }

    /**
     * 新增综合服务配合
     */
    @PreAuthorize("@ss.hasPermi('system:intserv:add')")
    @Log(title = "综合服务配合", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysIntserv sysIntserv)
    {
        return toAjax(sysIntservService.insertSysIntserv(sysIntserv));
    }

    /**
     * 修改综合服务配合
     */
    @PreAuthorize("@ss.hasPermi('system:intserv:edit')")
    @Log(title = "综合服务配合", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysIntserv sysIntserv)
    {
        return toAjax(sysIntservService.updateSysIntserv(sysIntserv));
    }

    /**
     * 删除综合服务配合
     */
    @PreAuthorize("@ss.hasPermi('system:intserv:remove')")
    @Log(title = "综合服务配合", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysIntservService.deleteSysIntservByIds(ids));
    }

    /**
     * 用户单点登录状态修改
     */
    @PreAuthorize("@ss.hasPermi('system:ssoConfig:edit')")
    @Log(title = "用户单点登录配置", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody SysIntserv sysIntserv)
    {

        logger.info("参数： {}",sysIntserv.toString());
        return toAjax(sysIntservService.updateSysIntservStatus(sysIntserv));
    }
}
