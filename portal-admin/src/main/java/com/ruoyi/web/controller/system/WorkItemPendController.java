package com.ruoyi.web.controller.system;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.service.IWorkItemBpmService;
import com.ruoyi.system.service.IWorkItemHrService;
import com.ruoyi.system.service.IWorkItemOaService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.WorkItemPend;
import com.ruoyi.system.service.IWorkItemPendService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 待办待阅信息Controller
 *
 * @author tjgc
 * @date 2022-05-09
 */
@RestController
@RequestMapping("/system/pend")
public class WorkItemPendController extends BaseController
{
    @Autowired
    private IWorkItemPendService workItemPendService;

    @Autowired
    private IWorkItemOaService iWorkItemOaService;

    @Autowired
    private IWorkItemHrService iWorkItemHrService;

    @Autowired
    private IWorkItemBpmService iWorkItemBpmService;

    @Autowired
    private TokenService tokenService;
    /**
     * 查询待办待阅信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:pend:list')")
    @GetMapping("/list")
    public TableDataInfo list(WorkItemPend workItemPend)
    {
        startPage();
        List<WorkItemPend> list = workItemPendService.selectWorkItemPendList(workItemPend);
        return getDataTable(list);
    }


    @PreAuthorize("@ss.hasPermi('system:pend:list')")
    @GetMapping("/allList")
    public TableDataInfo allList(WorkItemPend workItemPend)
    {
        startPage();
        List<WorkItemPend> list = workItemPendService.selectAllWorkItemPendList(workItemPend);
        return getDataTable(list);
    }


    /**
     * 导出待办待阅信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:pend:export')")
    @Log(title = "待办待阅信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WorkItemPend workItemPend)
    {
        List<WorkItemPend> list = workItemPendService.selectWorkItemPendList(workItemPend);
        ExcelUtil<WorkItemPend> util = new ExcelUtil<WorkItemPend>(WorkItemPend.class);
        return util.exportExcel(list, "pend");
    }

    /**
     * 获取待办待阅信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:pend:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(workItemPendService.selectWorkItemPendById(id));
    }

    /**
     * 新增待办待阅信息
     */
    @PreAuthorize("@ss.hasPermi('system:pend:add')")
    @Log(title = "待办待阅信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WorkItemPend workItemPend)
    {
        return toAjax(workItemPendService.insertWorkItemPend(workItemPend));
    }

    /**
     * 修改待办待阅信息
     */
    @PreAuthorize("@ss.hasPermi('system:pend:edit')")
    @Log(title = "待办待阅信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WorkItemPend workItemPend)
    {
        return toAjax(workItemPendService.updateWorkItemPend(workItemPend));
    }

    /**
     * 删除待办待阅信息
     */
    @PreAuthorize("@ss.hasPermi('system:pend:remove')")
    @Log(title = "待办待阅信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(workItemPendService.deleteWorkItemPendByIds(ids));
    }


    /**
     * 同步HR待办待阅信息
     */
    @PreAuthorize("@ss.hasPermi('system:pend:edit')")
    @Log(title = "同步HR待办待阅信息", businessType = BusinessType.UPDATE)
    @GetMapping("/syncWorkItemHr")
    public AjaxResult syncWorkItemHr()
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String operName = loginUser.getUser().getNickName();
        iWorkItemHrService.syncWorkItemHr(operName);
        return AjaxResult.success();
    }

    /**
     * 同步OA待办待阅信息
     */
    @PreAuthorize("@ss.hasPermi('system:pend:edit')")
    @Log(title = "同步OA待办待阅信息", businessType = BusinessType.UPDATE)
    @GetMapping("/syncWorkItemOa")
    public AjaxResult syncWorkItemOa()
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String operName = loginUser.getUser().getNickName();
        iWorkItemOaService.syncWorkItemOa(operName);
        return AjaxResult.success();
    }

    /**
     * 同步OA待办待阅信息
     */
    @PreAuthorize("@ss.hasPermi('system:pend:edit')")
    @Log(title = "同步OA待办待阅信息", businessType = BusinessType.UPDATE)
    @GetMapping("/syncWorkItemBpm")
    public AjaxResult syncWorkItemBpm()
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String operName = loginUser.getUser().getNickName();
        iWorkItemBpmService.syncWorkItemBpm(operName);
        return AjaxResult.success();
    }

}
