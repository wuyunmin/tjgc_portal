package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.UserInfoEmail;
import com.ruoyi.system.domain.UserInfoXiaoyu;
import com.ruoyi.system.service.IUserInfoEmailService;

/**
 * 企业邮箱用户信息Controller
 * 
 * @author tjec
 * @date 2021-01-28
 */
@RestController
@RequestMapping("/system/email")
public class UserInfoEmailController extends BaseController
{
    @Autowired
    private IUserInfoEmailService userInfoEmailService;
    
    @Autowired
    private TokenService tokenService;

    /**
     * 查询企业邮箱用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:email:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserInfoEmail userInfoEmail)
    {
        startPage();
        List<UserInfoEmail> list = userInfoEmailService.selectUserInfoEmailList(userInfoEmail);
        return getDataTable(list);
    }

    /**
     * 导出企业邮箱用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:email:export')")
    @Log(title = "企业邮箱用户信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UserInfoEmail userInfoEmail)
    {
        List<UserInfoEmail> list = userInfoEmailService.selectUserInfoEmailList(userInfoEmail);
        ExcelUtil<UserInfoEmail> util = new ExcelUtil<UserInfoEmail>(UserInfoEmail.class);
        return util.exportExcel(list, "email");
    }

    /**
     * 获取企业邮箱用户信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:email:query')")
    @GetMapping(value = "/{guid}")
    public AjaxResult getInfo(@PathVariable("guid") String guid)
    {
        return AjaxResult.success(userInfoEmailService.selectUserInfoEmailByGuid(guid));
    }

    /**
     * 新增企业邮箱用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:email:add')")
    @Log(title = "企业邮箱用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserInfoEmail userInfoEmail)
    {
        return toAjax(userInfoEmailService.insertUserInfoEmail(userInfoEmail));
    }

    /**
     * 修改企业邮箱用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:email:edit')")
    @Log(title = "企业邮箱用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserInfoEmail userInfoEmail)
    {
        return toAjax(userInfoEmailService.updateUserInfoEmail(userInfoEmail));
    }

    /**
     * 删除企业邮箱用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:email:remove')")
    @Log(title = "企业邮箱用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{guids}")
    public AjaxResult remove(@PathVariable String[] guids)
    {
        return toAjax(userInfoEmailService.deleteUserInfoEmailByGuids(guids));
    }
    
    @Log(title = "企业邮箱用户信息", businessType = BusinessType.IMPORT)
    // @PreAuthorize("@ss.hasPermi('system:xiaoyu:import')")
     @PostMapping("/importData")
     public AjaxResult importData(MultipartFile file) throws Exception {
         ExcelUtil<UserInfoEmail> util = new ExcelUtil<UserInfoEmail>(UserInfoEmail.class);
         List<UserInfoEmail> userInfoEmailList = util.importExcel(file.getInputStream());
         LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
         String operName = loginUser.getUser().getUserId();
         String message = userInfoEmailService.importUser(userInfoEmailList, operName);
         return AjaxResult.success(message);
     }
}
