package com.ruoyi.web.controller.common;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.exception.user.UserException;
import com.ruoyi.common.utils.MessageUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.VerifyCodeUtils;
import com.ruoyi.framework.manager.AsyncManager;
import com.ruoyi.framework.manager.factory.AsyncFactory;
import com.ruoyi.system.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.google.code.kaptcha.Producer;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.sign.Base64;
import com.ruoyi.common.utils.uuid.IdUtils;

/**
 * 验证码操作处理
 * 
 * @author ruoyi
 */
@RestController
@Slf4j
public class CaptchaController
{
    @Resource(name = "captchaProducer")
    private Producer captchaProducer;

    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

    @Autowired
    private RedisCache redisCache;
    
    // 验证码类型
    @Value("${ruoyi.captchaType}")
    private String captchaType;

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private com.ruoyi.common.utils.sms.service.MsgService msgService;

    public static String regMobileExp = "^1[0-9]{10}$";

    /**
     * 生成验证码
     */
    @GetMapping("/captchaImage")
    public AjaxResult getCode(HttpServletResponse response) throws IOException
    {
        // 保存验证码信息
        String uuid = IdUtils.simpleUUID();
        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;

        String capStr = null, code = null;
        BufferedImage image = null;

        // 生成验证码
        if ("math".equals(captchaType))
        {
            String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@") + 1);
            image = captchaProducerMath.createImage(capStr);
        }
        else if ("char".equals(captchaType))
        {
            capStr = code = captchaProducer.createText();
            image = captchaProducer.createImage(capStr);
        }

        redisCache.setCacheObject(verifyKey, code, Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);
        // 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try
        {
            ImageIO.write(image, "jpg", os);
        }
        catch (IOException e)
        {
            return AjaxResult.error(e.getMessage());
        }

        AjaxResult ajax = AjaxResult.success();
        ajax.put("uuid", uuid);
        ajax.put("img", Base64.encode(os.toByteArray()));
        return ajax;
    }

    /**
     * 获取短信验证码
     * @param phonenumber 手机号
     * @param isLogin 是否为登录时获取验证码
     * @return
     * @throws IOException
     */
    @GetMapping("/captchaSMS")
    public AjaxResult LoginCaptchaSMS(HttpServletResponse response, String phonenumber, boolean isLogin)  {
        log.info("开始进入短信发送");
        if (StringUtils.isBlank(phonenumber)) {
            return AjaxResult.error("手机号不能为空");
        }
        if(!Pattern.matches(regMobileExp, phonenumber)){
            return AjaxResult.error("手机号格式不对，请输入11位手机号码");
        }

        if(isLogin){
            log.info("通过手机号获取用户");
            //根据手机号获取用户名
            List<SysUser> sysUser = sysUserService.selectUserByPhonenumber(phonenumber);
            if (CollectionUtils.isEmpty(sysUser)) {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(phonenumber, Constants.LOGIN_FAIL, MessageUtils.message("user.mobile.not.exists")));
                throw new UserException("user.mobile.not.exists", null);
            }
        }
        Random random = new Random();
        // 生成随机字串
        String verifyCode = VerifyCodeUtils.getCaptchaSMS();

        // 唯一标识
        String uuid = IdUtils.simpleUUID();
        String verifyKey = Constants.CAPTCHA_CODE_KEY + phonenumber + uuid;

        redisCache.setCacheObject(verifyKey, verifyCode, Constants.SMS_CAPTCHA_EXPIRATION, TimeUnit.MINUTES);
        log.info("生成验证码");

        //发送短信
        msgService.SendMsg2(phonenumber,"您的验证码是："+ verifyCode+"，5分钟内有效，请勿泄漏。如非本人操作，请忽略此信息。");

        log.info("短信发送成功");
        AjaxResult ajax = AjaxResult.success();
        ajax.put("uuid", uuid);
        //上线后，删除返回值
//        ajax.put("verifyCode", verifyCode);
        return ajax;
    }
}
