package com.ruoyi.web.controller.system;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.TemplateDownUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.UserInfoEmail;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.UserInfoGroup;
import com.ruoyi.system.service.IUserInfoGroupService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 域用户信息Controller
 *
 * @author tjgc
 * @date 2021-04-15
 */
@RestController
@RequestMapping("/system/group")
public class UserInfoGroupController extends BaseController
{
    @Autowired
    private IUserInfoGroupService userInfoGroupService;

    @Autowired
    private TokenService tokenService;

    /**
     * 查询域用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:group:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserInfoGroup userInfoGroup)
    {
        startPage();
        List<UserInfoGroup> list = userInfoGroupService.selectUserInfoGroupList(userInfoGroup);
        return getDataTable(list);
    }

    /**
     * 导出域用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:group:export')")
    @Log(title = "域用户信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UserInfoGroup userInfoGroup)
    {
        List<UserInfoGroup> list = userInfoGroupService.selectUserInfoGroupList(userInfoGroup);
        ExcelUtil<UserInfoGroup> util = new ExcelUtil<UserInfoGroup>(UserInfoGroup.class);
        return util.exportExcel(list, "group");
    }

    /**
     * 获取域用户信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:group:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(userInfoGroupService.selectUserInfoGroupById(id));
    }

    /**
     * 新增域用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:group:add')")
    @Log(title = "域用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserInfoGroup userInfoGroup)
    {
        return toAjax(userInfoGroupService.insertUserInfoGroup(userInfoGroup));
    }

    /**
     * 修改域用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:group:edit')")
    @Log(title = "域用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserInfoGroup userInfoGroup)
    {
        return toAjax(userInfoGroupService.updateUserInfoGroup(userInfoGroup));
    }

    /**
     * 删除域用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:group:remove')")
    @Log(title = "域用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(userInfoGroupService.deleteUserInfoGroupByIds(ids));
    }

    @Log(title = "域用户信息", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('system:group:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception {
        ExcelUtil<UserInfoGroup> util = new ExcelUtil<UserInfoGroup>(UserInfoGroup.class);
        List<UserInfoGroup> userInfoGroupList = util.importExcel(file.getInputStream());
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String operName = loginUser.getUser().getUserId();
        String message = userInfoGroupService.importUser(userInfoGroupList, operName);
        return AjaxResult.success(message);
    }

    @Log(title = "域用户信息模板下载", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('system:group:down')")
    @GetMapping("/templateDown")
    public void templateDown() throws Exception {
        XSSFWorkbook xssfWorkbook = TemplateDownUtils.getXSSFWorkbook(this, "域用户模板.xlsx");
        try {
            String filePass = TemplateDownUtils.createTempFile("域用户模板"+ DateUtils.getDate() +".xlsx");
            logger.info("===创建的文件地址=="+filePass);
            //操作流，写入文件内容（根据前端传参）
            File tempFile = new File(filePass);
            tempFile.setExecutable(true);
            tempFile.setReadable(true);
            tempFile.setWritable(true);
            FileOutputStream outStream = new FileOutputStream(tempFile);
            xssfWorkbook.write(outStream);
            outStream.flush();
            outStream.close();
            logger.info("===文件写入完成==");
            ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletResponse response = servletRequestAttributes.getResponse();
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            String base64Str = TemplateDownUtils.fileToBase64(filePass);
            logger.info("==导出的文件=="+base64Str);
            response.getWriter().write(base64Str);
        } catch (Exception e) {
            throw new Exception("导出失败：失败原因："+ e.getMessage());
        }
        xssfWorkbook.close();
    }
}
