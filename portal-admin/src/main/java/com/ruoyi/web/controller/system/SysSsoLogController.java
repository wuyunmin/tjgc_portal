package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysSsoLog;
import com.ruoyi.system.service.ISysSsoLogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * SSO登陆日志Controller
 * 
 * @author tjgc
 * @date 2020-12-15
 */
@RestController
@RequestMapping("/system/log")
public class SysSsoLogController extends BaseController
{
    @Autowired
    private ISysSsoLogService sysSsoLogService;

    /**
     * 查询SSO登陆日志列表
     */
    @PreAuthorize("@ss.hasPermi('system:log:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysSsoLog sysSsoLog)
    {
        startPage();
        List<SysSsoLog> list = sysSsoLogService.selectSysSsoLogList(sysSsoLog);
        return getDataTable(list);
    }

    /**
     * 导出SSO登陆日志列表
     */
    @PreAuthorize("@ss.hasPermi('system:log:export')")
    @Log(title = "SSO登陆日志", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysSsoLog sysSsoLog)
    {
        List<SysSsoLog> list = sysSsoLogService.selectSysSsoLogList(sysSsoLog);
        ExcelUtil<SysSsoLog> util = new ExcelUtil<SysSsoLog>(SysSsoLog.class);
        return util.exportExcel(list, "log");
    }

    /**
     * 获取SSO登陆日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:log:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(sysSsoLogService.selectSysSsoLogById(id));
    }

    /**
     * 新增SSO登陆日志
     */
    @PreAuthorize("@ss.hasPermi('system:log:add')")
    @Log(title = "SSO登陆日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysSsoLog sysSsoLog)
    {
        return toAjax(sysSsoLogService.insertSysSsoLog(sysSsoLog));
    }

    /**
     * 修改SSO登陆日志
     */
    @PreAuthorize("@ss.hasPermi('system:log:edit')")
    @Log(title = "SSO登陆日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysSsoLog sysSsoLog)
    {
        return toAjax(sysSsoLogService.updateSysSsoLog(sysSsoLog));
    }

    /**
     * 删除SSO登陆日志
     */
    @PreAuthorize("@ss.hasPermi('system:log:remove')")
    @Log(title = "SSO登陆日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(sysSsoLogService.deleteSysSsoLogByIds(ids));
    }
}
