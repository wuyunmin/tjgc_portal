package com.ruoyi.web.controller.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.dao.UserInfoDao;
import com.ruoyi.system.domain.UserInfoJy;
import com.ruoyi.system.domain.UserInfoOa;
import com.ruoyi.system.service.IUserInfoJyService;
import com.ruoyi.system.service.IUserInfoOaService;

/**
 * OA用户信息Controller
 * 
 * @author tjgc
 * @date 2021-01-28
 */
@RestController
@RequestMapping("/system/oa")
public class UserInfoOAController extends BaseController
{
    @Autowired
    private IUserInfoOaService userInfoOaService;

    /**
     * 查询领款用户信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(UserInfoOa userInfoOa)
    {
    	startPage();
    	List<UserInfoOa> list = userInfoOaService.selectUserInfoOaList(userInfoOa);
        return getDataTable(list);
    }

    @GetMapping("/importUser")
    public AjaxResult importUser() {
    	
    	return AjaxResult.success(userInfoOaService.importUser());
    }

}
