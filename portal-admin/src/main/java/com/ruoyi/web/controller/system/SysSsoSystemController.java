package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysSsoSystem;
import com.ruoyi.system.service.ISysSsoSystemService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * SSO应用系统Controller
 * 
 * @author tjgc
 * @date 2020-12-15
 */
@RestController
@RequestMapping("/system/system")
public class SysSsoSystemController extends BaseController
{
    @Autowired
    private ISysSsoSystemService sysSsoSystemService;

    /**
     * 查询SSO应用系统列表
     */
    @PreAuthorize("@ss.hasPermi('system:system:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysSsoSystem sysSsoSystem)
    {
        startPage();
        List<SysSsoSystem> list = sysSsoSystemService.selectSysSsoSystemList(sysSsoSystem);
        return getDataTable(list);
    }

    /**
     * 导出SSO应用系统列表
     */
    @PreAuthorize("@ss.hasPermi('system:system:export')")
    @Log(title = "SSO应用系统", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysSsoSystem sysSsoSystem)
    {
        List<SysSsoSystem> list = sysSsoSystemService.selectSysSsoSystemList(sysSsoSystem);
        ExcelUtil<SysSsoSystem> util = new ExcelUtil<SysSsoSystem>(SysSsoSystem.class);
        return util.exportExcel(list, "system");
    }

    /**
     * 获取SSO应用系统详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:system:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(sysSsoSystemService.selectSysSsoSystemById(id));
    }

    /**
     * 新增SSO应用系统
     */
    @PreAuthorize("@ss.hasPermi('system:system:add')")
    @Log(title = "SSO应用系统", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysSsoSystem sysSsoSystem)
    {
        return toAjax(sysSsoSystemService.insertSysSsoSystem(sysSsoSystem));
    }

    /**
     * 修改SSO应用系统
     */
    @PreAuthorize("@ss.hasPermi('system:system:edit')")
    @Log(title = "SSO应用系统", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysSsoSystem sysSsoSystem)
    {
        return toAjax(sysSsoSystemService.updateSysSsoSystem(sysSsoSystem));
    }

    /**
     * 删除SSO应用系统
     */
    @PreAuthorize("@ss.hasPermi('system:system:remove')")
    @Log(title = "SSO应用系统", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(sysSsoSystemService.deleteSysSsoSystemByIds(ids));
    }
}
