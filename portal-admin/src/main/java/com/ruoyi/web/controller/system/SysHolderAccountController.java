package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysHolderAccount;
import com.ruoyi.system.service.ISysHolderAccountService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author tjgc
 * @date 2021-03-18
 */
@RestController
@RequestMapping("/system/account")
public class SysHolderAccountController extends BaseController
{
    @Autowired
    private ISysHolderAccountService sysHolderAccountService;

    /**
     * 查询【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('system:account:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysHolderAccount sysHolderAccount)
    {
        startPage();
        List<SysHolderAccount> list = sysHolderAccountService.selectSysHolderAccountList(sysHolderAccount);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysHolderAccount sysHolderAccount)
    {
        List<SysHolderAccount> list = sysHolderAccountService.selectSysHolderAccountList(sysHolderAccount);
        ExcelUtil<SysHolderAccount> util = new ExcelUtil<SysHolderAccount>(SysHolderAccount.class);
        return util.exportExcel(list, "account");
    }
}
