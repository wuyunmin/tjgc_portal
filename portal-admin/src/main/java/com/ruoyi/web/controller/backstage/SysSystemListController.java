package com.ruoyi.web.controller.backstage;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysSystemList;
import com.ruoyi.system.service.ISysSystemListService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.annotation.Resource;

/**
 * 系统清单Controller
 * 
 * @author tjgc
 * @date 2021-03-18
 */
@RestController
@RequestMapping("/system/sysList")
public class SysSystemListController extends BaseController
{
    @Resource
    private ISysSystemListService sysSystemListService;

    /**
     * 查询系统清单列表
     */
    @PreAuthorize("@ss.hasPermi('system:sysList:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysSystemList sysSystemList)
    {
        startPage();
        List<SysSystemList> list = sysSystemListService.selectSysSystemListList(sysSystemList);
        return getDataTable(list);
    }

    /**
     * 导出系统清单列表
     */
    @PreAuthorize("@ss.hasPermi('system:sysList:export')")
    @Log(title = "系统清单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysSystemList sysSystemList)
    {
        /**
         * sql转义
         */
        List<SysSystemList> list = sysSystemListService.selectSysSystemListForExport(sysSystemList);
        ExcelUtil<SysSystemList> util = new ExcelUtil<SysSystemList>(SysSystemList.class);
        return util.exportExcel(list, "sysList");
    }

    /**
     * 获取系统清单详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:sysList:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(sysSystemListService.selectSysSystemListById(id));
    }

    /**
     * 新增系统清单
     */
    @PreAuthorize("@ss.hasPermi('system:sysList:add')")
    @Log(title = "系统清单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysSystemList sysSystemList)
    {
        int i = sysSystemListService.checkSysSystemNumExist(sysSystemList.getSysNum());
        if (i > 0) {
            return AjaxResult.error("系统编码已存在，请检查后再添加");
        }
        return toAjax(sysSystemListService.insertSysSystemList(sysSystemList));
    }

    /**
     * 修改系统清单
     */
    @PreAuthorize("@ss.hasPermi('system:sysList:edit')")
    @Log(title = "系统清单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysSystemList sysSystemList)
    {
        int i = sysSystemListService.checkSysSystemNumExist(sysSystemList.getSysNum());
        if (i > 1) {
            return AjaxResult.error("系统编码已存在，请检查后再修改");
        }
        return toAjax(sysSystemListService.updateSysSystemList(sysSystemList));
    }

    /**
     * 删除系统清单
     */
    @PreAuthorize("@ss.hasPermi('system:sysList:remove')")
    @Log(title = "系统清单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysSystemListService.deleteSysSystemListByIds(ids));
    }
    
    /**
     * 获取统一用户管理系统字典（允许代持账号的系统）
     */
    @GetMapping("/getSystemDict")
    public AjaxResult getSystemDict()
    {
    	SysSystemList sysSystemList = new SysSystemList();
    	sysSystemList.setAllowShare("1");
        List<SysSystemList> list = sysSystemListService.selectSysSystemListList(sysSystemList);
        return AjaxResult.success(list);
    }
    
    /**
     * 获取统一用户管理系统字典（所有系统）
     */
    @GetMapping("/getAllSystemDict")
    public AjaxResult getAllSystemDict()
    {
    	SysSystemList sysSystemList = new SysSystemList();
        List<SysSystemList> list = sysSystemListService.selectSysSystemListList(sysSystemList);
        return AjaxResult.success(list);
    }
    
    /**
     * 获取统一用户管理系统字典（允许后台用户集成的系统）
     */
    @GetMapping("/getSystemDictInteg")
    public AjaxResult getSystemDictInteg()
    {
    	SysSystemList sysSystemList = new SysSystemList();
    	sysSystemList.setIntegraType("4");
        List<SysSystemList> list = sysSystemListService.selectSysSystemListList(sysSystemList);
        return AjaxResult.success(list);
    }

    /**
     * 查询所有有效的系统字典
     * @return
     */
    @GetMapping("/getSystemListDictData")
    public AjaxResult SystemListDictData(){
        List<SysSystemList> sysSystemLists = sysSystemListService.selectSystemListDictData();
        System.out.println(sysSystemLists.toString());
        return AjaxResult.success(sysSystemLists);
    }
}
