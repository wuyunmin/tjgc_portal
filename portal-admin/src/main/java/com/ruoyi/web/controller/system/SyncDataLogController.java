package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SyncDataLog;
import com.ruoyi.system.service.ISyncDataLogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 同步日志信息Controller
 * 
 * @author tjgc
 * @date 2022-06-06
 */
@RestController
@RequestMapping("/system/syncLog")
public class SyncDataLogController extends BaseController
{
    @Autowired
    private ISyncDataLogService syncDataLogService;

    /**
     * 查询同步日志信息列表
     */
//    @PreAuthorize("@ss.hasPermi('system:log:list')")
    @GetMapping("/list")
    public TableDataInfo list(SyncDataLog syncDataLog)
    {
        startPage();
        List<SyncDataLog> list = syncDataLogService.selectSyncDataLogList(syncDataLog);
        return getDataTable(list);
    }

    /**
     * 导出同步日志信息列表
     */
//    @PreAuthorize("@ss.hasPermi('system:log:export')")
    @Log(title = "同步日志信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SyncDataLog syncDataLog)
    {
        List<SyncDataLog> list = syncDataLogService.selectSyncDataLogList(syncDataLog);
        ExcelUtil<SyncDataLog> util = new ExcelUtil<SyncDataLog>(SyncDataLog.class);
        return util.exportExcel(list, "log");
    }

    /**
     * 获取同步日志信息详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:log:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(syncDataLogService.selectSyncDataLogById(id));
    }

    /**
     * 新增同步日志信息
     */
//    @PreAuthorize("@ss.hasPermi('system:log:add')")
    @Log(title = "同步日志信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SyncDataLog syncDataLog)
    {
        return toAjax(syncDataLogService.insertSyncDataLog(syncDataLog));
    }

    /**
     * 修改同步日志信息
     */
//    @PreAuthorize("@ss.hasPermi('system:log:edit')")
    @Log(title = "同步日志信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SyncDataLog syncDataLog)
    {
        return toAjax(syncDataLogService.updateSyncDataLog(syncDataLog));
    }

    /**
     * 删除同步日志信息
     */
//    @PreAuthorize("@ss.hasPermi('system:log:remove')")
    @Log(title = "同步日志信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(syncDataLogService.deleteSyncDataLogByIds(ids));
    }
}
