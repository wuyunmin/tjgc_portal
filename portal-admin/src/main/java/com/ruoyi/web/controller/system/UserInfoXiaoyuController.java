package com.ruoyi.web.controller.system;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.TemplateDownUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.UserInfoXiaoyu;
import com.ruoyi.system.service.IUserInfoXiaoyuService;

import javax.servlet.http.HttpServletResponse;

/**
 * 小鱼视频用户信息Controller
 *
 * @author tjec
 * @date 2021-01-28
 */
@RestController
@RequestMapping("/system/xiaoyu")
public class UserInfoXiaoyuController extends BaseController
{
    @Autowired
    private IUserInfoXiaoyuService userInfoXiaoyuService;

    @Autowired
    private TokenService tokenService;

    /**
     * 查询小鱼视频用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:xiaoyu:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserInfoXiaoyu userInfoXiaoyu)
    {
        startPage();
        List<UserInfoXiaoyu> list = userInfoXiaoyuService.selectUserInfoXiaoyuList(userInfoXiaoyu);
        return getDataTable(list);
    }

    @Log(title = "小鱼视频用户信息", businessType = BusinessType.IMPORT)
   // @PreAuthorize("@ss.hasPermi('system:xiaoyu:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception {
        ExcelUtil<UserInfoXiaoyu> util = new ExcelUtil<UserInfoXiaoyu>(UserInfoXiaoyu.class);
        List<UserInfoXiaoyu> userInfoXiaoyuList = util.importExcel(file.getInputStream());
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String operName = loginUser.getUser().getUserId();
        String message = userInfoXiaoyuService.importUser(userInfoXiaoyuList, operName);
        return AjaxResult.success(message);
    }

    /**
     * 导出小鱼视频用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:xiaoyu:export')")
    @Log(title = "小鱼视频用户信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UserInfoXiaoyu userInfoXiaoyu)
    {
        List<UserInfoXiaoyu> list = userInfoXiaoyuService.selectUserInfoXiaoyuList(userInfoXiaoyu);
        ExcelUtil<UserInfoXiaoyu> util = new ExcelUtil<UserInfoXiaoyu>(UserInfoXiaoyu.class);
        return util.exportExcel(list, "xiaoyu");
    }

    /**
     * 获取小鱼视频用户信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:xiaoyu:query')")
    @GetMapping(value = "/{guid}")
    public AjaxResult getInfo(@PathVariable("guid") String guid)
    {
        return AjaxResult.success(userInfoXiaoyuService.selectUserInfoXiaoyuByGuid(guid));
    }

    /**
     * 新增小鱼视频用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:xiaoyu:add')")
    @Log(title = "小鱼视频用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserInfoXiaoyu userInfoXiaoyu)
    {
        return toAjax(userInfoXiaoyuService.insertUserInfoXiaoyu(userInfoXiaoyu));
    }

    /**
     * 修改小鱼视频用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:xiaoyu:edit')")
    @Log(title = "小鱼视频用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserInfoXiaoyu userInfoXiaoyu)
    {
        return toAjax(userInfoXiaoyuService.updateUserInfoXiaoyu(userInfoXiaoyu));
    }

    /**
     * 删除小鱼视频用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:xiaoyu:remove')")
    @Log(title = "小鱼视频用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{guids}")
    public AjaxResult remove(@PathVariable String[] guids)
    {
        return toAjax(userInfoXiaoyuService.deleteUserInfoXiaoyuByGuids(guids));
    }

    @Log(title = "小鱼视频用户模板下载", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('system:xiaoyu:down')")
    @GetMapping("/templateDown")
    public void templateDown() throws Exception {
        XSSFWorkbook xssfWorkbook = TemplateDownUtils.getXSSFWorkbook(this, "小鱼组织架构用户模板.xlsx");
        try {
            String filePass = TemplateDownUtils.createTempFile("小鱼组织架构用户模板"+ DateUtils.getDate() +".xlsx");
            logger.info("===创建的文件地址=="+filePass);
            //操作流，写入文件内容（根据前端传参）
            File tempFile = new File(filePass);
            tempFile.setExecutable(true);
            tempFile.setReadable(true);
            tempFile.setWritable(true);
            FileOutputStream outStream = new FileOutputStream(tempFile);
            xssfWorkbook.write(outStream);
            outStream.flush();
            outStream.close();
            logger.info("===文件写入完成==");
            ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletResponse response = servletRequestAttributes.getResponse();
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            String base64Str = TemplateDownUtils.fileToBase64(filePass);
            logger.info("==导出的文件=="+base64Str);
            response.getWriter().write(base64Str);
        } catch (Exception e) {
            throw new Exception("导出失败：失败原因："+ e.getMessage());
        }
        xssfWorkbook.close();
    }
}
