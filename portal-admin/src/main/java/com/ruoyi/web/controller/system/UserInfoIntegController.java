package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.UserInfoInteg;
import com.ruoyi.system.service.IUserInfoIntegService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 后台集成用户Controller
 * 
 * @author tjgc
 * @date 2021-04-06
 */
@RestController
@RequestMapping("/system/integ")
public class UserInfoIntegController extends BaseController
{
    @Autowired
    private IUserInfoIntegService userInfoIntegService;

    /**
     * 查询后台集成用户列表
     */
    @PreAuthorize("@ss.hasPermi('system:integ:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserInfoInteg userInfoInteg)
    {
        startPage();
        List<UserInfoInteg> list = userInfoIntegService.selectUserInfoIntegList(userInfoInteg);
        return getDataTable(list);
    }

    /**
     * 导出后台集成用户列表
     */
    @PreAuthorize("@ss.hasPermi('system:integ:export')")
    @Log(title = "后台集成用户", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UserInfoInteg userInfoInteg)
    {
        List<UserInfoInteg> list = userInfoIntegService.selectUserInfoIntegList(userInfoInteg);
        ExcelUtil<UserInfoInteg> util = new ExcelUtil<UserInfoInteg>(UserInfoInteg.class);
        return util.exportExcel(list, "integ");
    }

    /**
     * 获取后台集成用户详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:integ:query')")
    @GetMapping(value = "/{guid}")
    public AjaxResult getInfo(@PathVariable("guid") String guid)
    {
        return AjaxResult.success(userInfoIntegService.selectUserInfoIntegByGuid(guid));
    }

    /**
     * 新增后台集成用户
     */
    @PreAuthorize("@ss.hasPermi('system:integ:add')")
    @Log(title = "后台集成用户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserInfoInteg userInfoInteg)
    {
        return toAjax(userInfoIntegService.insertUserInfoInteg(userInfoInteg));
    }

    /**
     * 修改后台集成用户
     */
    @PreAuthorize("@ss.hasPermi('system:integ:edit')")
    @Log(title = "后台集成用户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserInfoInteg userInfoInteg)
    {
        return toAjax(userInfoIntegService.updateUserInfoInteg(userInfoInteg));
    }

    /**
     * 删除后台集成用户
     */
    @PreAuthorize("@ss.hasPermi('system:integ:remove')")
    @Log(title = "后台集成用户", businessType = BusinessType.DELETE)
	@DeleteMapping("/{guids}")
    public AjaxResult remove(@PathVariable String[] guids)
    {
        return toAjax(userInfoIntegService.deleteUserInfoIntegByGuids(guids));
    }
}
