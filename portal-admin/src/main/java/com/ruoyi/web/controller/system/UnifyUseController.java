package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.SysHolderAccount;
import com.ruoyi.system.domain.UnifyUser;
import com.ruoyi.system.service.ISysHolderAccountService;
import com.ruoyi.system.service.IUnifyUserService;

/**
 * 统一用户管理Controller
 *
 * @author pyf
 * @date 2021-01-28
 */
@RestController
@RequestMapping("/system/unifyUser")
public class UnifyUseController extends BaseController
{
    @Autowired
    private TokenService tokenService;

    @Autowired
    private IUnifyUserService unifyUserService;

    @Autowired
    private ISysHolderAccountService sysHolderAccountService;

    /**
     * 查询统一用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:unifyUser:list')")
    @GetMapping("/list")
    public TableDataInfo list(UnifyUser unifyUser)
    {
        startPage();
        List<UnifyUser> list = unifyUserService.selectUserList(unifyUser);
        return getDataTable(list);
    }

    /**
     * 查询代持用户信息列表
     */
    @GetMapping("/holderAccountList")
    public AjaxResult holderAccountList(SysHolderAccount sysHolderAccount)
    {
        List<SysHolderAccount> list = sysHolderAccountService.selectSysHolderAccountList(sysHolderAccount);
        return AjaxResult.success(list);
    }

    /**
     * 新增代持用户信息
     */
    @Log(title = "新增代持用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody List<SysHolderAccount> sysHolderAccounts)
    {
        return toAjax(sysHolderAccountService.insertSysHolderAccount(sysHolderAccounts,tokenService.getLoginUser(ServletUtils.getRequest()).getUser().getId()));
    }

    /**
     * 删除代持用户信息
     */
    @Log(title = "删除代持用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{userId}")
    public AjaxResult remove(@PathVariable Long userId)
    {
        return AjaxResult.success(sysHolderAccountService.deleteSysHolderAccountByUserId(userId));
    }


}
