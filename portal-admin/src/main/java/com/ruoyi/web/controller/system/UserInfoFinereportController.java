package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.UserInfoFinereport;
import com.ruoyi.system.service.IUserInfoFinereportService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 帆软用户信息Controller
 * 
 * @author tjgc
 * @date 2021-04-22
 */
@RestController
@RequestMapping("/system/finereport")
public class UserInfoFinereportController extends BaseController
{
    @Autowired
    private IUserInfoFinereportService userInfoFinereportService;

    /**
     * 查询帆软用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:finereport:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserInfoFinereport userInfoFinereport)
    {
        startPage();
        List<UserInfoFinereport> list = userInfoFinereportService.selectUserInfoFinereportList(userInfoFinereport);
        return getDataTable(list);
    }

    /**
     * 导出帆软用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:finereport:export')")
    @Log(title = "帆软用户信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UserInfoFinereport userInfoFinereport)
    {
        List<UserInfoFinereport> list = userInfoFinereportService.selectUserInfoFinereportList(userInfoFinereport);
        ExcelUtil<UserInfoFinereport> util = new ExcelUtil<UserInfoFinereport>(UserInfoFinereport.class);
        return util.exportExcel(list, "finereport");
    }

    /**
     * 获取帆软用户信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:finereport:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(userInfoFinereportService.selectUserInfoFinereportById(id));
    }

    /**
     * 新增帆软用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:finereport:add')")
    @Log(title = "帆软用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserInfoFinereport userInfoFinereport)
    {
        return toAjax(userInfoFinereportService.insertUserInfoFinereport(userInfoFinereport));
    }

    /**
     * 修改帆软用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:finereport:edit')")
    @Log(title = "帆软用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserInfoFinereport userInfoFinereport)
    {
        return toAjax(userInfoFinereportService.updateUserInfoFinereport(userInfoFinereport));
    }

    /**
     * 删除帆软用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:finereport:remove')")
    @Log(title = "帆软用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(userInfoFinereportService.deleteUserInfoFinereportByIds(ids));
    }
}
