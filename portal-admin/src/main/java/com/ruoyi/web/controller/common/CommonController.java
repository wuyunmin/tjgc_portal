package com.ruoyi.web.controller.common;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.file.SftpUtil;
import com.ruoyi.system.domain.IFileEntireInfo;
import com.ruoyi.system.service.IFileEntireInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.framework.config.ServerConfig;

import java.util.Map;

/**
 * 通用请求处理
 * 
 * @author ruoyi
 */
@RestController
public class CommonController
{
    private static final Logger log = LoggerFactory.getLogger(CommonController.class);

    @Autowired
    private ServerConfig serverConfig;

    @Value("${ruoyi.profile}")
    private String uploadFolder;

    /**
     * ip
     */
    @Value("${sftp.ftpIp}")
    private String ftpIp;
    /**
     *  端口
     */
    @Value("${sftp.port}")
    private Integer PORT;

    /**
     * 用户名
     */
    @Value("${sftp.username}")
    private String USERNAME;

    /**
     * 密码
     */
    @Value("${sftp.password}")
    private String PASSWORD;

    @Value("${sftp.fileIP}")
    public String fileAddress;

    @Resource
    private IFileEntireInfoService fileInfoService;


    /**
     * 通用下载请求
     * 
     * @param fileName 文件名称
     * @param delete 是否删除
     */
    @GetMapping("common/download")
    public void fileDownload(String fileName, Boolean delete, HttpServletResponse response, HttpServletRequest request)
    {
        try
        {
            if (!FileUtils.checkAllowDownload(fileName))
            {
                throw new Exception(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName));
            }
            String realFileName = System.currentTimeMillis() + fileName.substring(fileName.indexOf("_") + 1);
            String filePath = RuoYiConfig.getDownloadPath() + fileName;

            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            FileUtils.setAttachmentResponseHeader(response, realFileName);
            FileUtils.writeBytes(filePath, response.getOutputStream());
            if (delete)
            {
                FileUtils.deleteFile(filePath);
            }
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }

    /**
     * 通用上传请求
     */
    @PostMapping("/common/upload")
    public AjaxResult uploadFile(MultipartFile file) throws Exception
    {
        try
        {

            SftpUtil sftpUtil = new SftpUtil(ftpIp,PORT,USERNAME,PASSWORD);
            // 上传并返回新文件名称
            Map<String, String> uploadNameMap = FileUploadUtils.upload(uploadFolder, file, sftpUtil);
            String fileName = uploadNameMap.get("fileName");
            String identifier = uploadNameMap.get("identifier");
            String location = uploadNameMap.get("location");
            String fileUrl = uploadNameMap.get("fileUrl");
            //增加端口号
            String url = fileAddress + fileUrl;
            IFileEntireInfo fileInfo = new IFileEntireInfo();
            fileInfo.setFileAddr(url);
            fileInfo.setFilename(fileName);
            fileInfo.setLocation(location);
            fileInfo.setIdentifier(identifier);
            fileInfo.setTotalSize(file.getSize());
            fileInfoService.insertAsyncFileInfo(fileInfo);
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            log.info("上传地址： {}",url);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 本地资源通用下载
     */
    @GetMapping("/common/download/resource")
    public void resourceDownload(String resource, HttpServletRequest request, HttpServletResponse response)
            throws Exception
    {
        try
        {
            if (!FileUtils.checkAllowDownload(resource))
            {
                throw new Exception(StringUtils.format("资源文件({})非法，不允许下载。 ", resource));
            }
            // 本地资源路径
            String localPath = RuoYiConfig.getProfile();
            // 数据库资源地址
            String downloadPath = localPath + StringUtils.substringAfter(resource, Constants.RESOURCE_PREFIX);
            // 下载名称
            String downloadName = StringUtils.substringAfterLast(downloadPath, "/");
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            FileUtils.setAttachmentResponseHeader(response, downloadName);
            FileUtils.writeBytes(downloadPath, response.getOutputStream());
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }


}
