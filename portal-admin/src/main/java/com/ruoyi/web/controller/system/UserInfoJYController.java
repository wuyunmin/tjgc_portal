package com.ruoyi.web.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.domain.UserInfoJy;
import com.ruoyi.system.service.IUserInfoJyService;

/**
 * OA用户信息Controller
 * 
 * @author tjgc
 * @date 2021-01-28
 */
@RestController
@RequestMapping("/system/jy")
public class UserInfoJYController extends BaseController
{
    @Autowired
    private IUserInfoJyService userInfoJyService;

    /**
     * 查询领款用户信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(UserInfoJy userInfoJy)
    {
    	startPage();
    	List<UserInfoJy> list = userInfoJyService.selectUserInfoJyList(userInfoJy);
        return getDataTable(list);
    }
    
    @GetMapping("/importUser")
    public AjaxResult importUser() {
    	return AjaxResult.success(userInfoJyService.importUser());
    }
}
