package com.ruoyi.web.controller.backstage;

import java.util.List;

import com.ruoyi.system.domain.SysDownload;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysDownloadFile;
import com.ruoyi.system.service.ISysDownloadFileService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 宣传资料服务配置Controller
 * 
 * @author tjgc
 * @date 2020-12-30
 */
@RestController
@RequestMapping("/system/file")
public class SysDownloadFileController extends BaseController
{
    @Autowired
    private ISysDownloadFileService sysDownloadFileService;

    /**
     * 查询宣传资料服务配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:file:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysDownloadFile sysDownloadFile)
    {
        startPage();
        List<SysDownloadFile> list = sysDownloadFileService.selectSysDownloadFileList(sysDownloadFile);
        return getDataTable(list);
    }

    /**
     * 查询宣传资料服务配置列表
     */
    @GetMapping("/allList")
    public TableDataInfo allList(SysDownloadFile sysDownloadFile)
    {
        startPage();
        List<SysDownloadFile> list = sysDownloadFileService.selectSysDownloadFileList(sysDownloadFile);
        return getDataTable(list);
    }

    /**
     * 导出宣传资料服务配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:file:export')")
    @Log(title = "宣传资料服务配置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysDownloadFile sysDownloadFile)
    {
        List<SysDownloadFile> list = sysDownloadFileService.selectSysDownloadFileList(sysDownloadFile);
        ExcelUtil<SysDownloadFile> util = new ExcelUtil<SysDownloadFile>(SysDownloadFile.class);
        return util.exportExcel(list, "file");
    }

    /**
     * 获取宣传资料服务配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:file:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(sysDownloadFileService.selectSysDownloadFileById(id));
    }

    /**
     * 新增宣传资料服务配置
     */
    @PreAuthorize("@ss.hasPermi('system:file:add')")
    @Log(title = "宣传资料服务配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysDownloadFile sysDownloadFile)
    {
        return toAjax(sysDownloadFileService.insertSysDownloadFile(sysDownloadFile));
    }

    /**
     * 修改宣传资料服务配置
     */
    @PreAuthorize("@ss.hasPermi('system:file:edit')")
    @Log(title = "宣传资料服务配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysDownloadFile sysDownloadFile)
    {
        return toAjax(sysDownloadFileService.updateSysDownloadFile(sysDownloadFile));
    }

    /**
     * 删除宣传资料服务配置
     */
    @PreAuthorize("@ss.hasPermi('system:file:remove')")
    @Log(title = "宣传资料服务配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysDownloadFileService.deleteSysDownloadFileByIds(ids));
    }

    /**
     * 状态修改
     */
    @PreAuthorize("@ss.hasPermi('system:ssoConfig:edit')")
    @Log(title = "用户单点登录配置", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody SysDownloadFile sysDownloadFile)
    {

        logger.info("参数： {}",sysDownloadFile.toString());
        return toAjax(sysDownloadFileService.updateSysDownloadFileStatus(sysDownloadFile));
    }
}
