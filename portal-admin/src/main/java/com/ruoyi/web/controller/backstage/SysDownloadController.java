package com.ruoyi.web.controller.backstage;

import java.util.List;

import com.ruoyi.system.domain.SysDownload;
import com.ruoyi.system.service.ISysDownloadService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.annotation.Resource;

/**
 * 文件下载服务配置Controller
 * 
 * @author tjgc
 * @date 2020-12-30
 */
@RestController
@RequestMapping("/system/download")
public class SysDownloadController extends BaseController
{
    @Resource
    private ISysDownloadService sysDownloadService;

    /**
     * 查询文件服务配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:download:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysDownload sysDownload)
    {
        startPage();
        List<SysDownload> list = sysDownloadService.selectSysDownloadList(sysDownload);
        return getDataTable(list);
    }

    /**
     * 查询文件服务配置列表
     */
    @GetMapping("/allList")
    public TableDataInfo allList(SysDownload sysDownload)
    {
        startPage();
        List<SysDownload> list = sysDownloadService.selectSysDownloadAllList(sysDownload);
        return getDataTable(list);
    }

    /**
     * 导出文件服务配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:download:export')")
    @Log(title = "文件服务配置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysDownload sysDownload)
    {
        List<SysDownload> list = sysDownloadService.selectSysDownloadList(sysDownload);
        ExcelUtil<SysDownload> util = new ExcelUtil<SysDownload>(SysDownload.class);
        return util.exportExcel(list, "download");
    }

    /**
     * 获取文件服务配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:download:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(sysDownloadService.selectSysDownloadById(id));
    }

    /**
     * 新增文件服务配置
     */
    @PreAuthorize("@ss.hasPermi('system:download:add')")
    @Log(title = "文件服务配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysDownload sysDownload)
    {
        return toAjax(sysDownloadService.insertSysDownload(sysDownload));
    }

    /**
     * 修改文件服务配置
     */
    @PreAuthorize("@ss.hasPermi('system:download:edit')")
    @Log(title = "文件服务配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysDownload sysDownload)
    {
        return toAjax(sysDownloadService.updateSysDownload(sysDownload));
    }

    /**
     * 删除文件服务配置
     */
    @PreAuthorize("@ss.hasPermi('system:download:remove')")
    @Log(title = "文件服务配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysDownloadService.deleteSysDownloadByIds(ids));
    }

    /**
     * 状态修改
     */
    @PreAuthorize("@ss.hasPermi('system:ssoConfig:edit')")
    @Log(title = "用户单点登录配置", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody SysDownload sysDownload)
    {

        logger.info("参数： {}",sysDownload.toString());
        return toAjax(sysDownloadService.updateSysDownloadStatus(sysDownload));
    }
}
