package com.ruoyi.web.controller.system;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.TemplateDownUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.UserInfoTb;
import com.ruoyi.system.domain.UserInfoXiaoyu;
import com.ruoyi.system.service.IUserInfoTbService;

import javax.servlet.http.HttpServletResponse;

/**
 * teambition用户信息Controller
 *
 * @author tjec
 * @date 2021-01-28
 */
@RestController
@RequestMapping("/system/tb")
public class UserInfoTbController extends BaseController
{
    @Autowired
    private IUserInfoTbService userInfoTbService;

    @Autowired
    private TokenService tokenService;

    /**
     * 查询teambition用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:tb:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserInfoTb userInfoTb)
    {
        startPage();
        List<UserInfoTb> list = userInfoTbService.selectUserInfoTbList(userInfoTb);
        return getDataTable(list);
    }

    /**
     * 导出teambition用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:tb:export')")
    @Log(title = "teambition用户信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UserInfoTb userInfoTb)
    {
        List<UserInfoTb> list = userInfoTbService.selectUserInfoTbList(userInfoTb);
        ExcelUtil<UserInfoTb> util = new ExcelUtil<UserInfoTb>(UserInfoTb.class);
        return util.exportExcel(list, "tb");
    }

    /**
     * 获取teambition用户信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tb:query')")
    @GetMapping(value = "/{guid}")
    public AjaxResult getInfo(@PathVariable("guid") String guid)
    {
        return AjaxResult.success(userInfoTbService.selectUserInfoTbByGuid(guid));
    }

    /**
     * 新增teambition用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:tb:add')")
    @Log(title = "teambition用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserInfoTb userInfoTb)
    {
        return toAjax(userInfoTbService.insertUserInfoTb(userInfoTb));
    }

    /**
     * 修改teambition用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:tb:edit')")
    @Log(title = "teambition用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserInfoTb userInfoTb)
    {
        return toAjax(userInfoTbService.updateUserInfoTb(userInfoTb));
    }

    /**
     * 删除teambition用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:tb:remove')")
    @Log(title = "teambition用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{guids}")
    public AjaxResult remove(@PathVariable String[] guids)
    {
        return toAjax(userInfoTbService.deleteUserInfoTbByGuids(guids));
    }

    @Log(title = "teambition用户信息", businessType = BusinessType.IMPORT)
    // @PreAuthorize("@ss.hasPermi('system:xiaoyu:import')")
     @PostMapping("/importData")
     public AjaxResult importData(MultipartFile file) throws Exception {
         ExcelUtil<UserInfoTb> util = new ExcelUtil<UserInfoTb>(UserInfoTb.class);
         List<UserInfoTb> userInfoTbList = util.importExcel(file.getInputStream());
         LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
         String operName = loginUser.getUser().getUserId();
         String message = userInfoTbService.importUser(userInfoTbList, operName);
         return AjaxResult.success(message);
     }

    @Log(title = "teambition用户信息模板下载", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('system:tb:down')")
    @GetMapping("/templateDown")
    public void templateDown() throws Exception {
        XSSFWorkbook xssfWorkbook = TemplateDownUtils.getXSSFWorkbook(this, "Teambition模板.xlsx");
        try {
            String filePass = TemplateDownUtils.createTempFile("Teambition模板"+ DateUtils.getDate() +".xlsx");
            logger.info("===创建的文件地址=="+filePass);
            //操作流，写入文件内容（根据前端传参）
            File tempFile = new File(filePass);
            tempFile.setExecutable(true);
            tempFile.setReadable(true);
            tempFile.setWritable(true);
            FileOutputStream outStream = new FileOutputStream(tempFile);
            xssfWorkbook.write(outStream);
            outStream.flush();
            outStream.close();
            logger.info("===文件写入完成==");
            ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletResponse response = servletRequestAttributes.getResponse();
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            String base64Str = TemplateDownUtils.fileToBase64(filePass);
            logger.info("==导出的文件=="+base64Str);
            response.getWriter().write(base64Str);
        } catch (Exception e) {
            throw new Exception("导出失败：失败原因："+ e.getMessage());
        }
        xssfWorkbook.close();
    }
}
