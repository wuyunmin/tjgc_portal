package com.ruoyi.web.controller.backstage;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.domain.server.Sys;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.SysSsoInfo;
import com.ruoyi.system.service.ISysSsoInfoService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户单点登录信息Controller
 * 
 * @author tjgc
 * @date 2020-12-17
 */
@RestController
@RequestMapping("/system/ssoInfo")
public class SysSsoInfoController extends BaseController
{
    @Resource
    private ISysSsoInfoService sysSsoInfoService;

    @Resource
    private TokenService tokenService;
    /**
     * 查询用户单点登录信息列表
     */

    @GetMapping("/list")
    public TableDataInfo list(SysSsoInfo sysSsoInfo)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userNo = loginUser.getUser().getUserNo();
        sysSsoInfo.setUserNo(userNo);
        startPage();
        List<SysSsoInfo> list = sysSsoInfoService.selectSysSsoInfoList(sysSsoInfo);
        return getDataTable(list);
    }

    /**
     * 导出用户单点登录信息列表
     */
    @Log(title = "用户单点登录信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysSsoInfo sysSsoInfo)
    {
        List<SysSsoInfo> list = sysSsoInfoService.selectSysSsoInfoList(sysSsoInfo);
        ExcelUtil<SysSsoInfo> util = new ExcelUtil<SysSsoInfo>(SysSsoInfo.class);
        return util.exportExcel(list, "info");
    }

    /**
     * 获取用户单点登录信息详细信息
     */
    @GetMapping(value = "/get/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {

        return AjaxResult.success(sysSsoInfoService.selectSysSsoInfoById(id));
    }

    /**
     * 新增用户单点登录信息
     */
    @Log(title = "用户单点登录信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody SysSsoInfo sysSsoInfo)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userNo = loginUser.getUser().getUserNo();
        sysSsoInfo.setUserNo(userNo);
        return toAjax(sysSsoInfoService.insertSysSsoInfo(sysSsoInfo));
    }

    /**
     * 修改用户单点登录信息
     */
    @Log(title = "用户单点登录信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody SysSsoInfo sysSsoInfo)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userNo = loginUser.getUser().getUserNo();
        sysSsoInfo.setUserNo(userNo);
        return toAjax(sysSsoInfoService.updateSysSsoInfo(sysSsoInfo));
    }

    /**
     * 删除用户单点登录信息
     */
    @Log(title = "用户单点登录信息", businessType = BusinessType.DELETE)
	@PostMapping("/delete")
    public AjaxResult remove(@RequestBody String ids)
    {
        Long[] longs = JSON.parseObject(ids,Long[].class);
        return toAjax(sysSsoInfoService.deleteSysSsoInfoByIds(longs));
    }
}
