package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysServiceRoom;
import com.ruoyi.system.service.ISysServiceRoomService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 服务大厅Controller
 * 
 * @author tjgc
 * @date 2021-03-09
 */
@RestController
@RequestMapping("/system/room")
public class SysServiceRoomController extends BaseController
{
    @Autowired
    private ISysServiceRoomService sysServiceRoomService;

    /**
     * 查询服务大厅列表
     */
    @PreAuthorize("@ss.hasPermi('system:room:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysServiceRoom sysServiceRoom)
    {
        startPage();
        List<SysServiceRoom> list = sysServiceRoomService.selectSysServiceRoomList(sysServiceRoom);
        return getDataTable(list);
    }

    /**
     * 导出服务大厅列表
     */
    @PreAuthorize("@ss.hasPermi('system:room:export')")
    @Log(title = "服务大厅", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysServiceRoom sysServiceRoom)
    {
        List<SysServiceRoom> list = sysServiceRoomService.selectSysServiceRoomList(sysServiceRoom);
        ExcelUtil<SysServiceRoom> util = new ExcelUtil<SysServiceRoom>(SysServiceRoom.class);
        return util.exportExcel(list, "room");
    }

    /**
     * 获取服务大厅详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:room:query')")
    @GetMapping(value = "/{guid}")
    public AjaxResult getInfo(@PathVariable("guid") String guid)
    {
        return AjaxResult.success(sysServiceRoomService.selectSysServiceRoomByGuid(guid));
    }

    /**
     * 新增服务大厅
     */
    @PreAuthorize("@ss.hasPermi('system:room:add')")
    @Log(title = "服务大厅", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysServiceRoom sysServiceRoom)
    {
        return toAjax(sysServiceRoomService.insertSysServiceRoom(sysServiceRoom));
    }

    /**
     * 修改服务大厅
     */
    @PreAuthorize("@ss.hasPermi('system:room:edit')")
    @Log(title = "服务大厅", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysServiceRoom sysServiceRoom)
    {
        return toAjax(sysServiceRoomService.updateSysServiceRoom(sysServiceRoom));
    }

    /**
     * 删除服务大厅
     */
    @PreAuthorize("@ss.hasPermi('system:room:remove')")
    @Log(title = "服务大厅", businessType = BusinessType.DELETE)
	@DeleteMapping("/{guids}")
    public AjaxResult remove(@PathVariable String[] guids)
    {
        return toAjax(sysServiceRoomService.deleteSysServiceRoomByGuids(guids));
    }
    
    /**
     * 查询服务大厅内容
     */
    @GetMapping("/searchServiceRoom")
    public AjaxResult searchServiceRoom(SysServiceRoom sysServiceRoom)
    {
    	return AjaxResult.success(sysServiceRoomService.searchServiceRoom(sysServiceRoom));
    }
}
