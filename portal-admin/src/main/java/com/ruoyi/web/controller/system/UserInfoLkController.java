package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.UserInfoLk;
import com.ruoyi.system.service.IUserInfoLkService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 领款用户信息Controller
 * 
 * @author tjgc
 * @date 2021-01-28
 */
@RestController
@RequestMapping("/system/lk")
public class UserInfoLkController extends BaseController
{
    @Autowired
    private IUserInfoLkService userInfoLkService;

    /**
     * 查询领款用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:lk:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserInfoLk userInfoLk)
    {
        startPage();
        List<UserInfoLk> list = userInfoLkService.selectUserInfoLkList(userInfoLk);
        return getDataTable(list);
    }

    /**
     * 导出领款用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:lk:export')")
    @Log(title = "领款用户信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UserInfoLk userInfoLk)
    {
        List<UserInfoLk> list = userInfoLkService.selectUserInfoLkList(userInfoLk);
        ExcelUtil<UserInfoLk> util = new ExcelUtil<UserInfoLk>(UserInfoLk.class);
        return util.exportExcel(list, "lk");
    }

    /**
     * 获取领款用户信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:lk:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(userInfoLkService.selectUserInfoLkById(id));
    }

    /**
     * 新增领款用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:lk:add')")
    @Log(title = "领款用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserInfoLk userInfoLk)
    {
        return toAjax(userInfoLkService.insertUserInfoLk(userInfoLk));
    }

    /**
     * 修改领款用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:lk:edit')")
    @Log(title = "领款用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserInfoLk userInfoLk)
    {
        return toAjax(userInfoLkService.updateUserInfoLk(userInfoLk));
    }

    /**
     * 删除领款用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:lk:remove')")
    @Log(title = "领款用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(userInfoLkService.deleteUserInfoLkByIds(ids));
    }
}
