package com.ruoyi.web.controller.backstage;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.system.domain.SysFileInfo;
import com.ruoyi.system.service.ISysFileInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author Administrator
 */
@RequestMapping("file")
@Controller
public class SysFileInfoController extends BaseController {

    @Autowired
    private ISysFileInfoService sysFileInfoService;

    /**
     * 获取文件信息详细信息
     */
    @GetMapping(value = "/{fileId}")
    @ResponseBody
    public String getInfo(@PathVariable("fileId") String fileId)
    {
        String s = sysFileInfoService.selectSysFilePathById(fileId);
        System.out.println(s);
        return s;
    }

    /**
     * 获取文件信息详细信息
     */
    @GetMapping(value = "/info/{fileId}")
    @ResponseBody
    public SysFileInfo getFileInfo(@PathVariable("fileId") String fileId)
    {
        SysFileInfo s = sysFileInfoService.selectSysFileInfoById(fileId);
        System.out.println(s);
        return s;
    }

}
