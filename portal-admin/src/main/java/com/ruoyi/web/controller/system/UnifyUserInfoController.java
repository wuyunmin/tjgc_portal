package com.ruoyi.web.controller.system;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.UnifyUserInfo;
import com.ruoyi.system.service.IUnifyUserInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 统一用户信息Controller
 *
 * @author tjgc
 * @date 2022-03-19
 */
@RestController
@RequestMapping("/system/info")
public class UnifyUserInfoController extends BaseController
{
    @Autowired
    private IUnifyUserInfoService unifyUserInfoService;

    @Autowired
    private TokenService tokenService;


    /**
     * 查询统一用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(UnifyUserInfo unifyUserInfo)
    {
        startPage();
        List<UnifyUserInfo> list = unifyUserInfoService.selectUnifyUserInfoList(unifyUserInfo);
        return getDataTable(list);
    }

    /**
     * 导出统一用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:info:export')")
    @Log(title = "统一用户信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UnifyUserInfo unifyUserInfo)
    {
        List<UnifyUserInfo> list = unifyUserInfoService.selectUnifyUserInfoList(unifyUserInfo);
        ExcelUtil<UnifyUserInfo> util = new ExcelUtil<UnifyUserInfo>(UnifyUserInfo.class);
        return util.exportExcel(list, "info");
    }

    /**
     * 获取统一用户信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(unifyUserInfoService.selectUnifyUserInfoById(id));
    }


    /**
     * 初始化统一用户信息
     */
    @GetMapping(value = "/initUserInfo")
    public AjaxResult initUserInfo()
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String operName = loginUser.getUser().getNickName();
        unifyUserInfoService.initUnifyUserInfo(operName);
        return AjaxResult.success();
    }

}
