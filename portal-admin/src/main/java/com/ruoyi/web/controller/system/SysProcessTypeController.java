package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysProcessType;
import com.ruoyi.system.service.ISysProcessTypeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 流程类别Controller
 * 
 * @author tjgc
 * @date 2021-01-28
 */
@RestController
@RequestMapping("/system/processType")
public class SysProcessTypeController extends BaseController
{
    @Autowired
    private ISysProcessTypeService sysProcessTypeService;

    /**
     * 查询流程类别列表
     */
    @PreAuthorize("@ss.hasPermi('system:processType:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysProcessType sysProcessType)
    {
        startPage();
        List<SysProcessType> list = sysProcessTypeService.selectSysProcessTypeList(sysProcessType);
        return getDataTable(list);
    }

    /**
     * 导出流程类别列表
     */
    @PreAuthorize("@ss.hasPermi('system:processType:export')")
    @Log(title = "流程类别", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysProcessType sysProcessType)
    {
        List<SysProcessType> list = sysProcessTypeService.selectSysProcessTypeList(sysProcessType);
        ExcelUtil<SysProcessType> util = new ExcelUtil<SysProcessType>(SysProcessType.class);
        return util.exportExcel(list, "type");
    }

    /**
     * 获取流程类别详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:processType:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(sysProcessTypeService.selectSysProcessTypeById(id));
    }

    /**
     * 新增流程类别
     */
    @PreAuthorize("@ss.hasPermi('system:processType:add')")
    @Log(title = "流程类别", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysProcessType sysProcessType)
    {
        return toAjax(sysProcessTypeService.insertSysProcessType(sysProcessType));
    }

    /**
     * 修改流程类别
     */
    @PreAuthorize("@ss.hasPermi('system:processType:edit')")
    @Log(title = "流程类别", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysProcessType sysProcessType)
    {
        return toAjax(sysProcessTypeService.updateSysProcessType(sysProcessType));
    }

    /**
     * 删除流程类别
     */
    @PreAuthorize("@ss.hasPermi('system:processType:remove')")
    @Log(title = "流程类别", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysProcessTypeService.deleteSysProcessTypeByIds(ids));
    }
}
