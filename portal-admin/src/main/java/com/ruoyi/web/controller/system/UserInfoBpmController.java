package com.ruoyi.web.controller.system;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.UserInfoBpm;
import com.ruoyi.system.service.IUserInfoBpmService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.annotation.Resource;

/**
 * bpm用户信息Controller
 * 
 * @author tjgc
 * @date 2021-01-28
 */
@RestController
@RequestMapping("/system/bpm")
public class UserInfoBpmController extends BaseController
{
    @Autowired
    private IUserInfoBpmService userInfoBpmService;


    @Resource
    private ISysUserService userService;

    /**
     * 查询bpm用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:bpm:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserInfoBpm userInfoBpm)
    {
        startPage();
        List<UserInfoBpm> list = userInfoBpmService.selectUserInfoBpmList(userInfoBpm);
        return getDataTable(list);
    }

    /**
     * 导出bpm用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:bpm:export')")
    @Log(title = "bpm用户信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UserInfoBpm userInfoBpm)
    {
        List<UserInfoBpm> list = userInfoBpmService.selectUserInfoBpmList(userInfoBpm);
        ExcelUtil<UserInfoBpm> util = new ExcelUtil<UserInfoBpm>(UserInfoBpm.class);
        return util.exportExcel(list, "bpm");
    }

    /**
     * 获取bpm用户信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:bpm:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(userInfoBpmService.selectUserInfoBpmById(id));
    }

    /**
     * 新增bpm用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:bpm:add')")
    @Log(title = "bpm用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserInfoBpm userInfoBpm)
    {
        return toAjax(userInfoBpmService.insertUserInfoBpm(userInfoBpm));
    }

    /**
     * 修改bpm用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:bpm:edit')")
    @Log(title = "bpm用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserInfoBpm userInfoBpm)
    {
        return toAjax(userInfoBpmService.updateUserInfoBpm(userInfoBpm));
    }

    /**
     * 删除bpm用户信息
     */
    @PreAuthorize("@ss.hasPermi('system:bpm:remove')")
    @Log(title = "bpm用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(userInfoBpmService.deleteUserInfoBpmByIds(ids));
    }

    /**
     *
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:bpm:edit')")
    @Log(title = "bpm状态信息同步", businessType = BusinessType.UPDATE)
    @PostMapping("syncBpm")
    public AjaxResult syncBpm() {
        userService.updateUserForceFlag();
        userService.updateCloseUserAccount();
        userService.resigningStatus();
        userService.userStatusLinkage();
        return AjaxResult.success("执行成功");
    }
}
