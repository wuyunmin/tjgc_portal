package com.ruoyi.framework.web.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.cas.authentication.CasAssertionAuthenticationToken;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enums.UserStatus;
import com.ruoyi.common.exception.BaseException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 用户验证处理
 *
 * @author ruoyi
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService,
        AuthenticationUserDetailsService<CasAssertionAuthenticationToken>
{
    private static final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    private ISysUserService userService;

    @Autowired
    private SysPermissionService permissionService;

    @Override
    public UserDetails loadUserByUsername(String phonenumber) throws UsernameNotFoundException
    {
       /* SysUser user = userService.selectUserByUserName(username);
        if (StringUtils.isNull(user))
        {
            log.info("登录用户：{} 不存在.", username);
            throw new UsernameNotFoundException("登录用户：" + username + " 不存在");
        }
        else if (UserStatus.DELETED.getCode().equals(user.getDelFlag()))
        {
            log.info("登录用户：{} 已被删除.", username);
            throw new BaseException("对不起，您的账号：" + username + " 已被删除");
        }
        else if (UserStatus.DISABLE.getCode().equals(user.getStatus()))
        {
            log.info("登录用户：{} 已被停用.", username);
            throw new BaseException("对不起，您的账号：" + username + " 已停用");
        }*/
        List<SysUser> users = userService.selectUserByPhonenumber(phonenumber);
        if (CollectionUtils.isEmpty(users))
        {
            log.info("登录账号：{} 不存在.", phonenumber);
            throw new UsernameNotFoundException("登录账号：" + phonenumber + " 不存在");
        }
        if (users.size()>1)
        {
            log.info("登录账号：{} 重复，请联系管理员", phonenumber);
            throw new BaseException("登录账号：" + phonenumber + " 重复，请联系管理员");
        }
        if (UserStatus.OK.getCode().equals(users.get(0).getStatus())||UserStatus.open.getCode().equals(users.get(0).getStatus())||"1".equals(users.get(0).getForceFlag()))
        {

        }else {
            log.info("登录账号：{} 已禁用，请联系管理员", phonenumber);
            throw new BaseException("登录账号：" + phonenumber + " 已禁用，请联系管理员");
        }
        return createLoginUser(users.get(0));
    }

    public UserDetails createLoginUser(SysUser user)
    {
        return new LoginUser(user, permissionService.getMenuPermission(user));
    }

    @Override
    public UserDetails loadUserDetails(CasAssertionAuthenticationToken casAssertionAuthenticationToken) throws UsernameNotFoundException {
        String name = casAssertionAuthenticationToken.getName();
        System.out.println("获得的用户名："+name);
        SysUser user = userService.selectUserByUserName(name);
        if (user==null){
            throw new UsernameNotFoundException(name+"不存在");
        }
        return createLoginUser(user);
    }
}
