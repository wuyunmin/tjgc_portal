package com.ruoyi.system.dao;

import java.util.List;
import java.util.Map;
public interface UserInfoDao {
	
	List<Map<String, Object>> getUserInfoList(String jobNo);


	List<String> getAuthUserInfo(String jobNo);
	
	List<Map<String, Object>> OAUserListPage(Map<String, String> reqMap);
	
	List<Map<String, Object>> JYUserListPage(Map<String, String> reqMap);
	
	int OAUserTotal(Map<String, String> reqMap);
	
	int JYUserTotal(Map<String, String> reqMap);
	
	List<Map<String, Object>> getAllOAUser();
	List<Map<String, Object>> getAllJYUser();
	
	public List<Map<String, Object>> getUserByAccountAndFlag(String account,String flag);
}
