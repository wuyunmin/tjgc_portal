package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.UserInfoDing;
import org.apache.ibatis.annotations.Param;

/**
 * 钉钉用户信息Mapper接口
 * 
 * @author tjgc
 * @date 2021-04-21
 */
public interface UserInfoDingMapper 
{
    /**
     * 查询钉钉用户信息
     * 
     * @param id 钉钉用户信息ID
     * @return 钉钉用户信息
     */
    public UserInfoDing selectUserInfoDingById(Long id);

    /**
     * 查询钉钉用户信息列表
     * 
     * @param userInfoDing 钉钉用户信息
     * @return 钉钉用户信息集合
     */
    public List<UserInfoDing> selectUserInfoDingList(UserInfoDing userInfoDing);

    /**
     * 新增钉钉用户信息
     * 
     * @param userInfoDing 钉钉用户信息
     * @return 结果
     */
    public int insertUserInfoDing(UserInfoDing userInfoDing);

    /**
     * 修改钉钉用户信息
     * 
     * @param userInfoDing 钉钉用户信息
     * @return 结果
     */
    public int updateUserInfoDing(UserInfoDing userInfoDing);

    /**
     * 删除钉钉用户信息
     * 
     * @param id 钉钉用户信息ID
     * @return 结果
     */
    public int deleteUserInfoDingById(Long id);

    /**
     * 批量删除钉钉用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserInfoDingByIds(Long[] ids);

    /**
     * 截断表
     */
    public void cleanDingUser();

    /**
     * 删除导入的数据
     * @param userNoList
     * @return
     */
    public int deleteUserInfoDingByUpdate(@Param("userNoList")List<String> userNoList);


    /**
     * 查询是都存在
     * @param userNo
     * @return
     */
    public int selectCountByUserNo(String userNo);

    /**
     * 更新用户信息
     * @param userInfoDing
     * @return
     */
    public int updateUserInfoDingByUserNo(UserInfoDing userInfoDing);
    
    List<Map<String, Object>> selectUserInfoALDDByJobNo(String jobNo);
}
