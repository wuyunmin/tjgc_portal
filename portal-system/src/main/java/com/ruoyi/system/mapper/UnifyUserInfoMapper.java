package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.UnifyUserInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 统一用户信息Mapper接口
 *
 * @author tjgc
 * @date 2022-03-19
 */
public interface UnifyUserInfoMapper
{
    /**
     * 查询统一用户信息
     *
     * @param id 统一用户信息ID
     * @return 统一用户信息
     */
    public UnifyUserInfo selectUnifyUserInfoById(Long id);

    /**
     * 查询统一用户信息列表
     *
     * @param unifyUserInfo 统一用户信息
     * @return 统一用户信息集合
     */
    public List<UnifyUserInfo> selectUnifyUserInfoList(UnifyUserInfo unifyUserInfo);

    /**
     * 新增统一用户信息
     *
     * @param unifyUserInfo 统一用户信息
     * @return 结果
     */
    public int insertUnifyUserInfo(UnifyUserInfo unifyUserInfo);

    /**
     * 批量插入
     * @param unifyUserInfos
     * @return
     */
    public int insertBatchUnifyUserInfo(@Param("unifyUserInfos")List<UnifyUserInfo> unifyUserInfos);

    /**
     * 修改统一用户信息
     *
     * @param unifyUserInfo 统一用户信息
     * @return 结果
     */
    public int updateUnifyUserInfo(UnifyUserInfo unifyUserInfo);

    /**
     * 删除统一用户信息
     *
     * @param id 统一用户信息ID
     * @return 结果
     */
    public int deleteUnifyUserInfoById(Long id);

    /**
     * 批量删除统一用户信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUnifyUserInfoByIds(Long[] ids);

    /**
     * 清空统一用户信息
     */
    public void cleanUnifyUserInfo();

    public List<UnifyUserInfo> selectBpmUserInfo();

    public List<UnifyUserInfo> selectEcUserInfo();


    public List<UnifyUserInfo> selectLkUserInfo();


    public List<UnifyUserInfo> selectTbUserInfo();



    public List<UnifyUserInfo> selectEmailUserInfo();

    public List<UnifyUserInfo> selectSchoolUserInfo();


    public List<UnifyUserInfo> selectGroupUserInfo();


    public List<UnifyUserInfo> selectProjectUserInfo();



    public List<UnifyUserInfo> selectDingUserInfo();

    public List<UnifyUserInfo> selectHrUserInfo();




    public List<UnifyUserInfo> selectFinereportUserInfo();



    public List<UnifyUserInfo> selectOaUserInfo();



    public List<UnifyUserInfo> selectJyUserInfo();

    public List<UnifyUserInfo> selectXiaoyuUserInfo();




    public List<UnifyUserInfo> selectXionganUserInfo();



    public List<UnifyUserInfo> selectHolderUserInfo();



    public List<UnifyUserInfo> selectIntegUserInfo();








}
