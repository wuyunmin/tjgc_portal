package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysContactOur;

/**
 * 联系我们Mapper接口
 * 
 * @author pangyongfeng
 * @date 2021-03-09
 */
public interface SysContactOurMapper 
{
    /**
     * 查询联系我们
     * 
     * @param guid 联系我们ID
     * @return 联系我们
     */
    public SysContactOur selectSysContactOurByGuid(String guid);

    /**
     * 查询联系我们列表
     * 
     * @param sysContactOur 联系我们
     * @return 联系我们集合
     */
    public List<SysContactOur> selectSysContactOurList(SysContactOur sysContactOur);

    /**
     * 新增联系我们
     * 
     * @param sysContactOur 联系我们
     * @return 结果
     */
    public int insertSysContactOur(SysContactOur sysContactOur);

    /**
     * 修改联系我们
     * 
     * @param sysContactOur 联系我们
     * @return 结果
     */
    public int updateSysContactOur(SysContactOur sysContactOur);

    /**
     * 删除联系我们
     * 
     * @param guid 联系我们ID
     * @return 结果
     */
    public int deleteSysContactOurByGuid(String guid);

    /**
     * 批量删除联系我们
     * 
     * @param guids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysContactOurByGuids(String[] guids);
    
    public List<SysContactOur> getContactOur();
}
