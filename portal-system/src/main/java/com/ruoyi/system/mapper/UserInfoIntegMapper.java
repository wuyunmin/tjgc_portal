package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.UserInfoInteg;

/**
 * 后台集成用户Mapper接口
 * 
 * @author tjgc
 * @date 2021-04-06
 */
public interface UserInfoIntegMapper 
{
    /**
     * 查询后台集成用户
     * 
     * @param guid 后台集成用户ID
     * @return 后台集成用户
     */
    public UserInfoInteg selectUserInfoIntegByGuid(String guid);

    /**
     * 查询后台集成用户列表
     * 
     * @param userInfoInteg 后台集成用户
     * @return 后台集成用户集合
     */
    public List<UserInfoInteg> selectUserInfoIntegList(UserInfoInteg userInfoInteg);
    
    public List<UserInfoInteg> selectIntegeHolderList(String userId);


    public List<String> selectSysNameList(String userId);

    /**
     * 新增后台集成用户
     * 
     * @param userInfoInteg 后台集成用户
     * @return 结果
     */
    public int insertUserInfoInteg(UserInfoInteg userInfoInteg);

    /**
     * 修改后台集成用户
     * 
     * @param userInfoInteg 后台集成用户
     * @return 结果
     */
    public int updateUserInfoInteg(UserInfoInteg userInfoInteg);

    /**
     * 删除后台集成用户
     * 
     * @param guid 后台集成用户ID
     * @return 结果
     */
    public int deleteUserInfoIntegByGuid(String guid);

    /**
     * 批量删除后台集成用户
     * 
     * @param guids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserInfoIntegByGuids(String[] guids);
}
