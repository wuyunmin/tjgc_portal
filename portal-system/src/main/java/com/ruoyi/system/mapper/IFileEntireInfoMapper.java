package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.IFileEntireInfo;

/**
 * 分片上传文件信息Mapper接口
 * 
 * @author tjgc
 * @date 2021-03-15
 */
public interface IFileEntireInfoMapper
{
    /**
     * 查询分片上传文件信息
     * 
     * @param id 分片上传文件信息ID
     * @return 分片上传文件信息
     */
    public IFileEntireInfo selectAsyncFileInfoByOne(IFileEntireInfo iFileEntireInfo);

    public IFileEntireInfo selectAsyncFileInfoByUrl(String fileAddr);

    public IFileEntireInfo selectAsyncFileInfoByIdentifier(String iFileEntireInfo);
    /**
     * 查询分片上传文件信息列表
     * 
     * @param iFileEntireInfo 分片上传文件信息
     * @return 分片上传文件信息集合
     */
    public List<IFileEntireInfo> selectAsyncFileInfoList(IFileEntireInfo iFileEntireInfo);

    /**
     * 新增分片上传文件信息
     * 
     * @param iFileEntireInfo 分片上传文件信息
     * @return 结果
     */
    public int insertAsyncFileInfo(IFileEntireInfo iFileEntireInfo);

    /**
     * 修改分片上传文件信息
     * 
     * @param iFileEntireInfo 分片上传文件信息
     * @return 结果
     */
    public int updateAsyncFileInfo(IFileEntireInfo iFileEntireInfo);

    /**
     * 删除分片上传文件信息
     * 
     * @param id 分片上传文件信息ID
     * @return 结果
     */
    public int deleteAsyncFileInfoById(String id);


    public int deleteAsyncFileInfoByFile(IFileEntireInfo iFileEntireInfo);

    /**
     * 批量删除分片上传文件信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAsyncFileInfoByIds(String[] ids);
}
