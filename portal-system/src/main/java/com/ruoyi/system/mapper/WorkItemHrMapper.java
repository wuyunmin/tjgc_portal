package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.WorkItemHr;

/**
 * hr待办待阅信息Mapper接口
 * 
 * @author tjgc
 * @date 2022-05-09
 */
public interface WorkItemHrMapper 
{
    /**
     * 查询hr待办待阅信息
     * 
     * @param id hr待办待阅信息ID
     * @return hr待办待阅信息
     */
    public WorkItemHr selectWorkItemHrById(Long id);

    /**
     * 查询hr待办待阅信息列表
     * 
     * @param workItemHr hr待办待阅信息
     * @return hr待办待阅信息集合
     */
    public List<WorkItemHr> selectWorkItemHrList(WorkItemHr workItemHr);

    /**
     * 新增hr待办待阅信息
     * 
     * @param workItemHr hr待办待阅信息
     * @return 结果
     */
    public int insertWorkItemHr(WorkItemHr workItemHr);

    /**
     * 修改hr待办待阅信息
     * 
     * @param workItemHr hr待办待阅信息
     * @return 结果
     */
    public int updateWorkItemHr(WorkItemHr workItemHr);

    /**
     * 删除hr待办待阅信息
     * 
     * @param id hr待办待阅信息ID
     * @return 结果
     */
    public int deleteWorkItemHrById(Long id);

    /**
     * 批量删除hr待办待阅信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWorkItemHrByIds(Long[] ids);
}
