package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.UserInfoEmail;
import org.apache.ibatis.annotations.Param;

/**
 * 企业邮箱用户信息Mapper接口
 * 
 * @author tjec
 * @date 2021-01-28
 */
public interface UserInfoEmailMapper 
{
    /**
     * 查询企业邮箱用户信息
     * 
     * @param guid 企业邮箱用户信息ID
     * @return 企业邮箱用户信息
     */
    public UserInfoEmail selectUserInfoEmailByGuid(String guid);


    public UserInfoEmail selectUserInfoEmailByUserNo(String userNo);
	
	 /**
     * 查询企业邮箱用户信息
     * 
     * @param email 电子邮件
     * @return 企业邮箱用户信息
     */
    public UserInfoEmail selectUserInfoEmailByEmail(String email);
	
    /**
     * 查询企业邮箱用户信息列表
     * 
     * @param userInfoEmail 企业邮箱用户信息
     * @return 企业邮箱用户信息集合
     */
    public List<UserInfoEmail> selectUserInfoEmailList(UserInfoEmail userInfoEmail);


    public List<Map<String,Object>> selectUserInfoByEmailHolder(@Param("userId")Long userId);

    /**
     * 新增企业邮箱用户信息
     * 
     * @param userInfoEmail 企业邮箱用户信息
     * @return 结果
     */
    public int insertUserInfoEmail(UserInfoEmail userInfoEmail);

    /**
     * 修改企业邮箱用户信息
     * 
     * @param userInfoEmail 企业邮箱用户信息
     * @return 结果
     */
    public int updateUserInfoEmail(UserInfoEmail userInfoEmail);

    /**
     * 删除企业邮箱用户信息
     * 
     * @param guid 企业邮箱用户信息ID
     * @return 结果
     */
    public int deleteUserInfoEmailByGuid(String guid);

    /**
     * 批量删除企业邮箱用户信息
     * 
     * @param guids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserInfoEmailByGuids(String[] guids);
    
    public void cleanEmailUser();
    
    /**
     * 批量逻辑删除企业邮箱用户信息
     * 
     * @param guids 需要删除的数据ID
     * @return 结果
     */
    public int updateUserInfoEmailByGuids(String[] guids);
    
    public List<Map<String,Object>> selectUserInfoEmailByJobNo(String jobNo);
    
}
