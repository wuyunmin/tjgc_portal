package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.WorkItemBpm;

/**
 * hr待办待阅信息Mapper接口
 * 
 * @author tjgc
 * @date 2022-05-16
 */
public interface WorkItemBpmMapper 
{
    /**
     * 查询hr待办待阅信息
     * 
     * @param id hr待办待阅信息ID
     * @return hr待办待阅信息
     */
    public WorkItemBpm selectWorkItemBpmById(Long id);

    /**
     * 查询hr待办待阅信息列表
     * 
     * @param workItemBpm hr待办待阅信息
     * @return hr待办待阅信息集合
     */
    public List<WorkItemBpm> selectWorkItemBpmList(WorkItemBpm workItemBpm);

    /**
     * 新增hr待办待阅信息
     * 
     * @param workItemBpm hr待办待阅信息
     * @return 结果
     */
    public int insertWorkItemBpm(WorkItemBpm workItemBpm);

    /**
     * 修改hr待办待阅信息
     * 
     * @param workItemBpm hr待办待阅信息
     * @return 结果
     */
    public int updateWorkItemBpm(WorkItemBpm workItemBpm);

    /**
     * 删除hr待办待阅信息
     * 
     * @param id hr待办待阅信息ID
     * @return 结果
     */
    public int deleteWorkItemBpmById(Long id);

    /**
     * 批量删除hr待办待阅信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWorkItemBpmByIds(Long[] ids);
}
