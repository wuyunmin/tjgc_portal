package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.UserInfoOa;

/**
 * OA系统用户信息Mapper接口
 * 
 * @author tjgc
 * @date 2021-02-24
 */
public interface UserInfoOaMapper 
{
    /**
     * 查询OA系统用户信息
     * 
     * @param guid OA系统用户信息ID
     * @return OA系统用户信息
     */
    public UserInfoOa selectUserInfoOaById(Long guid);

    /**
     * 查询OA系统用户信息列表
     * 
     * @param userInfoOa OA系统用户信息
     * @return OA系统用户信息集合
     */
    public List<UserInfoOa> selectUserInfoOaList(UserInfoOa userInfoOa);

    /**
     * 新增OA系统用户信息
     * 
     * @param userInfoOa OA系统用户信息
     * @return 结果
     */
    public int insertUserInfoOa(List<Map<String, Object>> oaUsers);

    /**
     * 修改OA系统用户信息
     * 
     * @param userInfoOa OA系统用户信息
     * @return 结果
     */
    public int updateUserInfoOa(UserInfoOa userInfoOa);
    
    public void cleanOaUser();
}
