package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.UserInfoBpm;
import org.apache.ibatis.annotations.Param;

/**
 * bpm用户信息Mapper接口
 * 
 * @author tjgc
 * @date 2021-01-06
 */
public interface UserInfoBpmMapper 
{
    /**
     * 查询bpm用户信息
     * 
     * @param id bpm用户信息ID
     * @return bpm用户信息
     */
    public UserInfoBpm selectUserInfoBpmById(Long id);

    /**
     * 查询bpm用户信息列表
     * 
     * @param userInfoBpm bpm用户信息
     * @return bpm用户信息集合
     */
    public List<UserInfoBpm> selectUserInfoBpmList(UserInfoBpm userInfoBpm);

    /**
     * 查询bpm用户信息
     * @param userNo
     * @return
     */
    public UserInfoBpm selectUserInfoBpmByOne(String userNo);
    /**
     * 新增bpm用户信息
     * 
     * @param userInfoBpm bpm用户信息
     * @return 结果
     */
    public int insertUserInfoBpm(UserInfoBpm userInfoBpm);

    /**
     * 修改bpm用户信息
     * 
     * @param userInfoBpm bpm用户信息
     * @return 结果
     */
    public int updateUserInfoBpm(UserInfoBpm userInfoBpm);

    public int updateUserInfoBpmBatch(@Param("userNoList")List<String> userNoList, @Param("status")String status);

    /**
     * 修改bpm用户信息
     *
     * @param userInfoBpm bpm用户信息
     * @return 结果
     */
    public int updateUserInfoBpmByUserNo(UserInfoBpm userInfoBpm);

    /**
     * 删除bpm用户信息
     * 
     * @param id bpm用户信息ID
     * @return 结果
     */
    public int deleteUserInfoBpmById(Long id);

    /**
     * 批量删除bpm用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserInfoBpmByIds(Long[] ids);
    
    public List<Map<String,Object>> selectUserInfoBpmByJobNo(String jobNo);
}
