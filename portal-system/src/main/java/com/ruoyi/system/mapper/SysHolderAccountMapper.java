package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysHolderAccount;

import io.lettuce.core.dynamic.annotation.Param;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author tjgc
 * @date 2021-02-04
 */
public interface SysHolderAccountMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param guid 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public List<SysHolderAccount> selectSysHolderAccountByUserId(long userId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param sysHolderAccount 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<SysHolderAccount> selectSysHolderAccountList(SysHolderAccount sysHolderAccount);

    /**
     * 新增【请填写功能名称】
     * 
     * @param sysHolderAccount 【请填写功能名称】
     * @return 结果
     */
    public int insertSysHolderAccount(SysHolderAccount sysHolderAccount);

    /**
     * 修改【请填写功能名称】
     * 
     * @param sysHolderAccount 【请填写功能名称】
     * @return 结果
     */
    public int updateSysHolderAccount(SysHolderAccount sysHolderAccount);

    /**
     * 删除【请填写功能名称】
     * 
     * @param guid 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteSysHolderAccountByGuid(String guid);
    
    /**
     * 删除【请填写功能名称】
     * 
     * @param userId 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteSysHolderAccountByUserId(Long userId);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param guids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysHolderAccountByGuids(String[] guids);
    
    public String selectHolderNamesOA(String account);
    public String selectHolderNamesJY(String account);
}
