package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.UserInfoTb;
import java.util.List;
import java.util.Map;

/**
 * teambition用户信息Mapper接口
 * 
 * @author tjec
 * @date 2021-01-28
 */
public interface UserInfoTbMapper 
{
    /**
     * 查询teambition用户信息
     * 
     * @param guid teambition用户信息ID
     * @return teambition用户信息
     */
    public UserInfoTb selectUserInfoTbByGuid(String guid);

    /**
     * 查询teambition用户信息列表
     * 
     * @param userInfoTb teambition用户信息
     * @return teambition用户信息集合
     */
    public List<UserInfoTb> selectUserInfoTbList(UserInfoTb userInfoTb);

    /**
     * 新增teambition用户信息
     * 
     * @param userInfoTb teambition用户信息
     * @return 结果
     */
    public int insertUserInfoTb(UserInfoTb userInfoTb);

    /**
     * 修改teambition用户信息
     * 
     * @param userInfoTb teambition用户信息
     * @return 结果
     */
    public int updateUserInfoTb(UserInfoTb userInfoTb);

    /**
     * 删除teambition用户信息
     * 
     * @param guid teambition用户信息ID
     * @return 结果
     */
    public int deleteUserInfoTbByGuid(String guid);

    /**
     * 批量删除teambition用户信息
     * 
     * @param guids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserInfoTbByGuids(String[] guids);
    
    public void cleanTbUser();
    
    public List<Map<String,Object>> selectUserInfoTbByJobNo(String jobNo);
}
