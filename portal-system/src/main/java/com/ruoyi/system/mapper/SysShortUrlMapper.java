package com.ruoyi.system.mapper;

import com.ruoyi.common.core.domain.entity.SysShortUrl;

import java.util.List;


/**
 * 文件信息Mapper接口
 * 
 * @author tjgc
 * @date 2021-02-08
 */
public interface SysShortUrlMapper 
{
    /**
     * 查询文件信息
     * 
     * @param id 文件信息ID
     * @return 文件信息
     */
    public SysShortUrl selectSysShortUrlById(Long id);

    /**
     * 查询文件信息列表
     *
     * @param sysShortUrl 文件信息
     * @return 文件信息集合
     */
    public SysShortUrl selectSysShortByShotUrl(String sysShortUrl);


    public Integer selectSysShortUrlCountByUserNo(String userNo);
    /**
     * 根据sysShortUrl删除短链接
     * @param sysShortUrl
     * @return
     */
    public int deleteSysShortUrlByShotUrl(String sysShortUrl);

    /**
     * 查询文件信息列表
     * 
     * @param sysShortUrl 文件信息
     * @return 文件信息集合
     */
    public List<SysShortUrl> selectSysShortUrlList(SysShortUrl sysShortUrl);





    /**
     * 新增文件信息
     * 
     * @param sysShortUrl 文件信息
     * @return 结果
     */
    public int insertSysShortUrl(SysShortUrl sysShortUrl);

    /**
     * 修改文件信息
     * 
     * @param sysShortUrl 文件信息
     * @return 结果
     */
    public int updateSysShortUrl(SysShortUrl sysShortUrl);

    /**
     * 删除文件信息
     * 
     * @param id 文件信息ID
     * @return 结果
     */
    public int deleteSysShortUrlById(Long id);

    /**
     * 批量删除文件信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysShortUrlByIds(Long[] ids);

    public int deleteSysShortUrlByUserNo(String userNo);
}
