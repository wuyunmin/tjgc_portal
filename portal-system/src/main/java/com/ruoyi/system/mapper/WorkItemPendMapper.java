package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.WorkItemPend;
import org.apache.ibatis.annotations.Param;

/**
 * 待办待阅信息Mapper接口
 *
 * @author tjgc
 * @date 2021-01-07
 */
public interface WorkItemPendMapper
{
    /**
     * 查询待办待阅信息
     *
     * @param id 待办待阅信息ID
     * @return 待办待阅信息
     */
    public WorkItemPend selectWorkItemPendById(Long id);

    /**
     * 查询待办待阅信息
     *
     * @param sequenceNo 待办待阅信息ID
     * @return 待办待阅信息
     */
    public int selectCountWorkItemPendBySequenceNo(String sequenceNo);
    /**
     * 查询待办待阅信息列表
     *
     * @param workItemPend 待办待阅信息
     * @return 待办待阅信息集合
     */
    public List<WorkItemPend> selectWorkItemPendList(WorkItemPend workItemPend);


    public List<WorkItemPend> selectAllWorkItemPendList(WorkItemPend workItemPend);

    /**
     * 新增待办待阅信息
     *
     * @param workItemPend 待办待阅信息
     * @return 结果
     */
    public int insertWorkItemPend(WorkItemPend workItemPend);

    /**
     * 修改待办待阅信息
     *
     * @param workItemPend 待办待阅信息
     * @return 结果
     */
    public int updateWorkItemPend(WorkItemPend workItemPend);

    /**
     * BPM专用，其他请勿使用
     * 修改待办待阅信息
     * 此方法更新时，状态设置是 2：未处理
     * @param workItemPend 待办待阅信息
     * @return 结果
     */
    public int updateWorkItemPendBySequenceNoInBpm(WorkItemPend workItemPend);

    /**
     * 通用修改待办待阅信息
     * @param workItemPend
     * @return
     */
    public int updateWorkItemPendBySequenceNoCommon(WorkItemPend workItemPend);
    /**
     * 修改待办待阅信息
     *
     * @param workItemPend 待办待阅信息
     * @return 结果
     */
    public int updateWorkItemPendStatus(WorkItemPend workItemPend);

    /**
     * 修改所有OA待办待阅为已完成
     * @return
     */
    public int updateOaWorkItemPendStatus();

    /**
     * 修改所有HR待办待阅为已完成
     * @return
     */
    public int updateHrWorkItemPendStatus();


    /**
     * 修改所有Bpm待办待阅为已完成
     * @return
     */
    public int updateBpmWorkItemPendStatus();
    /**
     * 删除待办待阅信息
     *
     * @param id 待办待阅信息ID
     * @return 结果
     */
    public int deleteWorkItemPendById(Long id);

    /**
     * 批量删除待办待阅信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWorkItemPendByIds(Long[] ids);


    /**
     * 查询待办待阅信息通过流程ID和系统名称
     *
     * @param userNo 流程ID
     * @return 待办待阅信息
     */
    public int selectWorkItemPendCountBySystem(@Param("systemName") String systemName,@Param("userNo") String userNo);

    /**
     * bpm待办数量（bpm待办数量为 ec + portal）
     * @param systemNames
     * @param userNo
     * @return
     */
    public int selectWorkItemPendCountBpm(@Param("systemNames") List<String> systemNames,@Param("userNo") String userNo);

    /**
     * 查询待办待阅信息通过流程ID和系统名称
     *
     * @param id
     * @return 待办待阅信息
     */
    public WorkItemPend selectWorkItemPendAndSSOById(Long id);

    /**
     * 统计待办待阅信息列表
     * @param workItemPend
     * @return
     */
    public int selectCountWorkItemPend(WorkItemPend workItemPend);
}
