package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SyncDataLog;

/**
 * 同步日志信息Mapper接口
 * 
 * @author tjgc
 * @date 2022-06-06
 */
public interface SyncDataLogMapper 
{
    /**
     * 查询同步日志信息
     * 
     * @param id 同步日志信息ID
     * @return 同步日志信息
     */
    public SyncDataLog selectSyncDataLogById(Long id);

    /**
     * 查询同步日志信息列表
     * 
     * @param syncDataLog 同步日志信息
     * @return 同步日志信息集合
     */
    public List<SyncDataLog> selectSyncDataLogList(SyncDataLog syncDataLog);



    public int selectCountSyncDataLogToday(SyncDataLog syncDataLog);

    /**
     * 新增同步日志信息
     * 
     * @param syncDataLog 同步日志信息
     * @return 结果
     */
    public int insertSyncDataLog(SyncDataLog syncDataLog);

    /**
     * 修改同步日志信息
     * 
     * @param syncDataLog 同步日志信息
     * @return 结果
     */
    public int updateSyncDataLog(SyncDataLog syncDataLog);

    /**
     * 删除同步日志信息
     * 
     * @param id 同步日志信息ID
     * @return 结果
     */
    public int deleteSyncDataLogById(Long id);

    /**
     * 批量删除同步日志信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSyncDataLogByIds(Long[] ids);
}
