package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.UserInfoJy;

/**
 * 经营系统用户信息Mapper接口
 * 
 * @author tjgc
 * @date 2021-02-24
 */
public interface UserInfoJyMapper 
{
    /**
     * 查询经营系统用户信息
     * 
     * @param guid 经营系统用户信息ID
     * @return 经营系统用户信息
     */
    public UserInfoJy selectUserInfoJyById(Long guid);

    /**
     * 查询经营系统用户信息列表
     * 
     * @param userInfoJy 经营系统用户信息
     * @return 经营系统用户信息集合
     */
    public List<UserInfoJy> selectUserInfoJyList(UserInfoJy userInfoJy);

    /**
     * 新增经营系统用户信息
     * 
     * @param userInfoJy 经营系统用户信息
     * @return 结果
     */
    public int insertUserInfoJy(List<Map<String, Object>> jyUsers);

    /**
     * 修改经营系统用户信息
     * 
     * @param userInfoJy 经营系统用户信息
     * @return 结果
     */
    public int updateUserInfoJy(UserInfoJy userInfoJy);
    
    public void cleanJyUser();
}
