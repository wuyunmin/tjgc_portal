package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.UserInfoLk;

/**
 * 领款用户信息Mapper接口
 * 
 * @author tjgc
 * @date 2021-01-06
 */
public interface UserInfoLkMapper 
{
    /**
     * 查询领款用户信息
     * 
     * @param id 领款用户信息ID
     * @return 领款用户信息
     */
    public UserInfoLk selectUserInfoLkById(Long id);


    /**
     * 查询领款用户信息
     *
     * @param userNo 领款用户信息ID
     * @return 领款用户信息
     */
    public int selectCountUserInfoLkByUserNo(String userNo);
    /**
     * 查询领款用户信息列表
     * 
     * @param userInfoLk 领款用户信息
     * @return 领款用户信息集合
     */
    public List<UserInfoLk> selectUserInfoLkList(UserInfoLk userInfoLk);

    /**
     * 新增领款用户信息
     * 
     * @param userInfoLk 领款用户信息
     * @return 结果
     */
    public int insertUserInfoLk(UserInfoLk userInfoLk);

    /**
     * 修改领款用户信息
     * 
     * @param userInfoLk 领款用户信息
     * @return 结果
     */
    public int updateUserInfoLk(UserInfoLk userInfoLk);

    /**
     * 修改领款用户信息
     *
     * @param userInfoLk 领款用户信息
     * @return 结果
     */
    public int updateUserInfoLkByUserNo(UserInfoLk userInfoLk);

    /**
     * 删除领款用户信息
     * 
     * @param id 领款用户信息ID
     * @return 结果
     */
    public int deleteUserInfoLkById(Long id);

    /**
     * 批量删除领款用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserInfoLkByIds(Long[] ids);
    
    public List<Map<String,Object>> selectUserInfoLkByJobNo(String jobNo);
}
