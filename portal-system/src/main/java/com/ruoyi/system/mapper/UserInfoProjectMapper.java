package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.UserInfoProject;
import java.util.List;
import java.util.Map;

/**
 * 项目协同平台用户信息Mapper接口
 * 
 * @author tjec
 * @date 2021-01-28
 */
public interface UserInfoProjectMapper 
{
    /**
     * 查询项目协同平台用户信息
     * 
     * @param guid 项目协同平台用户信息ID
     * @return 项目协同平台用户信息
     */
    public UserInfoProject selectUserInfoProjectByGuid(String guid);

    /**
     * 查询项目协同平台用户信息列表
     * 
     * @param userInfoProject 项目协同平台用户信息
     * @return 项目协同平台用户信息集合
     */
    public List<UserInfoProject> selectUserInfoProjectList(UserInfoProject userInfoProject);

    /**
     * 新增项目协同平台用户信息
     * 
     * @param userInfoProject 项目协同平台用户信息
     * @return 结果
     */
    public int insertUserInfoProject(UserInfoProject userInfoProject);

    /**
     * 修改项目协同平台用户信息
     * 
     * @param userInfoProject 项目协同平台用户信息
     * @return 结果
     */
    public int updateUserInfoProject(UserInfoProject userInfoProject);

    /**
     * 删除项目协同平台用户信息
     * 
     * @param guid 项目协同平台用户信息ID
     * @return 结果
     */
    public int deleteUserInfoProjectByGuid(String guid);

    /**
     * 批量删除项目协同平台用户信息
     * 
     * @param guids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserInfoProjectByGuids(String[] guids);
    
    public void cleanProjectUser();
    
    public void cleanProjectUserOutMember();
    
    public List<Map<String,Object>> selectUserInfoProjectByJobNo(String jobNo);
}
