package com.ruoyi.system.mapper;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.ruoyi.common.core.domain.entity.SysUser;

/**
 * 用户表 数据层
 * 
 * @author ruoyi
 */
public interface SysUserMapper
{
    /**
     * 根据条件分页查询用户列表
     * 
     * @param sysUser 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUserList(SysUser sysUser);
    
    public List<SysUser> userListSearch(SysUser sysUser);



    public List<SysUser> selectAllUserList(SysUser sysUser);
    /**
     * 通过用户名查询用户
     * 
     * @param userName 用户名
     * @return 用户对象信息
     */
    public SysUser selectUserByUserName(String userName);

    public List<String> selectUserByNickName(String nickName);

    public SysUser selectUserByUserNo(String userNo);
    /**
     * 通过用户ID查询用户
     * 
     * @param Id 用户ID
     * @return 用户对象信息
     */
    public SysUser selectUserById(Long Id);



    public SysUser selectUserByUserId(String Id);
    /**
     * 通过用户ID查询用户---同一用户管理使用
     * 
     * @param Id 用户ID
     * @return 用户对象信息
     */
    public SysUser selectUnifyUserById(Long Id);

    /**
     * 通过用户ID查询用户---同一用户查询使用
     *
     * @param Id 用户ID
     * @return 用户对象信息
     */
    public SysUser selectUnifyUserByUserId(Long Id);
    
    public String selectUserNames(List<Long> ids);


    public List<Map<String,Object>> checkedListSearch(@Param("ids")Long[] ids);

    /**
     * 新增用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    public int insertUser(SysUser user);

    /**
     * 修改用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    public int updateUser(SysUser user);

    /**
     * 修改用户头像
     * 
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    public int updateUserAvatar(@Param("userName") String userName, @Param("avatar") String avatar);

    /**
     * 重置用户密码
     * 
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    public int resetUserPwd(@Param("userName") String userName, @Param("password") String password);

    /**
     * 通过用户ID删除用户
     * 
     * @param Id 用户ID
     * @return 结果
     */
    public int deleteUserById(Long Id);

    /**
     * 批量删除用户信息
     * 
     * @param Ids 需要删除的用户ID
     * @return 结果
     */
    public int deleteUserByIds(Long[] Ids);

    /**
     * 校验用户名称是否唯一
     * 
     * @param userName 用户名称
     * @return 结果
     */
    public int checkUserNameUnique(String userName);


    public SysUser checkUserIdUnique(String userID);


    public SysUser checkUserIdCodeUnique(String userIdCode);
    /**
     * 校验手机号码是否唯一
     *
     * @param phonenumber 手机号码
     * @return 结果
     */
    public SysUser checkPhoneUnique(String phonenumber);

    /**
     * 校验email是否唯一
     *
     * @param email 用户邮箱
     * @return 结果
     */
    public SysUser checkEmailUnique(String email);

    /**
     * 通过手机号查询用户
     * @param phonenumber
     * @return
     */
    List<SysUser> selectUserByPhonenumber(String phonenumber);

    /**
     * 根据用户手机号修改密码
     * @param sysUser
     * @return
     */
    public int resetUserPwdByPhonenumber(SysUser sysUser);
    
    /**
     * 根据用户工号获取用户所有系统的信息
     * @param jobNo 工号
     * @return
     */
    public List<Map<String,Object>> getUserInfoAllSystem(String jobNo);
    
    /**
     * 根据手机号获取用户所有系统的信息
     * @param phonenumber 手机号
     * @return
     */
    public List<Map<String,Object>> getUserInfoAllSystemByPhone(String phonenumber);
    
    /**
     * 获取所有用户列表
     * @return
     */
    public List<SysUser> selectAllUser();
    
    public List<Map<String, Object>> findUserByOrgId(Long orgId);
    
    /**
     * 根据用户id查询所有在后台用户集成中匹配的账号
     * @param userId 用户表id
     * @return
     */
    public List<Map<String,Object>> selectIntegerByUserId(Long userId);

    /**
     * 判断强制结束状态大于当前时间的，强制状态为强制开启的，更新强制状态为取消强制
     * forceFlag 1强制开启 2强制关闭 0取消强制
     * @return
     */
    public int updateUserForceFlag();

    /**
     * 判断用户是 主管单位为同济咨询，人员类别为 (G101/G102/G103) 当前时间小于离职时间，但是在职状态是离职，更新状态为离职待关闭
     * status 0启用 1未开启 2离职待关闭 3离职停用
     * timingFlag 定时标志 1 待开启 2 待关闭  3 已处理
     * @return
     */
    public int resigningStatus();

    /**
     *判断用户是 主管单位为同济咨询，人员类别为 (G101/G102/G103) 当前时间大于离职时间 或 离职时间为null，在职状态是离职，更新状态为离职停用
     * status 0启用 1未开启 2离职待关闭 3离职停用
     * timingFlag 定时标志 1 待开启 2 待关闭  3 已处理
     * @return
     */
    public int updateCloseUserAccount();

    /**
     * 更新定时标志
     * @param userNo
     * @param timingFlag
     * @return
     */
    @Deprecated
    public int updateTimingFlag(@Param("userNo")String userNo, @Param("timingFlag") String timingFlag);




    public int updateTimingFlagBatch(@Param("userNoList")List<String> userNoList, @Param("timingFlag") String timingFlag);


    /**
     * 根据 开关毕逻辑结果 更新用户状态
     * @param userNo
     * @param status
     * @param forceFlag
     * @param forceEndDate
     * @return
     */
    public int updateUserStatusByUserNo(@Param("userNo") String userNo,@Param("status")String status,@Param("forceFlag")String forceFlag,@Param("forceEndDate") Date forceEndDate);


    /**
     * 获取定时标志为未处理的用户，
     * @param timingFlag 定时标志 1 待开启 2 待关闭  3 已处理
     * @return 需要关闭用户工号集合
     */
    @Deprecated
    public List<String> getUserStatusTimingFlag(String timingFlag);

    /**
     * 获取门户所有开启账户，可能包括强制关闭
     * @return
     */
    public List<String> getPortalUserOpen();


    /**
     * 获取门户所有关闭账户，包括强制开启
     * @return
     */
    public List<String> getPortalUserClose();

    /**
     * 用户同步bpm状态全部设置为未处理
     */
    public void setUserTimingFlag();
}
