package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.UserInfoXiongan;
import java.util.List;
import java.util.Map;

/**
 * 雄安协同平台用户信息Mapper接口
 * 
 * @author tjec
 * @date 2021-01-28
 */
public interface UserInfoXionganMapper 
{
    /**
     * 查询雄安协同平台用户信息
     * 
     * @param guid 雄安协同平台用户信息ID
     * @return 雄安协同平台用户信息
     */
    public UserInfoXiongan selectUserInfoXionganByGuid(String guid);

    /**
     * 查询雄安协同平台用户信息列表
     * 
     * @param userInfoXiongan 雄安协同平台用户信息
     * @return 雄安协同平台用户信息集合
     */
    public List<UserInfoXiongan> selectUserInfoXionganList(UserInfoXiongan userInfoXiongan);

    /**
     * 新增雄安协同平台用户信息
     * 
     * @param userInfoXiongan 雄安协同平台用户信息
     * @return 结果
     */
    public int insertUserInfoXiongan(UserInfoXiongan userInfoXiongan);

    /**
     * 修改雄安协同平台用户信息
     * 
     * @param userInfoXiongan 雄安协同平台用户信息
     * @return 结果
     */
    public int updateUserInfoXiongan(UserInfoXiongan userInfoXiongan);

    /**
     * 删除雄安协同平台用户信息
     * 
     * @param guid 雄安协同平台用户信息ID
     * @return 结果
     */
    public int deleteUserInfoXionganByGuid(String guid);

    /**
     * 批量删除雄安协同平台用户信息
     * 
     * @param guids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserInfoXionganByGuids(String[] guids);
    
    public void cleanXionganUser();
    
    public void cleanXionganUserOutMember();
    
    public List<Map<String,Object>> selectUserInfoXaByPhone(String phonenumber);
}
