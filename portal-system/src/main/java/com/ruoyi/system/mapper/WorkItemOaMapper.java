package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.WorkItemOa;

/**
 * Oa待办待阅信息Mapper接口
 * 
 * @author tjgc
 * @date 2021-05-12
 */
public interface WorkItemOaMapper 
{
    /**
     * 查询Oa待办待阅信息
     * 
     * @param id Oa待办待阅信息ID
     * @return Oa待办待阅信息
     */
    public WorkItemOa selectWorkItemOaById(Long id);

    /**
     * 查询Oa待办待阅信息列表
     * 
     * @param workItemOa Oa待办待阅信息
     * @return Oa待办待阅信息集合
     */
    public List<WorkItemOa> selectWorkItemOaList(WorkItemOa workItemOa);

    /**
     * 新增Oa待办待阅信息
     * 
     * @param workItemOa Oa待办待阅信息
     * @return 结果
     */
    public int insertWorkItemOa(WorkItemOa workItemOa);

    /**
     * 修改Oa待办待阅信息
     * 
     * @param workItemOa Oa待办待阅信息
     * @return 结果
     */
    public int updateWorkItemOa(WorkItemOa workItemOa);

    /**
     * 更新状态
     * @param workItemOa
     * @return
     */
    public int updateWorkItemOaStatus(WorkItemOa workItemOa);

    /**
     * 删除Oa待办待阅信息
     * 
     * @param id Oa待办待阅信息ID
     * @return 结果
     */
    public int deleteWorkItemOaById(Long id);

    /**
     * 批量删除Oa待办待阅信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWorkItemOaByIds(Long[] ids);
}
