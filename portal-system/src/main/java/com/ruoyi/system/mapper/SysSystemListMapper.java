package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.SysSystemList;
import org.apache.ibatis.annotations.MapKey;

/**
 * 系统清单Mapper接口
 * 
 * @author tjgc
 * @date 2021-03-18
 */
public interface SysSystemListMapper 
{
    /**
     * 查询系统清单
     * 
     * @param id 系统清单ID
     * @return 系统清单
     */
    public SysSystemList selectSysSystemListById(Long id);


    /**
     * 检查系统编码是否重复
     * @param sysNum
     * @return
     */
    public int checkSysSystemNumExist(String sysNum);

    /**
     * 查询系统清单列表
     * 
     * @param sysSystemList 系统清单
     * @return 系统清单集合
     */
    public List<SysSystemList> selectSysSystemListList(SysSystemList sysSystemList);

    /**
     * 导出系统清单列表
     * @param sysSystemList
     * @return
     */
    public List<SysSystemList> selectSysSystemListForExport(SysSystemList sysSystemList);

    @MapKey("sysNum")
    public Map<String, SysSystemList> selectSysSystemMap();

    /**
     * 新增系统清单
     * 
     * @param sysSystemList 系统清单
     * @return 结果
     */
    public int insertSysSystemList(SysSystemList sysSystemList);

    /**
     * 修改系统清单
     * 
     * @param sysSystemList 系统清单
     * @return 结果
     */
    public int updateSysSystemList(SysSystemList sysSystemList);

    /**
     * 删除系统清单
     * 
     * @param id 系统清单ID
     * @return 结果
     */
    public int deleteSysSystemListById(Long id);

    /**
     * 批量删除系统清单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysSystemListByIds(Long[] ids);

    /**
     * 获取可用所有的系统列表
     * @return
     */
    public List<SysSystemList> selectSystemListDictData();
}
