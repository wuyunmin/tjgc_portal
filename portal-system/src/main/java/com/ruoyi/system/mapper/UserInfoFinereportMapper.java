package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.UserInfoFinereport;

/**
 * 帆软用户信息Mapper接口
 * 
 * @author tjgc
 * @date 2021-04-22
 */
public interface UserInfoFinereportMapper 
{
    /**
     * 查询帆软用户信息
     * 
     * @param id 帆软用户信息ID
     * @return 帆软用户信息
     */
    public UserInfoFinereport selectUserInfoFinereportById(Long id);

    /**
     * 查询帆软用户信息列表
     * 
     * @param userInfoFinereport 帆软用户信息
     * @return 帆软用户信息集合
     */
    public List<UserInfoFinereport> selectUserInfoFinereportList(UserInfoFinereport userInfoFinereport);

    /**
     * 新增帆软用户信息
     * 
     * @param userInfoFinereport 帆软用户信息
     * @return 结果
     */
    public int insertUserInfoFinereport(UserInfoFinereport userInfoFinereport);

    /**
     * 修改帆软用户信息
     * 
     * @param userInfoFinereport 帆软用户信息
     * @return 结果
     */
    public int updateUserInfoFinereport(UserInfoFinereport userInfoFinereport);

    /**
     * 删除帆软用户信息
     * 
     * @param id 帆软用户信息ID
     * @return 结果
     */
    public int deleteUserInfoFinereportById(Long id);

    /**
     * 批量删除帆软用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserInfoFinereportByIds(Long[] ids);
    
    List<Map<String, Object>> selectUserInfoFRByJobNo(String jobNo);
}
