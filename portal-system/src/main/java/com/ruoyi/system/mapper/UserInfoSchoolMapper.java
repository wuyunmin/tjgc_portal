package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.UserInfoSchool;
import java.util.List;
import java.util.Map;

/**
 * 企业培训学校用户信息Mapper接口
 * 
 * @author tjec
 * @date 2021-01-28
 */
public interface UserInfoSchoolMapper 
{
    /**
     * 查询企业培训学校用户信息
     * 
     * @param guid 企业培训学校用户信息ID
     * @return 企业培训学校用户信息
     */
    public UserInfoSchool selectUserInfoSchoolByGuid(String guid);

    /**
     * 查询企业培训学校用户信息列表
     * 
     * @param userInfoSchool 企业培训学校用户信息
     * @return 企业培训学校用户信息集合
     */
    public List<UserInfoSchool> selectUserInfoSchoolList(UserInfoSchool userInfoSchool);

    /**
     * 新增企业培训学校用户信息
     * 
     * @param userInfoSchool 企业培训学校用户信息
     * @return 结果
     */
    public int insertUserInfoSchool(UserInfoSchool userInfoSchool);

    /**
     * 修改企业培训学校用户信息
     * 
     * @param userInfoSchool 企业培训学校用户信息
     * @return 结果
     */
    public int updateUserInfoSchool(UserInfoSchool userInfoSchool);

    /**
     * 删除企业培训学校用户信息
     * 
     * @param guid 企业培训学校用户信息ID
     * @return 结果
     */
    public int deleteUserInfoSchoolByGuid(String guid);

    /**
     * 批量删除企业培训学校用户信息
     * 
     * @param guids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserInfoSchoolByGuids(String[] guids);
    
    public void cleanSchoolUser();
    
    public List<Map<String,Object>> selectUserInfoSchoolByJobNo(String jobNo);
}
