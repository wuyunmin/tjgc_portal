package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SyncSysDept;

/**
 * 部门同步中间Mapper接口
 * 
 * @author tjgc
 * @date 2021-01-20
 */
public interface SyncSysDeptMapper 
{
    /**
     * 查询部门同步中间列表
     * 
     * @param syncSysDept 部门同步中间
     * @return 部门同步中间集合
     */
    public List<SyncSysDept> selectSyncSysDeptList(SyncSysDept syncSysDept);

    /**
     * 新增部门同步中间
     * 
     * @param syncSysDept 部门同步中间
     * @return 结果
     */
    public int insertSyncSysDept(SyncSysDept syncSysDept);

    /**
     * 修改部门同步中间
     * 
     * @param syncSysDept 部门同步中间
     * @return 结果
     */
    public int updateSyncSysDept(SyncSysDept syncSysDept);


    public int existSyncSysDept(String deptIdCode);
    /**
     * 删除部门同步中间
     * 
     * @param id 部门同步中间ID
     * @return 结果
     */
    public int deleteSyncSysDeptById(Long id);

    /**
     * 批量删除部门同步中间
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSyncSysDeptByIds(Long[] ids);

    public SyncSysDept selectDeptByDeptId(String id);
}
