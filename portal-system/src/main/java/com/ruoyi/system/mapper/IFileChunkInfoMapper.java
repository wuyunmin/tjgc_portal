package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.IFileChunkInfo;

/**
 * 分片上传单片信息Mapper接口
 * 
 * @author tjgc
 * @date 2021-03-15
 */
public interface IFileChunkInfoMapper
{
    /**
     * 查询分片上传单片信息
     * 
     * @param id 分片上传单片信息ID
     * @return 分片上传单片信息
     */
    public IFileChunkInfo selectAsyncChunkInfoById(String id);

    /**
     * 查询分片上传单片信息列表
     * 
     * @param IFileChunkInfo 分片上传单片信息
     * @return 分片上传单片信息集合
     */
    public List<IFileChunkInfo> selectAsyncChunkInfoList(IFileChunkInfo IFileChunkInfo);


    public List<Integer> selectAsyncChunkNumList(IFileChunkInfo IFileChunkInfo);

    /**
     * 新增分片上传单片信息
     * 
     * @param IFileChunkInfo 分片上传单片信息
     * @return 结果
     */
    public int insertAsyncChunkInfo(IFileChunkInfo IFileChunkInfo);

    /**
     * 修改分片上传单片信息
     * 
     * @param IFileChunkInfo 分片上传单片信息
     * @return 结果
     */
    public int updateAsyncChunkInfo(IFileChunkInfo IFileChunkInfo);

    /**
     * 删除分片上传单片信息
     * 
     * @param id 分片上传单片信息ID
     * @return 结果
     */
    public int deleteAsyncChunkInfoById(String id);

    /**
     * 批量删除分片上传单片信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAsyncChunkInfoByIds(String[] ids);

    /**
     * 删除文件
     * @param IFileChunkInfo
     * @return
     */
    public int  deleteAsyncChunkInfoByFile(IFileChunkInfo IFileChunkInfo);
}
