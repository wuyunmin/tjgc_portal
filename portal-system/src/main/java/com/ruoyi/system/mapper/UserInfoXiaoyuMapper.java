package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.UserInfoXiaoyu;
import java.util.List;
import java.util.Map;

/**
 * 小鱼视频用户信息Mapper接口
 * 
 * @author tjec
 * @date 2021-01-28
 */
public interface UserInfoXiaoyuMapper 
{
    /**
     * 查询小鱼视频用户信息
     * 
     * @param guid 小鱼视频用户信息ID
     * @return 小鱼视频用户信息
     */
    public UserInfoXiaoyu selectUserInfoXiaoyuByGuid(String guid);

    /**
     * 查询小鱼视频用户信息列表
     * 
     * @param userInfoXiaoyu 小鱼视频用户信息
     * @return 小鱼视频用户信息集合
     */
    public List<UserInfoXiaoyu> selectUserInfoXiaoyuList(UserInfoXiaoyu userInfoXiaoyu);

    /**
     * 新增小鱼视频用户信息
     * 
     * @param userInfoXiaoyu 小鱼视频用户信息
     * @return 结果
     */
    public int insertUserInfoXiaoyu(UserInfoXiaoyu userInfoXiaoyu);

    /**
     * 修改小鱼视频用户信息
     * 
     * @param userInfoXiaoyu 小鱼视频用户信息
     * @return 结果
     */
    public int updateUserInfoXiaoyu(UserInfoXiaoyu userInfoXiaoyu);

    /**
     * 删除小鱼视频用户信息
     * 
     * @param guid 小鱼视频用户信息ID
     * @return 结果
     */
    public int deleteUserInfoXiaoyuByGuid(String guid);

    /**
     * 批量删除小鱼视频用户信息
     * 
     * @param guids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserInfoXiaoyuByGuids(String[] guids);
    
    /**
     * 清空小鱼视频用户信息
     */
    public void cleanXiaoyuUser();
    
    public List<Map<String,Object>> selectUserInfoXiaoyuByPhone(String phonenumber);
}
