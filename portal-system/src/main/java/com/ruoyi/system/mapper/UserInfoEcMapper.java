package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.UserInfoEc;

/**
 * 领款用户信息Mapper接口
 * 
 * @author tjgc
 * @date 2021-01-07
 */
public interface UserInfoEcMapper 
{
    /**
     * 查询领款用户信息
     * 
     * @param id 领款用户信息ID
     * @return 领款用户信息
     */
    public UserInfoEc selectUserInfoEcById(Long id);

    /**
     * 查询领款用户信息列表
     * 
     * @param userInfoEc 领款用户信息
     * @return 领款用户信息集合
     */
    public List<UserInfoEc> selectUserInfoEcList(UserInfoEc userInfoEc);

    /***
     * 查询单个用户
     * @param userNo
     * @return
     */
    public UserInfoEc selectUserInfoEcByOne(String userNo);

    /**
     * 新增领款用户信息
     * 
     * @param userInfoEc 领款用户信息
     * @return 结果
     */
    public int insertUserInfoEc(UserInfoEc userInfoEc);


    /**
     * 修改领款用户信息
     * 
     * @param userInfoEc 领款用户信息
     * @return 结果
     */
    public int updateUserInfoEc(UserInfoEc userInfoEc);


    /**
     * 修改领款用户信息
     *
     * @param userInfoEc 领款用户信息
     * @return 结果
     */
    public int updateUserInfoEcByUserNo(UserInfoEc userInfoEc);

    /**
     * 删除领款用户信息
     * 
     * @param id 领款用户信息ID
     * @return 结果
     */
    public int deleteUserInfoEcById(Long id);

    /**
     * 批量删除领款用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserInfoEcByIds(Long[] ids);
    
    public List<Map<String,Object>> selectUserInfoEcByJobNo(String jobNo);
}
