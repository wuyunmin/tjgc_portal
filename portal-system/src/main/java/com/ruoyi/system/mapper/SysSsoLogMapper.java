package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysSsoLog;

/**
 * SSO登陆日志Mapper接口
 * 
 * @author tjgc
 * @date 2020-12-15
 */
public interface SysSsoLogMapper 
{
    /**
     * 查询SSO登陆日志
     * 
     * @param id SSO登陆日志ID
     * @return SSO登陆日志
     */
    public SysSsoLog selectSysSsoLogById(String id);

    /**
     * 查询SSO登陆日志列表
     * 
     * @param sysSsoLog SSO登陆日志
     * @return SSO登陆日志集合
     */
    public List<SysSsoLog> selectSysSsoLogList(SysSsoLog sysSsoLog);

    /**
     * 新增SSO登陆日志
     * 
     * @param sysSsoLog SSO登陆日志
     * @return 结果
     */
    public int insertSysSsoLog(SysSsoLog sysSsoLog);

    /**
     * 修改SSO登陆日志
     * 
     * @param sysSsoLog SSO登陆日志
     * @return 结果
     */
    public int updateSysSsoLog(SysSsoLog sysSsoLog);

    /**
     * 删除SSO登陆日志
     * 
     * @param id SSO登陆日志ID
     * @return 结果
     */
    public int deleteSysSsoLogById(String id);

    /**
     * 批量删除SSO登陆日志
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysSsoLogByIds(String[] ids);
}
