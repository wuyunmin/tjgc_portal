package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysIntserv;

/**
 * 综合服务配合Mapper接口
 * 
 * @author tjgc
 * @date 2020-12-29
 */
public interface SysIntservMapper 
{
    /**
     * 查询综合服务配合
     * 
     * @param id 综合服务配合ID
     * @return 综合服务配合
     */
    public SysIntserv selectSysIntservById(Long id);

    /**
     * 查询综合服务配合列表
     * 
     * @param sysIntserv 综合服务配合
     * @return 综合服务配合集合
     */
    public List<SysIntserv> selectSysIntservList(SysIntserv sysIntserv);


    /**
     * 查询可用链接集合
     * @return 可用链接集合
     */
    public List<SysIntserv> selectLinkList();

    /**
     * 查询可用微信二维码集合
     * @return 可用微信二维码集合
     */
    public List<SysIntserv> selectWeChatList();
    /**
     * 新增综合服务配合
     * 
     * @param sysIntserv 综合服务配合
     * @return 结果
     */
    public int insertSysIntserv(SysIntserv sysIntserv);

    /**
     * 修改综合服务配合
     * 
     * @param sysIntserv 综合服务配合
     * @return 结果
     */
    public int updateSysIntserv(SysIntserv sysIntserv);

    /**
     * 删除综合服务配合
     * 
     * @param id 综合服务配合ID
     * @return 结果
     */
    public int deleteSysIntservById(Long id);

    /**
     * 批量删除综合服务配合
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysIntservByIds(Long[] ids);
}
