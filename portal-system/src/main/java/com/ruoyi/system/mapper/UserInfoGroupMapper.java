package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.UserInfoGroup;
import org.apache.ibatis.annotations.Param;

/**
 * 域用户信息Mapper接口
 * 
 * @author tjgc
 * @date 2021-04-15
 */
public interface UserInfoGroupMapper 
{
    /**
     * 查询域用户信息
     * 
     * @param id 域用户信息ID
     * @return 域用户信息
     */
    public UserInfoGroup selectUserInfoGroupById(String id);

    /**
     * 查询域用户信息列表
     * 
     * @param userInfoGroup 域用户信息
     * @return 域用户信息集合
     */
    public List<UserInfoGroup> selectUserInfoGroupList(UserInfoGroup userInfoGroup);




    /**
     * 新增域用户信息
     * 
     * @param userInfoGroup 域用户信息
     * @return 结果
     */
    public int insertUserInfoGroup(UserInfoGroup userInfoGroup);



    public List<Map<String,Object>> selectUserInfoByGroupHolder(@Param("userId")Long userId);
    /**
     * 修改域用户信息
     * 
     * @param userInfoGroup 域用户信息
     * @return 结果
     */
    public int updateUserInfoGroup(UserInfoGroup userInfoGroup);

    public int selectCountUserInfoGroup(@Param("userNo")String userNo);

    public int updateUserInfoGroupByImport(UserInfoGroup userInfoGroup);

    /**
     * 删除域用户信息
     * 
     * @param id 域用户信息ID
     * @return 结果
     */
    public int deleteUserInfoGroupById(String id);

    /**
     * 批量删除域用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserInfoGroupByIds(String[] ids);


    public void cleanGroupUser();
    
    List<Map<String, Object>> selectUserInfoYYHByJobNo(String jobNo);
}
