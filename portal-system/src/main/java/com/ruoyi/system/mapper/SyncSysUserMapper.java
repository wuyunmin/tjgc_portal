package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.SyncSysTemp;
import com.ruoyi.system.domain.SyncSysUser;
import org.apache.ibatis.annotations.Param;

/**
 * 部门同步中间Mapper接口
 *
 * @author tjgc
 * @date 2021-01-20
 */
public interface SyncSysUserMapper
{
    /**
     * 查询部门同步中间列表
     *
     * @param syncSysUser 部门同步中间
     * @return 部门同步中间集合
     */
    public List<SyncSysUser> selectSyncSysUserList(SyncSysUser syncSysUser);


    /**
     * 查询用户信息
     * @param syncSysUser
     * @return
     */
    public List<SyncSysUser> selectSyncSysUserListForHR(SyncSysUser syncSysUser);

    /**
     * 新增部门同步中间
     *
     * @param syncSysUser 部门同步中间
     * @return 结果
     */
    public int insertSyncSysUser(SyncSysUser syncSysUser);


    public SyncSysUser selectUserInfoHrById(Long id);

    /**
     * 修改部门同步中间
     *
     * @param syncSysUser 部门同步中间
     * @return 结果
     */
    public int updateSyncSysUser(SyncSysUser syncSysUser);

    /**
     * 修改人员状态
     *
     * @param userStatus 人员状态状态
     * @param empNumber 工号
     * @return 结果
     */
    public int updateSyncSysUserStatusByEmpIdCode(@Param("userStatus")String userStatus, @Param("empNumber")String empNumber);

    /**
     * 查询用户信息是否已存在
     * @param empId 员工id
     * @return 数量
     */
    public int existSyncSysUser(String  empId);

    /**
     * 删除部门同步中间
     *
     * @param id 部门同步中间ID
     * @return 结果
     */
    public int deleteSyncSysUserById(Long id);

    /**
     * 批量删除部门同步中间
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSyncSysUserByIds(Long[] ids);

    public List<SyncSysTemp> selectSyncTempList();


    public int updataSyncTempStatus(String Id);


    public SyncSysUser selectUserByUserID (String id);

    List<Map<String, Object>> selectUserInfoHRByJobNo(String jobNo);
}
