package com.ruoyi.system.domain;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;
import java.util.List;

/**
 * 企业邮箱用户信息对象 user_info_email
 * 
 * @author tjec
 * @date 2021-01-28
 */
public class UserInfoEmail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String guid;

    /** 系统名称 */
    @Excel(name = "系统名称")
    private String systemName;

    /** 编号 */
    @Excel(name = "编号")
    private String userNo;

    /** 姓名 */
    @Excel(name = "姓名")
    private String userName;

    /** 电子邮件 */
    @Excel(name = "电子邮件")
    private String email;

    /** 邮箱持有人 */
    @Excel(name = "邮箱持有人")
    private String emailHolder;
    
    private String emailHolderText;

    private List<String> queryEmailHolders;

    /** 绑定微信 */
    @Excel(name = "绑定微信")
    private String bindWechat;
    
    /** 状态 */
    @Excel(name = "状态")
    private String userStatus;
    
    @Excel(name = "邮箱类型",dictType = "email_type")
    private String flag;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date openTime;
    private String openTimeStart;
    /* 计划开始时间止，查询用 */
    private String openTimeEnd;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date cancelTime;
    private String cancelTimeStart;
    /* 计划开始时间止，查询用 */
    private String cancelTimeEnd;

    private String handlePerson;

    private String mailHandover;

    private  String dateType;

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    public String getOpenTimeStart() {
        return openTimeStart;
    }

    public void setOpenTimeStart(String openTimeStart) {
        this.openTimeStart = openTimeStart;
    }

    public String getOpenTimeEnd() {
        return openTimeEnd;
    }

    public void setOpenTimeEnd(String openTimeEnd) {
        this.openTimeEnd = openTimeEnd;
    }

    public String getCancelTimeStart() {
        return cancelTimeStart;
    }

    public void setCancelTimeStart(String cancelTimeStart) {
        this.cancelTimeStart = cancelTimeStart;
    }

    public String getCancelTimeEnd() {
        return cancelTimeEnd;
    }

    public void setCancelTimeEnd(String cancelTimeEnd) {
        this.cancelTimeEnd = cancelTimeEnd;
    }

    public String getHandlePerson() {
        return handlePerson;
    }

    public void setHandlePerson(String handlePerson) {
        this.handlePerson = handlePerson;
    }

    public String getMailHandover() {
        return mailHandover;
    }

    public void setMailHandover(String mailHandover) {
        this.mailHandover = mailHandover;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public Date getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(Date cancelTime) {
        this.cancelTime = cancelTime;
    }

    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getGuid() 
    {
        return guid;
    }
    public void setSystemName(String systemName) 
    {
        this.systemName = systemName;
    }

    public String getSystemName() 
    {
        return systemName;
    }
    public void setUserNo(String userNo) 
    {
        this.userNo = userNo;
    }

    public String getUserNo() 
    {
        return userNo;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    
    public void setBindWechat(String bindWechat) 
    {
        this.bindWechat = bindWechat;
    }

    public String getBindWechat() 
    {
        return bindWechat;
    }
    public void setUserStatus(String userStatus) 
    {
        this.userStatus = userStatus;
    }

    public String getUserStatus() 
    {
        return userStatus;
    }
    

    public String getEmailHolder() {
		return emailHolder;
	}

	public void setEmailHolder(String emailHolder) {
		this.emailHolder = emailHolder;
	}

    public List<String> getQueryEmailHolder() {
        return queryEmailHolders;
    }

    public void setQueryEmailHolder(List<String> queryEmailHolders) {
        this.queryEmailHolders = queryEmailHolders;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("guid", getGuid())
            .append("systemName", getSystemName())
            .append("userNo", getUserNo())
            .append("userName", getUserName())
            .append("email", getEmail())
            .append("bindWechat", getBindWechat())
            .append("userStatus", getUserStatus())
            .append("emailHolder",getEmailHolder())
            .append("flag",getFlag())
            .append("emailHolderText",getEmailHolderText())
            .toString();
    }

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getEmailHolderText() {
		return emailHolderText;
	}

	public void setEmailHolderText(String emailHolderText) {
		this.emailHolderText = emailHolderText;
	}
}
