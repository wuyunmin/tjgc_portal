package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 小鱼视频用户信息对象 user_info_xiaoyu
 *
 * @author tjec
 * @date 2021-01-28
 */
public class UserInfoXiaoyu extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String guid;

    /** 系统名称 */
    private String systemName;

    /** 姓名 */
    @Excel(name = "姓名")
    private String userName;

    /** 手机号 */
    @Excel(name = "手机号")
    private String cellPhone;

    /** 个人云会议室 */
    @Excel(name = "个人云会议室")
    private String mettingRoom;

    /** 部门 */
    @Excel(name = "部门")
    private String dept;

    private String portalUserName;

    private String portalUserNo;

    //账号代持人
    private String holderNames;


    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getGuid()
    {
        return guid;
    }
    public void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }

    public String getSystemName()
    {
        return systemName;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setCellPhone(String cellPhone)
    {
        this.cellPhone = cellPhone;
    }

    public String getCellPhone()
    {
        return cellPhone;
    }
    public void setMettingRoom(String mettingRoom)
    {
        this.mettingRoom = mettingRoom;
    }

    public String getMettingRoom()
    {
        return mettingRoom;
    }
    public void setDept(String dept)
    {
        this.dept = dept;
    }

    public String getDept()
    {
        return dept;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("guid", getGuid())
            .append("systemName", getSystemName())
            .append("userName", getUserName())
            .append("cellPhone", getCellPhone())
            .append("mettingRoom", getMettingRoom())
            .append("dept", getDept())
            .append("portalUserName",getPortalUserName())
            .append("portalUserNo",getPortalUserNo())
            .append("holderNames",getHolderNames())
            .toString();
    }

	public String getPortalUserName() {
		return portalUserName;
	}

	public void setPortalUserName(String portalUserName) {
		this.portalUserName = portalUserName;
	}

	public String getPortalUserNo() {
		return portalUserNo;
	}

	public void setPortalUserNo(String portalUserNo) {
		this.portalUserNo = portalUserNo;
	}

	public String getHolderNames() {
		return holderNames;
	}

	public void setHolderNames(String holderNames) {
		this.holderNames = holderNames;
	}
}
