package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 项目协同平台用户信息对象 user_info_project
 * 
 * @author tjec
 * @date 2021-01-28
 */
public class UserInfoProject extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String guid;

    /** 系统名称 */
    private String systemName;

    /** 用户标识(0:外部用户，1:内部用户) */
    private String userFlag;

    /** 账号 */
    @Excel(name = "账号")
    private String loginName;

    /** 姓名 */
    @Excel(name = "姓名")
    private String userName;

    /** 员工编号 */
    @Excel(name = "员工编号")
    private String userNo;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String cellPhone;
    
    private String holderNames;

    public void setGuid(String guid) 
    {
        this.guid = guid;
    }

    public String getGuid() 
    {
        return guid;
    }
    public void setSystemName(String systemName) 
    {
        this.systemName = systemName;
    }

    public String getSystemName() 
    {
        return systemName;
    }
    public void setUserFlag(String userFlag) 
    {
        this.userFlag = userFlag;
    }

    public String getUserFlag() 
    {
        return userFlag;
    }
    public void setLoginName(String loginName) 
    {
        this.loginName = loginName;
    }

    public String getLoginName() 
    {
        return loginName;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setUserNo(String userNo) 
    {
        this.userNo = userNo;
    }

    public String getUserNo() 
    {
        return userNo;
    }
    public void setCellPhone(String cellPhone) 
    {
        this.cellPhone = cellPhone;
    }

    public String getCellPhone() 
    {
        return cellPhone;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("guid", getGuid())
            .append("systemName", getSystemName())
            .append("userFlag", getUserFlag())
            .append("loginName", getLoginName())
            .append("userName", getUserName())
            .append("userNo", getUserNo())
            .append("cellPhone", getCellPhone())
            .append("holderNames",getHolderNames())
            .toString();
    }

	public String getHolderNames() {
		return holderNames;
	}

	public void setHolderNames(String holderNames) {
		this.holderNames = holderNames;
	}
}
