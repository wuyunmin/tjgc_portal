package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 钉钉用户信息对象 user_info_ding
 *
 * @author tjgc
 * @date 2021-04-21
 */
public class UserInfoDing extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    private Long id;

    /** 系统名称 */
//    @Excel(name = "系统名称")
    private String systemName;

    /** 用户工号 */
    @Excel(name = "工号")
    private String userNo;

    /** 用户名称 */
    @Excel(name = "姓名")
    private String nickName;

    /** 手机号 */
    @Excel(name = "手机号")
    private String mobile;

    /** 部门名称 */
    @Excel(name = "部门")
    private String deptName;

    /** 是否主管 */
    @Excel(name = "是否主管(是/否)")
    private String deptManage;

    /** 激活状态 */
    @Excel(name = "激活状态")
    private String active;

    /** 角色 */
    @Excel(name = "角色")
    private String roleList;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 允许标志（0代表允许 1代表不允许） */
//    @Excel(name = "允许标志", readConverterExp = "0=代表允许,1=代表不允许")
    private String authFlag;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }

    public String getSystemName()
    {
        return systemName;
    }
    public void setUserNo(String userNo)
    {
        this.userNo = userNo;
    }

    public String getUserNo()
    {
        return userNo;
    }
    public void setNickName(String nickName)
    {
        this.nickName = nickName;
    }

    public String getNickName()
    {
        return nickName;
    }
    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getMobile()
    {
        return mobile;
    }
    public void setDeptName(String deptName)
    {
        this.deptName = deptName;
    }

    public String getDeptName()
    {
        return deptName;
    }
    public void setDeptManage(String deptManage)
    {
        this.deptManage = deptManage;
    }

    public String getDeptManage()
    {
        return deptManage;
    }
    public void setActive(String active)
    {
        this.active = active;
    }

    public String getActive()
    {
        return active;
    }
    public void setRoleList(String roleList)
    {
        this.roleList = roleList;
    }

    public String getRoleList()
    {
        return roleList;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }
    public void setAuthFlag(String authFlag)
    {
        this.authFlag = authFlag;
    }

    public String getAuthFlag()
    {
        return authFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("systemName", getSystemName())
            .append("userNo", getUserNo())
            .append("nickName", getNickName())
            .append("mobile", getMobile())
            .append("deptName", getDeptName())
            .append("deptManage", getDeptManage())
            .append("active", getActive())
            .append("roleList", getRoleList())
            .append("delFlag", getDelFlag())
            .append("authFlag", getAuthFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
