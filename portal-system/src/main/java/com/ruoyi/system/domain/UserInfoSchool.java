package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 企业培训学校用户信息对象 user_info_school
 *
 * @author tjec
 * @date 2021-01-28
 */
public class UserInfoSchool extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String guid;

    /** 系统名称 */
    private String systemName;

    /** 学员名称 */
    @Excel(name = "学员名称")
    private String userName;

    /** 员工号 */
    @Excel(name = "员工编号")
    private String userNo;

    //代持人姓名
    private String holderNames;

    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getGuid()
    {
        return guid;
    }
    public void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }

    public String getSystemName()
    {
        return systemName;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setUserNo(String userNo)
    {
        this.userNo = userNo;
    }

    public String getUserNo()
    {
        return userNo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("guid", getGuid())
            .append("systemName", getSystemName())
            .append("userName", getUserName())
            .append("userNo", getUserNo())
            .append("holderNames",getHolderNames())
            .toString();
    }

	public String getHolderNames() {
		return holderNames;
	}

	public void setHolderNames(String holderNames) {
		this.holderNames = holderNames;
	}
}
