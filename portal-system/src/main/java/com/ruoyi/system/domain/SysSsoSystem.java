package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * SSO应用系统对象 sys_sso_system
 * 
 * @author tjgc
 * @date 2020-12-15
 */
public class SysSsoSystem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 创建人 */
    @Excel(name = "创建人")
    private String creater;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdtime;

    /** 修改人 */
    @Excel(name = "修改人")
    private String modifier;

    /** 修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date modifiedtime;

    /** 系统名称 */
    @Excel(name = "系统名称")
    private String sysName;

    /** 系统编码 */
    @Excel(name = "系统编码")
    private String sysCode;

    /** 系统密钥 */
    @Excel(name = "系统密钥")
    private String sysSecret;

    /** 系统登陆回调地址 */
    @Excel(name = "系统登陆回调地址")
    private String sysUrl;

    /** 系统描述 */
    @Excel(name = "系统描述")
    private String sysDescribe;

    /** 系统注销回调地址 */
    @Excel(name = "系统注销回调地址")
    private String sysLogouturl;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setCreater(String creater) 
    {
        this.creater = creater;
    }

    public String getCreater() 
    {
        return creater;
    }
    public void setCreatedtime(Date createdtime) 
    {
        this.createdtime = createdtime;
    }

    public Date getCreatedtime() 
    {
        return createdtime;
    }
    public void setModifier(String modifier) 
    {
        this.modifier = modifier;
    }

    public String getModifier() 
    {
        return modifier;
    }
    public void setModifiedtime(Date modifiedtime) 
    {
        this.modifiedtime = modifiedtime;
    }

    public Date getModifiedtime() 
    {
        return modifiedtime;
    }
    public void setSysName(String sysName) 
    {
        this.sysName = sysName;
    }

    public String getSysName() 
    {
        return sysName;
    }
    public void setSysCode(String sysCode) 
    {
        this.sysCode = sysCode;
    }

    public String getSysCode() 
    {
        return sysCode;
    }
    public void setSysSecret(String sysSecret) 
    {
        this.sysSecret = sysSecret;
    }

    public String getSysSecret() 
    {
        return sysSecret;
    }
    public void setSysUrl(String sysUrl) 
    {
        this.sysUrl = sysUrl;
    }

    public String getSysUrl() 
    {
        return sysUrl;
    }
    public void setSysDescribe(String sysDescribe) 
    {
        this.sysDescribe = sysDescribe;
    }

    public String getSysDescribe() 
    {
        return sysDescribe;
    }
    public void setSysLogouturl(String sysLogouturl) 
    {
        this.sysLogouturl = sysLogouturl;
    }

    public String getSysLogouturl() 
    {
        return sysLogouturl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("creater", getCreater())
            .append("createdtime", getCreatedtime())
            .append("modifier", getModifier())
            .append("modifiedtime", getModifiedtime())
            .append("sysName", getSysName())
            .append("sysCode", getSysCode())
            .append("sysSecret", getSysSecret())
            .append("sysUrl", getSysUrl())
            .append("sysDescribe", getSysDescribe())
            .append("sysLogouturl", getSysLogouturl())
            .toString();
    }
}
