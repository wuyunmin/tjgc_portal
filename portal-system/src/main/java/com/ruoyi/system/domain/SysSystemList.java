package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 系统清单对象 sys_system_list
 * 
 * @author tjgc
 * @date 2021-03-18
 */
public class SysSystemList extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 系统编码 */
    @Excel(name = "系统编码")
    private String sysNum;

    /** 系统名称 */
    @Excel(name = "系统名称")
    private String sysName;

    /** 系统有效性 */
    @Excel(name = "系统有效性")
    private String valid;

    /** 系统启用时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "系统启用时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date enableTime;

    /** 系统停用时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "系统停用时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date disableTime;

    /** 是否允许代持账号 */
    @Excel(name = "是否允许代持账号")
    private String allowShare;

    /** 支持门户登陆 */
    @Excel(name = "支持门户登陆")
    private String portalLogin;

    /** 集成用户方式 */
    @Excel(name = "集成用户方式")
    private String integraType;

    /** 集成用户方式 */
    @Excel(name = "排序")
    private String fieldSort;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSysNum(String sysNum) 
    {
        this.sysNum = sysNum;
    }

    public String getSysNum() 
    {
        return sysNum;
    }
    public void setSysName(String sysName) 
    {
        this.sysName = sysName;
    }

    public String getSysName() 
    {
        return sysName;
    }
    public void setValid(String valid) 
    {
        this.valid = valid;
    }

    public String getValid() 
    {
        return valid;
    }
    public void setEnableTime(Date enableTime) 
    {
        this.enableTime = enableTime;
    }

    public Date getEnableTime() 
    {
        return enableTime;
    }
    public void setDisableTime(Date disableTime) 
    {
        this.disableTime = disableTime;
    }

    public Date getDisableTime() 
    {
        return disableTime;
    }
    public void setAllowShare(String allowShare) 
    {
        this.allowShare = allowShare;
    }

    public String getAllowShare() 
    {
        return allowShare;
    }
    public void setPortalLogin(String portalLogin) 
    {
        this.portalLogin = portalLogin;
    }

    public String getPortalLogin() 
    {
        return portalLogin;
    }
    public void setIntegraType(String integraType) 
    {
        this.integraType = integraType;
    }

    public String getIntegraType() 
    {
        return integraType;
    }

    public String getFieldSort() {
        return fieldSort;
    }

    public void setFieldSort(String fieldSort) {
        this.fieldSort = fieldSort;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("sysNum", getSysNum())
                .append("sysName", getSysName())
                .append("valid", getValid())
                .append("enableTime", getEnableTime())
                .append("disableTime", getDisableTime())
                .append("allowShare", getAllowShare())
                .append("portalLogin", getPortalLogin())
                .append("integraType", getIntegraType())
                .append("fieldSort", getFieldSort())
                .append("remark", getRemark())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
