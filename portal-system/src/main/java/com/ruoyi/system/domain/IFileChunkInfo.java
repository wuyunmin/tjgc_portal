package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.springframework.web.multipart.MultipartFile;

/**
 * 分片上传单片信息对象 async_chunk_info
 * 
 * @author tjgc
 * @date 2021-03-15
 */
public class IFileChunkInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String id;

    /** 当前文件块（从1开始） */
    @Excel(name = "当前文件块", readConverterExp = "从=1开始")
    private Long chunkNumber;

    /** 每块大小 */
    @Excel(name = "每块大小")
    private Long chunkSize;

    /** 当前文件块，每块大小 */
    @Excel(name = "当前文件块，每块大小")
    private Long currentChunksize;

    /** 文件标识 */
    @Excel(name = "文件标识")
    private String identifier;

    /** 文件名 */
    @Excel(name = "文件名")
    private String filename;

    /** 文件相对路径 */
    @Excel(name = "文件相对路径")
    private String relativePath;

    /** 文件总块数 */
    @Excel(name = "文件总块数")
    private Long totalChunks;

    /** 文件类型 */
    @Excel(name = "文件类型")
    private Long type;

    /**
     * 块内容
     */
    private transient MultipartFile upfile;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setChunkNumber(Long chunkNumber) 
    {
        this.chunkNumber = chunkNumber;
    }

    public Long getChunkNumber() 
    {
        return chunkNumber;
    }
    public void setChunkSize(Long chunkSize) 
    {
        this.chunkSize = chunkSize;
    }

    public Long getChunkSize() 
    {
        return chunkSize;
    }
    public void setCurrentChunksize(Long currentChunksize) 
    {
        this.currentChunksize = currentChunksize;
    }

    public Long getCurrentChunksize() 
    {
        return currentChunksize;
    }
    public void setIdentifier(String identifier) 
    {
        this.identifier = identifier;
    }

    public String getIdentifier() 
    {
        return identifier;
    }
    public void setFilename(String filename) 
    {
        this.filename = filename;
    }

    public String getFilename() 
    {
        return filename;
    }
    public void setRelativePath(String relativePath) 
    {
        this.relativePath = relativePath;
    }

    public String getRelativePath() 
    {
        return relativePath;
    }
    public void setTotalChunks(Long totalChunks) 
    {
        this.totalChunks = totalChunks;
    }

    public Long getTotalChunks() 
    {
        return totalChunks;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }

    public MultipartFile getUpfile() {
        return upfile;
    }

    public void setUpfile(MultipartFile upfile) {
        this.upfile = upfile;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("chunkNumber", getChunkNumber())
            .append("chunkSize", getChunkSize())
            .append("currentChunksize", getCurrentChunksize())
            .append("identifier", getIdentifier())
            .append("filename", getFilename())
            .append("relativePath", getRelativePath())
            .append("totalChunks", getTotalChunks())
            .append("type", getType())
            .toString();
    }
}
