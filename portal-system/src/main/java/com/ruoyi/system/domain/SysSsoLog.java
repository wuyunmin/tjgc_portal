package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * SSO登陆日志对象 sys_sso_log
 * 
 * @author tjgc
 * @date 2020-12-15
 */
public class SysSsoLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 系统编码 */
    @Excel(name = "系统编码")
    private String sysCode;

    /** 系统登陆用户 */
    @Excel(name = "系统登陆用户")
    private String sysLoginuser;

    /** 系统登出时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "系统登出时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date sysLogouttime;

    /** 系统登陆时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "系统登陆时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date sysLogintime;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setSysCode(String sysCode) 
    {
        this.sysCode = sysCode;
    }

    public String getSysCode() 
    {
        return sysCode;
    }
    public void setSysLoginuser(String sysLoginuser) 
    {
        this.sysLoginuser = sysLoginuser;
    }

    public String getSysLoginuser() 
    {
        return sysLoginuser;
    }
    public void setSysLogouttime(Date sysLogouttime) 
    {
        this.sysLogouttime = sysLogouttime;
    }

    public Date getSysLogouttime() 
    {
        return sysLogouttime;
    }
    public void setSysLogintime(Date sysLogintime) 
    {
        this.sysLogintime = sysLogintime;
    }

    public Date getSysLogintime() 
    {
        return sysLogintime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sysCode", getSysCode())
            .append("sysLoginuser", getSysLoginuser())
            .append("sysLogouttime", getSysLogouttime())
            .append("sysLogintime", getSysLogintime())
            .toString();
    }
}
