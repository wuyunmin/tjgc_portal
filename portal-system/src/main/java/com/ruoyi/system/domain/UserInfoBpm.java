package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * bpm用户信息对象 user_info_bpm
 * 
 * @author tjgc
 * @date 2021-01-06
 */
public class UserInfoBpm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    private Long id;

    /** 系统名称 */
    @Excel(name = "系统名称")
    private String systemName;

    /** 用户工号 */
    @Excel(name = "用户工号")
    private String userNo;

    private String userName;

    /** 用户账号 */
    @Excel(name = "用户账号")
    private String loginName;

    /** 主管单位 */
    @Excel(name = "主管单位")
    private String competentDept;

    /** 主管部门 */
    @Excel(name = "主管部门")
    private String competentOrg;

    /** 最后登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后登录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date loginDate;

    /** 帐号状态（0正常 1停用） */
    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    private String userStatus;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 允许标志（0代表允许 1代表不允许） */
    private String authFlag;

    private Date sysUpdateTime;
    
    private String holderNames;

    public String getAuthFlag() {
        return authFlag;
    }

    public void setAuthFlag(String authFlag) {
        this.authFlag = authFlag;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSystemName(String systemName) 
    {
        this.systemName = systemName;
    }

    public String getSystemName() 
    {
        return systemName;
    }
    public void setUserNo(String userNo) 
    {
        this.userNo = userNo;
    }

    public String getUserNo() 
    {
        return userNo;
    }
    public void setLoginName(String loginName) 
    {
        this.loginName = loginName;
    }

    public String getLoginName() 
    {
        return loginName;
    }
    public void setCompetentDept(String competentDept) 
    {
        this.competentDept = competentDept;
    }

    public String getCompetentDept() 
    {
        return competentDept;
    }
    public void setCompetentOrg(String competentOrg) 
    {
        this.competentOrg = competentOrg;
    }

    public String getCompetentOrg() 
    {
        return competentOrg;
    }
    public void setLoginDate(Date loginDate) 
    {
        this.loginDate = loginDate;
    }

    public Date getLoginDate() 
    {
        return loginDate;
    }
    public void setUserStatus(String userStatus) 
    {
        this.userStatus = userStatus;
    }

    public String getUserStatus() 
    {
        return userStatus;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getSysUpdateTime() {
        return sysUpdateTime;
    }

    public void setSysUpdateTime(Date sysUpdateTime) {
        this.sysUpdateTime = sysUpdateTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("systemName", getSystemName())
            .append("userNo", getUserNo())
            .append("loginName", getLoginName())
            .append("competentDept", getCompetentDept())
            .append("competentOrg", getCompetentOrg())
            .append("loginDate", getLoginDate())
            .append("userStatus", getUserStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("holderNames",getHolderNames())
            .toString();
    }

	public String getHolderNames() {
		return holderNames;
	}

	public void setHolderNames(String holderNames) {
		this.holderNames = holderNames;
	}
}
