package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 服务大厅对象 sys_service_room
 * 
 * @author tjgc
 * @date 2021-03-09
 */
public class SysServiceRoom extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String guid;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 服务类别 */
    @Excel(name = "服务类别")
    private String serviceType;

    /** 二级类别 */
    
    private String subclass;
    @Excel(name = "二级类别")
    private String subclassText;

    /** 类型(1=普通链接,2=下载链接,3=bpm流程,4=hr流程,5=oa流程) */
    @Excel(name = "类型(1=普通链接,2=下载链接,3=bpm流程,4=hr流程,5=oa流程)")
    private String type;
    
    @Excel(name="链接地址")
    private String link;
    
    //图片地址
    private String picture;
    
    //排序
    private double num;
    
    //状态
    private String status;

    public void setGuid(String guid) 
    {
        this.guid = guid;
    }

    public String getGuid() 
    {
        return guid;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setServiceType(String serviceType) 
    {
        this.serviceType = serviceType;
    }

    public String getServiceType() 
    {
        return serviceType;
    }
    public void setSubclass(String subclass) 
    {
        this.subclass = subclass;
    }

    public String getSubclass() 
    {
        return subclass;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("guid", getGuid())
            .append("name", getName())
            .append("remark", getRemark())
            .append("serviceType", getServiceType())
            .append("subclass", getSubclass())
            .append("type", getType())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("link",getLink())
            .append("subclassText",getSubclassText())
            .append("picture",getPicture())
            .append("num",getNum())
            .append("status",getStatus())
            .toString();
    }

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getSubclassText() {
		return subclassText;
	}

	public void setSubclassText(String subclassText) {
		this.subclassText = subclassText;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public double getNum() {
		return num;
	}

	public void setNum(double num) {
		this.num = num;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
