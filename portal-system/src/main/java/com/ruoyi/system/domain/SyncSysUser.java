package com.ruoyi.system.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 部门同步中间对象 sync_sys_user
 *
 * @author tjgc
 * @date 2021-01-20
 */
public class SyncSysUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    private Long id;

    /** 员工全局ID */
    @Excel(name = "员工全局ID")
    private String empId;

    /** 部门编号 */
    @Excel(name = "部门编号")
    private String deptId;

    /** 员工名称 */
    @Excel(name = "员工名称")
    private String empName;

    /** 员工编号 */
    @Excel(name = "员工编号")
    private String empNumber;

    /** 项目负责人全局ID */
    @Excel(name = "项目负责人全局ID")
    private String empProjectLeaderId;

    /** 领款经办人全局ID */
    @Excel(name = "领款经办人全局ID")
    private String empProjectPayeeId;

    /** 员工移动电话号 */
    @Excel(name = "员工移动电话号")
    private String empMobilePhone;

    /** 公司邮箱 */
    @Excel(name = "公司邮箱")
    private String empCompanyEmail;

    /** 证件类型 */
    @Excel(name = "证件类型")
    private String idTypeContent;

    /** 证件号码 */
    @Excel(name = "证件号码")
    private String empIdNumber;

    /** 性别 */
    @Excel(name = "性别")
    private String empSex;

    /** 入职日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入职日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date empEntryDate;

    /** 离职日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "离职日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date empQuitDate;

    /** 主管单位名称 */
    @Excel(name = "主管单位名称")
    private String competentOrgContent;

    /** 在职状态 */
    @Excel(name = "在职状态")
    private String incumbencyContent;

    /** 在职状态编码 */
    @Excel(name = "在职状态编码")
    private String empIncumbencyState;

    /** 主职 */
    @Excel(name = "主职")
    private String partType;

    /** 员工编号有效性 */
    @Excel(name = "员工编号有效性")
    private String empNumberEffectiveness;

    /** 员工编号有效性 */
    @Excel(name = "员工编号有效性")
    private String empIsvalidContent;

    /** 人事资料有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "人事资料有效期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date empDataValid;

    /** 人员类别ID */
    @Excel(name = "人员类别ID")
    private String empPersonnelType;

    /** 人员类别 */
    @Excel(name = "人员类别")
    private String personTypeContent;

    /** HR中员工ID */
    @Excel(name = "HR中员工ID")
    private String empIdCode;

    /** HR部门ID */
    @Excel(name = "HR部门ID")
    private String deptIdCode;

    /** 是否项目负责人 */
    @Excel(name = "是否项目负责人")
    private String empJudgeProjectLeader;

    /** 是否项目领款经办人 */
    @Excel(name = "是否项目领款经办人")
    private String empJudgeProjectPayee;

    /** hr的项目负责人编号 */
    @Excel(name = "hr的项目负责人编号")
    private String empProjectLeader;

    /** hr的项目领款经办人编号 */
    @Excel(name = "hr的项目领款经办人编号")
    private String empProjectPayee;

    /** TJEC 在职状态编码 **/
    private String empIncumbencyStateTjec;
    /** TJEC 人员类别ID **/
    private String empPersonnelTypeTjec;

    /** 同步标志（0代表未同步 1代表已同步） */
    @Excel(name = "同步标志", readConverterExp = "0=代表未同步,1=代表已同步")
    private String syncFlag;

    private String flag;

    private String userStatus;

    private String deptContent;

    private List<String> competentOrgList;

    private List<String> deptList;

    private List<String> incumbencyList;

    private List<String> personList;

    private List<String> userStatusList;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setEmpId(String empId)
    {
        this.empId = empId;
    }

    public String getEmpId()
    {
        return empId;
    }
    public void setDeptId(String deptId)
    {
        this.deptId = deptId;
    }

    public String getDeptId()
    {
        return deptId;
    }
    public void setEmpName(String empName)
    {
        this.empName = empName;
    }

    public String getEmpName()
    {
        return empName;
    }
    public void setEmpNumber(String empNumber)
    {
        this.empNumber = empNumber;
    }

    public String getEmpNumber()
    {
        return empNumber;
    }
    public void setEmpProjectLeaderId(String empProjectLeaderId)
    {
        this.empProjectLeaderId = empProjectLeaderId;
    }

    public String getEmpProjectLeaderId()
    {
        return empProjectLeaderId;
    }
    public void setEmpProjectPayeeId(String empProjectPayeeId)
    {
        this.empProjectPayeeId = empProjectPayeeId;
    }

    public String getEmpProjectPayeeId()
    {
        return empProjectPayeeId;
    }
    public void setEmpMobilePhone(String empMobilePhone)
    {
        this.empMobilePhone = empMobilePhone;
    }

    public String getEmpMobilePhone()
    {
        return empMobilePhone;
    }
    public void setEmpCompanyEmail(String empCompanyEmail)
    {
        this.empCompanyEmail = empCompanyEmail;
    }

    public String getEmpCompanyEmail()
    {
        return empCompanyEmail;
    }
    public void setIdTypeContent(String idTypeContent)
    {
        this.idTypeContent = idTypeContent;
    }

    public String getIdTypeContent()
    {
        return idTypeContent;
    }
    public void setEmpIdNumber(String empIdNumber)
    {
        this.empIdNumber = empIdNumber;
    }

    public String getEmpIdNumber()
    {
        return empIdNumber;
    }
    public void setEmpSex(String empSex)
    {
        this.empSex = empSex;
    }

    public String getEmpSex()
    {
        return empSex;
    }
    public void setEmpEntryDate(Date empEntryDate)
    {
        this.empEntryDate = empEntryDate;
    }

    public Date getEmpEntryDate()
    {
        return empEntryDate;
    }
    public void setEmpQuitDate(Date empQuitDate)
    {
        this.empQuitDate = empQuitDate;
    }

    public Date getEmpQuitDate()
    {
        return empQuitDate;
    }
    public void setCompetentOrgContent(String competentOrgContent)
    {
        this.competentOrgContent = competentOrgContent;
    }

    public String getCompetentOrgContent()
    {
        return competentOrgContent;
    }
    public void setIncumbencyContent(String incumbencyContent)
    {
        this.incumbencyContent = incumbencyContent;
    }

    public String getIncumbencyContent()
    {
        return incumbencyContent;
    }
    public void setEmpIncumbencyState(String empIncumbencyState)
    {
        this.empIncumbencyState = empIncumbencyState;
    }

    public String getEmpIncumbencyState()
    {
        return empIncumbencyState;
    }
    public void setPartType(String partType)
    {
        this.partType = partType;
    }

    public String getPartType()
    {
        return partType;
    }
    public void setEmpNumberEffectiveness(String empNumberEffectiveness)
    {
        this.empNumberEffectiveness = empNumberEffectiveness;
    }

    public String getEmpNumberEffectiveness()
    {
        return empNumberEffectiveness;
    }
    public void setEmpIsvalidContent(String empIsvalidContent)
    {
        this.empIsvalidContent = empIsvalidContent;
    }

    public String getEmpIsvalidContent()
    {
        return empIsvalidContent;
    }
    public void setEmpDataValid(Date empDataValid)
    {
        this.empDataValid = empDataValid;
    }

    public Date getEmpDataValid()
    {
        return empDataValid;
    }
    public void setEmpPersonnelType(String empPersonnelType)
    {
        this.empPersonnelType = empPersonnelType;
    }

    public String getEmpPersonnelType()
    {
        return empPersonnelType;
    }
    public void setPersonTypeContent(String personTypeContent)
    {
        this.personTypeContent = personTypeContent;
    }

    public String getPersonTypeContent()
    {
        return personTypeContent;
    }
    public void setEmpIdCode(String empIdCode)
    {
        this.empIdCode = empIdCode;
    }

    public String getEmpIdCode()
    {
        return empIdCode;
    }
    public void setDeptIdCode(String deptIdCode)
    {
        this.deptIdCode = deptIdCode;
    }

    public String getDeptIdCode()
    {
        return deptIdCode;
    }
    public void setEmpJudgeProjectLeader(String empJudgeProjectLeader)
    {
        this.empJudgeProjectLeader = empJudgeProjectLeader;
    }

    public String getEmpJudgeProjectLeader()
    {
        return empJudgeProjectLeader;
    }
    public void setEmpJudgeProjectPayee(String empJudgeProjectPayee)
    {
        this.empJudgeProjectPayee = empJudgeProjectPayee;
    }

    public String getEmpJudgeProjectPayee()
    {
        return empJudgeProjectPayee;
    }
    public void setEmpProjectLeader(String empProjectLeader)
    {
        this.empProjectLeader = empProjectLeader;
    }

    public String getEmpProjectLeader()
    {
        return empProjectLeader;
    }
    public void setEmpProjectPayee(String empProjectPayee)
    {
        this.empProjectPayee = empProjectPayee;
    }

    public String getEmpProjectPayee()
    {
        return empProjectPayee;
    }

    public String getEmpIncumbencyStateTjec() {
        return empIncumbencyStateTjec;
    }

    public void setEmpIncumbencyStateTjec(String empIncumbencyStateTjec) {
        this.empIncumbencyStateTjec = empIncumbencyStateTjec;
    }

    public String getEmpPersonnelTypeTjec() {
        return empPersonnelTypeTjec;
    }

    public void setEmpPersonnelTypeTjec(String empPersonnelTypeTjec) {
        this.empPersonnelTypeTjec = empPersonnelTypeTjec;
    }

    public void setSyncFlag(String syncFlag)
    {
        this.syncFlag = syncFlag;
    }

    public String getSyncFlag()
    {
        return syncFlag;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getDeptContent() {
        return deptContent;
    }

    public void setDeptContent(String deptContent) {
        this.deptContent = deptContent;
    }

    public List<String> getCompetentOrgList() {
        return competentOrgList;
    }

    public void setCompetentOrgList(List<String> competentOrgList) {
        this.competentOrgList = competentOrgList;
    }

    public List<String> getDeptList() {
        return deptList;
    }

    public void setDeptList(List<String> deptList) {
        this.deptList = deptList;
    }

    public List<String> getIncumbencyList() {
        return incumbencyList;
    }

    public void setIncumbencyList(List<String> incumbencyList) {
        this.incumbencyList = incumbencyList;
    }

    public List<String> getPersonList() {
        return personList;
    }

    public void setPersonList(List<String> personList) {
        this.personList = personList;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public List<String> getUserStatusList() {
        return userStatusList;
    }

    public void setUserStatusList(List<String> userStatusList) {
        this.userStatusList = userStatusList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("empId", getEmpId())
                .append("deptId", getDeptId())
                .append("empName", getEmpName())
                .append("empNumber", getEmpNumber())
                .append("empProjectLeaderId", getEmpProjectLeaderId())
                .append("empProjectPayeeId", getEmpProjectPayeeId())
                .append("empMobilePhone", getEmpMobilePhone())
                .append("empCompanyEmail", getEmpCompanyEmail())
                .append("idTypeContent", getIdTypeContent())
                .append("empIdNumber", getEmpIdNumber())
                .append("empSex", getEmpSex())
                .append("empEntryDate", getEmpEntryDate())
                .append("empQuitDate", getEmpQuitDate())
                .append("competentOrgContent", getCompetentOrgContent())
                .append("incumbencyContent", getIncumbencyContent())
                .append("empIncumbencyState", getEmpIncumbencyState())
                .append("partType", getPartType())
                .append("empNumberEffectiveness", getEmpNumberEffectiveness())
                .append("empIsvalidContent", getEmpIsvalidContent())
                .append("empDataValid", getEmpDataValid())
                .append("empPersonnelType", getEmpPersonnelType())
                .append("personTypeContent", getPersonTypeContent())
                .append("empIdCode", getEmpIdCode())
                .append("deptIdCode", getDeptIdCode())
                .append("empJudgeProjectLeader", getEmpJudgeProjectLeader())
                .append("empJudgeProjectPayee", getEmpJudgeProjectPayee())
                .append("empProjectLeader", getEmpProjectLeader())
                .append("empProjectPayee", getEmpProjectPayee())
                .append("empIncumbencyStateTjec", getEmpIncumbencyStateTjec())
                .append("empPersonnelTypeTjec", getEmpPersonnelTypeTjec())
                .append("syncFlag", getSyncFlag())
                .append("flag", getFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
