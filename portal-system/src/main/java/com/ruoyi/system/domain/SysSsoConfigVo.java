package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 用户单点登录配置对象 sys_sso_config
 * 
 * @author tjgc
 * @date 2020-12-21
 */
/**
 * 用户单点登录配置对象 sys_sso_config
 *
 * @author tjgc
 * @date 2020-12-28
 */
public class SysSsoConfigVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 系统名称 */
    @Excel(name = "系统名称")
    private String systemName;

    /** 系统名称 */
    @Excel(name = "系统名称展示")
    private String systemNameCn;

    /** 单点登陆类型 */
    @Excel(name = "单点登陆类型")
    private String ssoType;

    /** 系统类型 */
    @Excel(name = "系统类型")
    private String systemType;

    private String openWay;

    /** 单点登陆地址 */
    @Excel(name = "单点登陆地址")
    private String ssoUrl;

    /** 账号字符 */
    @Excel(name = "账号字符")
    private String usernameStr;

    /** 密码字符 */
    @Excel(name = "密码字符")
    private String passwordStr;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 图片 */
    @Excel(name = "图片")
    private String images;

    /** 排序字段 */
    @Excel(name = "排序字段")
    private String fieldSort;

    /**用户允许登陆系统*/
    private List<String> auths;

    private int pendCount;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }

    public String getSystemName()
    {
        return systemName;
    }
    public void setSsoType(String ssoType)
    {
        this.ssoType = ssoType;
    }

    public String getSsoType()
    {
        return ssoType;
    }
    public void setSsoUrl(String ssoUrl)
    {
        this.ssoUrl = ssoUrl;
    }

    public String getSystemType() {
        return systemType;
    }

    public void setSystemType(String systemType) {
        this.systemType = systemType;
    }

    public String getOpenWay() {
        return openWay;
    }

    public void setOpenWay(String openWay) {
        this.openWay = openWay;
    }

    public String getSsoUrl()
    {
        return ssoUrl;
    }
    public void setUsernameStr(String usernameStr)
    {
        this.usernameStr = usernameStr;
    }

    public String getUsernameStr()
    {
        return usernameStr;
    }
    public void setPasswordStr(String passwordStr)
    {
        this.passwordStr = passwordStr;
    }

    public String getPasswordStr()
    {
        return passwordStr;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setImages(String images)
    {
        this.images = images;
    }

    public String getImages()
    {
        return images;
    }
    public void setFieldSort(String fieldSort)
    {
        this.fieldSort = fieldSort;
    }

    public String getFieldSort()
    {
        return fieldSort;
    }

    public String getSystemNameCn() {
        return systemNameCn;
    }

    public void setSystemNameCn(String systemNameCn) {
        this.systemNameCn = systemNameCn;
    }

    public List<String> getAuths() {
        return auths;
    }

    public void setAuths(List<String> auths) {
        this.auths = auths;
    }

    public int getPendCount() {
        return pendCount;
    }

    public void setPendCount(int pendCount) {
        this.pendCount = pendCount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("systemName", getSystemName())
                .append("ssoType", getSsoType())
                .append("ssoUrl", getSsoUrl())
                .append("usernameStr", getUsernameStr())
                .append("passwordStr", getPasswordStr())
                .append("status", getStatus())
                .append("images", getImages())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("fieldSort", getFieldSort())
                .append("remark", getRemark())
                .toString();
    }
}
