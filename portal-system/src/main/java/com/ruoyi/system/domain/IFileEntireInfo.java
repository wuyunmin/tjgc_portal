package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 分片上传文件信息对象 async_file_info
 * 
 * @author tjgc
 * @date 2021-03-15
 */
public class IFileEntireInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 文件编号 */
    private String id;

    /** 文件名称 */
    @Excel(name = "文件名称")
    private String filename;

    /** 文件MD5标识 */
    @Excel(name = "文件MD5标识")
    private String identifier;

    /** 文件类型 */
    @Excel(name = "文件类型")
    private String type;

    /** 文件总大小 */
    @Excel(name = "文件总大小")
    private Long totalSize;

    /** 文件存储位置 */
    @Excel(name = "文件存储位置")
    private String location;

    /** 文件删除标注 */
    private String delFlag;

    /** 文件所属目标（预留字段） */
    @Excel(name = "文件所属目标", readConverterExp = "预=留字段")
    private String refProjectId;

    /** 上传人 */
    @Excel(name = "上传人")
    private String uploadBy;

    private String uploadUrl;

    private String fileUrl;

    private String fileAddr;

    /** 上传时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "上传时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date uploadTime;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setFilename(String filename) 
    {
        this.filename = filename;
    }

    public String getFilename() 
    {
        return filename;
    }
    public void setIdentifier(String identifier) 
    {
        this.identifier = identifier;
    }

    public String getIdentifier() 
    {
        return identifier;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setTotalSize(Long totalSize) 
    {
        this.totalSize = totalSize;
    }

    public Long getTotalSize() 
    {
        return totalSize;
    }
    public void setLocation(String location) 
    {
        this.location = location;
    }

    public String getLocation() 
    {
        return location;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setRefProjectId(String refProjectId) 
    {
        this.refProjectId = refProjectId;
    }

    public String getRefProjectId() 
    {
        return refProjectId;
    }
    public void setUploadBy(String uploadBy) 
    {
        this.uploadBy = uploadBy;
    }

    public String getUploadBy() 
    {
        return uploadBy;
    }
    public void setUploadTime(Date uploadTime) 
    {
        this.uploadTime = uploadTime;
    }

    public Date getUploadTime() 
    {
        return uploadTime;
    }

    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }

    public String getFileAddr() {
        return fileAddr;
    }

    public void setFileAddr(String fileAddr) {
        this.fileAddr = fileAddr;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("filename", getFilename())
                .append("identifier", getIdentifier())
                .append("type", getType())
                .append("totalSize", getTotalSize())
                .append("location", getLocation())
                .append("fileUrl", getFileUrl())
                .append("fileAddr", getFileAddr())
                .append("delFlag", getDelFlag())
                .append("refProjectId", getRefProjectId())
                .append("uploadBy", getUploadBy())
                .append("uploadTime", getUploadTime())
                .toString();
    }
}
