package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 部门同步中间对象 sync_sys_user
 *
 * @author tjgc
 * @date 2021-01-20
 */
public class UnifyUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private String guid;

    private String userName;

    /** 员工编号 */
    private String userNo;

    /** 账号 */
    private String accountNo;

    /** 主管部门 */
    private String dept;

    /** 用户状态 */
    private String userStatus;

    /** 最后登陆时间 */
    private Date loginDate;

    /** 数据来源系统 */
    private Date systemType;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("userNo", getUserNo())
            .append("accountNo", getAccountNo())
            .append("dept", getDept())
            .append("userStatus", getUserStatus())
            .append("loginDate", getLoginDate())
            .append("systemType",getSystemType())
            .append("userName",getUserName())
            .append("guid",getGuid())
            .toString();
    }

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public Date getSystemType() {
		return systemType;
	}

	public void setSystemType(Date systemType) {
		this.systemType = systemType;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


}
