package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户单点登录信息对象 sys_sso_info
 *
 * @author tjgc
 * @date 2020-12-17
 */
public class SysSsoInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 系统名称 */
    @Excel(name = "系统名称")
    private String systemName;

    /** 系统名称 */
    @Excel(name = "系统名称")
    private String systemNameCn;

    /** 用户工号 */
    @Excel(name = "用户工号")
    private String userNo;

    /** 用户账号 */
    @Excel(name = "用户账号")
    private String userName;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }

    public String getSystemName()
    {
        return systemName;
    }
    public void setUserNo(String userNo)
    {
        this.userNo = userNo;
    }

    public String getUserNo()
    {
        return userNo;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        return password;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public String getSystemNameCn() {
        return systemNameCn;
    }

    public void setSystemNameCn(String systemNameCn) {
        this.systemNameCn = systemNameCn;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("systemName", getSystemName())
                .append("userNo", getUserNo())
                .append("userName", getUserName())
                .append("password", getPassword())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
