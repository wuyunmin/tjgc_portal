package com.ruoyi.system.domain;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 服务大厅对象 sys_service_room
 * 
 * @author tjgc
 * @date 2021-03-09
 */
public class SysServiceRoomDto
{
    private static final long serialVersionUID = 1L;
    
    private List<SysServiceRoom> serviceLinks;
    
    private String name;
    
    private double num;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("name", getName())
            .append("num", getNum())
            .append("serviceLinks", getServiceLinks())
            .toString();
    }
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getNum() {
		return num;
	}

	public void setNum(double num) {
		this.num = num;
	}

	public List<SysServiceRoom> getServiceLinks() {
		return serviceLinks;
	}
	public void setServiceLinks(List<SysServiceRoom> serviceLinks) {
		this.serviceLinks = serviceLinks;
	}

}
