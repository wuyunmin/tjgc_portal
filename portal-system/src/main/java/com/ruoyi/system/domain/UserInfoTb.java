package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * teambition用户信息对象 user_info_tb
 *
 * @author tjec
 * @date 2021-01-28
 */
public class UserInfoTb extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String guid;

    /** 系统名称 */
    private String systemName;

    /** 工号 */
    @Excel(name = "员工工号")
    private String userNo;

    /** 姓名 */
    @Excel(name = "姓名")
    private String userName;

    /** 账号昵称 */
    @Excel(name = "账号昵称")
    private String nickName;

    /** 部门 */
    @Excel(name = "部门")
    private String deptName;

    /** 企业角色 */
    @Excel(name = "企业角色")
    private String companyRole;

    private String holderNames;

    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getGuid()
    {
        return guid;
    }
    public void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }

    public String getSystemName()
    {
        return systemName;
    }
    public void setUserNo(String userNo)
    {
        this.userNo = userNo;
    }

    public String getUserNo()
    {
        return userNo;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setNickName(String nickName)
    {
        this.nickName = nickName;
    }

    public String getNickName()
    {
        return nickName;
    }
    public void setDeptName(String deptName)
    {
        this.deptName = deptName;
    }

    public String getDeptName()
    {
        return deptName;
    }
    public void setCompanyRole(String companyRole)
    {
        this.companyRole = companyRole;
    }

    public String getCompanyRole()
    {
        return companyRole;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("guid", getGuid())
            .append("systemName", getSystemName())
            .append("userNo", getUserNo())
            .append("userName", getUserName())
            .append("nickName", getNickName())
            .append("deptName", getDeptName())
            .append("companyRole", getCompanyRole())
            .append("holderNames",getHolderNames())
            .toString();
    }

	public String getHolderNames() {
		return holderNames;
	}

	public void setHolderNames(String holderNames) {
		this.holderNames = holderNames;
	}
}
