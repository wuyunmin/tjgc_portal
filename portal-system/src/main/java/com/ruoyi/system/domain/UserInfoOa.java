package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * OA系统用户信息对象 user_info_oa
 * 
 * @author tjgc
 * @date 2021-02-24
 */
public class UserInfoOa extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long guid;

    /** 系统类型 */
    @Excel(name = "系统类型")
    private String systemName;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String userName;

    /** 用户工号 */
    @Excel(name = "用户工号")
    private String userNo;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String idCard;

    /** 用户账号 */
    @Excel(name = "用户账号")
    private String loginName;

    /** 启用状态 */
    @Excel(name = "启用状态 0 禁用 1启用" )
    private String isActive;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String orgName;

    /** 休眠状态 */
    @Excel(name = "休眠状态 1休眠")
    private String isSleep;
    
    private String holderNames;

    /** 最后登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后登录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lastLogonTime;

    /** mdm数据库id */
    @Excel(name = "mdm数据库id")
    private String mdmId;

    public void setGuid(Long guid) 
    {
        this.guid = guid;
    }

    public Long getGuid() 
    {
        return guid;
    }
    public void setSystemName(String systemName) 
    {
        this.systemName = systemName;
    }

    public String getSystemName() 
    {
        return systemName;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setUserNo(String userNo) 
    {
        this.userNo = userNo;
    }

    public String getUserNo() 
    {
        return userNo;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setLoginName(String loginName) 
    {
        this.loginName = loginName;
    }

    public String getLoginName() 
    {
        return loginName;
    }
    public void setIsActive(String isActive) 
    {
        this.isActive = isActive;
    }

    public String getIsActive() 
    {
        return isActive;
    }
    public void setOrgName(String orgName) 
    {
        this.orgName = orgName;
    }

    public String getOrgName() 
    {
        return orgName;
    }
    public void setIsSleep(String isSleep) 
    {
        this.isSleep = isSleep;
    }

    public String getIsSleep() 
    {
        return isSleep;
    }
    public void setLastLogonTime(Date lastLogonTime) 
    {
        this.lastLogonTime = lastLogonTime;
    }

    public Date getLastLogonTime() 
    {
        return lastLogonTime;
    }
    public void setMdmId(String mdmId) 
    {
        this.mdmId = mdmId;
    }

    public String getMdmId() 
    {
        return mdmId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("guid", getGuid())
            .append("systemName", getSystemName())
            .append("userName", getUserName())
            .append("userNo", getUserNo())
            .append("idCard", getIdCard())
            .append("loginName", getLoginName())
            .append("isActive", getIsActive())
            .append("orgName", getOrgName())
            .append("isSleep", getIsSleep())
            .append("lastLogonTime", getLastLogonTime())
            .append("mdmId", getMdmId())
            .append("holderNames",getHolderNames())
            .toString();
    }

	public String getHolderNames() {
		return holderNames;
	}

	public void setHolderNames(String holderNames) {
		this.holderNames = holderNames;
	}
}
