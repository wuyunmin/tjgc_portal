package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 部门同步中间对象 sync_sys_dept
 * 
 * @author tjgc
 * @date 2021-01-20
 */
public class SyncSysDept extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    private Long id;

    /** 部门全局ID */
    @Excel(name = "部门全局ID")
    private String deptId;

    /** 父级部门全局ID */
    @Excel(name = "父级部门全局ID")
    private String deptParentId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String deptContent;

    /** HR部门代码 */
    @Excel(name = "HR部门代码")
    private String deptCode;

    /** 部门全称 */
    @Excel(name = "部门全称")
    private String deptAllContent;

    /** 部门层数 */
    @Excel(name = "部门层数")
    private Integer deptGrade;

    /** 父级部门HR代码 */
    @Excel(name = "父级部门HR代码")
    private String deptParentCode;

    /** 分管领导HR代码 */
    @Excel(name = "分管领导HR代码")
    private String deptInchargeLeader;

    /** 部门领导HR代码 */
    @Excel(name = "部门领导HR代码")
    private String deptLeader;

    /** OA组织编码 */
    @Excel(name = "OA组织编码")
    private String deptOaOrgCode;

    /** HR部门ID */
    @Excel(name = "HR部门ID")
    private String deptIdCode;

    /** 排序code */
    @Excel(name = "排序code")
    private Integer deptOrderCode;

    /** 父级HR部门ID */
    @Excel(name = "父级HR部门ID")
    private String deptParentIdCode;

    /** 删除标志（0代表存在 2代表删除） */
    @Excel(name = "删除标志", readConverterExp = "0=代表存在,2=代表删除")
    private String deptDeleted;

    /** 部门类型 */
    @Excel(name = "部门类型")
    private String deptType;

    /** 末级标志 */
    @Excel(name = "末级标志")
    private String deptEndMark;

    /** 分管领导全局ID */
    @Excel(name = "分管领导全局ID")
    private String deptInchargeLeaderId;

    /** 主管领导全局ID */
    @Excel(name = "主管领导全局ID")
    private String deptLeaderId;

    /** 同步标志（0代表未同步 1代表已同步） */
    @Excel(name = "同步标志", readConverterExp = "0=代表未同步,1=代表已同步")
    private String syncFlag;

    private String flag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDeptId(String deptId) 
    {
        this.deptId = deptId;
    }

    public String getDeptId() 
    {
        return deptId;
    }
    public void setDeptParentId(String deptParentId) 
    {
        this.deptParentId = deptParentId;
    }

    public String getDeptParentId() 
    {
        return deptParentId;
    }
    public void setDeptContent(String deptContent) 
    {
        this.deptContent = deptContent;
    }

    public String getDeptContent() 
    {
        return deptContent;
    }
    public void setDeptCode(String deptCode) 
    {
        this.deptCode = deptCode;
    }

    public String getDeptCode() 
    {
        return deptCode;
    }
    public void setDeptAllContent(String deptAllContent) 
    {
        this.deptAllContent = deptAllContent;
    }

    public String getDeptAllContent() 
    {
        return deptAllContent;
    }
    public void setDeptGrade(Integer deptGrade) 
    {
        this.deptGrade = deptGrade;
    }

    public Integer getDeptGrade() 
    {
        return deptGrade;
    }
    public void setDeptParentCode(String deptParentCode) 
    {
        this.deptParentCode = deptParentCode;
    }

    public String getDeptParentCode() 
    {
        return deptParentCode;
    }
    public void setDeptInchargeLeader(String deptInchargeLeader) 
    {
        this.deptInchargeLeader = deptInchargeLeader;
    }

    public String getDeptInchargeLeader() 
    {
        return deptInchargeLeader;
    }
    public void setDeptLeader(String deptLeader) 
    {
        this.deptLeader = deptLeader;
    }

    public String getDeptLeader() 
    {
        return deptLeader;
    }
    public void setDeptOaOrgCode(String deptOaOrgCode) 
    {
        this.deptOaOrgCode = deptOaOrgCode;
    }

    public String getDeptOaOrgCode() 
    {
        return deptOaOrgCode;
    }
    public void setDeptIdCode(String deptIdCode) 
    {
        this.deptIdCode = deptIdCode;
    }

    public String getDeptIdCode() 
    {
        return deptIdCode;
    }
    public void setDeptOrderCode(Integer deptOrderCode) 
    {
        this.deptOrderCode = deptOrderCode;
    }

    public Integer getDeptOrderCode() 
    {
        return deptOrderCode;
    }
    public void setDeptParentIdCode(String deptParentIdCode) 
    {
        this.deptParentIdCode = deptParentIdCode;
    }

    public String getDeptParentIdCode() 
    {
        return deptParentIdCode;
    }
    public void setDeptDeleted(String deptDeleted) 
    {
        this.deptDeleted = deptDeleted;
    }

    public String getDeptDeleted() 
    {
        return deptDeleted;
    }
    public void setDeptType(String deptType) 
    {
        this.deptType = deptType;
    }

    public String getDeptType() 
    {
        return deptType;
    }
    public void setDeptEndMark(String deptEndMark) 
    {
        this.deptEndMark = deptEndMark;
    }

    public String getDeptEndMark() 
    {
        return deptEndMark;
    }
    public void setDeptInchargeLeaderId(String deptInchargeLeaderId) 
    {
        this.deptInchargeLeaderId = deptInchargeLeaderId;
    }

    public String getDeptInchargeLeaderId() 
    {
        return deptInchargeLeaderId;
    }
    public void setDeptLeaderId(String deptLeaderId) 
    {
        this.deptLeaderId = deptLeaderId;
    }

    public String getDeptLeaderId() 
    {
        return deptLeaderId;
    }
    public void setSyncFlag(String syncFlag) 
    {
        this.syncFlag = syncFlag;
    }

    public String getSyncFlag() 
    {
        return syncFlag;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("deptId", getDeptId())
            .append("deptParentId", getDeptParentId())
            .append("deptContent", getDeptContent())
            .append("deptCode", getDeptCode())
            .append("deptAllContent", getDeptAllContent())
            .append("deptGrade", getDeptGrade())
            .append("deptParentCode", getDeptParentCode())
            .append("deptInchargeLeader", getDeptInchargeLeader())
            .append("deptLeader", getDeptLeader())
            .append("deptOaOrgCode", getDeptOaOrgCode())
            .append("deptIdCode", getDeptIdCode())
            .append("deptOrderCode", getDeptOrderCode())
            .append("deptParentIdCode", getDeptParentIdCode())
            .append("deptDeleted", getDeptDeleted())
            .append("deptType", getDeptType())
            .append("deptEndMark", getDeptEndMark())
            .append("deptInchargeLeaderId", getDeptInchargeLeaderId())
            .append("deptLeaderId", getDeptLeaderId())
            .append("syncFlag", getSyncFlag())
            .append("flag",getFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
