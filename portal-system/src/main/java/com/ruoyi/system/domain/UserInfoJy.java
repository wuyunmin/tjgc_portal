package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 经营系统用户信息对象 user_info_jy
 * 
 * @author tjgc
 * @date 2021-02-24
 */
public class UserInfoJy extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long guid;

    /** 系统类型 */
    @Excel(name = "系统类型")
    private String systemName;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String userName;

    /** 用户工号 */
    @Excel(name = "用户工号")
    private String userNo;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String idCard;

    /** 登录名称 */
    @Excel(name = "登录名称")
    private String loginName;

    /** 用户状态 */
    @Excel(name = "用户状态")
    private String isActive;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private String isDeleted;

    /** 部门 */
    @Excel(name = "部门")
    private String orgName;

    /** 是否管理员 */
    @Excel(name = "是否管理员")
    private String isAdmin;

    /** mdm数据库id */
    @Excel(name = "mdm数据库id")
    private String mdmId;
    
    private String holderNames;

    public void setGuid(Long guid) 
    {
        this.guid = guid;
    }

    public Long getGuid() 
    {
        return guid;
    }
    public void setSystemName(String systemName) 
    {
        this.systemName = systemName;
    }

    public String getSystemName() 
    {
        return systemName;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setUserNo(String userNo) 
    {
        this.userNo = userNo;
    }

    public String getUserNo() 
    {
        return userNo;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setLoginName(String loginName) 
    {
        this.loginName = loginName;
    }

    public String getLoginName() 
    {
        return loginName;
    }
    public void setIsActive(String isActive) 
    {
        this.isActive = isActive;
    }

    public String getIsActive() 
    {
        return isActive;
    }
    public void setIsDeleted(String isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public String getIsDeleted() 
    {
        return isDeleted;
    }
    public void setOrgName(String orgName) 
    {
        this.orgName = orgName;
    }

    public String getOrgName() 
    {
        return orgName;
    }
    public void setIsAdmin(String isAdmin) 
    {
        this.isAdmin = isAdmin;
    }

    public String getIsAdmin() 
    {
        return isAdmin;
    }
    public void setMdmId(String mdmId) 
    {
        this.mdmId = mdmId;
    }

    public String getMdmId() 
    {
        return mdmId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("guid", getGuid())
            .append("systemName", getSystemName())
            .append("userName", getUserName())
            .append("userNo", getUserNo())
            .append("idCard", getIdCard())
            .append("loginName", getLoginName())
            .append("isActive", getIsActive())
            .append("isDeleted", getIsDeleted())
            .append("orgName", getOrgName())
            .append("isAdmin", getIsAdmin())
            .append("mdmId", getMdmId())
            .append("holderNames",getHolderNames())
            .toString();
    }

	public String getHolderNames() {
		return holderNames;
	}

	public void setHolderNames(String holderNames) {
		this.holderNames = holderNames;
	}
}
