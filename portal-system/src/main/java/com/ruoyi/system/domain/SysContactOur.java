package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 联系我们对象 sys_contact_our
 * 
 * @author pangyongfeng
 * @date 2021-03-09
 */
public class SysContactOur extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String guid;

    /** 部门 */
    @Excel(name = "部门")
    private String dept;
    
    private String deptName;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 办公电话 */
    @Excel(name = "办公电话")
    private String phone;

    /** QQ */
    @Excel(name = "QQ")
    private String qq;
    
    @Excel(name = "排序")
    private double num;

    public void setGuid(String guid) 
    {
        this.guid = guid;
    }

    public String getGuid() 
    {
        return guid;
    }
    public void setDept(String dept) 
    {
        this.dept = dept;
    }

    public String getDept() 
    {
        return dept;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setQq(String qq) 
    {
        this.qq = qq;
    }

    public String getQq() 
    {
        return qq;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("guid", getGuid())
            .append("dept", getDept())
            .append("name", getName())
            .append("phone", getPhone())
            .append("qq", getQq())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("num",getNum())
            .append("deptName",getDeptName())
            .toString();
    }

	public double getNum() {
		return num;
	}

	public void setNum(double num) {
		this.num = num;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
}
