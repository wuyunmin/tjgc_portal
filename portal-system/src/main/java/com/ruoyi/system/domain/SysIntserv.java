package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 综合服务配合对象 sys_intserv
 * 
 * @author tjgc
 * @date 2020-12-29
 */
public class SysIntserv extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 功能名称 */
    @Excel(name = "功能名称")
    private String functionName;

    /** 功能类型（1微信扫码  2常用链接 ） */
    @Excel(name = "功能类型")
    private String functionType;

    /** 应用地址 */
    @Excel(name = "应用地址")
    private String functionUrl;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 图片地址 */
    @Excel(name = "图片地址")
    private String filePath;

    /** 排序 */
    @Excel(name = "排序")
    private String fieldSort;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFunctionName(String functionName) 
    {
        this.functionName = functionName;
    }

    public String getFunctionName() 
    {
        return functionName;
    }
    public void setFunctionType(String functionType) 
    {
        this.functionType = functionType;
    }

    public String getFunctionType() 
    {
        return functionType;
    }
    public void setFunctionUrl(String functionUrl) 
    {
        this.functionUrl = functionUrl;
    }

    public String getFunctionUrl() 
    {
        return functionUrl;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setFilePath(String filePath) 
    {
        this.filePath = filePath;
    }

    public String getFilePath() 
    {
        return filePath;
    }
    public void setFieldSort(String fieldSort) 
    {
        this.fieldSort = fieldSort;
    }

    public String getFieldSort() 
    {
        return fieldSort;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("functionName", getFunctionName())
            .append("functionType", getFunctionType())
            .append("functionUrl", getFunctionUrl())
            .append("status", getStatus())
            .append("filePath", getFilePath())
            .append("fieldSort", getFieldSort())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
