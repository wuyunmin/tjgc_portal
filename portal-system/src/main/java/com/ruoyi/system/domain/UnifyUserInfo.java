package com.ruoyi.system.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 统一用户信息对象 unify_user_info
 *
 * @author tjgc
 * @date 2022-03-19
 */
public class UnifyUserInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    private String userName;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String deptName;

    /** 系统名称 */
    @Excel(name = "系统名称")
    private String systemName;

    /** 在职状态 */
    @Excel(name = "在职状态")
    private String incumbencyContent;

    /** 人员类别 */
    @Excel(name = "人员类别")
    private String personnelType;

    /** 用户工号 */
    @Excel(name = "用户工号")
    private String userNo;

    /** 用户账号 */
    @Excel(name = "用户账号")
    private String accountNo;

    /** 入职日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入职日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date entryDate;

    /** 离职日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "离职日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date quitDate;

    /** 主管单位 */
    @Excel(name = "主管单位")
    private String competentDept;

    /** 主管部门 */
    @Excel(name = "主管部门")
    private String competentOrg;

    /** 门户状态 */
    @Excel(name = "状态")
    private String accountStatus;

    /** 是否项目负责人 */
    @Excel(name = "是否项目负责人")
    private String judgeProjectLeader;

    /** 是否项目领款经办人 */
    @Excel(name = "是否项目领款经办人")
    private String judgeProjectPayee;

    /** 项目负责人 */
    @Excel(name = "项目负责人")
    private String projectLeader;

    /** 持有方式 */
    @Excel(name = "持有方式")
    private String holderType;

    /** 最后登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后登录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date loginDate;

    /** 门户角色 */
    @Excel(name = "门户角色")
    private String portalRole;

    /** 门户状态 */
    @Excel(name = "门户状态")
    private String portalStatus;

    /** 人员id */
    @Excel(name = "人员id")
    private String userId;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String phoneNum;

    /** 人员信息id */
    @Excel(name = "人员信息id")
    private String userInfoId;

    private List<String> incumbencyContentList;

    private List<String> personTypeContentList;

    public List<String> getIncumbencyContentList() {
        return incumbencyContentList;
    }

    public void setIncumbencyContentList(List<String> incumbencyContentList) {
        this.incumbencyContentList = incumbencyContentList;
    }

    public List<String> getPersonTypeContentList() {
        return personTypeContentList;
    }

    public void setPersonTypeContentList(List<String> personTypeContentList) {
        this.personTypeContentList = personTypeContentList;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setDeptName(String deptName)
    {
        this.deptName = deptName;
    }

    public String getDeptName()
    {
        return deptName;
    }
    public void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }

    public String getSystemName()
    {
        return systemName;
    }
    public void setIncumbencyContent(String incumbencyContent)
    {
        this.incumbencyContent = incumbencyContent;
    }

    public String getIncumbencyContent()
    {
        return incumbencyContent;
    }
    public void setPersonnelType(String personnelType)
    {
        this.personnelType = personnelType;
    }

    public String getPersonnelType()
    {
        return personnelType;
    }
    public void setUserNo(String userNo)
    {
        this.userNo = userNo;
    }

    public String getUserNo()
    {
        return userNo;
    }
    public void setAccountNo(String accountNo)
    {
        this.accountNo = accountNo;
    }

    public String getAccountNo()
    {
        return accountNo;
    }
    public void setEntryDate(Date entryDate)
    {
        this.entryDate = entryDate;
    }

    public Date getEntryDate()
    {
        return entryDate;
    }
    public void setQuitDate(Date quitDate)
    {
        this.quitDate = quitDate;
    }

    public Date getQuitDate()
    {
        return quitDate;
    }
    public void setCompetentDept(String competentDept)
    {
        this.competentDept = competentDept;
    }

    public String getCompetentDept()
    {
        return competentDept;
    }
    public void setCompetentOrg(String competentOrg)
    {
        this.competentOrg = competentOrg;
    }

    public String getCompetentOrg()
    {
        return competentOrg;
    }
    public void setAccountStatus(String accountStatus)
    {
        this.accountStatus = accountStatus;
    }

    public String getAccountStatus()
    {
        return accountStatus;
    }
    public void setJudgeProjectLeader(String judgeProjectLeader)
    {
        this.judgeProjectLeader = judgeProjectLeader;
    }

    public String getJudgeProjectLeader()
    {
        return judgeProjectLeader;
    }
    public void setJudgeProjectPayee(String judgeProjectPayee)
    {
        this.judgeProjectPayee = judgeProjectPayee;
    }

    public String getJudgeProjectPayee()
    {
        return judgeProjectPayee;
    }
    public void setProjectLeader(String projectLeader)
    {
        this.projectLeader = projectLeader;
    }

    public String getProjectLeader()
    {
        return projectLeader;
    }
    public void setHolderType(String holderType)
    {
        this.holderType = holderType;
    }

    public String getHolderType()
    {
        return holderType;
    }
    public void setLoginDate(Date loginDate)
    {
        this.loginDate = loginDate;
    }

    public Date getLoginDate()
    {
        return loginDate;
    }
    public void setPortalRole(String portalRole)
    {
        this.portalRole = portalRole;
    }

    public String getPortalRole()
    {
        return portalRole;
    }
    public void setPortalStatus(String portalStatus)
    {
        this.portalStatus = portalStatus;
    }

    public String getPortalStatus()
    {
        return portalStatus;
    }
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserId()
    {
        return userId;
    }
    public void setPhoneNum(String phoneNum)
    {
        this.phoneNum = phoneNum;
    }

    public String getPhoneNum()
    {
        return phoneNum;
    }

    public String getUserInfoId() {
        return userInfoId;
    }

    public void setUserInfoId(String userInfoId) {
        this.userInfoId = userInfoId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userName", getUserName())
            .append("deptName", getDeptName())
            .append("systemName", getSystemName())
            .append("incumbencyContent", getIncumbencyContent())
            .append("personnelType", getPersonnelType())
            .append("userNo", getUserNo())
            .append("accountNo", getAccountNo())
            .append("entryDate", getEntryDate())
            .append("quitDate", getQuitDate())
            .append("competentDept", getCompetentDept())
            .append("competentOrg", getCompetentOrg())
            .append("accountStatus", getAccountStatus())
            .append("judgeProjectLeader", getJudgeProjectLeader())
            .append("judgeProjectPayee", getJudgeProjectPayee())
            .append("projectLeader", getProjectLeader())
            .append("holderType", getHolderType())
            .append("loginDate", getLoginDate())
            .append("portalRole", getPortalRole())
            .append("portalStatus", getPortalStatus())
            .append("userId", getUserId())
            .append("phoneNum", getPhoneNum())
            .toString();
    }
}
