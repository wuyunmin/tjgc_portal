package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 域用户信息对象 user_info_group
 *
 * @author tjgc
 * @date 2021-04-15
 */
public class UserInfoGroup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 系统名称 */
    private String systemName;

    /** 编号 */
    @Excel(name = "员工编号")
    private String userNo;

    /** 姓名 */
    @Excel(name = "账号姓名")
    private String userName;

    /** 账号 */
    @Excel(name = "账号")
    private String loginName;

    /** $column.columnComment */
    private String holderName;

    private String holderIds;

    /** 状态(1:启用 、0:禁用、2:删除) */
    @Excel(name = "状态", readConverterExp = "1=正常,2=停用,3=未启用")
    private String userStatus;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date openTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date cancelTime;

    @Excel(name = "备注")
    private String remark;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }

    public String getSystemName()
    {
        return systemName;
    }
    public void setUserNo(String userNo)
    {
        this.userNo = userNo;
    }

    public String getUserNo()
    {
        return userNo;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setLoginName(String loginName)
    {
        this.loginName = loginName;
    }

    public String getLoginName()
    {
        return loginName;
    }
    public void setHolderName(String holderName)
    {
        this.holderName = holderName;
    }

    public String getHolderName()
    {
        return holderName;
    }
    public void setUserStatus(String userStatus)
    {
        this.userStatus = userStatus;
    }

    public String getHolderIds() {
        return holderIds;
    }

    public void setHolderIds(String holderIds) {
        this.holderIds = holderIds;
    }

    public String getUserStatus()
    {
        return userStatus;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public Date getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(Date cancelTime) {
        this.cancelTime = cancelTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("systemName", getSystemName())
            .append("userNo", getUserNo())
            .append("userName", getUserName())
            .append("loginName", getLoginName())
            .append("holderName", getHolderName())
            .append("userStatus", getUserStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
