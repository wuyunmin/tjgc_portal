package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * hr待办待阅信息对象 work_item_hr_sync
 * 
 * @author tjgc
 * @date 2022-05-09
 */
public class WorkItemHr extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 系统名称 */
    @Excel(name = "系统名称")
    private String systemName;

    /** 待办待阅识别标志（1待办 1待阅） */
    @Excel(name = "待办待阅识别标志", readConverterExp = "1=待办,1=待阅")
    private String pendType;

    /** 流程发起人工号 */
    @Excel(name = "流程发起人工号")
    private String promoterNo;

    /** 流程发起人名字 */
    @Excel(name = "流程发起人名字")
    private String promoter;

    /** 流程发起部门id */
    @Excel(name = "流程发起部门id")
    private String promoterOrgId;

    /** 流程发起部门 */
    @Excel(name = "流程发起部门")
    private String promoterOrg;

    /** 待办/待阅当前节点名称 */
    @Excel(name = "待办/待阅当前节点名称")
    private String node;

    /** 待办/待阅接收人（工号） */
    @Excel(name = "待办/待阅接收人", readConverterExp = "工=号")
    private String receiverUserCode;

    /** 流程名称 */
    @Excel(name = "流程名称")
    private String procedureName;

    /** 流程编码 */
    @Excel(name = "流程编码")
    private String procedureNo;

    /** 消息标题 */
    @Excel(name = "消息标题")
    private String title;

    /** 流程ID */
    @Excel(name = "流程ID")
    private String sequenceNo;

    /** 流程创建时间点 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "流程创建时间点", width = 30, dateFormat = "yyyy-MM-dd")
    private Date initiationTime;

    /** 流程待办/待阅接收时间点 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "流程待办/待阅接收时间点", width = 30, dateFormat = "yyyy-MM-dd")
    private Date receiverTime;

    /** 待办待阅URL */
    @Excel(name = "待办待阅URL")
    private String pcUrl;

    /** 处理状态，待办待阅处理状态（1处理完成 2未处理 3撤回） */
    @Excel(name = "处理状态，待办待阅处理状态", readConverterExp = "1=处理完成,2=未处理,3=撤回")
    private String status;

    /** 门户是否已处理（1未处理，2已处理） */
    @Excel(name = "门户是否已处理", readConverterExp = "1=未处理，2已处理")
    private String flag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSystemName(String systemName) 
    {
        this.systemName = systemName;
    }

    public String getSystemName() 
    {
        return systemName;
    }
    public void setPendType(String pendType) 
    {
        this.pendType = pendType;
    }

    public String getPendType() 
    {
        return pendType;
    }
    public void setPromoterNo(String promoterNo) 
    {
        this.promoterNo = promoterNo;
    }

    public String getPromoterNo() 
    {
        return promoterNo;
    }
    public void setPromoter(String promoter) 
    {
        this.promoter = promoter;
    }

    public String getPromoter() 
    {
        return promoter;
    }
    public void setPromoterOrgId(String promoterOrgId) 
    {
        this.promoterOrgId = promoterOrgId;
    }

    public String getPromoterOrgId() 
    {
        return promoterOrgId;
    }
    public void setPromoterOrg(String promoterOrg) 
    {
        this.promoterOrg = promoterOrg;
    }

    public String getPromoterOrg() 
    {
        return promoterOrg;
    }
    public void setNode(String node) 
    {
        this.node = node;
    }

    public String getNode() 
    {
        return node;
    }
    public void setReceiverUserCode(String receiverUserCode) 
    {
        this.receiverUserCode = receiverUserCode;
    }

    public String getReceiverUserCode() 
    {
        return receiverUserCode;
    }
    public void setProcedureName(String procedureName) 
    {
        this.procedureName = procedureName;
    }

    public String getProcedureName() 
    {
        return procedureName;
    }
    public void setProcedureNo(String procedureNo) 
    {
        this.procedureNo = procedureNo;
    }

    public String getProcedureNo() 
    {
        return procedureNo;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setSequenceNo(String sequenceNo) 
    {
        this.sequenceNo = sequenceNo;
    }

    public String getSequenceNo() 
    {
        return sequenceNo;
    }
    public void setInitiationTime(Date initiationTime) 
    {
        this.initiationTime = initiationTime;
    }

    public Date getInitiationTime() 
    {
        return initiationTime;
    }
    public void setReceiverTime(Date receiverTime) 
    {
        this.receiverTime = receiverTime;
    }

    public Date getReceiverTime() 
    {
        return receiverTime;
    }
    public void setPcUrl(String pcUrl) 
    {
        this.pcUrl = pcUrl;
    }

    public String getPcUrl() 
    {
        return pcUrl;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setFlag(String flag) 
    {
        this.flag = flag;
    }

    public String getFlag() 
    {
        return flag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("systemName", getSystemName())
            .append("pendType", getPendType())
            .append("promoterNo", getPromoterNo())
            .append("promoter", getPromoter())
            .append("promoterOrgId", getPromoterOrgId())
            .append("promoterOrg", getPromoterOrg())
            .append("node", getNode())
            .append("receiverUserCode", getReceiverUserCode())
            .append("procedureName", getProcedureName())
            .append("procedureNo", getProcedureNo())
            .append("title", getTitle())
            .append("sequenceNo", getSequenceNo())
            .append("initiationTime", getInitiationTime())
            .append("receiverTime", getReceiverTime())
            .append("pcUrl", getPcUrl())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("flag", getFlag())
            .toString();
    }
}
