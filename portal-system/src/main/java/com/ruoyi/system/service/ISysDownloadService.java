package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SysDownload;
import com.ruoyi.system.domain.SysIntserv;

/**
 * 文件服务配置Service接口
 * 
 * @author tjgc
 * @date 2020-12-30
 */

public interface ISysDownloadService 
{
    /**
     * 查询文件服务配置
     * 
     * @param id 文件服务配置ID
     * @return 文件服务配置
     */
    public SysDownload selectSysDownloadById(Long id);

    /**
     * 查询文件服务配置列表
     * 
     * @param sysDownload 文件服务配置
     * @return 文件服务配置集合
     */
    public List<SysDownload> selectSysDownloadList(SysDownload sysDownload);


    /**
     * 查询文件服务配置列表(前台使用)
     *
     * @param sysDownload 文件服务配置
     * @return 文件服务配置集合
     */
    public List<SysDownload> selectSysDownloadAllList(SysDownload sysDownload);

    /**
     * 新增文件服务配置
     * 
     * @param sysDownload 文件服务配置
     * @return 结果
     */
    public int insertSysDownload(SysDownload sysDownload);

    /**
     * 修改文件服务配置
     * 
     * @param sysDownload 文件服务配置
     * @return 结果
     */
    public int updateSysDownload(SysDownload sysDownload);

    /**
     * 批量删除文件服务配置
     * 
     * @param ids 需要删除的文件服务配置ID
     * @return 结果
     */
    public int deleteSysDownloadByIds(Long[] ids);

    /**
     * 删除文件服务配置信息
     * 
     * @param id 文件服务配置ID
     * @return 结果
     */
    public int deleteSysDownloadById(Long id);

    /**
     * 修改状态
     * @param sysDownload
     * @return
     */
    public int updateSysDownloadStatus(SysDownload sysDownload);
}
