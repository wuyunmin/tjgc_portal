package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UserInfoProjectMapper;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.UserInfoProject;
import com.ruoyi.system.domain.UserInfoXiongan;
import com.ruoyi.system.service.IUserInfoProjectService;

/**
 * 项目协同平台用户信息Service业务层处理
 * 
 * @author tjec
 * @date 2021-01-28
 */
@Service
public class UserInfoProjectServiceImpl implements IUserInfoProjectService 
{
    @Autowired
    private UserInfoProjectMapper userInfoProjectMapper;

    /**
     * 查询项目协同平台用户信息
     * 
     * @param guid 项目协同平台用户信息ID
     * @return 项目协同平台用户信息
     */
    @Override
    public UserInfoProject selectUserInfoProjectByGuid(String guid)
    {
        return userInfoProjectMapper.selectUserInfoProjectByGuid(guid);
    }

    /**
     * 查询项目协同平台用户信息列表
     * 
     * @param userInfoProject 项目协同平台用户信息
     * @return 项目协同平台用户信息
     */
    @Override
    public List<UserInfoProject> selectUserInfoProjectList(UserInfoProject userInfoProject)
    {
        return userInfoProjectMapper.selectUserInfoProjectList(userInfoProject);
    }

    /**
     * 新增项目协同平台用户信息
     * 
     * @param userInfoProject 项目协同平台用户信息
     * @return 结果
     */
    @Override
    public int insertUserInfoProject(UserInfoProject userInfoProject)
    {
        return userInfoProjectMapper.insertUserInfoProject(userInfoProject);
    }

    /**
     * 修改项目协同平台用户信息
     * 
     * @param userInfoProject 项目协同平台用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoProject(UserInfoProject userInfoProject)
    {
        return userInfoProjectMapper.updateUserInfoProject(userInfoProject);
    }

    /**
     * 批量删除项目协同平台用户信息
     * 
     * @param guids 需要删除的项目协同平台用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoProjectByGuids(String[] guids)
    {
        return userInfoProjectMapper.deleteUserInfoProjectByGuids(guids);
    }

    /**
     * 删除项目协同平台用户信息信息
     * 
     * @param guid 项目协同平台用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoProjectByGuid(String guid)
    {
        return userInfoProjectMapper.deleteUserInfoProjectByGuid(guid);
    }

	@Override
	public String importUser(List<UserInfoProject> userInfoProjectList, String operName) {
		//1、先删除所有内部成员原表数据
		userInfoProjectMapper.cleanProjectUser();
		//2、导入新数据
		if (StringUtils.isNull(userInfoProjectList) || userInfoProjectList.size() == 0) {
			throw new CustomException("导入用户数据不能为空！");
		}
		int successNum = 0;
		int failureNum = 0;
		StringBuilder successMsg = new StringBuilder();
		StringBuilder failureMsg = new StringBuilder();
		for (UserInfoProject userInfoProject : userInfoProjectList) {
			try {
				userInfoProject.setCreateBy(operName);
				userInfoProject.setUserFlag("内部用户");
				userInfoProject.setUserNo(userInfoProject.getUserNo()==null?"":userInfoProject.getUserNo().trim());
				userInfoProject.setCellPhone(userInfoProject.getCellPhone()==null?"":userInfoProject.getCellPhone().trim());
				this.insertUserInfoProject(userInfoProject);
				successNum++;
				//successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 导入成功");
			} catch (Exception e) {
				failureNum++;
				String msg = "<br/>" + failureNum + "、姓名 " + userInfoProject.getUserName() + " 导入失败：";
				failureMsg.append(msg + e.getMessage());
			}
		}
		if (failureNum > 0) {
			failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
			throw new CustomException(failureMsg.toString());
		} else {
			successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
		}
		return successMsg.toString();
	}

	@Override
	public String importOutMemberUser(List<UserInfoProject> userInfoProjectList, String operName) {
		//1、先删除所有内部成员原表数据
		userInfoProjectMapper.cleanProjectUserOutMember();
		//2、导入新数据
		if (StringUtils.isNull(userInfoProjectList) || userInfoProjectList.size() == 0) {
			throw new CustomException("导入用户数据不能为空！");
		}
		int successNum = 0;
		int failureNum = 0;
		StringBuilder successMsg = new StringBuilder();
		StringBuilder failureMsg = new StringBuilder();
		for (UserInfoProject userInfoProject : userInfoProjectList) {
			try {
				userInfoProject.setCreateBy(operName);
				userInfoProject.setUserFlag("外部用户");
				this.insertUserInfoProject(userInfoProject);
				successNum++;
				//successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 导入成功");
			} catch (Exception e) {
				failureNum++;
				String msg = "<br/>" + failureNum + "、姓名 " + userInfoProject.getUserName() + " 导入失败：";
				failureMsg.append(msg + e.getMessage());
			}
		}
		if (failureNum > 0) {
			failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
			throw new CustomException(failureMsg.toString());
		} else {
			successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
		}
		return successMsg.toString();
	}
}
