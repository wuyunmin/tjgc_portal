package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UserInfoSchoolMapper;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.UserInfoSchool;
import com.ruoyi.system.domain.UserInfoTb;
import com.ruoyi.system.service.IUserInfoSchoolService;

/**
 * 企业培训学校用户信息Service业务层处理
 * 
 * @author tjec
 * @date 2021-01-28
 */
@Service
public class UserInfoSchoolServiceImpl implements IUserInfoSchoolService 
{
    @Autowired
    private UserInfoSchoolMapper userInfoSchoolMapper;

    /**
     * 查询企业培训学校用户信息
     * 
     * @param guid 企业培训学校用户信息ID
     * @return 企业培训学校用户信息
     */
    @Override
    public UserInfoSchool selectUserInfoSchoolByGuid(String guid)
    {
        return userInfoSchoolMapper.selectUserInfoSchoolByGuid(guid);
    }

    /**
     * 查询企业培训学校用户信息列表
     * 
     * @param userInfoSchool 企业培训学校用户信息
     * @return 企业培训学校用户信息
     */
    @Override
    public List<UserInfoSchool> selectUserInfoSchoolList(UserInfoSchool userInfoSchool)
    {
        return userInfoSchoolMapper.selectUserInfoSchoolList(userInfoSchool);
    }

    /**
     * 新增企业培训学校用户信息
     * 
     * @param userInfoSchool 企业培训学校用户信息
     * @return 结果
     */
    @Override
    public int insertUserInfoSchool(UserInfoSchool userInfoSchool)
    {
        return userInfoSchoolMapper.insertUserInfoSchool(userInfoSchool);
    }

    /**
     * 修改企业培训学校用户信息
     * 
     * @param userInfoSchool 企业培训学校用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoSchool(UserInfoSchool userInfoSchool)
    {
        return userInfoSchoolMapper.updateUserInfoSchool(userInfoSchool);
    }

    /**
     * 批量删除企业培训学校用户信息
     * 
     * @param guids 需要删除的企业培训学校用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoSchoolByGuids(String[] guids)
    {
        return userInfoSchoolMapper.deleteUserInfoSchoolByGuids(guids);
    }

    /**
     * 删除企业培训学校用户信息信息
     * 
     * @param guid 企业培训学校用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoSchoolByGuid(String guid)
    {
        return userInfoSchoolMapper.deleteUserInfoSchoolByGuid(guid);
    }

	@Override
	public String importUser(List<UserInfoSchool> userInfoSchoolList, String operName) {
		//1、先删除所有原表数据
		userInfoSchoolMapper.cleanSchoolUser();
		//2、导入新数据
		if (StringUtils.isNull(userInfoSchoolList) || userInfoSchoolList.size() == 0) {
			throw new CustomException("导入用户数据不能为空！");
		}
		int successNum = 0;
		int failureNum = 0;
		StringBuilder successMsg = new StringBuilder();
		StringBuilder failureMsg = new StringBuilder();
		for (UserInfoSchool userInfoSchool : userInfoSchoolList) {
			try {
				userInfoSchool.setCreateBy(operName);
				userInfoSchool.setUserName(userInfoSchool.getUserName()==null?"":userInfoSchool.getUserName().trim());
				this.insertUserInfoSchool(userInfoSchool);
				successNum++;
				//successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 导入成功");
			} catch (Exception e) {
				failureNum++;
				String msg = "<br/>" + failureNum + "、姓名 " + userInfoSchool.getUserName() + " 导入失败：";
				failureMsg.append(msg + e.getMessage());
			}
		}
		if (failureNum > 0) {
			failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
			throw new CustomException(failureMsg.toString());
		} else {
			successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
		}
		return successMsg.toString();
	}
}
