package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.SysSsoConfigVo;

/**
 * 用户单点登录配置Service接口
 * 
 * @author tjgc
 * @date 2020-12-21
 */
public interface ISysSsoConfigVoService
{
    /**
     * 查询用户单点登录配置
     * 
     * @param id 用户单点登录配置ID
     * @return 用户单点登录配置
     */
    public SysSsoConfigVo selectSysSsoConfigVOById(Long id);

    public SysSsoConfigVo selectSysSsoConfigVOByName(String name);

    /**
     * 查询用户单点登录配置列表
     * 
     * @param sysSsoConfigVO 用户单点登录配置
     * @return 用户单点登录配置集合
     */
    public List<SysSsoConfigVo> selectSysSsoConfigVOList(SysSsoConfigVo sysSsoConfigVO);


    public List<SysSsoConfigVo> selectSysSsoConfigList(SysSsoConfigVo sysSsoConfigVO);
    /**
     * 新增用户单点登录配置
     * 
     * @param sysSsoConfigVO 用户单点登录配置
     * @return 结果
     */
    public int insertSysSsoConfigVO(SysSsoConfigVo sysSsoConfigVO);

    /**
     * 修改用户单点登录配置
     * 
     * @param sysSsoConfigVO 用户单点登录配置
     * @return 结果
     */
    public int updateSysSsoConfigVO(SysSsoConfigVo sysSsoConfigVO);

    /**
     * 批量删除用户单点登录配置
     * 
     * @param ids 需要删除的用户单点登录配置ID
     * @return 结果
     */
    public int deleteSysSsoConfigVOByIds(Long[] ids);

    /**
     * 删除用户单点登录配置信息
     * 
     * @param id 用户单点登录配置ID
     * @return 结果
     */
    public int deleteSysSsoConfigVOById(Long id);


    /**
     * 修改用户单点登录配置状态
     * @param sysSsoConfigVO
     * @return
     */
    public int updateSysSsoConfigStatus(SysSsoConfigVo sysSsoConfigVO);

    public List<String> selectAuthSystemList(String userNo);

    public List<String> selectAuthSystemListByPhone(String phone);

    public List<SysSsoConfigVo> selectAuthSysSsoConfigVOList(SysSsoConfigVo sysSsoConfigVO);
}
