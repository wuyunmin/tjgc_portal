package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.UnifyUserInfo;

/**
 * 统一用户信息Service接口
 *
 * @author tjgc
 * @date 2022-03-19
 */
public interface IUnifyUserInfoService
{
    /**
     * 查询统一用户信息
     *
     * @param id 统一用户信息ID
     * @return 统一用户信息
     */
    public UnifyUserInfo selectUnifyUserInfoById(Long id);

    /**
     * 查询统一用户信息列表
     *
     * @param unifyUserInfo 统一用户信息
     * @return 统一用户信息集合
     */
    public List<UnifyUserInfo> selectUnifyUserInfoList(UnifyUserInfo unifyUserInfo);

    /**
     * 新增统一用户信息
     *
     * @param unifyUserInfo 统一用户信息
     * @return 结果
     */
    public int insertUnifyUserInfo(UnifyUserInfo unifyUserInfo);

    /**
     * 批量插入
     * @param unifyUserInfos
     * @return
     */
    public int insertBatchUnifyUserInfo(List<UnifyUserInfo> unifyUserInfos);

    /**
     * 修改统一用户信息
     *
     * @param unifyUserInfo 统一用户信息
     * @return 结果
     */
    public int updateUnifyUserInfo(UnifyUserInfo unifyUserInfo);

    /**
     * 批量删除统一用户信息
     *
     * @param ids 需要删除的统一用户信息ID
     * @return 结果
     */
    public int deleteUnifyUserInfoByIds(Long[] ids);

    /**
     * 删除统一用户信息信息
     *
     * @param id 统一用户信息ID
     * @return 结果
     */
    public int deleteUnifyUserInfoById(Long id);

    /**
     * 清空统一用户信息
     */
    public void cleanUnifyUserInfo();


    public void initUnifyUserInfo(String userName);
}
