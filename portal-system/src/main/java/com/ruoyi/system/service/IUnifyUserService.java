package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.UnifyUser;

/**
 * 部门同步中间Service接口
 *
 * @author tjgc
 * @date 2021-01-20
 */
public interface IUnifyUserService
{
	public List<UnifyUser> selectUserList(UnifyUser unifyUser);

	public void UserList(String unifyUserInfo);
}
