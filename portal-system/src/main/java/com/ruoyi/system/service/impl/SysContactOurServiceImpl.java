package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysContactOurMapper;
import com.ruoyi.system.domain.SysContactOur;
import com.ruoyi.system.service.ISysContactOurService;

/**
 * 联系我们Service业务层处理
 * 
 * @author pangyongfeng
 * @date 2021-03-09
 */
@Service
public class SysContactOurServiceImpl implements ISysContactOurService 
{
    @Autowired
    private SysContactOurMapper sysContactOurMapper;

    /**
     * 查询联系我们
     * 
     * @param guid 联系我们ID
     * @return 联系我们
     */
    @Override
    public SysContactOur selectSysContactOurByGuid(String guid)
    {
        return sysContactOurMapper.selectSysContactOurByGuid(guid);
    }

    /**
     * 查询联系我们列表
     * 
     * @param sysContactOur 联系我们
     * @return 联系我们
     */
    @Override
    public List<SysContactOur> selectSysContactOurList(SysContactOur sysContactOur)
    {
        return sysContactOurMapper.selectSysContactOurList(sysContactOur);
    }

    /**
     * 新增联系我们
     * 
     * @param sysContactOur 联系我们
     * @return 结果
     */
    @Override
    public int insertSysContactOur(SysContactOur sysContactOur)
    {
        sysContactOur.setCreateTime(DateUtils.getNowDate());
        return sysContactOurMapper.insertSysContactOur(sysContactOur);
    }

    /**
     * 修改联系我们
     * 
     * @param sysContactOur 联系我们
     * @return 结果
     */
    @Override
    public int updateSysContactOur(SysContactOur sysContactOur)
    {
        sysContactOur.setUpdateTime(DateUtils.getNowDate());
        return sysContactOurMapper.updateSysContactOur(sysContactOur);
    }

    /**
     * 批量删除联系我们
     * 
     * @param guids 需要删除的联系我们ID
     * @return 结果
     */
    @Override
    public int deleteSysContactOurByGuids(String[] guids)
    {
        return sysContactOurMapper.deleteSysContactOurByGuids(guids);
    }

    /**
     * 删除联系我们信息
     * 
     * @param guid 联系我们ID
     * @return 结果
     */
    @Override
    public int deleteSysContactOurByGuid(String guid)
    {
        return sysContactOurMapper.deleteSysContactOurByGuid(guid);
    }

	@Override
	public List<SysContactOur> getContactOur() {
		
		return sysContactOurMapper.getContactOur();
	}
}
