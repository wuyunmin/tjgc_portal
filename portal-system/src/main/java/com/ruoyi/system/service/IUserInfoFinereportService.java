package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.UserInfoFinereport;

/**
 * 帆软用户信息Service接口
 * 
 * @author tjgc
 * @date 2021-04-22
 */
public interface IUserInfoFinereportService 
{
    /**
     * 查询帆软用户信息
     * 
     * @param id 帆软用户信息ID
     * @return 帆软用户信息
     */
    public UserInfoFinereport selectUserInfoFinereportById(Long id);

    /**
     * 查询帆软用户信息列表
     * 
     * @param userInfoFinereport 帆软用户信息
     * @return 帆软用户信息集合
     */
    public List<UserInfoFinereport> selectUserInfoFinereportList(UserInfoFinereport userInfoFinereport);

    /**
     * 新增帆软用户信息
     * 
     * @param userInfoFinereport 帆软用户信息
     * @return 结果
     */
    public int insertUserInfoFinereport(UserInfoFinereport userInfoFinereport);

    /**
     * 修改帆软用户信息
     * 
     * @param userInfoFinereport 帆软用户信息
     * @return 结果
     */
    public int updateUserInfoFinereport(UserInfoFinereport userInfoFinereport);

    /**
     * 批量删除帆软用户信息
     * 
     * @param ids 需要删除的帆软用户信息ID
     * @return 结果
     */
    public int deleteUserInfoFinereportByIds(Long[] ids);

    /**
     * 删除帆软用户信息信息
     * 
     * @param id 帆软用户信息ID
     * @return 结果
     */
    public int deleteUserInfoFinereportById(Long id);
}
