package com.ruoyi.system.service;

import com.ruoyi.system.domain.UserInfoTb;
import com.ruoyi.system.domain.UserInfoXiaoyu;

import java.util.List;

/**
 * teambition用户信息Service接口
 * 
 * @author tjec
 * @date 2021-01-28
 */
public interface IUserInfoTbService 
{
    /**
     * 查询teambition用户信息
     * 
     * @param guid teambition用户信息ID
     * @return teambition用户信息
     */
    public UserInfoTb selectUserInfoTbByGuid(String guid);

    /**
     * 查询teambition用户信息列表
     * 
     * @param userInfoTb teambition用户信息
     * @return teambition用户信息集合
     */
    public List<UserInfoTb> selectUserInfoTbList(UserInfoTb userInfoTb);

    /**
     * 新增teambition用户信息
     * 
     * @param userInfoTb teambition用户信息
     * @return 结果
     */
    public int insertUserInfoTb(UserInfoTb userInfoTb);

    /**
     * 修改teambition用户信息
     * 
     * @param userInfoTb teambition用户信息
     * @return 结果
     */
    public int updateUserInfoTb(UserInfoTb userInfoTb);

    /**
     * 批量删除teambition用户信息
     * 
     * @param guids 需要删除的teambition用户信息ID
     * @return 结果
     */
    public int deleteUserInfoTbByGuids(String[] guids);

    /**
     * 删除teambition用户信息信息
     * 
     * @param guid teambition用户信息ID
     * @return 结果
     */
    public int deleteUserInfoTbByGuid(String guid);
    
    /**
     * 导入用户数据
     *
     * @param userList        用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName        操作用户
     * @return 结果
     */
    public String importUser(List<UserInfoTb> userInfoTbList, String operName);
}
