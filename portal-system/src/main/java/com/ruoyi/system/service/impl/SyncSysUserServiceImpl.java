package com.ruoyi.system.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SyncSysDept;
import com.ruoyi.system.domain.SyncSysTemp;
import com.ruoyi.system.domain.UserInfoBpm;
import com.ruoyi.system.mapper.SyncSysDeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SyncSysUserMapper;
import com.ruoyi.system.domain.SyncSysUser;
import com.ruoyi.system.service.ISyncSysUserService;

import javax.annotation.Resource;

import static com.ruoyi.common.utils.DateUtils.parseDate;

/**
 * 部门同步中间Service业务层处理
 *
 * @author tjgc
 * @date 2021-01-20
 */
@Service
public class SyncSysUserServiceImpl implements ISyncSysUserService {
    @Resource
    private SyncSysUserMapper syncSysUserMapper;


    @Resource
    private SyncSysDeptMapper syncSysDeptMapper;


    /**
     * 查询部门同步中间列表
     *
     * @param syncSysUser 部门同步中间
     * @return 部门同步中间
     */
    @Override
    public List<SyncSysUser> selectSyncSysUserList(SyncSysUser syncSysUser) {
        return syncSysUserMapper.selectSyncSysUserList(syncSysUser);
    }


    @Override
    public List<SyncSysUser> selectSyncSysUserListForHR(SyncSysUser syncSysUser) {
        return syncSysUserMapper.selectSyncSysUserListForHR(syncSysUser);
    }
    /**
     * 新增部门同步中间
     *
     * @param syncSysUser 部门同步中间
     * @return 结果
     */
    @Override
    public int insertSyncSysUser(SyncSysUser syncSysUser) {
        syncSysUser.setCreateTime(DateUtils.getNowDate());
        return syncSysUserMapper.insertSyncSysUser(syncSysUser);
    }

    @Override
    public SyncSysUser selectUserInfoHrById(Long id) {
        return syncSysUserMapper.selectUserInfoHrById(id);
    }
    /**
     * 新增部门同步中间
     *
     * @param syncSysUserMap 部门同步中间
     * @return 结果
     */
    @Override
    public int insertSyncSysUser(Map<String, Object> syncSysUserMap) {
        SyncSysUser syncSysUser = convertValue(syncSysUserMap);
        syncSysUser.setCreateTime(DateUtils.getNowDate());
        return syncSysUserMapper.insertSyncSysUser(syncSysUser);
    }

    @Override
    public int existSyncSysUser(String empId) {
        return syncSysUserMapper.existSyncSysUser(empId);
    }
    /**
     * 修改部门同步中间
     *
     * @param syncSysUser 部门同步中间
     * @return 结果
     */
    @Override
    public int updateSyncSysUser(SyncSysUser syncSysUser) {
        syncSysUser.setUpdateTime(DateUtils.getNowDate());
        return syncSysUserMapper.updateSyncSysUser(syncSysUser);
    }


    /**
     * 修改部门同步中间
     *
     * @param syncSysUserMap 部门同步中间
     * @return 结果
     */
    @Override
    public int updateSyncSysUser(Map<String, Object> syncSysUserMap) {
        SyncSysUser syncSysUser = convertValue(syncSysUserMap);
        syncSysUser.setUpdateTime(DateUtils.getNowDate());
        return syncSysUserMapper.updateSyncSysUser(syncSysUser);
    }

    /**
     * 批量删除部门同步中间
     *
     * @param ids 需要删除的部门同步中间ID
     * @return 结果
     */
    @Override
    public int deleteSyncSysUserByIds(Long[] ids) {
        return syncSysUserMapper.deleteSyncSysUserByIds(ids);
    }

    /**
     * 删除部门同步中间信息
     *
     * @param id 部门同步中间ID
     * @return 结果
     */
    @Override
    public int deleteSyncSysUserById(Long id) {
        return syncSysUserMapper.deleteSyncSysUserById(id);
    }

    @Override
    public List<SyncSysTemp> selectSyncTempList( ) {
        return syncSysUserMapper.selectSyncTempList();
    }

    @Override
    public int updataSyncTempStatus(String id) {
        return syncSysUserMapper.updataSyncTempStatus(id);
    }


    @Override
    public SyncSysUser selectUserByUserID(String id) {
        return syncSysUserMapper.selectUserByUserID(id);
    }

    private SyncSysUser convertValue(Map<String, Object> syncSysUserMap) {
        SyncSysUser syncSysUser = new SyncSysUser();
        System.out.println(syncSysUserMap.toString());
        // 员工全局ID
        Object empId = syncSysUserMap.get("emp_id");
        //HR中员工ID
        Object empIdCode = syncSysUserMap.get("emp_idcode");
        //员工编号
        Object empNumber = syncSysUserMap.get("emp_number");
        //员工名称
        Object empName = syncSysUserMap.get("emp_name");
        // 部门编号
        Object deptId = syncSysUserMap.get("dept_id");
        //HR部门ID
        Object deptIdCode = syncSysUserMap.get("dept_idcode");
        //员工移动电话号
        Object empMobilePhone = syncSysUserMap.get("emp_mobilephone");
        //hr的项目负责人编号
        Object empProjectLeader = syncSysUserMap.get("emp_projectleader");
        //hr的项目领款经办人编号
        Object empProjectPayee = syncSysUserMap.get("emp_projectpayee");
        //是否项目负责人
        Object empJudgeProjectLeader = syncSysUserMap.get("emp_judgeprojectleader");
        //是否项目领款经办人
        Object empJudgeProjectPayee = syncSysUserMap.get("emp_judgeprojectpayee");
        //在职状态编码
        Object empIncumbencyState = syncSysUserMap.get("emp_incumbencystate");
        //项目负责人全局ID
        Object empProjectLeaderId = syncSysUserMap.get("emp_projectleaderid");
        //领款经办人全局ID
        Object empProjectPayeeId = syncSysUserMap.get("emp_projectpayeeid");
        //性别
        Object empSex = syncSysUserMap.get("emp_sex");
        //主职
        Object partType = syncSysUserMap.get("part_type");
        //证件号码
        Object empIdNumber = syncSysUserMap.get("emp_idnumber");
        //证件类型
        Object idTypeContent = syncSysUserMap.get("idtype_content");
        //主管单位名称
        Object competentOrgContent = syncSysUserMap.get("competentorg_content");
        //在职状态
        Object incumbencyContent = syncSysUserMap.get("incumbency_content");
        //员工编号有效性
        Object empNumberEffectiveness = syncSysUserMap.get("emp_numbereffectiveness");
        //员工编号有效性
        Object empIsValidContent = syncSysUserMap.get("empisvalid_content");
        //人事资料有效期
        Object empDataValId = syncSysUserMap.get("emp_datavalid");
        //公司邮箱
        Object empCompanyEmail = syncSysUserMap.get("emp_companyemail");
        //人员类别ID
        Object empPersonnelType = syncSysUserMap.get("emp_personneltype");
        //人员类别
        Object personTypeContent = syncSysUserMap.get("persontype_content");
        //入职日期
        Object empEntryDate = syncSysUserMap.get("emp_entrydate");
        //离职日期
        Object empQuitDate = syncSysUserMap.get("emp_quitdate");
        //状态
        Object flag = syncSysUserMap.get("flag");

        syncSysUser.setEmpId(null == empId ? null : empId.toString());
        syncSysUser.setDeptId(null == deptId ? null : deptId.toString());
        syncSysUser.setCompetentOrgContent(null == competentOrgContent ? null : competentOrgContent.toString());
        syncSysUser.setDeptIdCode(null == deptIdCode ? null : deptIdCode.toString());
        syncSysUser.setEmpCompanyEmail(null == empCompanyEmail ? null : empCompanyEmail.toString());
        syncSysUser.setEmpDataValid(null == empDataValId ? null : parseDate(empDataValId));
        syncSysUser.setEmpEntryDate(null == empEntryDate ? null : parseDate(empEntryDate));
        syncSysUser.setEmpQuitDate(null == empQuitDate ? null : parseDate(empQuitDate));
        syncSysUser.setEmpIdCode(null == empIdCode ? null : empIdCode.toString());
        syncSysUser.setEmpNumber(null == empNumber ? null : empNumber.toString());
        syncSysUser.setEmpName(null == empName ? null : empName.toString());
        syncSysUser.setEmpMobilePhone(null == empMobilePhone ? null : empMobilePhone.toString());
        syncSysUser.setEmpProjectLeader(null == empProjectLeader ? null : empProjectLeader.toString());
        syncSysUser.setEmpProjectPayee(null == empProjectPayee ? null : empProjectPayee.toString());
        syncSysUser.setEmpJudgeProjectLeader(null == empJudgeProjectLeader ? null : empJudgeProjectLeader.toString());
        syncSysUser.setEmpJudgeProjectPayee(null == empJudgeProjectPayee ? null : empJudgeProjectPayee.toString());
        syncSysUser.setEmpIncumbencyState(null == empIncumbencyState ? null : empIncumbencyState.toString());
        syncSysUser.setEmpProjectLeaderId(null == empProjectLeaderId ? null : empProjectLeaderId.toString());
        syncSysUser.setEmpProjectPayeeId(null == empProjectPayeeId ? null : empProjectPayeeId.toString());
        syncSysUser.setEmpSex(null == empSex ? null : empSex.toString());
        syncSysUser.setPartType(null == partType ? null : partType.toString());
        syncSysUser.setEmpIdNumber(null == empIdNumber ? null : empIdNumber.toString());
        syncSysUser.setIdTypeContent(null == idTypeContent ? null : idTypeContent.toString());
        syncSysUser.setIncumbencyContent(null == incumbencyContent ? null : incumbencyContent.toString());
        syncSysUser.setEmpNumberEffectiveness(null == empNumberEffectiveness ? null : empNumberEffectiveness.toString());
        syncSysUser.setEmpIsvalidContent(null == empIsValidContent ? null : empIsValidContent.toString());
        syncSysUser.setEmpPersonnelType(null == empPersonnelType ? null : empPersonnelType.toString());
        syncSysUser.setPersonTypeContent(null == personTypeContent ? null : personTypeContent.toString());
        syncSysUser.setFlag(null == flag ? null : flag.toString());
        //设置未同步，同步标志（0代表未同步 1代表已同步）
        syncSysUser.setSyncFlag("0");
        return syncSysUser;
    }
}
