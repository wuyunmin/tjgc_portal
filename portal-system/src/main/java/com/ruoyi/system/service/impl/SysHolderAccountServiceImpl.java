package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysHolderAccountMapper;
import com.ruoyi.system.domain.SysHolderAccount;
import com.ruoyi.system.service.ISysHolderAccountService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author tjgc
 * @date 2021-02-04
 */
@Service
public class SysHolderAccountServiceImpl implements ISysHolderAccountService {
	@Autowired
	private SysHolderAccountMapper sysHolderAccountMapper;

	/**
	 * 查询【请填写功能名称】
	 * 
	 * @param guid 【请填写功能名称】ID
	 * @return 【请填写功能名称】
	 */
	@Override
	public List<SysHolderAccount> selectSysHolderAccountByUserId(long userId) {
		return sysHolderAccountMapper.selectSysHolderAccountByUserId(userId);
	}

	/**
	 * 查询【请填写功能名称】列表
	 * 
	 * @param sysHolderAccount 【请填写功能名称】
	 * @return 【请填写功能名称】
	 */
	@Override
	public List<SysHolderAccount> selectSysHolderAccountList(SysHolderAccount sysHolderAccount) {
		return sysHolderAccountMapper.selectSysHolderAccountList(sysHolderAccount);
	}

	/**
	 * 新增【请填写功能名称】
	 * 
	 * @param sysHolderAccount 【请填写功能名称】
	 * @return 结果
	 */
	@Override
	public int insertSysHolderAccount(List<SysHolderAccount> sysHolderAccounts, Long userId) {
		// 1、先根据userId查出这个人下有多少代持账号
		SysHolderAccount sysHolderAccount = new SysHolderAccount();
		sysHolderAccount.setUserId(sysHolderAccounts.get(0).getUserId());
		List<SysHolderAccount> oldList = this.selectSysHolderAccountList(sysHolderAccount);

		if (oldList != null && oldList.size() > 0) {// 不存在数据，直接添加

			// 不存在的
			List<SysHolderAccount> insertList = new ArrayList<SysHolderAccount>();
			// 存在需要修改的
			List<SysHolderAccount> updateList = new ArrayList<SysHolderAccount>();
			// 删除的
			List<SysHolderAccount> deleteList = new ArrayList<SysHolderAccount>();

			sysHolderAccounts.forEach(item -> {
				if (!StringUtils.isNotBlank(item.getGuid())) {
					item.setCreateBy(userId.toString());
					insertList.add(item);
				} else {
					oldList.forEach(it -> {
						if (item.getGuid().equals(it.getGuid())) {
							if(!StringUtils.isNotBlank(item.getRemark())) {
								item.setRemark("");
							}
							if(!StringUtils.isNotBlank(it.getRemark())) {
								it.setRemark("");
							}
							if (!item.getSystemName().equals(it.getSystemName())
									|| !item.getAccount().equals(it.getAccount())
									|| !item.getRemark().equals(it.getRemark())
									) {
								item.setUpdateBy(userId.toString());
								updateList.add(item);
							}
						}
					});
				}

			});

			oldList.forEach(item -> {
				List<String> strs = new ArrayList<String>();
				sysHolderAccounts.forEach(it -> {
					if (item.getGuid().equals(it.getGuid())) {
						strs.add(it.getGuid());
					}
				});

				if (!strs.contains(item.getGuid())) {
					deleteList.add(item);
				}
			});

			if (insertList.size() > 0) {
				insertList.forEach(item -> {
					sysHolderAccountMapper.insertSysHolderAccount(item);
				});
			}

			if (updateList.size() > 0) {
				updateList.forEach(item -> {
					this.updateSysHolderAccount(item);
				});
			}

			if (deleteList.size() > 0) {
				deleteList.forEach(item -> {
					this.deleteSysHolderAccountByGuid(item.getGuid());
				});
			}

		} else {// 直接添加
			sysHolderAccounts.forEach(item -> {
				item.setCreateBy(userId.toString());
				sysHolderAccountMapper.insertSysHolderAccount(item);
			});

		}

		// msReportType.setCreateTime(DateUtils.getNowDate());
		// bmsReportTypeMapper.insertBmsReportType(bmsReportType)
		return 1;
	}

	/**
	 * 修改【请填写功能名称】
	 * 
	 * @param sysHolderAccount 【请填写功能名称】
	 * @return 结果
	 */
	@Override
	public int updateSysHolderAccount(SysHolderAccount sysHolderAccount) {
		sysHolderAccount.setUpdateTime(DateUtils.getNowDate());
		return sysHolderAccountMapper.updateSysHolderAccount(sysHolderAccount);
	}

	/**
	 * 批量删除【请填写功能名称】
	 * 
	 * @param guids 需要删除的【请填写功能名称】ID
	 * @return 结果
	 */
	@Override
	public int deleteSysHolderAccountByGuids(String[] guids) {
		return sysHolderAccountMapper.deleteSysHolderAccountByGuids(guids);
	}

	/**
	 * 删除【请填写功能名称】信息
	 * 
	 * @param guid 【请填写功能名称】ID
	 * @return 结果
	 */
	@Override
	public int deleteSysHolderAccountByGuid(String guid) {
		return sysHolderAccountMapper.deleteSysHolderAccountByGuid(guid);
	}

	@Override
	public int deleteSysHolderAccountByUserId(Long userId) {
		return sysHolderAccountMapper.deleteSysHolderAccountByUserId(userId);
	}
}
