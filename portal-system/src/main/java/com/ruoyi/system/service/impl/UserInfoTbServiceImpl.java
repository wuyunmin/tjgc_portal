package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UserInfoTbMapper;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.UserInfoEmail;
import com.ruoyi.system.domain.UserInfoTb;
import com.ruoyi.system.domain.UserInfoXiaoyu;
import com.ruoyi.system.service.IUserInfoTbService;

/**
 * teambition用户信息Service业务层处理
 * 
 * @author tjec
 * @date 2021-01-28
 */
@Service
public class UserInfoTbServiceImpl implements IUserInfoTbService 
{
    @Autowired
    private UserInfoTbMapper userInfoTbMapper;

    /**
     * 查询teambition用户信息
     * 
     * @param guid teambition用户信息ID
     * @return teambition用户信息
     */
    @Override
    public UserInfoTb selectUserInfoTbByGuid(String guid)
    {
        return userInfoTbMapper.selectUserInfoTbByGuid(guid);
    }

    /**
     * 查询teambition用户信息列表
     * 
     * @param userInfoTb teambition用户信息
     * @return teambition用户信息
     */
    @Override
    public List<UserInfoTb> selectUserInfoTbList(UserInfoTb userInfoTb)
    {
        return userInfoTbMapper.selectUserInfoTbList(userInfoTb);
    }

    /**
     * 新增teambition用户信息
     * 
     * @param userInfoTb teambition用户信息
     * @return 结果
     */
    @Override
    public int insertUserInfoTb(UserInfoTb userInfoTb)
    {
        return userInfoTbMapper.insertUserInfoTb(userInfoTb);
    }

    /**
     * 修改teambition用户信息
     * 
     * @param userInfoTb teambition用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoTb(UserInfoTb userInfoTb)
    {
        return userInfoTbMapper.updateUserInfoTb(userInfoTb);
    }

    /**
     * 批量删除teambition用户信息
     * 
     * @param guids 需要删除的teambition用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoTbByGuids(String[] guids)
    {
        return userInfoTbMapper.deleteUserInfoTbByGuids(guids);
    }

    /**
     * 删除teambition用户信息信息
     * 
     * @param guid teambition用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoTbByGuid(String guid)
    {
        return userInfoTbMapper.deleteUserInfoTbByGuid(guid);
    }

	@Override
	public String importUser(List<UserInfoTb> userInfoTbList, String operName) {
		//1、先删除所有原表数据
		userInfoTbMapper.cleanTbUser();
		//2、导入新数据
		if (StringUtils.isNull(userInfoTbList) || userInfoTbList.size() == 0) {
			throw new CustomException("导入用户数据不能为空！");
		}
		int successNum = 0;
		int failureNum = 0;
		StringBuilder successMsg = new StringBuilder();
		StringBuilder failureMsg = new StringBuilder();
		for (UserInfoTb userInfoTb : userInfoTbList) {
			try {
				userInfoTb.setCreateBy(operName);
				userInfoTb.setUserNo(userInfoTb.getUserNo()==null?"":userInfoTb.getUserNo().trim());
				this.insertUserInfoTb(userInfoTb);
				successNum++;
				//successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 导入成功");
			} catch (Exception e) {
				failureNum++;
				String msg = "<br/>" + failureNum + "、姓名 " + userInfoTb.getUserName() + " 导入失败：";
				failureMsg.append(msg + e.getMessage());
			}
		}
		if (failureNum > 0) {
			failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
			throw new CustomException(failureMsg.toString());
		} else {
			successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
		}
		return successMsg.toString();
	}
}
