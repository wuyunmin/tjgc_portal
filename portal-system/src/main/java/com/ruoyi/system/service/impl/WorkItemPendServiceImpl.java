package com.ruoyi.system.service.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;


import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.mapper.SysDeptMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.WorkItemPendMapper;
import com.ruoyi.system.domain.WorkItemPend;
import com.ruoyi.system.service.IWorkItemPendService;

import javax.annotation.Resource;

import static com.ruoyi.common.utils.DateUtils.parseDate;

/**
 * 待办待阅信息Service业务层处理
 *
 * @author tjgc
 * @date 2021-01-07
 */
@Service
public class WorkItemPendServiceImpl implements IWorkItemPendService
{

    private static final Logger log = LoggerFactory.getLogger(WorkItemPendServiceImpl.class);

    @Resource
    private WorkItemPendMapper workItemPendMapper;


    @Resource
    private SysDeptMapper sysDeptMapper;

    /**
     * 查询待办待阅信息
     *
     * @param id 待办待阅信息ID
     * @return 待办待阅信息
     */
    @Override
    public WorkItemPend selectWorkItemPendById(Long id)
    {
        return workItemPendMapper.selectWorkItemPendById(id);
    }

    /**
     * 查询待办待阅信息列表
     *
     * @param workItemPend 待办待阅信息
     * @return 待办待阅信息
     */
    @Override
    public List<WorkItemPend> selectWorkItemPendList(WorkItemPend workItemPend)
    {
        return workItemPendMapper.selectWorkItemPendList(workItemPend);
    }


    @Override
    public List<WorkItemPend> selectAllWorkItemPendList(WorkItemPend workItemPend)
    {
        return workItemPendMapper.selectAllWorkItemPendList(workItemPend);
    }

    /**
     * 新增待办待阅信息
     *
     * @param workItemPend 待办待阅信息
     * @return 结果
     */
    @Override
    public int insertWorkItemPend(WorkItemPend workItemPend)
    {
        workItemPend.setCreateTime(DateUtils.getNowDate());
        return workItemPendMapper.insertWorkItemPend(workItemPend);
    }


    @Override
    public int insertOrUpdateWorkItemPendBpm(Map<String,Object> workItemPendMap)
    {

        WorkItemPend workItemPendObj =(WorkItemPend) map2Object(workItemPendMap, WorkItemPend.class);
        SysDept sysDept = sysDeptMapper.selectDeptByDeptName(workItemPendObj.getPromoterOrg());
        int i = workItemPendMapper.selectCountWorkItemPendBySequenceNo(workItemPendObj.getSequenceNo());
        if (i > 0) {
            log.info("更新Bpm待办");
            workItemPendObj.setPromoterOrgId(null == sysDept ? null :sysDept.getDeptId().toString());
            workItemPendObj.setUpdateTime(DateUtils.getNowDate());
            return workItemPendMapper.updateWorkItemPendBySequenceNoInBpm(workItemPendObj);
        }else {
            log.info("新增Bpm待办");
            workItemPendObj.setPromoterOrgId(null == sysDept ? null :sysDept.getDeptId().toString());
            workItemPendObj.setCreateTime(DateUtils.getNowDate());
            return workItemPendMapper.insertWorkItemPend(workItemPendObj);
        }

    }

    @Override
    public int insertOrUpdateWorkItemPendCommon(Map<String,Object> workItemPendMap) {
        WorkItemPend workItemPendObj =(WorkItemPend) map2Object(workItemPendMap, WorkItemPend.class);
        int i = workItemPendMapper.selectCountWorkItemPendBySequenceNo(workItemPendObj.getSequenceNo());
        if (i>0){
            workItemPendObj.setUpdateTime(DateUtils.getNowDate());
            return workItemPendMapper.updateWorkItemPendBySequenceNoCommon(workItemPendObj);
        }else {
            workItemPendObj.setCreateTime(DateUtils.getNowDate());
            return workItemPendMapper.insertWorkItemPend(workItemPendObj);
        }
    }

    @Override
    public int insertWorkItemPend(Map<String,Object> workItemPendMap)
    {
        WorkItemPend workItemPendObj =(WorkItemPend) map2Object(workItemPendMap, WorkItemPend.class);
        workItemPendObj.setCreateTime(DateUtils.getNowDate());
        SysDept sysDept = sysDeptMapper.selectDeptByDeptName(workItemPendObj.getPromoterOrg());
        workItemPendObj.setPromoterOrgId(null == sysDept ? null : sysDept.getDeptId().toString());
        return workItemPendMapper.insertWorkItemPend(workItemPendObj);
    }


    @Override
    public int insertHrWorkItemPend(Map<String,Object> workItemPendMap)
    {
        WorkItemPend workItemPendObj =(WorkItemPend) map2Object(workItemPendMap, WorkItemPend.class);
        SysDept sysDept = sysDeptMapper.selectDeptByDeptAllContent(workItemPendObj.getPromoterOrg());
        workItemPendObj.setPromoterOrgId(null == sysDept ? null :sysDept.getDeptId().toString());
        workItemPendObj.setCreateTime(DateUtils.getNowDate());
        return workItemPendMapper.insertWorkItemPend(workItemPendObj);
    }

    /**
     * 修改待办待阅信息
     *
     * @param workItemPend 待办待阅信息
     * @return 结果
     */
    @Override
    public int updateWorkItemPend(WorkItemPend workItemPend)
    {
        workItemPend.setUpdateTime(DateUtils.getNowDate());
        return workItemPendMapper.updateWorkItemPend(workItemPend);
    }

    /**
     * 修改待办待阅信息
     *
     * @param workItemPend 待办待阅信息
     * @return 结果
     */
    @Override
    public int updateWorkItemPendStatus(WorkItemPend workItemPend)
    {
        workItemPend.setUpdateTime(DateUtils.getNowDate());
        return workItemPendMapper.updateWorkItemPendStatus(workItemPend);
    }


    /**
     * 批量删除待办待阅信息
     *
     * @param ids 需要删除的待办待阅信息ID
     * @return 结果
     */
    @Override
    public int deleteWorkItemPendByIds(Long[] ids)
    {
        return workItemPendMapper.deleteWorkItemPendByIds(ids);
    }

    /**
     * 删除待办待阅信息信息
     *
     * @param id 待办待阅信息ID
     * @return 结果
     */
    @Override
    public int deleteWorkItemPendById(Long id)
    {
        return workItemPendMapper.deleteWorkItemPendById(id);
    }

    @Override
    public int selectWorkItemPendCountBySystem(String systemName,String userNo)
    {
        return workItemPendMapper.selectWorkItemPendCountBySystem(systemName,userNo);
    }


    @Override
    public int selectWorkItemPendCountBpm(List<String> systemNames,String userNo)
    {
        return workItemPendMapper.selectWorkItemPendCountBpm(systemNames,userNo);
    }
    /**
     * 查询待办待阅信息通过流程ID和系统名称
     * @param id
     * @return
     */
    @Override
    public WorkItemPend selectWorkItemPendAndSSOById(Long id) {
        return workItemPendMapper.selectWorkItemPendAndSSOById(id);
    }

    /**
     * 统计待办待阅信息列表
     * @param workItemPend 待办待阅信息
     * @return
     */
    @Override
    public int selectCountWorkItemPend(WorkItemPend workItemPend) {
        return workItemPendMapper.selectCountWorkItemPend(workItemPend);
    }

    @Override
    public int selectCountWorkItemPendBySequenceNo(String sequenceNo) {
        return workItemPendMapper.selectCountWorkItemPendBySequenceNo(sequenceNo);
    }

    @Override
    public int updateWorkItemPendBySequenceNoCommon(WorkItemPend workItemPend) {
        workItemPend.setUpdateTime(DateUtils.getNowDate());
        return workItemPendMapper.updateWorkItemPendBySequenceNoCommon(workItemPend);
    }


    private static Object map2Object(Map<String, Object> map, Class<?> clazz) {


        if (map == null) {
            return null;
        }
        Object obj = null;
        try {
            obj = clazz.newInstance();


            Field[] fields = obj.getClass().getDeclaredFields();
            for (Field field : fields) {
                int mod = field.getModifiers();
                if (Modifier.isStatic(mod) || Modifier.isFinal(mod)) {
                    continue;
                }

                Type genericType = field.getGenericType();
                if ("class java.util.Date".equals(genericType.toString())){
                    field.setAccessible(true);
                    field.set(obj, parseDate(map.get(field.getName())));
                }else {
                    field.setAccessible(true);
                    field.set(obj, map.get(field.getName()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

}
