package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.UserInfoGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UserInfoDingMapper;
import com.ruoyi.system.domain.UserInfoDing;
import com.ruoyi.system.service.IUserInfoDingService;

import javax.annotation.Resource;

/**
 * 钉钉用户信息Service业务层处理
 * 
 * @author tjgc
 * @date 2021-04-21
 */
@Service
public class UserInfoDingServiceImpl implements IUserInfoDingService 
{
    @Resource
    private UserInfoDingMapper userInfoDingMapper;

    /**
     * 查询钉钉用户信息
     * 
     * @param id 钉钉用户信息ID
     * @return 钉钉用户信息
     */
    @Override
    public UserInfoDing selectUserInfoDingById(Long id)
    {
        return userInfoDingMapper.selectUserInfoDingById(id);
    }

    /**
     * 查询钉钉用户信息列表
     * 
     * @param userInfoDing 钉钉用户信息
     * @return 钉钉用户信息
     */
    @Override
    public List<UserInfoDing> selectUserInfoDingList(UserInfoDing userInfoDing)
    {
        return userInfoDingMapper.selectUserInfoDingList(userInfoDing);
    }

    /**
     * 新增钉钉用户信息
     * 
     * @param userInfoDing 钉钉用户信息
     * @return 结果
     */
    @Override
    public int insertUserInfoDing(UserInfoDing userInfoDing)
    {
        userInfoDing.setCreateTime(DateUtils.getNowDate());
        return userInfoDingMapper.insertUserInfoDing(userInfoDing);
    }

    /**
     * 修改钉钉用户信息
     * 
     * @param userInfoDing 钉钉用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoDing(UserInfoDing userInfoDing)
    {
        userInfoDing.setUpdateTime(DateUtils.getNowDate());
        return userInfoDingMapper.updateUserInfoDing(userInfoDing);
    }

    /**
     * 批量删除钉钉用户信息
     * 
     * @param ids 需要删除的钉钉用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoDingByIds(Long[] ids)
    {
        return userInfoDingMapper.deleteUserInfoDingByIds(ids);
    }

    /**
     * 删除钉钉用户信息信息
     * 
     * @param id 钉钉用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoDingById(Long id)
    {
        return userInfoDingMapper.deleteUserInfoDingById(id);
    }


    @Override
    public int updateUserInfoDingByUserNo(UserInfoDing userInfoDing) {
        userInfoDing.setUpdateTime(DateUtils.getNowDate());
        return userInfoDingMapper.updateUserInfoDingByUserNo(userInfoDing);
    }

    @Override
    public String importUser(List<UserInfoDing> userInfoDingList, String operName,List<String> userNoList) {
        userInfoDingMapper.cleanDingUser();
        if (StringUtils.isNull(userInfoDingMapper) || userInfoDingList.size() == 0) {
            throw new CustomException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (UserInfoDing userInfoDing : userInfoDingList) {
            try {
                userInfoDing.setCreateBy(operName);
                userInfoDing.setUpdateTime(DateUtils.getNowDate());
                String userNo = userInfoDing.getUserNo();
                if (userNo != null && !"".equals(userNo)) {
                    userInfoDing.setUserNo(userNo.trim());
                }
                int i = userInfoDingMapper.selectCountByUserNo(userInfoDing.getUserNo());
                if (i>0){
                    this.updateUserInfoDingByUserNo(userInfoDing);
                }else {
                    this.insertUserInfoDing(userInfoDing);
                }
                successNum++;
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、姓名 " + userInfoDing.getNickName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new CustomException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
