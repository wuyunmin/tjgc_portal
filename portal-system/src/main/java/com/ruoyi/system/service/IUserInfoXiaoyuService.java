package com.ruoyi.system.service;

import com.ruoyi.system.domain.UserInfoXiaoyu;

import java.util.List;

/**
 * 小鱼视频用户信息Service接口
 * 
 * @author tjec
 * @date 2021-01-28
 */
public interface IUserInfoXiaoyuService 
{
    /**
     * 查询小鱼视频用户信息
     * 
     * @param guid 小鱼视频用户信息ID
     * @return 小鱼视频用户信息
     */
    public UserInfoXiaoyu selectUserInfoXiaoyuByGuid(String guid);
    
    /**
     * 导入用户数据
     *
     * @param userList        用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName        操作用户
     * @return 结果
     */
    public String importUser(List<UserInfoXiaoyu> userInfoXiaoyuList, String operName);

    /**
     * 查询小鱼视频用户信息列表
     * 
     * @param userInfoXiaoyu 小鱼视频用户信息
     * @return 小鱼视频用户信息集合
     */
    public List<UserInfoXiaoyu> selectUserInfoXiaoyuList(UserInfoXiaoyu userInfoXiaoyu);

    /**
     * 新增小鱼视频用户信息
     * 
     * @param userInfoXiaoyu 小鱼视频用户信息
     * @return 结果
     */
    public int insertUserInfoXiaoyu(UserInfoXiaoyu userInfoXiaoyu);

    /**
     * 修改小鱼视频用户信息
     * 
     * @param userInfoXiaoyu 小鱼视频用户信息
     * @return 结果
     */
    public int updateUserInfoXiaoyu(UserInfoXiaoyu userInfoXiaoyu);

    /**
     * 批量删除小鱼视频用户信息
     * 
     * @param guids 需要删除的小鱼视频用户信息ID
     * @return 结果
     */
    public int deleteUserInfoXiaoyuByGuids(String[] guids);

    /**
     * 删除小鱼视频用户信息信息
     * 
     * @param guid 小鱼视频用户信息ID
     * @return 结果
     */
    public int deleteUserInfoXiaoyuByGuid(String guid);
}
