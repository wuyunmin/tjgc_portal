package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.WorkItemPend;

/**
 * 待办待阅信息Service接口
 *
 * @author tjgc
 * @date 2021-01-07
 */
public interface IWorkItemPendService
{
    /**
     * 查询待办待阅信息
     *
     * @param id 待办待阅信息ID
     * @return 待办待阅信息
     */
    public WorkItemPend selectWorkItemPendAndSSOById(Long id);

    /**
     * 查询待办待阅信息列表
     *
     * @param workItemPend 待办待阅信息
     * @return 待办待阅信息集合
     */
    public List<WorkItemPend> selectWorkItemPendList(WorkItemPend workItemPend);

    public List<WorkItemPend> selectAllWorkItemPendList(WorkItemPend workItemPend);

    /**
     * 新增待办待阅信息
     *
     * @param workItemPend 待办待阅信息
     * @return 结果
     */
    public int insertWorkItemPend(WorkItemPend workItemPend);


    /**
     * 新增或更新待办待阅信息bpm专用
     *
     * @param workItemPendMap 待办待阅信息
     * @return 结果
     */
    public int insertOrUpdateWorkItemPendBpm(Map<String,Object> workItemPendMap);

    /**
     * 新增或更新待办待阅信息通用
     *
     * @param workItemPendMap 待办待阅信息
     * @return 结果
     */
    public int insertOrUpdateWorkItemPendCommon(Map<String,Object> workItemPendMap);

    /**
     * 新增待办待阅信息
     *
     * @param workItemPendMap 待办待阅信息
     * @return 结果
     */
    public int insertWorkItemPend(Map<String,Object> workItemPendMap);


    /**
     * 新增HR待办待阅信息
     *
     * @param workItemPendMap 待办待阅信息
     * @return 结果
     */
    public int insertHrWorkItemPend(Map<String,Object> workItemPendMap);
    /**
     * 修改待办待阅信息
     *
     * @param workItemPend 待办待阅信息
     * @return 结果
     */
    public int updateWorkItemPend(WorkItemPend workItemPend);

    /**
     * 修改待办待阅信息
     *
     * @param workItemPend 待办待阅信息
     * @return 结果
     */
    public int updateWorkItemPendStatus(WorkItemPend workItemPend);


    /**
     * 批量删除待办待阅信息
     *
     * @param ids 需要删除的待办待阅信息ID
     * @return 结果
     */
    public int deleteWorkItemPendByIds(Long[] ids);

    /**
     * 删除待办待阅信息信息
     *
     * @param id 待办待阅信息ID
     * @return 结果
     */
    public int deleteWorkItemPendById(Long id);

    /**
     * 查询待办待阅信息通过流程ID和系统名称
     *
     * @param userNo 流程ID
     * @return 待办待阅信息
     */
    public int selectWorkItemPendCountBySystem(String systemName,String userNo);

    /**
     * 查询bpm待办待阅数量(bpm数量是portal+ec)
     * @param systemNames
     * @param userNo
     * @return
     */
    public int selectWorkItemPendCountBpm(List<String> systemNames,String userNo);


    /**
     * 查询待办待阅信息通过流程ID和系统名称
     *
     * @param id 流程ID
     * @return 待办待阅信息
     */
    public WorkItemPend selectWorkItemPendById(Long id);

    /**
     * 统计待办待阅信息列表
     *
     * @param workItemPend 待办待阅信息
     * @return 待办待阅信息集合
     */
    public int selectCountWorkItemPend(WorkItemPend workItemPend);

    public int selectCountWorkItemPendBySequenceNo(String sequenceNo);


    /**
     * 通用更新待办数据
     * @param workItemPend
     * @return
     */
    public int updateWorkItemPendBySequenceNoCommon(WorkItemPend workItemPend);
}
