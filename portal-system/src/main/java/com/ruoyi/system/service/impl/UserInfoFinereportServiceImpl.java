package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UserInfoFinereportMapper;
import com.ruoyi.system.domain.UserInfoFinereport;
import com.ruoyi.system.service.IUserInfoFinereportService;

/**
 * 帆软用户信息Service业务层处理
 * 
 * @author tjgc
 * @date 2021-04-22
 */
@Service
public class UserInfoFinereportServiceImpl implements IUserInfoFinereportService 
{
    @Autowired
    private UserInfoFinereportMapper userInfoFinereportMapper;

    /**
     * 查询帆软用户信息
     * 
     * @param id 帆软用户信息ID
     * @return 帆软用户信息
     */
    @Override
    public UserInfoFinereport selectUserInfoFinereportById(Long id)
    {
        return userInfoFinereportMapper.selectUserInfoFinereportById(id);
    }

    /**
     * 查询帆软用户信息列表
     * 
     * @param userInfoFinereport 帆软用户信息
     * @return 帆软用户信息
     */
    @Override
    public List<UserInfoFinereport> selectUserInfoFinereportList(UserInfoFinereport userInfoFinereport)
    {
        return userInfoFinereportMapper.selectUserInfoFinereportList(userInfoFinereport);
    }

    /**
     * 新增帆软用户信息
     * 
     * @param userInfoFinereport 帆软用户信息
     * @return 结果
     */
    @Override
    public int insertUserInfoFinereport(UserInfoFinereport userInfoFinereport)
    {
        userInfoFinereport.setCreateTime(DateUtils.getNowDate());
        return userInfoFinereportMapper.insertUserInfoFinereport(userInfoFinereport);
    }

    /**
     * 修改帆软用户信息
     * 
     * @param userInfoFinereport 帆软用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoFinereport(UserInfoFinereport userInfoFinereport)
    {
        userInfoFinereport.setUpdateTime(DateUtils.getNowDate());
        return userInfoFinereportMapper.updateUserInfoFinereport(userInfoFinereport);
    }

    /**
     * 批量删除帆软用户信息
     * 
     * @param ids 需要删除的帆软用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoFinereportByIds(Long[] ids)
    {
        return userInfoFinereportMapper.deleteUserInfoFinereportByIds(ids);
    }

    /**
     * 删除帆软用户信息信息
     * 
     * @param id 帆软用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoFinereportById(Long id)
    {
        return userInfoFinereportMapper.deleteUserInfoFinereportById(id);
    }
}
