package com.ruoyi.system.service.impl;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UserInfoBpmMapper;
import com.ruoyi.system.domain.UserInfoBpm;
import com.ruoyi.system.service.IUserInfoBpmService;

import javax.annotation.Resource;

import static com.ruoyi.common.utils.DateUtils.*;
import static com.ruoyi.common.utils.DateUtils.parseDate;

/**
 * bpm用户信息Service业务层处理
 * 
 * @author tjgc
 * @date 2021-01-06
 */
@Service
public class UserInfoBpmServiceImpl implements IUserInfoBpmService 
{
    @Resource
    private UserInfoBpmMapper userInfoBpmMapper;

    /**
     * 查询bpm用户信息
     * 
     * @param id bpm用户信息ID
     * @return bpm用户信息
     */
    @Override
    public UserInfoBpm selectUserInfoBpmById(Long id)
    {
        return userInfoBpmMapper.selectUserInfoBpmById(id);
    }

    /**
     * 查询bpm用户信息列表
     * 
     * @param userInfoBpm bpm用户信息
     * @return bpm用户信息
     */
    @Override
    public List<UserInfoBpm> selectUserInfoBpmList(UserInfoBpm userInfoBpm)
    {
        return userInfoBpmMapper.selectUserInfoBpmList(userInfoBpm);
    }

    /**
     * 新增bpm用户信息
     *
     * @param userInfoMap bpm用户信息
     * @return 结果
     */
    @Override
    public int insertUserInfoBpm(Map<String,Object> userInfoMap)
    {
        UserInfoBpm userInfoBpm = convertValue(userInfoMap);
        userInfoBpm.setCreateTime(DateUtils.getNowDate());
        return userInfoBpmMapper.insertUserInfoBpm(userInfoBpm);
    }
    /**
     * 新增bpm用户信息
     * 
     * @param userInfoBpm bpm用户信息
     * @return 结果
     */
    @Override
    public int insertUserInfoBpm(UserInfoBpm userInfoBpm)
    {
        userInfoBpm.setCreateTime(DateUtils.getNowDate());
        return userInfoBpmMapper.insertUserInfoBpm(userInfoBpm);
    }

    /**
     * 修改bpm用户信息
     *
     * @param userInfoMap bpm用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoBpm(Map<String,Object> userInfoMap)
    {
        UserInfoBpm userInfoBpm = convertValue(userInfoMap);
        userInfoBpm.setUpdateTime(DateUtils.getNowDate());
        return userInfoBpmMapper.updateUserInfoBpmByUserNo(userInfoBpm);
    }

    /**
     * 修改bpm用户信息
     * 
     * @param userInfoBpm bpm用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoBpm(UserInfoBpm userInfoBpm)
    {
        userInfoBpm.setUpdateTime(DateUtils.getNowDate());
        return userInfoBpmMapper.updateUserInfoBpm(userInfoBpm);
    }

    /**
     * 批量删除bpm用户信息
     * 
     * @param ids 需要删除的bpm用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoBpmByIds(Long[] ids)
    {
        return userInfoBpmMapper.deleteUserInfoBpmByIds(ids);
    }

    /**
     * 删除bpm用户信息信息
     * 
     * @param id bpm用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoBpmById(Long id)
    {
        return userInfoBpmMapper.deleteUserInfoBpmById(id);
    }


    private UserInfoBpm convertValue(Map<String,Object> userInfoMap){
        UserInfoBpm userInfoBpm = new UserInfoBpm();
        Object userNo = userInfoMap.get("userNo");
        Object userName = userInfoMap.get("userName");
        Object loginName = userInfoMap.get("loginName");
        Object loginDate = userInfoMap.get("loginDate");
        Object systemName = userInfoMap.get("systemName");
        Object updateTime = userInfoMap.get("updateTime");
        Object userStatus = userInfoMap.get("userStatus");
        Object competentOrg = userInfoMap.get("competentOrg");
        Object competentDept = userInfoMap.get("competentDept");
        userInfoBpm.setUserNo(null == userNo ? null : userNo.toString());
        userInfoBpm.setUserName(null==userName?null:userName.toString());
        userInfoBpm.setLoginName(null == loginName ? null : loginName.toString());
        userInfoBpm.setSystemName(null == systemName ? null : systemName.toString());
        userInfoBpm.setUserStatus(null == userStatus ? null : userStatus.toString());
        userInfoBpm.setCompetentOrg(null == competentOrg ? null : competentOrg.toString());
        userInfoBpm.setCompetentDept(null == competentDept ? null : competentDept.toString());
        userInfoBpm.setLoginDate(null == loginDate ? null : parseDate(loginDate));
        userInfoBpm.setSysUpdateTime(null == updateTime ? null : parseDate(updateTime));
        return userInfoBpm;
    }
}

