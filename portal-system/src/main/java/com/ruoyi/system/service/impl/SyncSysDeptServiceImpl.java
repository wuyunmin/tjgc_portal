package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SyncSysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SyncSysDeptMapper;
import com.ruoyi.system.domain.SyncSysDept;
import com.ruoyi.system.service.ISyncSysDeptService;

import javax.annotation.Resource;

/**
 * 部门同步中间Service业务层处理
 * 
 * @author tjgc
 * @date 2021-01-20
 */
@Service
public class SyncSysDeptServiceImpl implements ISyncSysDeptService 
{
    @Resource
    private SyncSysDeptMapper syncSysDeptMapper;


    /**
     * 查询部门同步中间列表
     * 
     * @param syncSysDept 部门同步中间
     * @return 部门同步中间
     */
    @Override
    public List<SyncSysDept> selectSyncSysDeptList(SyncSysDept syncSysDept)
    {
        return syncSysDeptMapper.selectSyncSysDeptList(syncSysDept);
    }

    /**
     * 新增部门同步中间
     * 
     * @param syncSysDept 部门同步中间
     * @return 结果
     */
    @Override
    public int insertSyncSysDept(SyncSysDept syncSysDept)
    {
        syncSysDept.setCreateTime(DateUtils.getNowDate());
        return syncSysDeptMapper.insertSyncSysDept(syncSysDept);
    }


    /**
     * 新增部门同步中间
     *
     * @param syncSysDeptMap 部门同步中间
     * @return 结果
     */
    @Override
    public int insertSyncSysDept(Map<String,Object> syncSysDeptMap)
    {
        SyncSysDept syncSysDept = convertValue(syncSysDeptMap);
        syncSysDept.setCreateTime(DateUtils.getNowDate());
        return syncSysDeptMapper.insertSyncSysDept(syncSysDept);
    }
    /**
     * 修改部门同步中间
     * 
     * @param syncSysDept 部门同步中间
     * @return 结果
     */
    @Override
    public int updateSyncSysDept(SyncSysDept syncSysDept)
    {
        syncSysDept.setUpdateTime(DateUtils.getNowDate());
        return syncSysDeptMapper.updateSyncSysDept(syncSysDept);
    }

    @Override
    public int existSyncSysDept(String deptIdCode)
    {
        return syncSysDeptMapper.existSyncSysDept(deptIdCode);
    }

    /**
     * 修改部门同步中间
     *
     * @param syncSysDeptMap 部门同步中间
     * @return 结果
     */
    @Override
    public int updateSyncSysDept(Map<String,Object> syncSysDeptMap)
    {
        SyncSysDept syncSysDept = convertValue(syncSysDeptMap);
        syncSysDept.setUpdateTime(DateUtils.getNowDate());
        return syncSysDeptMapper.updateSyncSysDept(syncSysDept);
    }
    /**
     * 批量删除部门同步中间
     * 
     * @param ids 需要删除的部门同步中间ID
     * @return 结果
     */
    @Override
    public int deleteSyncSysDeptByIds(Long[] ids)
    {
        return syncSysDeptMapper.deleteSyncSysDeptByIds(ids);
    }

    /**
     * 删除部门同步中间信息
     * 
     * @param id 部门同步中间ID
     * @return 结果
     */
    @Override
    public int deleteSyncSysDeptById(Long id)
    {
        return syncSysDeptMapper.deleteSyncSysDeptById(id);
    }

    @Override
    public SyncSysDept selectDeptByDeptId(String id) {
        return syncSysDeptMapper.selectDeptByDeptId(id);
    }

    private SyncSysDept convertValue(Map<String,Object> syncSysUserMap){
        SyncSysDept syncSysDept = new SyncSysDept();
        //部门全局ID
        Object deptId = syncSysUserMap.get("dept_id");
        //父级部门全局ID
        Object deptParentId = syncSysUserMap.get("dept_parentid");
        //部门名称
        Object deptContent = syncSysUserMap.get("dept_content");
        //HR部门代码
        Object deptCode = syncSysUserMap.get("dept_code");
        //部门全称
        Object deptAllContent = syncSysUserMap.get("dept_allcontent");
        //部门层数
        Object deptGrade = syncSysUserMap.get("dept_grade");
        //父级部门HR代码
        Object deptParentCode = syncSysUserMap.get("dept_parentcode");
        //分管领导HR代码
        Object deptInchargeLeader = syncSysUserMap.get("dept_inchargeleader");
        //部门领导HR代码
        Object deptLeader = syncSysUserMap.get("dept_leader");
        //OA组织编码
        Object deptOaOrgCode = syncSysUserMap.get("dept_oaorgcode");
        //HR部门ID
        Object deptIdCode = syncSysUserMap.get("dept_idcode");
        //排序code
        Object deptOrderCode = syncSysUserMap.get("dept_ordercode");
        //父级HR部门ID
        Object deptParentIdCode = syncSysUserMap.get("dept_parentidcode");
        //删除标志（0代表存在 2代表删除）
        Object deptDeleted = syncSysUserMap.get("dept_deleted");
        //部门类型
        Object deptType = syncSysUserMap.get("dept_type");
        //末级标志
        Object deptEndMark = syncSysUserMap.get("dept_endmark");
        //分管领导全局ID
        Object deptInchargeLeaderId = syncSysUserMap.get("dept_inchargeleaderid");
        //主管领导全局ID
        Object deptLeaderId = syncSysUserMap.get("dept_leaderid");
        //状态
        Object flag = syncSysUserMap.get("flag");

        syncSysDept.setDeptId(null == deptId ? null : deptId.toString());
        syncSysDept.setDeptParentId(null == deptParentId ? null : deptParentId.toString());
        syncSysDept.setDeptCode(null == deptCode ? null : deptCode.toString());
        syncSysDept.setDeptDeleted(null == deptDeleted ? null : deptDeleted.toString());
        syncSysDept.setDeptIdCode(null == deptIdCode ? null : deptIdCode.toString());
        syncSysDept.setDeptContent(null == deptContent ? null : deptContent.toString());
        syncSysDept.setDeptGrade(null == deptGrade ? null : Integer.parseInt(deptGrade.toString()));
        syncSysDept.setDeptAllContent(null == deptAllContent ? null : deptAllContent.toString());
        syncSysDept.setDeptParentCode(null == deptParentCode ? null : deptParentCode.toString());
        syncSysDept.setDeptInchargeLeader(null == deptInchargeLeader ? null : deptInchargeLeader.toString());
        syncSysDept.setDeptLeader(null == deptLeader ? null : deptLeader.toString());
        syncSysDept.setDeptOaOrgCode(null == deptOaOrgCode ? null : deptOaOrgCode.toString());
        syncSysDept.setDeptOrderCode(null == deptOrderCode ? null : Integer.parseInt(deptOrderCode.toString()));
        syncSysDept.setDeptParentIdCode(null == deptParentIdCode ? null : deptParentIdCode.toString());
        syncSysDept.setDeptType(null == deptType ? null : deptType.toString());
        syncSysDept.setDeptEndMark(null == deptEndMark ? null : deptEndMark.toString());
        syncSysDept.setDeptInchargeLeaderId(null == deptInchargeLeaderId ? null : deptInchargeLeaderId.toString());
        syncSysDept.setDeptLeaderId(null == deptLeaderId ? null : deptLeaderId.toString());
        syncSysDept.setFlag(null == flag ? null : flag.toString());
        //设置未同步，同步标志（0代表未同步 1代表已同步）
        syncSysDept.setSyncFlag("0");
        return syncSysDept;
    }
}
