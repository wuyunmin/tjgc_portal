package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysShortUrl;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysShortUrlMapper;
import com.ruoyi.system.service.ISysShortUrlService;

import javax.annotation.Resource;

/**
 * 文件信息Service业务层处理
 * 
 * @author tjgc
 * @date 2021-02-08
 */
@Service
public class SysShortUrlServiceImpl implements ISysShortUrlService 
{
    @Resource
    private SysShortUrlMapper sysShortUrlMapper;

    /**
     * 查询文件信息
     * 
     * @param id 文件信息ID
     * @return 文件信息
     */
    @Override
    public SysShortUrl selectSysShortUrlById(Long id)
    {
        return sysShortUrlMapper.selectSysShortUrlById(id);
    }


    /**
     * 查询文件信息列表
     *
     * @param shortUrl 文件信息
     * @return 文件信息
     */
    @Override
    public SysShortUrl selectSysShortUrlByShotUrl(String shortUrl)
    {
        return sysShortUrlMapper.selectSysShortByShotUrl(shortUrl);
    }

    @Override
    public int selectSysShortUrlCountByUserNo(String userNo)
    {
        return sysShortUrlMapper.selectSysShortUrlCountByUserNo(userNo);
    }

    /**
     * 删除文件信息信息
     *
     * @param shortUrl 文件信息ID
     * @return 结果
     */
    @Override
    public int deleteSysShortUrlByShotUrl(String shortUrl)
    {
        return sysShortUrlMapper.deleteSysShortUrlByShotUrl(shortUrl);
    }
    /**
     * 查询文件信息列表
     * 
     * @param sysShortUrl 文件信息
     * @return 文件信息
     */
    @Override
    public List<SysShortUrl> selectSysShortUrlList(SysShortUrl sysShortUrl)
    {
        return sysShortUrlMapper.selectSysShortUrlList(sysShortUrl);
    }


    /**
     * 新增文件信息
     * 
     * @param sysShortUrl 文件信息
     * @return 结果
     */
    @Override
    public int insertSysShortUrl(SysShortUrl sysShortUrl)
    {
        sysShortUrl.setCreateTime(DateUtils.getNowDate());
        return sysShortUrlMapper.insertSysShortUrl(sysShortUrl);
    }

    /**
     * 修改文件信息
     * 
     * @param sysShortUrl 文件信息
     * @return 结果
     */
    @Override
    public int updateSysShortUrl(SysShortUrl sysShortUrl)
    {
        sysShortUrl.setUpdateTime(DateUtils.getNowDate());
        return sysShortUrlMapper.updateSysShortUrl(sysShortUrl);
    }

    /**
     * 批量删除文件信息
     * 
     * @param ids 需要删除的文件信息ID
     * @return 结果
     */
    @Override
    public int deleteSysShortUrlByIds(Long[] ids)
    {
        return sysShortUrlMapper.deleteSysShortUrlByIds(ids);
    }


    @Override
    public int deleteSysShortUrlByUserNo(String userNo)
    {
        return sysShortUrlMapper.deleteSysShortUrlByUserNo(userNo);
    }

    /**
     * 删除文件信息信息
     * 
     * @param id 文件信息ID
     * @return 结果
     */
    @Override
    public int deleteSysShortUrlById(Long id)
    {
        return sysShortUrlMapper.deleteSysShortUrlById(id);
    }
}
