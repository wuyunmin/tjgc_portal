package com.ruoyi.system.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.entity.SysUser;
import org.apache.poi.hssf.dev.ReSave;
import org.apache.poi.ss.formula.functions.IDStarAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.mapper.UserInfoEmailMapper;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.UserInfoEmail;
import com.ruoyi.system.domain.UserInfoXiaoyu;
import com.ruoyi.system.service.IUserInfoEmailService;

/**
 * 企业邮箱用户信息Service业务层处理
 * 
 * @author tjec
 * @date 2021-01-28
 */
@Service
public class UserInfoEmailServiceImpl implements IUserInfoEmailService 
{
    @Autowired
    private UserInfoEmailMapper userInfoEmailMapper;
    
    @Autowired
    private SysUserMapper userMapper;

    /**
     * 查询企业邮箱用户信息
     * 
     * @param guid 企业邮箱用户信息ID
     * @return 企业邮箱用户信息
     */
    @Override
    public UserInfoEmail selectUserInfoEmailByGuid(String guid)
    {
        return userInfoEmailMapper.selectUserInfoEmailByGuid(guid);
    }

    @Override
    public UserInfoEmail selectUserInfoEmailByUserNo(String userNo) {
        return userInfoEmailMapper.selectUserInfoEmailByUserNo(userNo);
    }

    /**
     * 查询企业邮箱用户信息列表
     * 
     * @param userInfoEmail 企业邮箱用户信息
     * @return 企业邮箱用户信息
     */
    @Override
    public List<UserInfoEmail> selectUserInfoEmailList(UserInfoEmail userInfoEmail)
    {
		String emailHolder = userInfoEmail.getEmailHolder();
		if (emailHolder!=null&&!"".equals(emailHolder)){
			List<String> sysUsers = userMapper.selectUserByNickName(emailHolder);
			if (sysUsers != null && sysUsers.size() > 0) {
				userInfoEmail.setQueryEmailHolder(sysUsers);
			}
		}
		List<UserInfoEmail> resList = userInfoEmailMapper.selectUserInfoEmailList(userInfoEmail);
    	if(resList!=null && resList.size()>0) {
    		for (UserInfoEmail userInfoEmail2 : resList) {
    			if(StringUtils.isNotBlank(userInfoEmail2.getEmailHolder())) {
    				List<Long> ids = Arrays.stream(userInfoEmail2.getEmailHolder().split(";"))
    		        .map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
    				userInfoEmail2.setEmailHolderText(userMapper.selectUserNames(ids));
    			}

			}
    	}
        return resList;
    }

    /**
     * 新增企业邮箱用户信息
     * 
     * @param userInfoEmail 企业邮箱用户信息
     * @return 结果
     */
    @Override
    public int insertUserInfoEmail(UserInfoEmail userInfoEmail)
    {
    	//1、判断电子邮件是否重复
    	if(StringUtils.isNotBlank(userInfoEmail.getEmail())) {
    		UserInfoEmail checkRes = userInfoEmailMapper.selectUserInfoEmailByEmail(userInfoEmail.getEmail());
        	if(checkRes == null) {
        		return userInfoEmailMapper.insertUserInfoEmail(userInfoEmail);
        	}else {
        		throw new CustomException("电子邮件已存在，请勿重复添加！");
        	}
    	}else {
    		throw new CustomException("电子邮件为必填项，请填写电子邮件后再保存！");
    	}
        
    }

    /**
     * 修改企业邮箱用户信息
     * 
     * @param userInfoEmail 企业邮箱用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoEmail(UserInfoEmail userInfoEmail)
    {
        return userInfoEmailMapper.updateUserInfoEmail(userInfoEmail);
    }

    /**
     * 批量删除企业邮箱用户信息
     * 
     * @param guids 需要删除的企业邮箱用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoEmailByGuids(String[] guids)
    {
    	//根据guid修改企业邮箱用户状态为删除
        return userInfoEmailMapper.updateUserInfoEmailByGuids(guids);
    }

    /**
     * 删除企业邮箱用户信息信息
     * 
     * @param guid 企业邮箱用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoEmailByGuid(String guid)
    {
        return userInfoEmailMapper.deleteUserInfoEmailByGuid(guid);
    }

	@Override
	public String importUser(List<UserInfoEmail> userInfoEmailList, String operName) {
		//1、先删除所有原表数据
		userInfoEmailMapper.cleanEmailUser();
		//2、导入新数据
		if (StringUtils.isNull(userInfoEmailMapper) || userInfoEmailList.size() == 0) {
			throw new CustomException("导入用户数据不能为空！");
		}
		int successNum = 0;
		int failureNum = 0;
		StringBuilder successMsg = new StringBuilder();
		StringBuilder failureMsg = new StringBuilder();
		for (UserInfoEmail userInfoEmail : userInfoEmailList) {
			try {
				userInfoEmail.setCreateBy(operName);
				if(userInfoEmail.getUserNo()!=null) {
					userInfoEmail.setUserNo(userInfoEmail.getUserNo().trim());
				}
				this.insertUserInfoEmail(userInfoEmail);
				successNum++;
				//successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 导入成功");
			} catch (Exception e) {
				failureNum++;
				String msg = "<br/>" + failureNum + "、姓名 " + userInfoEmail.getUserName() + " 导入失败：";
				failureMsg.append(msg + e.getMessage());
			}
		}
		if (failureNum > 0) {
			failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
			throw new CustomException(failureMsg.toString());
		} else {
			successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
		}
		return successMsg.toString();
	}
}
