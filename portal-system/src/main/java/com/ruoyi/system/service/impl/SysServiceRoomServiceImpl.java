package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;

import org.apache.commons.collections4.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.system.mapper.SysDictDataMapper;
import com.ruoyi.system.mapper.SysServiceRoomMapper;
import com.ruoyi.system.domain.SysServiceRoom;
import com.ruoyi.system.domain.SysServiceRoomDto;
import com.ruoyi.system.service.ISysServiceRoomService;

/**
 * 服务大厅Service业务层处理
 * 
 * @author tjgc
 * @date 2021-03-09
 */
@Service
public class SysServiceRoomServiceImpl implements ISysServiceRoomService 
{
    @Autowired
    private SysServiceRoomMapper sysServiceRoomMapper;
    
    @Autowired
    private SysDictDataMapper sysDictDataMapper;

    /**
     * 查询服务大厅
     * 
     * @param guid 服务大厅ID
     * @return 服务大厅
     */
    @Override
    public SysServiceRoom selectSysServiceRoomByGuid(String guid)
    {
        return sysServiceRoomMapper.selectSysServiceRoomByGuid(guid);
    }

    /**
     * 查询服务大厅列表
     * 
     * @param sysServiceRoom 服务大厅
     * @return 服务大厅
     */
    @Override
    public List<SysServiceRoom> selectSysServiceRoomList(SysServiceRoom sysServiceRoom)
    {
        return sysServiceRoomMapper.selectSysServiceRoomList(sysServiceRoom);
    }

    /**
     * 新增服务大厅
     * 
     * @param sysServiceRoom 服务大厅
     * @return 结果
     */
    @Override
    public int insertSysServiceRoom(SysServiceRoom sysServiceRoom)
    {
        sysServiceRoom.setCreateTime(DateUtils.getNowDate());
        return sysServiceRoomMapper.insertSysServiceRoom(sysServiceRoom);
    }

    /**
     * 修改服务大厅
     * 
     * @param sysServiceRoom 服务大厅
     * @return 结果
     */
    @Override
    public int updateSysServiceRoom(SysServiceRoom sysServiceRoom)
    {
        sysServiceRoom.setUpdateTime(DateUtils.getNowDate());
        return sysServiceRoomMapper.updateSysServiceRoom(sysServiceRoom);
    }

    /**
     * 批量删除服务大厅
     * 
     * @param guids 需要删除的服务大厅ID
     * @return 结果
     */
    @Override
    public int deleteSysServiceRoomByGuids(String[] guids)
    {
        return sysServiceRoomMapper.deleteSysServiceRoomByGuids(guids);
    }

    /**
     * 删除服务大厅信息
     * 
     * @param guid 服务大厅ID
     * @return 结果
     */
    @Override
    public int deleteSysServiceRoomByGuid(String guid)
    {
        return sysServiceRoomMapper.deleteSysServiceRoomByGuid(guid);
    }
/*
	@Override
	public LinkedHashMap<String, Object> searchServiceRoom(SysServiceRoom sysServiceRoom) {
		// 1、根据服务类别字典按顺序查出所有的服务类别项
		// 2、循环遍历顺序查询本服务类别下的所有二级类别
		// 3、顺序查询所有三级项
		LinkedHashMap<String, Object> resMap = new LinkedHashMap<String, Object>();
		if(!StringUtils.isNotBlank(sysServiceRoom.getServiceType())) {//全部
			List<SysDictData> serviceTypes = sysDictDataMapper.selectDictDataByType("service_type");
			if(serviceTypes!=null && serviceTypes.size()>0) {
				for (SysDictData sysDictData : serviceTypes) {
						SysServiceRoom room = new SysServiceRoom();
						room.setName(sysServiceRoom.getName());
						room.setServiceType(sysDictData.getDictValue());
						List<SysServiceRoom> resEntity = sysServiceRoomMapper.selectSysServiceRoomByName(room);
						if(resEntity!=null && resEntity.size()>0) {
							LinkedHashMap<String, Object> res = new LinkedHashMap<String, Object>();
							res.put("serviceLinks",resEntity);
							resMap.put(resEntity.get(0).getSubclassText(),res);
						}
					//}
				}
			}
		}else {//有服务类别
			SysServiceRoom room = new SysServiceRoom();
			room.setName(sysServiceRoom.getName());
			room.setServiceType(sysServiceRoom.getServiceType());
			List<SysServiceRoom> resEntity = sysServiceRoomMapper.selectSysServiceRoomByName(room);
			if(resEntity!=null && resEntity.size()>0) {
				LinkedHashMap<String, Object> res = new LinkedHashMap<String, Object>();
				res.put("serviceLinks",resEntity);
				resMap.put(resEntity.get(0).getSubclassText(),res);
			}
		//}
	
		}
		System.out.println(resMap);
		return resMap;
	}*/

	@Override
	public List<SysServiceRoomDto> searchServiceRoom(SysServiceRoom sysServiceRoom) {
		// 1、根据服务类别字典按顺序查出所有的服务类别项
		// 2、循环遍历顺序查询本服务类别下的所有二级类别
		// 3、顺序查询所有三级项
		List<SysServiceRoomDto> resMap = new ArrayList<SysServiceRoomDto>();
		if(!StringUtils.isNotBlank(sysServiceRoom.getServiceType())) {//全部
			List<SysDictData> serviceTypes = sysDictDataMapper.selectDictDataByType("service_type");
			if(serviceTypes!=null && serviceTypes.size()>0) {
				for (SysDictData sysDictData : serviceTypes) {
					List<SysDictData> sysDictDatas = sysDictDataMapper.selectDictDataByType(sysDictData.getDictValue());
					SysServiceRoom room = new SysServiceRoom();
					room.setName(sysServiceRoom.getName());
					List<SysServiceRoom> resEntity = sysServiceRoomMapper.selectSysServiceRoomByName(room);
					for (SysDictData sysDictData2 : sysDictDatas) {
						if(resEntity!=null && resEntity.size()>0) {
							List<SysServiceRoom> sysServiceRoomList = new ArrayList<SysServiceRoom>();
							for (SysServiceRoom sysServiceRoom2 : resEntity) {
								if(sysDictData2.getDictType().equals(sysServiceRoom2.getServiceType()) && sysDictData2.getDictValue().equals(sysServiceRoom2.getSubclass())) {
									sysServiceRoomList.add(sysServiceRoom2);
								}
								
							}
							if(sysServiceRoomList!=null && sysServiceRoomList.size()>0) {
								SysServiceRoomDto sysServiceRoomDto = new SysServiceRoomDto();
								sysServiceRoomDto.setServiceLinks(sysServiceRoomList);
								sysServiceRoomDto.setName(sysDictData2.getDictLabel());
								sysServiceRoomDto.setNum(1);
								resMap.add(sysServiceRoomDto);
							}
							
						}
					}
						/*SysServiceRoom room = new SysServiceRoom();
						room.setName(sysServiceRoom.getName());
						room.setServiceType(sysDictData.getDictValue());
						List<SysServiceRoom> resEntity = sysServiceRoomMapper.selectSysServiceRoomByName(room);
						if(resEntity!=null && resEntity.size()>0) {
							SysServiceRoomDto sysServiceRoomDto = new SysServiceRoomDto();
							sysServiceRoomDto.setServiceLinks(resEntity);
							sysServiceRoomDto.setName(resEntity.get(0).getSubclassText());
							sysServiceRoomDto.setNum(sysDictData.getDictSort());
							resMap.add(sysServiceRoomDto);
						}*/
					//}
				}
			}
		}else {//有服务类别
			List<SysDictData> serviceTypes = sysDictDataMapper.selectDictDataByType(sysServiceRoom.getServiceType());
			SysServiceRoom room = new SysServiceRoom();
			room.setName(sysServiceRoom.getName());
			room.setServiceType(sysServiceRoom.getServiceType());
			List<SysServiceRoom> resEntity = sysServiceRoomMapper.selectSysServiceRoomByName(room);
			for (SysDictData sysDictData : serviceTypes) {
				if(resEntity!=null && resEntity.size()>0) {
					List<SysServiceRoom> sysServiceRoomList = new ArrayList<SysServiceRoom>();
					for (SysServiceRoom sysServiceRoom2 : resEntity) {
						if(sysDictData.getDictValue().equals(sysServiceRoom2.getSubclass())) {
							sysServiceRoomList.add(sysServiceRoom2);
						}
						
					}
					if(sysServiceRoomList!=null && sysServiceRoomList.size()>0) {
						SysServiceRoomDto sysServiceRoomDto = new SysServiceRoomDto();
						sysServiceRoomDto.setServiceLinks(sysServiceRoomList);
						sysServiceRoomDto.setName(sysDictData.getDictLabel());
						sysServiceRoomDto.setNum(1);
						resMap.add(sysServiceRoomDto);
					}
					
				}
			}
			
			
		//}
	
		}
		System.out.println(resMap);
		return resMap;
	}
}
