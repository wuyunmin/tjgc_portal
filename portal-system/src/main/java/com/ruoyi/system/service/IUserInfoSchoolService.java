package com.ruoyi.system.service;

import com.ruoyi.system.domain.UserInfoSchool;
import com.ruoyi.system.domain.UserInfoTb;

import java.util.List;

/**
 * 企业培训学校用户信息Service接口
 * 
 * @author tjec
 * @date 2021-01-28
 */
public interface IUserInfoSchoolService 
{
    /**
     * 查询企业培训学校用户信息
     * 
     * @param guid 企业培训学校用户信息ID
     * @return 企业培训学校用户信息
     */
    public UserInfoSchool selectUserInfoSchoolByGuid(String guid);

    /**
     * 查询企业培训学校用户信息列表
     * 
     * @param userInfoSchool 企业培训学校用户信息
     * @return 企业培训学校用户信息集合
     */
    public List<UserInfoSchool> selectUserInfoSchoolList(UserInfoSchool userInfoSchool);

    /**
     * 新增企业培训学校用户信息
     * 
     * @param userInfoSchool 企业培训学校用户信息
     * @return 结果
     */
    public int insertUserInfoSchool(UserInfoSchool userInfoSchool);

    /**
     * 修改企业培训学校用户信息
     * 
     * @param userInfoSchool 企业培训学校用户信息
     * @return 结果
     */
    public int updateUserInfoSchool(UserInfoSchool userInfoSchool);

    /**
     * 批量删除企业培训学校用户信息
     * 
     * @param guids 需要删除的企业培训学校用户信息ID
     * @return 结果
     */
    public int deleteUserInfoSchoolByGuids(String[] guids);

    /**
     * 删除企业培训学校用户信息信息
     * 
     * @param guid 企业培训学校用户信息ID
     * @return 结果
     */
    public int deleteUserInfoSchoolByGuid(String guid);
    
    /**
     * 导入用户数据
     *
     * @param userList        用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName        操作用户
     * @return 结果
     */
    public String importUser(List<UserInfoSchool> userInfoSchoolList, String operName);
}
