package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.UserInfoBpm;

/**
 * bpm用户信息Service接口
 * 
 * @author tjgc
 * @date 2021-01-06
 */
public interface IUserInfoBpmService 
{
    /**
     * 查询bpm用户信息
     * 
     * @param id bpm用户信息ID
     * @return bpm用户信息
     */
    public UserInfoBpm selectUserInfoBpmById(Long id);

    /**
     * 查询bpm用户信息列表
     * 
     * @param userInfoBpm bpm用户信息
     * @return bpm用户信息集合
     */
    public List<UserInfoBpm> selectUserInfoBpmList(UserInfoBpm userInfoBpm);


    /**
     * 新增bpm用户信息
     *
     * @param userInfoMap bpm用户信息
     * @return 结果
     */
    public int insertUserInfoBpm(Map<String,Object> userInfoMap);

    /**
     * 新增bpm用户信息
     * 
     * @param userInfoBpm bpm用户信息
     * @return 结果
     */
    public int insertUserInfoBpm(UserInfoBpm userInfoBpm);

    /**
     * 修改bpm用户信息
     *
     * @param userInfoMap bpm用户信息
     * @return 结果
     */
    public int updateUserInfoBpm(Map<String,Object> userInfoMap);
    /**
     * 修改bpm用户信息
     * 
     * @param userInfoBpm bpm用户信息
     * @return 结果
     */
    public int updateUserInfoBpm(UserInfoBpm userInfoBpm);

    /**
     * 批量删除bpm用户信息
     * 
     * @param ids 需要删除的bpm用户信息ID
     * @return 结果
     */
    public int deleteUserInfoBpmByIds(Long[] ids);

    /**
     * 删除bpm用户信息信息
     * 
     * @param id bpm用户信息ID
     * @return 结果
     */
    public int deleteUserInfoBpmById(Long id);
}
