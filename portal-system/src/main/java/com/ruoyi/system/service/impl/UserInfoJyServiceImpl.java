package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ruoyi.system.mapper.UserInfoJyMapper;
import com.ruoyi.system.dao.UserInfoDao;
import com.ruoyi.system.domain.UserInfoJy;
import com.ruoyi.system.service.IUserInfoJyService;

/**
 * 经营系统用户信息Service业务层处理
 * 
 * @author tjgc
 * @date 2021-02-24
 */
@Service
public class UserInfoJyServiceImpl implements IUserInfoJyService 
{
    @Autowired
    private UserInfoJyMapper userInfoJyMapper;
    
    @Autowired
    private UserInfoDao userInfoDao;

    /**
     * 查询经营系统用户信息
     * 
     * @param guid 经营系统用户信息ID
     * @return 经营系统用户信息
     */
    @Override
    public UserInfoJy selectUserInfoJyById(Long guid)
    {
        return userInfoJyMapper.selectUserInfoJyById(guid);
    }

    /**
     * 查询经营系统用户信息列表
     * 
     * @param userInfoJy 经营系统用户信息
     * @return 经营系统用户信息
     */
    @Override
    public List<UserInfoJy> selectUserInfoJyList(UserInfoJy userInfoJy)
    {
        return userInfoJyMapper.selectUserInfoJyList(userInfoJy);
    }

    /**
     * 修改经营系统用户信息
     * 
     * @param userInfoJy 经营系统用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoJy(UserInfoJy userInfoJy)
    {
        return userInfoJyMapper.updateUserInfoJy(userInfoJy);
    }

    @Transactional
	@Override
	public int importUser() {
    	//清除用户
    	userInfoJyMapper.cleanJyUser();
    	//导入用户
    	List<Map<String, Object>> jyUsers = userInfoDao.getAllJYUser();
    	if(jyUsers!=null && jyUsers.size()>0) {
    		userInfoJyMapper.insertUserInfoJy(jyUsers);
    	}else {
    		return 0;
    	}
		return 1;
	}
}
