package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SysHolderAccount;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author tjgc
 * @date 2021-02-04
 */
public interface ISysHolderAccountService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param guid 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public List<SysHolderAccount> selectSysHolderAccountByUserId(long userId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param sysHolderAccount 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<SysHolderAccount> selectSysHolderAccountList(SysHolderAccount sysHolderAccount);

    /**
     * 新增【请填写功能名称】
     * 
     * @param sysHolderAccount 【请填写功能名称】
     * @return 结果
     */
    public int insertSysHolderAccount(List<SysHolderAccount> sysHolderAccounts,Long userId);

    /**
     * 修改【请填写功能名称】
     * 
     * @param sysHolderAccount 【请填写功能名称】
     * @return 结果
     */
    public int updateSysHolderAccount(SysHolderAccount sysHolderAccount);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param guids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    public int deleteSysHolderAccountByGuids(String[] guids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param guid 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteSysHolderAccountByGuid(String guid);
    
    public int deleteSysHolderAccountByUserId(Long userId);
}
