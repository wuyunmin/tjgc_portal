package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.UserInfoDing;
import com.ruoyi.system.domain.UserInfoGroup;

/**
 * 钉钉用户信息Service接口
 * 
 * @author tjgc
 * @date 2021-04-21
 */
public interface IUserInfoDingService 
{
    /**
     * 查询钉钉用户信息
     * 
     * @param id 钉钉用户信息ID
     * @return 钉钉用户信息
     */
    public UserInfoDing selectUserInfoDingById(Long id);

    /**
     * 查询钉钉用户信息列表
     * 
     * @param userInfoDing 钉钉用户信息
     * @return 钉钉用户信息集合
     */
    public List<UserInfoDing> selectUserInfoDingList(UserInfoDing userInfoDing);

    /**
     * 新增钉钉用户信息
     * 
     * @param userInfoDing 钉钉用户信息
     * @return 结果
     */
    public int insertUserInfoDing(UserInfoDing userInfoDing);

    /**
     * 修改钉钉用户信息
     * 
     * @param userInfoDing 钉钉用户信息
     * @return 结果
     */
    public int updateUserInfoDing(UserInfoDing userInfoDing);

    /**
     * 批量删除钉钉用户信息
     * 
     * @param ids 需要删除的钉钉用户信息ID
     * @return 结果
     */
    public int deleteUserInfoDingByIds(Long[] ids);

    /**
     * 删除钉钉用户信息信息
     * 
     * @param id 钉钉用户信息ID
     * @return 结果
     */
    public int deleteUserInfoDingById(Long id);

    public int updateUserInfoDingByUserNo(UserInfoDing userInfoDing);

    public String importUser(List<UserInfoDing> userInfoDingList, String operName,List<String> userNoList);
}
