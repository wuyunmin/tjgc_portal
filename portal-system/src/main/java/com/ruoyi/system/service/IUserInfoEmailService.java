package com.ruoyi.system.service;

import com.ruoyi.system.domain.UserInfoEmail;
import com.ruoyi.system.domain.UserInfoSchool;

import java.util.List;

/**
 * 企业邮箱用户信息Service接口
 * 
 * @author tjec
 * @date 2021-01-28
 */
public interface IUserInfoEmailService 
{
    /**
     * 查询企业邮箱用户信息
     * 
     * @param guid 企业邮箱用户信息ID
     * @return 企业邮箱用户信息
     */
    public UserInfoEmail selectUserInfoEmailByGuid(String guid);

    public UserInfoEmail selectUserInfoEmailByUserNo(String userNo);
    /**
     * 查询企业邮箱用户信息列表
     * 
     * @param userInfoEmail 企业邮箱用户信息
     * @return 企业邮箱用户信息集合
     */
    public List<UserInfoEmail> selectUserInfoEmailList(UserInfoEmail userInfoEmail);

    /**
     * 新增企业邮箱用户信息
     * 
     * @param userInfoEmail 企业邮箱用户信息
     * @return 结果
     */
    public int insertUserInfoEmail(UserInfoEmail userInfoEmail);

    /**
     * 修改企业邮箱用户信息
     * 
     * @param userInfoEmail 企业邮箱用户信息
     * @return 结果
     */
    public int updateUserInfoEmail(UserInfoEmail userInfoEmail);

    /**
     * 批量删除企业邮箱用户信息
     * 
     * @param guids 需要删除的企业邮箱用户信息ID
     * @return 结果
     */
    public int deleteUserInfoEmailByGuids(String[] guids);

    /**
     * 删除企业邮箱用户信息信息
     * 
     * @param guid 企业邮箱用户信息ID
     * @return 结果
     */
    public int deleteUserInfoEmailByGuid(String guid);
    
    /**
     * 导入用户数据
     *
     * @param userList        用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName        操作用户
     * @return 结果
     */
    public String importUser(List<UserInfoEmail> userInfoEmailList, String operName);
}
