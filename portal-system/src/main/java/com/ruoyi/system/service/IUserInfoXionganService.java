package com.ruoyi.system.service;

import com.ruoyi.system.domain.UserInfoXiaoyu;
import com.ruoyi.system.domain.UserInfoXiongan;
import java.util.List;

/**
 * 雄安协同平台用户信息Service接口
 * 
 * @author tjec
 * @date 2021-01-28
 */
public interface IUserInfoXionganService 
{
    /**
     * 查询雄安协同平台用户信息
     * 
     * @param guid 雄安协同平台用户信息ID
     * @return 雄安协同平台用户信息
     */
    public UserInfoXiongan selectUserInfoXionganByGuid(String guid);

    /**
     * 查询雄安协同平台用户信息列表
     * 
     * @param userInfoXiongan 雄安协同平台用户信息
     * @return 雄安协同平台用户信息集合
     */
    public List<UserInfoXiongan> selectUserInfoXionganList(UserInfoXiongan userInfoXiongan);

    /**
     * 新增雄安协同平台用户信息
     * 
     * @param userInfoXiongan 雄安协同平台用户信息
     * @return 结果
     */
    public int insertUserInfoXiongan(UserInfoXiongan userInfoXiongan);

    /**
     * 修改雄安协同平台用户信息
     * 
     * @param userInfoXiongan 雄安协同平台用户信息
     * @return 结果
     */
    public int updateUserInfoXiongan(UserInfoXiongan userInfoXiongan);

    /**
     * 批量删除雄安协同平台用户信息
     * 
     * @param guids 需要删除的雄安协同平台用户信息ID
     * @return 结果
     */
    public int deleteUserInfoXionganByGuids(String[] guids);

    /**
     * 删除雄安协同平台用户信息信息
     * 
     * @param guid 雄安协同平台用户信息ID
     * @return 结果
     */
    public int deleteUserInfoXionganByGuid(String guid);
    
    /**
     * 导入用户数据
     *
     * @param userList        用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName        操作用户
     * @return 结果
     */
    public String importUser(List<UserInfoXiongan> UserInfoXionganList, String operName);
    
    /**
     * 导入用户数据
     *
     * @param userList        用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName        操作用户
     * @return 结果
     */
    public String importOutMemberUser(List<UserInfoXiongan> userInfoXionganList, String operName);
}
