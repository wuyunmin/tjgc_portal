package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.entity.SysShortUrl;

import java.util.List;

/**
 * 文件信息Service接口
 * 
 * @author tjgc
 * @date 2021-02-08
 */
public interface ISysShortUrlService 
{
    /**
     * 查询文件信息
     * 
     * @param id 文件信息ID
     * @return 文件信息
     */
    public SysShortUrl selectSysShortUrlById(Long id);

    /**
     * 查询文件信息
     *
     * @param shotUrl 文件信息ID
     * @return 文件信息
     */
    public SysShortUrl selectSysShortUrlByShotUrl(String shotUrl);

    public int selectSysShortUrlCountByUserNo(String userNo);

    public int deleteSysShortUrlByShotUrl(String shortUrl);

    /**
     * 查询文件信息列表
     * 
     * @param sysShortUrl 文件信息
     * @return 文件信息集合
     */
    public List<SysShortUrl> selectSysShortUrlList(SysShortUrl sysShortUrl);


    /**
     * 新增文件信息
     * 
     * @param sysShortUrl 文件信息
     * @return 结果
     */
    public int insertSysShortUrl(SysShortUrl sysShortUrl);

    /**
     * 修改文件信息
     * 
     * @param sysShortUrl 文件信息
     * @return 结果
     */
    public int updateSysShortUrl(SysShortUrl sysShortUrl);

    /**
     * 批量删除文件信息
     * 
     * @param ids 需要删除的文件信息ID
     * @return 结果
     */
    public int deleteSysShortUrlByIds(Long[] ids);


    public int deleteSysShortUrlByUserNo(String userNo);

    /**
     * 删除文件信息信息
     * 
     * @param id 文件信息ID
     * @return 结果
     */
    public int deleteSysShortUrlById(Long id);
}
