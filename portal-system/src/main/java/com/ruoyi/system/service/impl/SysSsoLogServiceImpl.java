package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysSsoLogMapper;
import com.ruoyi.system.domain.SysSsoLog;
import com.ruoyi.system.service.ISysSsoLogService;

/**
 * SSO登陆日志Service业务层处理
 * 
 * @author tjgc
 * @date 2020-12-15
 */
@Service
public class SysSsoLogServiceImpl implements ISysSsoLogService 
{
    @Autowired
    private SysSsoLogMapper sysSsoLogMapper;

    /**
     * 查询SSO登陆日志
     * 
     * @param id SSO登陆日志ID
     * @return SSO登陆日志
     */
    @Override
    public SysSsoLog selectSysSsoLogById(String id)
    {
        return sysSsoLogMapper.selectSysSsoLogById(id);
    }

    /**
     * 查询SSO登陆日志列表
     * 
     * @param sysSsoLog SSO登陆日志
     * @return SSO登陆日志
     */
    @Override
    public List<SysSsoLog> selectSysSsoLogList(SysSsoLog sysSsoLog)
    {
        return sysSsoLogMapper.selectSysSsoLogList(sysSsoLog);
    }

    /**
     * 新增SSO登陆日志
     * 
     * @param sysSsoLog SSO登陆日志
     * @return 结果
     */
    @Override
    public int insertSysSsoLog(SysSsoLog sysSsoLog)
    {
        sysSsoLog.setId(IdUtils.fastSimpleUUID());
        return sysSsoLogMapper.insertSysSsoLog(sysSsoLog);
    }

    /**
     * 修改SSO登陆日志
     * 
     * @param sysSsoLog SSO登陆日志
     * @return 结果
     */
    @Override
    public int updateSysSsoLog(SysSsoLog sysSsoLog)
    {
        return sysSsoLogMapper.updateSysSsoLog(sysSsoLog);
    }

    /**
     * 批量删除SSO登陆日志
     * 
     * @param ids 需要删除的SSO登陆日志ID
     * @return 结果
     */
    @Override
    public int deleteSysSsoLogByIds(String[] ids)
    {
        return sysSsoLogMapper.deleteSysSsoLogByIds(ids);
    }

    /**
     * 删除SSO登陆日志信息
     * 
     * @param id SSO登陆日志ID
     * @return 结果
     */
    @Override
    public int deleteSysSsoLogById(String id)
    {
        return sysSsoLogMapper.deleteSysSsoLogById(id);
    }
}
