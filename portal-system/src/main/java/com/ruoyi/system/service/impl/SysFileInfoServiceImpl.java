package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysFileInfoMapper;
import com.ruoyi.system.domain.SysFileInfo;
import com.ruoyi.system.service.ISysFileInfoService;

import javax.annotation.Resource;

/**
 * 文件信息Service业务层处理
 * 
 * @author tjgc
 * @date 2021-01-19
 */
@Service
public class SysFileInfoServiceImpl implements ISysFileInfoService 
{
    @Resource
    private SysFileInfoMapper sysFileInfoMapper;



    @Override
    public String selectSysFilePathById(String fileId)
    {
        return sysFileInfoMapper.selectSysFilePathById(fileId);
    }
    /**
     * 查询文件信息
     * 
     * @param fileId 文件信息ID
     * @return 文件信息
     */
    @Override
    public SysFileInfo selectSysFileInfoById(String fileId)
    {
        return sysFileInfoMapper.selectSysFileInfoById(fileId);
    }

    /**
     * 查询文件信息列表
     * 
     * @param sysFileInfo 文件信息
     * @return 文件信息
     */
    @Override
    public List<SysFileInfo> selectSysFileInfoList(SysFileInfo sysFileInfo)
    {
        return sysFileInfoMapper.selectSysFileInfoList(sysFileInfo);
    }


    /**
     * 查询文件信息列表
     *
     * @param sysFileIds 文件信息
     * @return 文件信息
     */
    @Override
    public List<String> selectSysFileInfoList(List<String> sysFileIds)
    {
        return sysFileInfoMapper.selectSysFileInfoListByList(sysFileIds);
    }
    /**
     * 新增文件信息
     * 
     * @param sysFileInfo 文件信息
     * @return 结果
     */
    @Override
    public String insertSysFileInfo(SysFileInfo sysFileInfo)
    {
        String uuid = UUID.randomUUID().toString();
        sysFileInfo.setFileId(uuid);
        int i = sysFileInfoMapper.insertSysFileInfo(sysFileInfo);
        if (i > 0) {
            return uuid;
        }
        return null;
    }

    /**
     * 修改文件信息
     * 
     * @param sysFileInfo 文件信息
     * @return 结果
     */
    @Override
    public int updateSysFileInfo(SysFileInfo sysFileInfo)
    {
        return sysFileInfoMapper.updateSysFileInfo(sysFileInfo);
    }

    /**
     * 批量删除文件信息
     * 
     * @param fileIds 需要删除的文件信息ID
     * @return 结果
     */
    @Override
    public int deleteSysFileInfoByIds(String[] fileIds)
    {
        return sysFileInfoMapper.deleteSysFileInfoByIds(fileIds);
    }

    /**
     * 删除文件信息信息
     * 
     * @param fileId 文件信息ID
     * @return 结果
     */
    @Override
    public int deleteSysFileInfoById(String fileId)
    {
        return sysFileInfoMapper.deleteSysFileInfoById(fileId);
    }
}
