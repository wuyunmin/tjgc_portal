package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SysIntserv;

/**
 * 综合服务配合Service接口
 * 
 * @author tjgc
 * @date 2020-12-29
 */
public interface ISysIntservService 
{
    /**
     * 查询综合服务配合
     * 
     * @param id 综合服务配合ID
     * @return 综合服务配合
     */
    public SysIntserv selectSysIntservById(Long id);

    /**
     * 查询综合服务配合列表
     * 
     * @param sysIntserv 综合服务配合
     * @return 综合服务配合集合
     */
    public List<SysIntserv> selectSysIntservList(SysIntserv sysIntserv);


    /**
     * 查询可用链接
     * @return
     */
    public List<SysIntserv> selectLinkList();


    /**
     * 查询可用二维码
     * @return
     */
    public List<SysIntserv> selectWeChatList();
    /**
     * 新增综合服务配合
     * 
     * @param sysIntserv 综合服务配合
     * @return 结果
     */
    public int insertSysIntserv(SysIntserv sysIntserv);

    /**
     * 修改综合服务配合
     * 
     * @param sysIntserv 综合服务配合
     * @return 结果
     */
    public int updateSysIntserv(SysIntserv sysIntserv);

    /**
     * 批量删除综合服务配合
     * 
     * @param ids 需要删除的综合服务配合ID
     * @return 结果
     */
    public int deleteSysIntservByIds(Long[] ids);

    /**
     * 删除综合服务配合信息
     * 
     * @param id 综合服务配合ID
     * @return 结果
     */
    public int deleteSysIntservById(Long id);

    /**
     * 修改综合服务配合信息状态
     * @param sysIntserv
     * @return
     */
    public int updateSysIntservStatus(SysIntserv sysIntserv);
}
