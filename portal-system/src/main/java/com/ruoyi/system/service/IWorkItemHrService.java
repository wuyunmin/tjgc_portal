package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.WorkItemHr;

/**
 * hr待办待阅信息Service接口
 *
 * @author tjgc
 * @date 2022-05-09
 */
public interface IWorkItemHrService
{
    /**
     * 查询hr待办待阅信息
     *
     * @param id hr待办待阅信息ID
     * @return hr待办待阅信息
     */
    public WorkItemHr selectWorkItemHrById(Long id);

    /**
     * 查询hr待办待阅信息列表
     *
     * @param workItemHr hr待办待阅信息
     * @return hr待办待阅信息集合
     */
    public List<WorkItemHr> selectWorkItemHrList(WorkItemHr workItemHr);

    /**
     * 新增hr待办待阅信息
     *
     * @param workItemHr hr待办待阅信息
     * @return 结果
     */
    public int insertWorkItemHr(WorkItemHr workItemHr);

    /**
     * 修改hr待办待阅信息
     *
     * @param workItemHr hr待办待阅信息
     * @return 结果
     */
    public int updateWorkItemHr(WorkItemHr workItemHr);

    /**
     * 批量删除hr待办待阅信息
     *
     * @param ids 需要删除的hr待办待阅信息ID
     * @return 结果
     */
    public int deleteWorkItemHrByIds(Long[] ids);

    /**
     * 删除hr待办待阅信息信息
     *
     * @param id hr待办待阅信息ID
     * @return 结果
     */
    public int deleteWorkItemHrById(Long id);


    /**
     * 同步hr待办待阅
     */
    public void syncWorkItemHr(String userName);

}
