package com.ruoyi.system.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.UserInfoBpm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UserInfoLkMapper;
import com.ruoyi.system.domain.UserInfoLk;
import com.ruoyi.system.service.IUserInfoLkService;

import javax.annotation.Resource;

import static com.ruoyi.common.utils.DateUtils.*;

/**
 * 领款用户信息Service业务层处理
 * 
 * @author tjgc
 * @date 2021-01-06
 */
@Service
public class UserInfoLkServiceImpl implements IUserInfoLkService 
{
    @Resource
    private UserInfoLkMapper userInfoLkMapper;

    /**
     * 查询领款用户信息
     * 
     * @param id 领款用户信息ID
     * @return 领款用户信息
     */
    @Override
    public UserInfoLk selectUserInfoLkById(Long id)
    {
        return userInfoLkMapper.selectUserInfoLkById(id);
    }

    /**
     * 查询领款用户信息列表
     * 
     * @param userInfoLk 领款用户信息
     * @return 领款用户信息
     */
    @Override
    public List<UserInfoLk> selectUserInfoLkList(UserInfoLk userInfoLk)
    {
        return userInfoLkMapper.selectUserInfoLkList(userInfoLk);
    }

    /**
     * 新增领款用户信息
     * 
     * @param userInfoLk 领款用户信息
     * @return 结果
     */
    @Override
    public int insertUserInfoLk(UserInfoLk userInfoLk)
    {
        userInfoLk.setCreateTime(DateUtils.getNowDate());
        return userInfoLkMapper.insertUserInfoLk(userInfoLk);
    }

    /**
     * 新增领款用户信息
     *
     * @param userInfoLkMap 领款用户信息
     * @return 结果
     */
    @Override
    public int insertUserInfoLk(Map<String,Object> userInfoLkMap)
    {
        int userCount = userInfoLkMapper.selectCountUserInfoLkByUserNo(userInfoLkMap.get("userNo").toString());
        UserInfoLk userInfoLk = convertValue(userInfoLkMap);
        if (userCount > 0) {
            return userInfoLkMapper.updateUserInfoLkByUserNo(userInfoLk);
        } else {
            userInfoLk.setCreateTime(DateUtils.getNowDate());
            return userInfoLkMapper.insertUserInfoLk(userInfoLk);
        }
    }
    /**
     * 修改领款用户信息
     * 
     * @param userInfoLk 领款用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoLk(UserInfoLk userInfoLk)
    {
        userInfoLk.setUpdateTime(DateUtils.getNowDate());
        return userInfoLkMapper.updateUserInfoLk(userInfoLk);
    }

    /**
     * 修改领款用户信息
     *
     * @param userInfoLkMap 领款用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoLk(Map<String,Object> userInfoLkMap)
    {
        UserInfoLk userInfoLk = convertValue(userInfoLkMap);
        userInfoLk.setUpdateTime(DateUtils.getNowDate());
        return userInfoLkMapper.updateUserInfoLkByUserNo(userInfoLk);
    }

    /**
     * 批量删除领款用户信息
     * 
     * @param ids 需要删除的领款用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoLkByIds(Long[] ids)
    {
        return userInfoLkMapper.deleteUserInfoLkByIds(ids);
    }

    /**
     * 删除领款用户信息信息
     * 
     * @param id 领款用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoLkById(Long id)
    {
        return userInfoLkMapper.deleteUserInfoLkById(id);
    }

    private UserInfoLk convertValue(Map<String,Object> userInfoMap){
        UserInfoLk userInfoLk = new UserInfoLk();
        Object userNo = userInfoMap.get("userNo");
        Object userName = userInfoMap.get("userName");
        Object loginName = userInfoMap.get("loginName");
        Object loginDate = userInfoMap.get("loginDate");
        Object systemName = userInfoMap.get("systemName");
        Object updateTime = userInfoMap.get("updateTime");
        Object userStatus = userInfoMap.get("userStatus");
        Object competentOrg = userInfoMap.get("competentOrg");
        Object competentDept = userInfoMap.get("competentDept");
        userInfoLk.setUserNo(null == userNo ? null : userNo.toString());
        userInfoLk.setLoginName(null == loginName ? null : loginName.toString());
        userInfoLk.setUserName(null == userName ? null : userName.toString());
        userInfoLk.setSystemName(null == systemName ? null : systemName.toString());
        userInfoLk.setUserStatus(null == userStatus ? null : userStatus.toString());
        userInfoLk.setCompetentOrg(null == competentOrg ? null : competentOrg.toString());
        userInfoLk.setCompetentDept(null == competentDept ? null : competentDept.toString());
        userInfoLk.setLoginDate(null == loginDate ? null : parseDate(loginDate));
        userInfoLk.setSysUpdateTime(null == updateTime ? null : parseDate(updateTime));
        return userInfoLk;
    }
}
