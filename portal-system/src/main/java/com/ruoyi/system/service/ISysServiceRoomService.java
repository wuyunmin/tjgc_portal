package com.ruoyi.system.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.SysServiceRoom;
import com.ruoyi.system.domain.SysServiceRoomDto;

/**
 * 服务大厅Service接口
 * 
 * @author tjgc
 * @date 2021-03-09
 */
public interface ISysServiceRoomService 
{
    /**
     * 查询服务大厅
     * 
     * @param guid 服务大厅ID
     * @return 服务大厅
     */
    public SysServiceRoom selectSysServiceRoomByGuid(String guid);

    /**
     * 查询服务大厅列表
     * 
     * @param sysServiceRoom 服务大厅
     * @return 服务大厅集合
     */
    public List<SysServiceRoom> selectSysServiceRoomList(SysServiceRoom sysServiceRoom);

    /**
     * 新增服务大厅
     * 
     * @param sysServiceRoom 服务大厅
     * @return 结果
     */
    public int insertSysServiceRoom(SysServiceRoom sysServiceRoom);

    /**
     * 修改服务大厅
     * 
     * @param sysServiceRoom 服务大厅
     * @return 结果
     */
    public int updateSysServiceRoom(SysServiceRoom sysServiceRoom);

    /**
     * 批量删除服务大厅
     * 
     * @param guids 需要删除的服务大厅ID
     * @return 结果
     */
    public int deleteSysServiceRoomByGuids(String[] guids);

    /**
     * 删除服务大厅信息
     * 
     * @param guid 服务大厅ID
     * @return 结果
     */
    public int deleteSysServiceRoomByGuid(String guid);
    
    //public LinkedHashMap<String, Object> searchServiceRoom(SysServiceRoom sysServiceRoom); 
    
    public List<SysServiceRoomDto> searchServiceRoom(SysServiceRoom sysServiceRoom); 
}
