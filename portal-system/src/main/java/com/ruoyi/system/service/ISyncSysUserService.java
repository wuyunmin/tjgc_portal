package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.SyncSysDept;
import com.ruoyi.system.domain.SyncSysTemp;
import com.ruoyi.system.domain.SyncSysUser;

/**
 * 部门同步中间Service接口
 * 
 * @author tjgc
 * @date 2021-01-20
 */
public interface ISyncSysUserService 
{

    /**
     * 查询部门同步中间列表
     * 
     * @param syncSysUser 部门同步中间
     * @return 部门同步中间集合
     */
    public List<SyncSysUser> selectSyncSysUserList(SyncSysUser syncSysUser);

    /**
     * 查询用户
     * @param syncSysUser
     * @return
     */
    public List<SyncSysUser> selectSyncSysUserListForHR(SyncSysUser syncSysUser);

    /**
     * 新增部门同步中间
     * 
     * @param syncSysUser 部门同步中间
     * @return 结果
     */
    public int insertSyncSysUser(SyncSysUser syncSysUser);


    /**
     * 根据id查询用户
     * @param id
     * @return
     */
    public SyncSysUser selectUserInfoHrById(Long id);


    /**
     * 新增部门同步中间
     *
     * @param syncSysUserMap 部门同步中间
     * @return 结果
     */
    public int insertSyncSysUser(Map<String,Object> syncSysUserMap);


    /**
     * 查询用户信息是否已存在
     * @param empId 员工工号
     * @return 数量
     */
    public int existSyncSysUser(String empId);

    /**
     * 修改部门同步中间
     * 
     * @param syncSysUser 部门同步中间
     * @return 结果
     */
    public int updateSyncSysUser(SyncSysUser syncSysUser);

    /**
     * 修改部门同步中间
     *
     * @param syncSysUserMap 部门同步中间
     * @return 结果
     */
    public int updateSyncSysUser(Map<String,Object> syncSysUserMap);

    /**
     * 批量删除部门同步中间
     * 
     * @param ids 需要删除的部门同步中间ID
     * @return 结果
     */
    public int deleteSyncSysUserByIds(Long[] ids);

    /**
     * 删除部门同步中间信息
     * 
     * @param id 部门同步中间ID
     * @return 结果
     */
    public int deleteSyncSysUserById(Long id);

    public List<SyncSysTemp> selectSyncTempList();

    public int updataSyncTempStatus(String id);


    public SyncSysUser selectUserByUserID(String id);
}
