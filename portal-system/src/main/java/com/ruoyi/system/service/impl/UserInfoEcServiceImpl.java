package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.UserInfoLk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UserInfoEcMapper;
import com.ruoyi.system.domain.UserInfoEc;
import com.ruoyi.system.service.IUserInfoEcService;

import javax.annotation.Resource;

import static com.ruoyi.common.utils.DateUtils.parseDate;

/**
 * 领款用户信息Service业务层处理
 * 
 * @author tjgc
 * @date 2021-01-07
 */
@Service
public class UserInfoEcServiceImpl implements IUserInfoEcService 
{
    @Resource
    private UserInfoEcMapper userInfoEcMapper;

    /**
     * 查询领款用户信息
     * 
     * @param id 领款用户信息ID
     * @return 领款用户信息
     */
    @Override
    public UserInfoEc selectUserInfoEcById(Long id)
    {
        return userInfoEcMapper.selectUserInfoEcById(id);
    }

    /**
     * 查询领款用户信息列表
     * 
     * @param userInfoEc 领款用户信息
     * @return 领款用户信息
     */
    @Override
    public List<UserInfoEc> selectUserInfoEcList(UserInfoEc userInfoEc)
    {
        return userInfoEcMapper.selectUserInfoEcList(userInfoEc);
    }

    /**
     * 新增领款用户信息
     * 
     * @param userInfoMap 领款用户信息
     * @return 结果
     */
    @Override
    public int insertUserInfoEc(Map<String,Object> userInfoMap)
    {
        UserInfoEc userInfoEc = convertValue(userInfoMap);
        userInfoEc.setCreateTime(DateUtils.getNowDate());
        return userInfoEcMapper.insertUserInfoEc(userInfoEc);
    }


    /**
     * 新增领款用户信息
     *
     * @param userInfoEc 领款用户信息
     * @return 结果
     */
    @Override
    public int insertUserInfoEc(UserInfoEc userInfoEc)
    {
        userInfoEc.setCreateTime(DateUtils.getNowDate());
        return userInfoEcMapper.insertUserInfoEc(userInfoEc);
    }
    /**
     * 修改领款用户信息
     * 
     * @param userInfoEc 领款用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoEc(UserInfoEc userInfoEc)
    {
        userInfoEc.setUpdateTime(DateUtils.getNowDate());
        return userInfoEcMapper.updateUserInfoEc(userInfoEc);
    }

    /**
     * 修改领款用户信息
     *
     * @param userInfoMap 领款用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoEc(Map<String,Object> userInfoMap)
    {
        UserInfoEc userInfoEc = convertValue(userInfoMap);
        userInfoEc.setUpdateTime(DateUtils.getNowDate());
        return userInfoEcMapper.updateUserInfoEcByUserNo(userInfoEc);
    }

    /**
     * 批量删除领款用户信息
     * 
     * @param ids 需要删除的领款用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoEcByIds(Long[] ids)
    {
        return userInfoEcMapper.deleteUserInfoEcByIds(ids);
    }

    /**
     * 删除领款用户信息信息
     * 
     * @param id 领款用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoEcById(Long id)
    {
        return userInfoEcMapper.deleteUserInfoEcById(id);
    }



    private UserInfoEc convertValue(Map<String,Object> userInfoMap){
        UserInfoEc userInfoEc = new UserInfoEc();
        Object userNo = userInfoMap.get("userNo");
        Object userName = userInfoMap.get("userName");
        Object loginName = userInfoMap.get("loginName");
        Object loginDate = userInfoMap.get("loginDate");
        Object systemName = userInfoMap.get("systemName");
        Object updateTime = userInfoMap.get("updateTime");
        Object userStatus = userInfoMap.get("userStatus");
        Object competentOrg = userInfoMap.get("competentOrg");
        Object competentDept = userInfoMap.get("competentDept");
        userInfoEc.setUserNo(null == userNo ? null : userNo.toString());
        userInfoEc.setUserName(null == userName ? null : userName.toString());
        userInfoEc.setLoginName(null == loginName ? null : loginName.toString());
        userInfoEc.setSystemName(null == systemName ? null : systemName.toString());
        userInfoEc.setUserStatus(null == userStatus ? null : userStatus.toString());
        userInfoEc.setLoginDate(null == loginDate ? null : parseDate(loginDate));
        userInfoEc.setSysUpdateTime(null == updateTime ? null : parseDate(updateTime));
        userInfoEc.setCompetentDept(null==competentDept?null:competentDept.toString());
        userInfoEc.setCompetentOrg(null==competentOrg? null:competentOrg.toString());
        return userInfoEc;
    }
}
