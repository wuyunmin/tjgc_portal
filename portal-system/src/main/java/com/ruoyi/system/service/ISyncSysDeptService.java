package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.SyncSysDept;

/**
 * 部门同步中间Service接口
 * 
 * @author tjgc
 * @date 2021-01-20
 */
public interface ISyncSysDeptService 
{

    /**
     * 查询部门同步中间列表
     * 
     * @param syncSysDept 部门同步中间
     * @return 部门同步中间集合
     */
    public List<SyncSysDept> selectSyncSysDeptList(SyncSysDept syncSysDept);

    /**
     * 新增部门同步中间
     * 
     * @param syncSysDept 部门同步中间
     * @return 结果
     */
    public int insertSyncSysDept(SyncSysDept syncSysDept);



    /**
     * 新增部门同步中间
     *
     * @param syncSysDeptMap 部门同步中间
     * @return 结果
     */
    public int insertSyncSysDept(Map<String,Object> syncSysDeptMap);
    /**
     * 修改部门同步中间
     * 
     * @param syncSysDept 部门同步中间
     * @return 结果
     */
    public int updateSyncSysDept(SyncSysDept syncSysDept);



    public int existSyncSysDept(String deptIdCode);


    /**
     * 修改部门同步中间
     *
     * @param syncSysDeptMap 部门同步中间
     * @return 结果
     */
    public int updateSyncSysDept(Map<String,Object> syncSysDeptMap);

    /**
     * 批量删除部门同步中间
     * 
     * @param ids 需要删除的部门同步中间ID
     * @return 结果
     */
    public int deleteSyncSysDeptByIds(Long[] ids);

    /**
     * 删除部门同步中间信息
     * 
     * @param id 部门同步中间ID
     * @return 结果
     */
    public int deleteSyncSysDeptById(Long id);

    /**
     * 根据部门id获取部门名称
     * @param id
     * @return
     */
    public SyncSysDept selectDeptByDeptId(String id);
}
