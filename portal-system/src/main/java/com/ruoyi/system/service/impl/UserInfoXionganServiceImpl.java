package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UserInfoXionganMapper;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.UserInfoXiaoyu;
import com.ruoyi.system.domain.UserInfoXiongan;
import com.ruoyi.system.service.IUserInfoXionganService;

/**
 * 雄安协同平台用户信息Service业务层处理
 * 
 * @author tjec
 * @date 2021-01-28
 */
@Service
public class UserInfoXionganServiceImpl implements IUserInfoXionganService 
{
    @Autowired
    private UserInfoXionganMapper userInfoXionganMapper;

    /**
     * 查询雄安协同平台用户信息
     * 
     * @param guid 雄安协同平台用户信息ID
     * @return 雄安协同平台用户信息
     */
    @Override
    public UserInfoXiongan selectUserInfoXionganByGuid(String guid)
    {
        return userInfoXionganMapper.selectUserInfoXionganByGuid(guid);
    }

    /**
     * 查询雄安协同平台用户信息列表
     * 
     * @param userInfoXiongan 雄安协同平台用户信息
     * @return 雄安协同平台用户信息
     */
    @Override
    public List<UserInfoXiongan> selectUserInfoXionganList(UserInfoXiongan userInfoXiongan)
    {
        return userInfoXionganMapper.selectUserInfoXionganList(userInfoXiongan);
    }

    /**
     * 新增雄安协同平台用户信息
     * 
     * @param userInfoXiongan 雄安协同平台用户信息
     * @return 结果
     */
    @Override
    public int insertUserInfoXiongan(UserInfoXiongan userInfoXiongan)
    {
        return userInfoXionganMapper.insertUserInfoXiongan(userInfoXiongan);
    }

    /**
     * 修改雄安协同平台用户信息
     * 
     * @param userInfoXiongan 雄安协同平台用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoXiongan(UserInfoXiongan userInfoXiongan)
    {
        return userInfoXionganMapper.updateUserInfoXiongan(userInfoXiongan);
    }

    /**
     * 批量删除雄安协同平台用户信息
     * 
     * @param guids 需要删除的雄安协同平台用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoXionganByGuids(String[] guids)
    {
        return userInfoXionganMapper.deleteUserInfoXionganByGuids(guids);
    }

    /**
     * 删除雄安协同平台用户信息信息
     * 
     * @param guid 雄安协同平台用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoXionganByGuid(String guid)
    {
        return userInfoXionganMapper.deleteUserInfoXionganByGuid(guid);
    }

	@Override
	public String importUser(List<UserInfoXiongan> UserInfoXionganList, String operName) {
		//1、先删除所有内部成员原表数据
		userInfoXionganMapper.cleanXionganUser();
		//2、导入新数据
		if (StringUtils.isNull(UserInfoXionganList) || UserInfoXionganList.size() == 0) {
			throw new CustomException("导入用户数据不能为空！");
		}
		int successNum = 0;
		int failureNum = 0;
		StringBuilder successMsg = new StringBuilder();
		StringBuilder failureMsg = new StringBuilder();
		for (UserInfoXiongan userInfoXiongan : UserInfoXionganList) {
			try {
				userInfoXiongan.setCreateBy(operName);
				userInfoXiongan.setUserFlag("内部用户");
				userInfoXiongan.setUserNo(userInfoXiongan.getUserNo()==null?"":userInfoXiongan.getUserNo().trim());
				userInfoXiongan.setCellPhone(userInfoXiongan.getCellPhone()==null?"":userInfoXiongan.getCellPhone().trim());
				this.insertUserInfoXiongan(userInfoXiongan);
				successNum++;
				//successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 导入成功");
			} catch (Exception e) {
				failureNum++;
				String msg = "<br/>" + failureNum + "、姓名 " + userInfoXiongan.getUserName() + " 导入失败：";
				failureMsg.append(msg + e.getMessage());
			}
		}
		if (failureNum > 0) {
			failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
			throw new CustomException(failureMsg.toString());
		} else {
			successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
		}
		return successMsg.toString();
	}

	@Override
	public String importOutMemberUser(List<UserInfoXiongan> userInfoXionganList, String operName) {
		//1、先删除所有内部成员原表数据
		userInfoXionganMapper.cleanXionganUserOutMember();
		//2、导入新数据
		if (StringUtils.isNull(userInfoXionganList) || userInfoXionganList.size() == 0) {
			throw new CustomException("导入用户数据不能为空！");
		}
		int successNum = 0;
		int failureNum = 0;
		StringBuilder successMsg = new StringBuilder();
		StringBuilder failureMsg = new StringBuilder();
		for (UserInfoXiongan userInfoXiongan : userInfoXionganList) {
			try {
				userInfoXiongan.setCreateBy(operName);
				userInfoXiongan.setUserFlag("外部用户");
				this.insertUserInfoXiongan(userInfoXiongan);
				successNum++;
				//successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 导入成功");
			} catch (Exception e) {
				failureNum++;
				String msg = "<br/>" + failureNum + "、姓名 " + userInfoXiongan.getUserName() + " 导入失败：";
				failureMsg.append(msg + e.getMessage());
			}
		}
		if (failureNum > 0) {
			failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
			throw new CustomException(failureMsg.toString());
		} else {
			successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
		}
		return successMsg.toString();
	}
}
