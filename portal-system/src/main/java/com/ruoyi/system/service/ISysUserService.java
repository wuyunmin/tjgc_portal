package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.core.domain.entity.SysUser;

/**
 * 用户 业务层
 *
 * @author ruoyi
 */
public interface ISysUserService
{
    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUserList(SysUser user);

    public List<SysUser> userListSearch(SysUser user);


    public List<Map<String,Object>> checkedListSearch(Long[] ids);

    public List<SysUser> selectAllUserList(SysUser user);

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    public SysUser selectUserByUserName(String userName);


    public String selectUserStatusByUserNo(String userNo) throws Exception;

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public SysUser selectUserById(Long Id);

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public SysUser selectUserByUserId(String Id);



    public SysUser selectUnifyUserById(Long Id);
    /**
     * 通过用户ID查询用户---同一用户管理使用
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public SysUser selectUnifyUserByUserId(Long Id);

    /**
     * 根据用户ID查询用户所属角色组
     *
     * @param userName 用户名
     * @return 结果
     */
    public String selectUserRoleGroup(String userName);

    /**
     * 根据用户ID查询用户所属岗位组
     *
     * @param userName 用户名
     * @return 结果
     */
    public String selectUserPostGroup(String userName);

    /**
     * 校验用户名称是否唯一
     *
     * @param userName 用户名称
     * @return 结果
     */
    public String checkUserNameUnique(String userName);


    public SysUser checkUserIdUnique(String userName);


    public SysUser checkUserIdCodeUnique(String userIdCode);

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public String checkPhoneUnique(SysUser user);

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public String checkEmailUnique(SysUser user);

    /**
     * 校验用户是否允许操作
     *
     * @param user 用户信息
     */
    public void checkUserAllowed(SysUser user);

    /**
     * 新增用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public int insertUser(SysUser user);

    /**
     * 修改用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public int updateUser(SysUser user);

    /**
     * 修改用户状态
     *
     * @param user 用户信息
     * @return 结果
     */
    public int updateUserStatus(SysUser user);

    /**
     * 修改用户基本信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public int updateUserProfile(SysUser user);

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    public boolean updateUserAvatar(String userName, String avatar);

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    public int resetPwd(SysUser user);

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    public int resetUserPwd(String userName, String password);

    /**
     * 通过用户ID删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    public int deleteUserById(Long Id);

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    public int deleteUserByIds(Long[] Ids);

    /**
     * 导入用户数据
     *
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importUser(List<SysUser> userList, Boolean isUpdateSupport, String operName);

    /**
     * 通过手机号查询用户
     *
     * @param phonenumber 手机号
     * @return 用户对象信息
     */
    public List<SysUser> selectUserByPhonenumber(String phonenumber);

    /**
     * 根据用户手机号修改密码
     * @param sysUser
     * @return
     */
    public int resetUserPwdByPhonenumber(SysUser sysUser);

    /**
     * 根据用户工号获取用户在各个系统的信息
     * @param sysUser
     * @return
     */
    public Map<String, Object> getUserInfoByJobNoOrPhone(SysUser sysUser);




    /**
     * 获取所有用户列表
     * @return
     */
    public List<SysUser> selectAllUser();

    /**
     * 判断强制结束状态大于当前时间的，更新强制状态为取消强制
     * @return
     */
    public int updateUserForceFlag();

    /**
     * 判断当前时间小于离职时间，但是人员状态为离职，更新状态为离职待关闭
     * @return
     */
    public int resigningStatus();

    /**
     * 判断当前时间大于离职时间，人员状态为离职，更新状态为离职停用
     * @return
     */
    public int updateCloseUserAccount();


    /**
     * 初始化门户账号
     * @param user
     * @return
     */
    public void initUserAccountStatus(SysUser user);

    /**
     * 初始化hr账号
     * @param user
     * @return
     */
    public void initHrUserAccountStatus(SysUser user);


    /**
     * 用户状态联动
     */
    public void userStatusLinkage ();

}
