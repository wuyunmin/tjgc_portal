package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.HrWebServiceUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.UserInfoEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UserInfoGroupMapper;
import com.ruoyi.system.domain.UserInfoGroup;
import com.ruoyi.system.service.IUserInfoGroupService;

/**
 * 域用户信息Service业务层处理
 *
 * @author tjgc
 * @date 2021-04-15
 */
@Service
public class UserInfoGroupServiceImpl implements IUserInfoGroupService
{

    private static final Logger logger = LoggerFactory.getLogger(UserInfoGroupServiceImpl.class);


    @Autowired
    private UserInfoGroupMapper userInfoGroupMapper;

    /**
     * 查询域用户信息
     *
     * @param id 域用户信息ID
     * @return 域用户信息
     */
    @Override
    public UserInfoGroup selectUserInfoGroupById(String id)
    {
        return userInfoGroupMapper.selectUserInfoGroupById(id);
    }

    /**
     * 查询域用户信息列表
     *
     * @param userInfoGroup 域用户信息
     * @return 域用户信息
     */
    @Override
    public List<UserInfoGroup> selectUserInfoGroupList(UserInfoGroup userInfoGroup)
    {
        return userInfoGroupMapper.selectUserInfoGroupList(userInfoGroup);
    }

    /**
     * 新增域用户信息
     *
     * @param userInfoGroup 域用户信息
     * @return 结果
     */
    @Override
    public int insertUserInfoGroup(UserInfoGroup userInfoGroup)
    {
        userInfoGroup.setCreateTime(DateUtils.getNowDate());
        return userInfoGroupMapper.insertUserInfoGroup(userInfoGroup);
    }

    /**
     * 修改域用户信息
     *
     * @param userInfoGroup 域用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoGroup(UserInfoGroup userInfoGroup)
    {
        userInfoGroup.setUpdateTime(DateUtils.getNowDate());
        return userInfoGroupMapper.updateUserInfoGroup(userInfoGroup);
    }

    /**
     * 批量删除域用户信息
     *
     * @param ids 需要删除的域用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoGroupByIds(String[] ids)
    {
        return userInfoGroupMapper.deleteUserInfoGroupByIds(ids);
    }

    /**
     * 删除域用户信息信息
     *
     * @param id 域用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoGroupById(String id)
    {
        return userInfoGroupMapper.deleteUserInfoGroupById(id);
    }


    @Override
    public String importUser(List<UserInfoGroup> userInfoGroupList, String operName) {
        //1、先删除所有原表数据
//        userInfoGroupMapper.cleanGroupUser();
        //2、导入新数据
        if (StringUtils.isNull(userInfoGroupMapper) || userInfoGroupList.size() == 0) {
            throw new CustomException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (UserInfoGroup userInfoGroup : userInfoGroupList) {
            try {
                int count = userInfoGroupMapper.selectCountUserInfoGroup(userInfoGroup.getUserNo());
                String status = userInfoGroup.getUserStatus() == null ? null : userInfoGroup.getUserStatus().trim();
                logger.info("域用户导入：status :{}",status);
                if (count == 0) {
                    userInfoGroup.setCreateBy(operName);
                    if(userInfoGroup.getLoginName()!=null) {
                        userInfoGroup.setLoginName(userInfoGroup.getLoginName().trim());
                    }

                    userInfoGroupMapper.insertUserInfoGroup(userInfoGroup);
                }else {
                    userInfoGroupMapper.updateUserInfoGroupByImport(userInfoGroup);
                }

                successNum++;
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、姓名 " + userInfoGroup.getUserName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new CustomException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
