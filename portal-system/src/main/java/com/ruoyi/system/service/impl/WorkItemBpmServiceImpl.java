package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SyncDataLog;
import com.ruoyi.system.domain.WorkItemHr;
import com.ruoyi.system.domain.WorkItemPend;
import com.ruoyi.system.mapper.SyncDataLogMapper;
import com.ruoyi.system.mapper.WorkItemPendMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.WorkItemBpmMapper;
import com.ruoyi.system.domain.WorkItemBpm;
import com.ruoyi.system.service.IWorkItemBpmService;

import javax.annotation.Resource;

/**
 * hr待办待阅信息Service业务层处理
 *
 * @author tjgc
 * @date 2022-05-16
 */
@Service
public class WorkItemBpmServiceImpl implements IWorkItemBpmService
{
    @Resource
    private WorkItemBpmMapper workItemBpmMapper;

    @Resource
    private WorkItemPendMapper workItemPendMapper;

    @Resource
    private  SyncDataLogMapper syncDataLogMapper;

    /**
     * 查询hr待办待阅信息
     *
     * @param id hr待办待阅信息ID
     * @return hr待办待阅信息
     */
    @Override
    public WorkItemBpm selectWorkItemBpmById(Long id)
    {
        return workItemBpmMapper.selectWorkItemBpmById(id);
    }

    /**
     * 查询hr待办待阅信息列表
     *
     * @param workItemBpm hr待办待阅信息
     * @return hr待办待阅信息
     */
    @Override
    public List<WorkItemBpm> selectWorkItemBpmList(WorkItemBpm workItemBpm)
    {
        return workItemBpmMapper.selectWorkItemBpmList(workItemBpm);
    }

    /**
     * 新增hr待办待阅信息
     *
     * @param workItemBpm hr待办待阅信息
     * @return 结果
     */
    @Override
    public int insertWorkItemBpm(WorkItemBpm workItemBpm)
    {
        workItemBpm.setCreateTime(DateUtils.getNowDate());
        return workItemBpmMapper.insertWorkItemBpm(workItemBpm);
    }

    /**
     * 修改hr待办待阅信息
     *
     * @param workItemBpm hr待办待阅信息
     * @return 结果
     */
    @Override
    public int updateWorkItemBpm(WorkItemBpm workItemBpm)
    {
        workItemBpm.setUpdateTime(DateUtils.getNowDate());
        return workItemBpmMapper.updateWorkItemBpm(workItemBpm);
    }

    /**
     * 批量删除hr待办待阅信息
     *
     * @param ids 需要删除的hr待办待阅信息ID
     * @return 结果
     */
    @Override
    public int deleteWorkItemBpmByIds(Long[] ids)
    {
        return workItemBpmMapper.deleteWorkItemBpmByIds(ids);
    }

    /**
     * 删除hr待办待阅信息信息
     *
     * @param id hr待办待阅信息ID
     * @return 结果
     */
    @Override
    public int deleteWorkItemBpmById(Long id)
    {
        return workItemBpmMapper.deleteWorkItemBpmById(id);
    }


    @Override
    public void syncWorkItemBpm(String userName) {
        WorkItemBpm workItemChangeBpm = new WorkItemBpm();
        workItemChangeBpm.setFlag("1");
        List<WorkItemBpm> workItemChangBpms = workItemBpmMapper.selectWorkItemBpmList(workItemChangeBpm);
        if (workItemChangBpms.size()==0){
            return;
        }
        WorkItemBpm workItemBpm = new WorkItemBpm();
        workItemBpm.setStatus("2");
        List<WorkItemBpm> workItemBpms = workItemBpmMapper.selectWorkItemBpmList(workItemBpm);
        if (workItemBpms.size() > 0) {
            SyncDataLog syncDataLog = new SyncDataLog();
            syncDataLog.setType("2");
            syncDataLog.setFlag("1");
            String format = String.format("%s 在 %s 执行的BPM初始化", userName, DateUtils.getTime());
            syncDataLog.setInfo(format);
            syncDataLogMapper.insertSyncDataLog(syncDataLog);
            workItemPendMapper.updateBpmWorkItemPendStatus();
            for (WorkItemBpm one : workItemBpms) {
                WorkItemPend workItem = new WorkItemPend();
                workItem.setPendType(one.getPendType());
                workItem.setNode(one.getNode());
                workItem.setReceiverUserCode(one.getReceiverUserCode());
                workItem.setPromoterOrg(one.getPromoterOrg());
                workItem.setSystemName(one.getSystemName());
                workItem.setStatus(one.getStatus());
                workItem.setPromoter(one.getPromoter());
                workItem.setInitiationTime(one.getInitiationTime());
                workItem.setPcUrl(one.getPcUrl());
                workItem.setProcedureName(one.getProcedureName());
                workItem.setPromoterOrgId(one.getPromoterOrgId());
                workItem.setReceiverTime(one.getReceiverTime());
                workItem.setTitle(one.getTitle());
                workItem.setSequenceNo(one.getSequenceNo());
                workItem.setPromoterNo(one.getPromoterNo());
                workItem.setProcedureNo(one.getProcedureNo());
                workItem.setPcUrl(one.getPcUrl());
                int i = workItemPendMapper.selectCountWorkItemPendBySequenceNo(one.getSequenceNo());
                try {
                    if (i > 0) {
                        workItemPendMapper.updateWorkItemPendBySequenceNoCommon(workItem);
                    } else {
                        workItemPendMapper.insertWorkItemPend(workItem);
                    }
                    WorkItemBpm bpmWorkItem = new WorkItemBpm();
                    bpmWorkItem.setId(one.getId());
                    bpmWorkItem.setFlag("2");
                    workItemBpmMapper.updateWorkItemBpm(bpmWorkItem);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            syncDataLog.setFlag("2");
            String concat = format.concat(String.format("已完成,完成时间为 %s", DateUtils.getTime()));
            syncDataLog.setInfo(concat);
            syncDataLogMapper.updateSyncDataLog(syncDataLog);
        }
    }
}
