package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SysDownload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysDownloadFileMapper;
import com.ruoyi.system.domain.SysDownloadFile;
import com.ruoyi.system.service.ISysDownloadFileService;

/**
 * 宣传资料服务配置Service业务层处理
 * 
 * @author tjgc
 * @date 2020-12-30
 */
@Service
public class SysDownloadFileServiceImpl implements ISysDownloadFileService 
{
    @Autowired
    private SysDownloadFileMapper sysDownloadFileMapper;

    /**
     * 查询宣传资料服务配置
     * 
     * @param id 宣传资料服务配置ID
     * @return 宣传资料服务配置
     */
    @Override
    public SysDownloadFile selectSysDownloadFileById(Long id)
    {
        return sysDownloadFileMapper.selectSysDownloadFileById(id);
    }

    /**
     * 查询宣传资料服务配置列表
     * 
     * @param sysDownloadFile 宣传资料服务配置
     * @return 宣传资料服务配置
     */
    @Override
    public List<SysDownloadFile> selectSysDownloadFileList(SysDownloadFile sysDownloadFile)
    {
        return sysDownloadFileMapper.selectSysDownloadFileList(sysDownloadFile);
    }

    /**
     * 新增宣传资料服务配置
     * 
     * @param sysDownloadFile 宣传资料服务配置
     * @return 结果
     */
    @Override
    public int insertSysDownloadFile(SysDownloadFile sysDownloadFile)
    {
        sysDownloadFile.setCreateTime(DateUtils.getNowDate());
        return sysDownloadFileMapper.insertSysDownloadFile(sysDownloadFile);
    }

    /**
     * 修改宣传资料服务配置
     * 
     * @param sysDownloadFile 宣传资料服务配置
     * @return 结果
     */
    @Override
    public int updateSysDownloadFile(SysDownloadFile sysDownloadFile)
    {
        sysDownloadFile.setUpdateTime(DateUtils.getNowDate());
        return sysDownloadFileMapper.updateSysDownloadFile(sysDownloadFile);
    }

    /**
     * 批量删除宣传资料服务配置
     * 
     * @param ids 需要删除的宣传资料服务配置ID
     * @return 结果
     */
    @Override
    public int deleteSysDownloadFileByIds(Long[] ids)
    {
        return sysDownloadFileMapper.deleteSysDownloadFileByIds(ids);
    }

    /**
     * 删除宣传资料服务配置信息
     * 
     * @param id 宣传资料服务配置ID
     * @return 结果
     */
    @Override
    public int deleteSysDownloadFileById(Long id)
    {
        return sysDownloadFileMapper.deleteSysDownloadFileById(id);
    }
    @Override
    public int updateSysDownloadFileStatus(SysDownloadFile sysDownloadFile)
    {
        return sysDownloadFileMapper.updateSysDownloadFile(sysDownloadFile);
    }
}
