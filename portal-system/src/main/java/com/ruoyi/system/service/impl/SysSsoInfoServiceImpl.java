package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SysSsoConfigVo;
import com.ruoyi.system.domain.SysSsoInfo;
import com.ruoyi.system.mapper.SysSsoConfigVoMapper;
import com.ruoyi.system.mapper.SysSsoInfoMapper;
import com.ruoyi.system.service.ISysSsoInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户单点登录信息Service业务层处理
 * 
 * @author tjgc
 * @date 2020-12-17
 */
@Service
public class SysSsoInfoServiceImpl implements ISysSsoInfoService
{
    private static final Logger log = LoggerFactory.getLogger(SysSsoInfoServiceImpl.class);

    @Resource
    private SysSsoInfoMapper sysSsoInfoMapper;

    @Resource
    private SysSsoConfigVoMapper sysSsoConfigVOMapper;

    /**
     * 查询用户单点登录信息
     * 
     * @param id 用户单点登录信息ID
     * @return 用户单点登录信息
     */
    @Override
    public SysSsoInfo selectSysSsoInfoById(Long id)
    {
        return sysSsoInfoMapper.selectSysSsoInfoById(id);
    }



    @Override
    public SysSsoInfo selectSysSsoInfoByOne(String systemName, String userNo) {
        return sysSsoInfoMapper.selectSysSsoInfoByOne(systemName, userNo);
    }

    /**
     * 查询用户单点登录信息列表
     * 
     * @param sysSsoInfo 用户单点登录信息
     * @return 用户单点登录信息
     */
    @Override
    public List<SysSsoInfo> selectSysSsoInfoList(SysSsoInfo sysSsoInfo)
    {
        return sysSsoInfoMapper.selectSysSsoInfoList(sysSsoInfo);
    }

    /**
     * 新增用户单点登录信息
     * 
     * @param sysSsoInfo 用户单点登录信息
     * @return 结果
     */
    @Override
    public int insertSysSsoInfo(SysSsoInfo sysSsoInfo)
    {
        sysSsoInfo.setCreateTime(DateUtils.getNowDate());
        return sysSsoInfoMapper.insertSysSsoInfo(sysSsoInfo);
    }

    /**
     * 修改用户单点登录信息
     * 
     * @param sysSsoInfo 用户单点登录信息
     * @return 结果
     */
    @Override
    public int updateSysSsoInfo(SysSsoInfo sysSsoInfo)
    {
        sysSsoInfo.setUpdateTime(DateUtils.getNowDate());
        return sysSsoInfoMapper.updateSysSsoInfo(sysSsoInfo);
    }

    /**
     * 批量删除用户单点登录信息
     * 
     * @param ids 需要删除的用户单点登录信息ID
     * @return 结果
     */
    @Override
    public int deleteSysSsoInfoByIds(Long[] ids)
    {
        return sysSsoInfoMapper.deleteSysSsoInfoByIds(ids);
    }

    /**
     * 删除用户单点登录信息信息
     * 
     * @param id 用户单点登录信息ID
     * @return 结果
     */
    @Override
    public int deleteSysSsoInfoById(Long id)
    {
        return sysSsoInfoMapper.deleteSysSsoInfoById(id);
    }
}
