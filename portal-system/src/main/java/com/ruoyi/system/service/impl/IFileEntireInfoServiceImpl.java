package com.ruoyi.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.IFileEntireInfoMapper;
import com.ruoyi.system.domain.IFileEntireInfo;
import com.ruoyi.system.service.IFileEntireInfoService;

import javax.annotation.Resource;

/**
 * 分片上传文件信息Service业务层处理
 * 
 * @author tjgc
 * @date 2021-03-15
 */
@Service
public class IFileEntireInfoServiceImpl implements IFileEntireInfoService
{
    @Resource
    private IFileEntireInfoMapper iFileEntireInfoMapper;

    /**
     * 查询分片上传文件信息
     * 
     * @param iFileEntireInfo 分片上传文件信息
     * @return 分片上传文件信息
     */
    @Override
    public IFileEntireInfo selectAsyncFileInfoByOne(IFileEntireInfo iFileEntireInfo)
    {
        return iFileEntireInfoMapper.selectAsyncFileInfoByOne(iFileEntireInfo);
    }

    @Override
    public IFileEntireInfo selectAsyncFileInfoByUrl(String fileAddr) {
        return iFileEntireInfoMapper.selectAsyncFileInfoByUrl(fileAddr);
    }

    @Override
    public IFileEntireInfo selectAsyncFileInfoByIdentifier(String iFileEntireInfo)
    {
        return iFileEntireInfoMapper.selectAsyncFileInfoByIdentifier(iFileEntireInfo);
    }
    /**
     * 查询分片上传文件信息列表
     * 
     * @param iFileEntireInfo 分片上传文件信息
     * @return 分片上传文件信息
     */
    @Override
    public List<IFileEntireInfo> selectAsyncFileInfoList(IFileEntireInfo iFileEntireInfo)
    {
        return iFileEntireInfoMapper.selectAsyncFileInfoList(iFileEntireInfo);
    }

    /**
     * 新增分片上传文件信息
     * 
     * @param iFileEntireInfo 分片上传文件信息
     * @return 结果
     */
    @Override
    public int insertAsyncFileInfo(IFileEntireInfo iFileEntireInfo)
    {
        return iFileEntireInfoMapper.insertAsyncFileInfo(iFileEntireInfo);
    }

    /**
     * 修改分片上传文件信息
     * 
     * @param iFileEntireInfo 分片上传文件信息
     * @return 结果
     */
    @Override
    public int updateAsyncFileInfo(IFileEntireInfo iFileEntireInfo)
    {
        return iFileEntireInfoMapper.updateAsyncFileInfo(iFileEntireInfo);
    }

    /**
     * 批量删除分片上传文件信息
     * 
     * @param ids 需要删除的分片上传文件信息ID
     * @return 结果
     */
    @Override
    public int deleteAsyncFileInfoByIds(String[] ids)
    {
        return iFileEntireInfoMapper.deleteAsyncFileInfoByIds(ids);
    }

    /**
     * 删除分片上传文件信息信息
     * 
     * @param id 分片上传文件信息ID
     * @return 结果
     */
    @Override
    public int deleteAsyncFileInfoById(String id)
    {
        return iFileEntireInfoMapper.deleteAsyncFileInfoById(id);
    }

    @Override
    public int deleteAsyncFileInfoByFile(IFileEntireInfo iFileEntireInfo)
    {
        return iFileEntireInfoMapper.deleteAsyncFileInfoByFile(iFileEntireInfo);
    }
}
