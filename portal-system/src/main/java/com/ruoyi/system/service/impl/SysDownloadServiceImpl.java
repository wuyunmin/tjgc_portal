package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SysIntserv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysDownloadMapper;
import com.ruoyi.system.domain.SysDownload;
import com.ruoyi.system.service.ISysDownloadService;

import javax.annotation.Resource;

/**
 * 文件服务配置Service业务层处理
 * 
 * @author tjgc
 * @date 2020-12-30
 */
@Service
public class SysDownloadServiceImpl implements ISysDownloadService 
{
    @Resource
    private SysDownloadMapper sysDownloadMapper;

    /**
     * 查询文件服务配置
     * 
     * @param id 文件服务配置ID
     * @return 文件服务配置
     */
    @Override
    public SysDownload selectSysDownloadById(Long id)
    {
        return sysDownloadMapper.selectSysDownloadById(id);
    }

    /**
     * 查询文件服务配置列表
     * 
     * @param sysDownload 文件服务配置
     * @return 文件服务配置
     */
    @Override
    public List<SysDownload> selectSysDownloadList(SysDownload sysDownload)
    {
        return sysDownloadMapper.selectSysDownloadList(sysDownload);
    }


    /**
     * 查询文件服务配置列表
     *
     * @param sysDownload 文件服务配置
     * @return 文件服务配置
     */
    @Override
    public List<SysDownload> selectSysDownloadAllList(SysDownload sysDownload)
    {
        return sysDownloadMapper.selectSysDownloadAllList(sysDownload);
    }

    /**
     * 新增文件服务配置
     * 
     * @param sysDownload 文件服务配置
     * @return 结果
     */
    @Override
    public int insertSysDownload(SysDownload sysDownload)
    {
        sysDownload.setCreateTime(DateUtils.getNowDate());
        return sysDownloadMapper.insertSysDownload(sysDownload);
    }

    /**
     * 修改文件服务配置
     * 
     * @param sysDownload 文件服务配置
     * @return 结果
     */
    @Override
    public int updateSysDownload(SysDownload sysDownload)
    {
        sysDownload.setUpdateTime(DateUtils.getNowDate());
        return sysDownloadMapper.updateSysDownload(sysDownload);
    }

    /**
     * 批量删除文件服务配置
     * 
     * @param ids 需要删除的文件服务配置ID
     * @return 结果
     */
    @Override
    public int deleteSysDownloadByIds(Long[] ids)
    {
        return sysDownloadMapper.deleteSysDownloadByIds(ids);
    }

    /**
     * 删除文件服务配置信息
     * 
     * @param id 文件服务配置ID
     * @return 结果
     */
    @Override
    public int deleteSysDownloadById(Long id)
    {
        return sysDownloadMapper.deleteSysDownloadById(id);
    }


    @Override
    public int updateSysDownloadStatus(SysDownload sysDownload)
    {
        return sysDownloadMapper.updateSysDownload(sysDownload);
    }
}
