package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.UserInfoGroup;

/**
 * 域用户信息Service接口
 * 
 * @author tjgc
 * @date 2021-04-15
 */
public interface IUserInfoGroupService 
{
    /**
     * 查询域用户信息
     * 
     * @param id 域用户信息ID
     * @return 域用户信息
     */
    public UserInfoGroup selectUserInfoGroupById(String id);

    /**
     * 查询域用户信息列表
     * 
     * @param userInfoGroup 域用户信息
     * @return 域用户信息集合
     */
    public List<UserInfoGroup> selectUserInfoGroupList(UserInfoGroup userInfoGroup);

    /**
     * 新增域用户信息
     * 
     * @param userInfoGroup 域用户信息
     * @return 结果
     */
    public int insertUserInfoGroup(UserInfoGroup userInfoGroup);

    /**
     * 修改域用户信息
     * 
     * @param userInfoGroup 域用户信息
     * @return 结果
     */
    public int updateUserInfoGroup(UserInfoGroup userInfoGroup);

    /**
     * 批量删除域用户信息
     * 
     * @param ids 需要删除的域用户信息ID
     * @return 结果
     */
    public int deleteUserInfoGroupByIds(String[] ids);

    /**
     * 删除域用户信息信息
     * 
     * @param id 域用户信息ID
     * @return 结果
     */
    public int deleteUserInfoGroupById(String id);


    public String importUser(List<UserInfoGroup> userInfoGroupList, String operName);
}
