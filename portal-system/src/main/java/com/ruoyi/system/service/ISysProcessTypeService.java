package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SysProcessType;

/**
 * 流程类别Service接口
 * 
 * @author tjgc
 * @date 2021-01-28
 */
public interface ISysProcessTypeService 
{
    /**
     * 查询流程类别
     * 
     * @param id 流程类别ID
     * @return 流程类别
     */
    public SysProcessType selectSysProcessTypeById(Long id);

    /**
     * 查询流程类别列表
     * 
     * @param sysProcessType 流程类别
     * @return 流程类别集合
     */
    public List<SysProcessType> selectSysProcessTypeList(SysProcessType sysProcessType);

    /**
     * 新增流程类别
     * 
     * @param sysProcessType 流程类别
     * @return 结果
     */
    public int insertSysProcessType(SysProcessType sysProcessType);

    /**
     * 修改流程类别
     * 
     * @param sysProcessType 流程类别
     * @return 结果
     */
    public int updateSysProcessType(SysProcessType sysProcessType);


    /**
     * 修改流程类别
     *
     * @param sysProcessType 流程类别
     * @return 结果
     */
    public int updateSysProcessTypeByNo(SysProcessType sysProcessType);

    /**
     * 批量删除流程类别
     * 
     * @param ids 需要删除的流程类别ID
     * @return 结果
     */
    public int deleteSysProcessTypeByIds(Long[] ids);

    /**
     * 删除流程类别信息
     * 
     * @param id 流程类别ID
     * @return 结果
     */
    public int deleteSysProcessTypeById(Long id);
}
