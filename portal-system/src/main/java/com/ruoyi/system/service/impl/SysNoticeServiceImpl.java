package com.ruoyi.system.service.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.system.domain.SyncSysDept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.mapper.SysNoticeMapper;
import com.ruoyi.system.service.ISysNoticeService;

import javax.annotation.Resource;

import static com.ruoyi.common.utils.DateUtils.parseDate;

/**
 * 公告 服务层实现
 * 
 * @author ruoyi
 */
@Service
public class SysNoticeServiceImpl implements ISysNoticeService
{
    @Resource
    private SysNoticeMapper noticeMapper;

    /**
     * 查询公告信息
     * 
     * @param noticeId 公告ID
     * @return 公告信息
     */
    @Override
    public SysNotice selectNoticeById(Long noticeId)
    {
        return noticeMapper.selectNoticeById(noticeId);
    }

    /**
     * 查询公告信息
     *
     * @param noticeId 公告ID
     * @return 公告信息
     */
    @Override
    public SysNotice selectNoticeDetailsById(Long noticeId)
    {
        return noticeMapper.selectNoticeDetailsById(noticeId);
    }
    /**
     * 查询公告列表
     * 
     * @param userId 用户工号
     * @return 公告集合
     */
    @Override
    public List<SysNotice> selectUserNoticeList(String userId)
    {
        return noticeMapper.selectUserNoticeList(userId);
    }



    /**
     * 查询公告列表
     *
     * @param userId 用户工号
     * @return 公告集合
     */
    @Override
    public List<SysNotice> selectUserAllNoticeList(SysNotice notice,String userId)
    {
        return noticeMapper.selectUserAllNoticeList(notice);
    }


    /**
     * 查询公告列表
     *
     * @param notice 公告信息
     * @return 公告集合
     */
    @Override
    public List<SysNotice> selectNoticeList(SysNotice notice)
    {
        return noticeMapper.selectNoticeList(notice);
    }
    /**
     * 新增公告
     * 
     * @param noticeMap 公告信息
     * @return 结果
     */
    @Override
    public int insertNotice(Map<String,Object> noticeMap)
    {
        SysNotice sysNotice = convertValue(noticeMap);
        return noticeMapper.insertNotice(sysNotice);
    }


    /**
     * 新增公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    public int insertNotice(SysNotice notice)
    {
        return noticeMapper.insertNotice(notice);
    }
    /**
     * 修改公告
     * 
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    public int updateNotice(SysNotice notice)
    {
        return noticeMapper.updateNotice(notice);
    }

    /**
     * 删除公告对象
     * 
     * @param noticeId 公告ID
     * @return 结果
     */
    @Override
    public int deleteNoticeById(Long noticeId)
    {
        return noticeMapper.deleteNoticeById(noticeId);
    }

    /**
     * 批量删除公告信息
     * 
     * @param noticeIds 需要删除的公告ID
     * @return 结果
     */
    @Override
    public int deleteNoticeByIds(Long[] noticeIds)
    {
        return noticeMapper.deleteNoticeByIds(noticeIds);
    }

    private SysNotice convertValue(Map<String,Object> sysNoticeMap){
        SysNotice sysNotice = new SysNotice();
        Object createOrg = sysNoticeMap.get("draftDept");
        Object noticeContent = sysNoticeMap.get("noticeContent");
        Object createBy = sysNoticeMap.get("createBy");
        Object createTime = sysNoticeMap.get("createTime");
        Object noticeType = sysNoticeMap.get("noticeType");
        Object noticeTitle = sysNoticeMap.get("noticeTitle");
        Object docNum = sysNoticeMap.get("docNum");
        Object draftDept = sysNoticeMap.get("draftDept");
        Object fileIds = sysNoticeMap.get("fileIds");
        Object visibleRange = sysNoticeMap.get("visibleRange");
        sysNotice.setCreateOrg(null == createOrg ? null : createOrg.toString());
        sysNotice.setFileIds(null == fileIds ? null : fileIds.toString());
        sysNotice.setNoticeTitle(null == noticeTitle ? null : noticeTitle.toString());
        sysNotice.setNoticeContent(null == noticeContent ? null : noticeContent.toString());
        sysNotice.setCreateBy(null == createBy ? null : createBy.toString());
        sysNotice.setCreateTime(null == createTime ? null : parseDate(createTime));
        sysNotice.setNoticeType(null == noticeType ? null : noticeType.toString());
        sysNotice.setVisibleRange(null == visibleRange ? null : visibleRange.toString());
        sysNotice.setDocNum(null == docNum ? null : docNum.toString());
        sysNotice.setDraftDept(null == draftDept ? null : draftDept.toString());
        return sysNotice;
    }
}
