package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.mapper.SyncDataLogMapper;
import com.ruoyi.system.mapper.WorkItemPendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.WorkItemOaMapper;
import com.ruoyi.system.service.IWorkItemOaService;

import javax.annotation.Resource;

/**
 * Oa待办待阅信息Service业务层处理
 *
 * @author tjgc
 * @date 2021-05-12
 */
@Service
public class WorkItemOaServiceImpl implements IWorkItemOaService
{
    @Resource
    private WorkItemOaMapper workItemOaMapper;

    @Resource
    private WorkItemPendMapper workItemPendMapper;

    @Resource
    private SyncDataLogMapper syncDataLogMapper;

    /**
     * 查询Oa待办待阅信息
     *
     * @param id Oa待办待阅信息ID
     * @return Oa待办待阅信息
     */
    @Override
    public WorkItemOa selectWorkItemOaById(Long id)
    {
        return workItemOaMapper.selectWorkItemOaById(id);
    }

    /**
     * 查询Oa待办待阅信息列表
     *
     * @param workItemOa Oa待办待阅信息
     * @return Oa待办待阅信息
     */
    @Override
    public List<WorkItemOa> selectWorkItemOaList(WorkItemOa workItemOa)
    {
        return workItemOaMapper.selectWorkItemOaList(workItemOa);
    }

    /**
     * 新增Oa待办待阅信息
     *
     * @param workItemOa Oa待办待阅信息
     * @return 结果
     */
    @Override
    public int insertWorkItemOa(WorkItemOa workItemOa)
    {
        workItemOa.setCreateTime(DateUtils.getNowDate());
        return workItemOaMapper.insertWorkItemOa(workItemOa);
    }

    /**
     * 修改Oa待办待阅信息
     *
     * @param workItemOa Oa待办待阅信息
     * @return 结果
     */
    @Override
    public int updateWorkItemOa(WorkItemOa workItemOa)
    {
        workItemOa.setUpdateTime(DateUtils.getNowDate());
        return workItemOaMapper.updateWorkItemOa(workItemOa);
    }

    @Override
    public int updateWorkItemOaStatus(WorkItemOa workItemOa)
    {
        workItemOa.setUpdateTime(DateUtils.getNowDate());
        return workItemOaMapper.updateWorkItemOaStatus(workItemOa);
    }
    /**
     * 批量删除Oa待办待阅信息
     *
     * @param ids 需要删除的Oa待办待阅信息ID
     * @return 结果
     */
    @Override
    public int deleteWorkItemOaByIds(Long[] ids)
    {
        return workItemOaMapper.deleteWorkItemOaByIds(ids);
    }

    /**
     * 删除Oa待办待阅信息信息
     *
     * @param id Oa待办待阅信息ID
     * @return 结果
     */
    @Override
    public int deleteWorkItemOaById(Long id)
    {
        return workItemOaMapper.deleteWorkItemOaById(id);
    }

    @Override
    public void syncWorkItemOa(String userName) {
        WorkItemOa workItemChangOa = new WorkItemOa();
        workItemChangOa.setFlag("1");
        List<WorkItemOa> workItemChangOas = workItemOaMapper.selectWorkItemOaList(workItemChangOa);
        if (workItemChangOas.size()==0){
            return;
        }
        WorkItemOa workItemOa = new WorkItemOa();
        workItemOa.setStatus("2");
        List<WorkItemOa> workItemOas = workItemOaMapper.selectWorkItemOaList(workItemOa);
        if (workItemOas.size() > 0) {
            SyncDataLog syncDataLog = new SyncDataLog();
            syncDataLog.setType("4");
            syncDataLog.setFlag("1");
            String format = String.format("%s 在 %s 执行的OA初始化", userName, DateUtils.getTime());
            syncDataLog.setInfo(format);
            syncDataLogMapper.insertSyncDataLog(syncDataLog);
            workItemPendMapper.updateOaWorkItemPendStatus();
            for (WorkItemOa one : workItemOas) {
                WorkItemPend workItem = new WorkItemPend();
                workItem.setPendType(one.getPendType());
                workItem.setNode(one.getNode());
                workItem.setReceiverUserCode(one.getReceiverUserCode());
                workItem.setPromoterOrg(one.getPromoterOrg());
                workItem.setSystemName(one.getSystemName());
                workItem.setStatus(one.getStatus());
                workItem.setPromoter(one.getPromoter());
                workItem.setInitiationTime(one.getInitiationTime());
                workItem.setPcUrl(one.getPcUrl());
                workItem.setProcedureName(one.getProcedureName());
                workItem.setPromoterOrgId(one.getPromoterOrgId());
                workItem.setReceiverTime(one.getReceiverTime());
                workItem.setTitle(one.getTitle());
                workItem.setSequenceNo(one.getSequenceNo());
                workItem.setPromoterNo(one.getPromoterNo());
                workItem.setProcedureNo(one.getProcedureNo());
                workItem.setPcUrl(one.getPcUrl());
                int i = workItemPendMapper.selectCountWorkItemPendBySequenceNo(one.getSequenceNo());
                try {
                    if (i > 0) {
                        workItemPendMapper.updateWorkItemPendBySequenceNoCommon(workItem);
                    } else {
                        workItemPendMapper.insertWorkItemPend(workItem);
                    }
                    WorkItemOa oaWorkItem = new WorkItemOa();
                    oaWorkItem.setId(one.getId());
                    oaWorkItem.setFlag("2");
                    workItemOaMapper.updateWorkItemOa(oaWorkItem);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            syncDataLog.setFlag("2");
            String concat = format.concat(String.format("已完成,完成时间为 %s", DateUtils.getTime()));
            syncDataLog.setInfo(concat);
            syncDataLogMapper.updateSyncDataLog(syncDataLog);
        }
    }
}
