package com.ruoyi.system.service;

import com.ruoyi.system.domain.UserInfoProject;
import com.ruoyi.system.domain.UserInfoXiongan;

import java.util.List;

/**
 * 项目协同平台用户信息Service接口
 * 
 * @author tjec
 * @date 2021-01-28
 */
public interface IUserInfoProjectService 
{
    /**
     * 查询项目协同平台用户信息
     * 
     * @param guid 项目协同平台用户信息ID
     * @return 项目协同平台用户信息
     */
    public UserInfoProject selectUserInfoProjectByGuid(String guid);

    /**
     * 查询项目协同平台用户信息列表
     * 
     * @param userInfoProject 项目协同平台用户信息
     * @return 项目协同平台用户信息集合
     */
    public List<UserInfoProject> selectUserInfoProjectList(UserInfoProject userInfoProject);

    /**
     * 新增项目协同平台用户信息
     * 
     * @param userInfoProject 项目协同平台用户信息
     * @return 结果
     */
    public int insertUserInfoProject(UserInfoProject userInfoProject);

    /**
     * 修改项目协同平台用户信息
     * 
     * @param userInfoProject 项目协同平台用户信息
     * @return 结果
     */
    public int updateUserInfoProject(UserInfoProject userInfoProject);

    /**
     * 批量删除项目协同平台用户信息
     * 
     * @param guids 需要删除的项目协同平台用户信息ID
     * @return 结果
     */
    public int deleteUserInfoProjectByGuids(String[] guids);

    /**
     * 删除项目协同平台用户信息信息
     * 
     * @param guid 项目协同平台用户信息ID
     * @return 结果
     */
    public int deleteUserInfoProjectByGuid(String guid);
    
    /**
     * 导入用户数据
     *
     * @param userList        用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName        操作用户
     * @return 结果
     */
    public String importUser(List<UserInfoProject> userInfoProjectList, String operName);
    
    /**
     * 导入用户数据
     *
     * @param userList        用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName        操作用户
     * @return 结果
     */
    public String importOutMemberUser(List<UserInfoProject> userInfoProjectList, String operName);
}
