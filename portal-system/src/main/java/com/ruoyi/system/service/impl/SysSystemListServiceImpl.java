package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysSystemListMapper;
import com.ruoyi.system.domain.SysSystemList;
import com.ruoyi.system.service.ISysSystemListService;

import javax.annotation.Resource;

/**
 * 系统清单Service业务层处理
 * 
 * @author tjgc
 * @date 2021-03-18
 */
@Service
public class SysSystemListServiceImpl implements ISysSystemListService 
{
    @Resource
    private SysSystemListMapper sysSystemListMapper;

    /**
     * 查询系统清单
     * 
     * @param id 系统清单ID
     * @return 系统清单
     */
    @Override
    public SysSystemList selectSysSystemListById(Long id)
    {
        return sysSystemListMapper.selectSysSystemListById(id);
    }

    @Override
    public int checkSysSystemNumExist(String sysNum) {
        return sysSystemListMapper.checkSysSystemNumExist(sysNum);
    }

    /**
     * 查询系统清单列表
     * 
     * @param sysSystemList 系统清单
     * @return 系统清单
     */
    @Override
    public List<SysSystemList> selectSysSystemListList(SysSystemList sysSystemList)
    {
        return sysSystemListMapper.selectSysSystemListList(sysSystemList);
    }


    @Override
    public List<SysSystemList> selectSysSystemListForExport(SysSystemList sysSystemList)
    {
        return sysSystemListMapper.selectSysSystemListForExport(sysSystemList);
    }


    @Override
    public Map<String, SysSystemList> selectSysSystemMap() {
        return sysSystemListMapper.selectSysSystemMap();
    }

    /**
     * 新增系统清单
     * 
     * @param sysSystemList 系统清单
     * @return 结果
     */
    @Override
    public int insertSysSystemList(SysSystemList sysSystemList)
    {
        sysSystemList.setCreateTime(DateUtils.getNowDate());
        return sysSystemListMapper.insertSysSystemList(sysSystemList);
    }

    /**
     * 修改系统清单
     * 
     * @param sysSystemList 系统清单
     * @return 结果
     */
    @Override
    public int updateSysSystemList(SysSystemList sysSystemList)
    {
        sysSystemList.setUpdateTime(DateUtils.getNowDate());
        return sysSystemListMapper.updateSysSystemList(sysSystemList);
    }

    /**
     * 批量删除系统清单
     * 
     * @param ids 需要删除的系统清单ID
     * @return 结果
     */
    @Override
    public int deleteSysSystemListByIds(Long[] ids)
    {
        return sysSystemListMapper.deleteSysSystemListByIds(ids);
    }

    /**
     * 删除系统清单信息
     * 
     * @param id 系统清单ID
     * @return 结果
     */
    @Override
    public int deleteSysSystemListById(Long id)
    {
        return sysSystemListMapper.deleteSysSystemListById(id);
    }

    @Override
    public List<SysSystemList> selectSystemListDictData() {
        return sysSystemListMapper.selectSystemListDictData();
    }


}
