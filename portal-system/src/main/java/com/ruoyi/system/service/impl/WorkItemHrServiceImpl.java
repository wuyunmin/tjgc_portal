package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SyncDataLog;
import com.ruoyi.system.domain.WorkItemPend;
import com.ruoyi.system.mapper.SyncDataLogMapper;
import com.ruoyi.system.mapper.WorkItemPendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.WorkItemHrMapper;
import com.ruoyi.system.domain.WorkItemHr;
import com.ruoyi.system.service.IWorkItemHrService;

import javax.annotation.Resource;

/**
 * hr待办待阅信息Service业务层处理
 *
 * @author tjgc
 * @date 2022-05-09
 */
@Service
public class WorkItemHrServiceImpl implements IWorkItemHrService
{
    @Resource
    private WorkItemHrMapper workItemHrMapper;

    @Resource
    private WorkItemPendMapper workItemPendMapper;

    @Resource
    private SyncDataLogMapper syncDataLogMapper;

    /**
     * 查询hr待办待阅信息
     *
     * @param id hr待办待阅信息ID
     * @return hr待办待阅信息
     */
    @Override
    public WorkItemHr selectWorkItemHrById(Long id)
    {
        return workItemHrMapper.selectWorkItemHrById(id);
    }

    /**
     * 查询hr待办待阅信息列表
     *
     * @param workItemHr hr待办待阅信息
     * @return hr待办待阅信息
     */
    @Override
    public List<WorkItemHr> selectWorkItemHrList(WorkItemHr workItemHr)
    {
        return workItemHrMapper.selectWorkItemHrList(workItemHr);
    }

    /**
     * 新增hr待办待阅信息
     *
     * @param workItemHr hr待办待阅信息
     * @return 结果
     */
    @Override
    public int insertWorkItemHr(WorkItemHr workItemHr)
    {
        workItemHr.setCreateTime(DateUtils.getNowDate());
        return workItemHrMapper.insertWorkItemHr(workItemHr);
    }

    /**
     * 修改hr待办待阅信息
     *
     * @param workItemHr hr待办待阅信息
     * @return 结果
     */
    @Override
    public int updateWorkItemHr(WorkItemHr workItemHr)
    {
        workItemHr.setUpdateTime(DateUtils.getNowDate());
        return workItemHrMapper.updateWorkItemHr(workItemHr);
    }

    /**
     * 批量删除hr待办待阅信息
     *
     * @param ids 需要删除的hr待办待阅信息ID
     * @return 结果
     */
    @Override
    public int deleteWorkItemHrByIds(Long[] ids)
    {
        return workItemHrMapper.deleteWorkItemHrByIds(ids);
    }

    /**
     * 删除hr待办待阅信息信息
     *
     * @param id hr待办待阅信息ID
     * @return 结果
     */
    @Override
    public int deleteWorkItemHrById(Long id)
    {
        return workItemHrMapper.deleteWorkItemHrById(id);
    }


    @Override
    public void syncWorkItemHr(String userName) {
        WorkItemHr workItemChangeHr = new WorkItemHr();
        workItemChangeHr.setFlag("1");
        List<WorkItemHr> workItemChangHrs = workItemHrMapper.selectWorkItemHrList(workItemChangeHr);
        if (workItemChangHrs.size()==0){
            return;
        }
        WorkItemHr workItemHr = new WorkItemHr();
        workItemHr.setStatus("2");
        List<WorkItemHr> workItemHrs = workItemHrMapper.selectWorkItemHrList(workItemHr);
        if (workItemHrs.size() > 0) {
            SyncDataLog syncDataLog = new SyncDataLog();
            syncDataLog.setType("3");
            syncDataLog.setFlag("1");
            String format = String.format("%s 在 %s 执行的HR初始化", userName, DateUtils.getTime());
            syncDataLog.setInfo(format);
            syncDataLogMapper.insertSyncDataLog(syncDataLog);
            workItemPendMapper.updateHrWorkItemPendStatus();
            for (WorkItemHr one : workItemHrs) {
                WorkItemPend workItem = new WorkItemPend();
                workItem.setPendType(one.getPendType());
                workItem.setNode(one.getNode());
                workItem.setReceiverUserCode(one.getReceiverUserCode());
                workItem.setPromoterOrg(one.getPromoterOrg());
                workItem.setSystemName(one.getSystemName());
                workItem.setStatus(one.getStatus());
                workItem.setPromoter(one.getPromoter());
                workItem.setInitiationTime(one.getInitiationTime());
                workItem.setPcUrl(one.getPcUrl());
                workItem.setProcedureName(one.getProcedureName());
                workItem.setPromoterOrgId(one.getPromoterOrgId());
                workItem.setReceiverTime(one.getReceiverTime());
                workItem.setTitle(one.getTitle());
                workItem.setSequenceNo(one.getSequenceNo());
                workItem.setPromoterNo(one.getPromoterNo());
                workItem.setProcedureNo(one.getProcedureNo());
                workItem.setPcUrl(one.getPcUrl());
                int i = workItemPendMapper.selectCountWorkItemPendBySequenceNo(one.getSequenceNo());
                try {
                    if (i > 0) {
                        workItemPendMapper.updateWorkItemPendBySequenceNoCommon(workItem);
                    } else {
                        workItemPendMapper.insertWorkItemPend(workItem);
                    }
                    WorkItemHr hrWorkItem = new WorkItemHr();
                    hrWorkItem.setId(one.getId());
                    hrWorkItem.setFlag("2");
                    workItemHrMapper.updateWorkItemHr(hrWorkItem);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            syncDataLog.setFlag("2");
            String concat = format.concat(String.format("已完成,完成时间为 %s", DateUtils.getTime()));
            syncDataLog.setInfo(concat);
            syncDataLogMapper.updateSyncDataLog(syncDataLog);
        }
    }
}
