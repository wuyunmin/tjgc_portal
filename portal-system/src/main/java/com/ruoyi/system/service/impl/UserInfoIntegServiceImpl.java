package com.ruoyi.system.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.UserInfoEmail;
import com.ruoyi.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UserInfoIntegMapper;
import com.ruoyi.system.domain.UserInfoInteg;
import com.ruoyi.system.service.IUserInfoIntegService;

/**
 * 后台集成用户Service业务层处理
 * 
 * @author tjgc
 * @date 2021-04-06
 */
@Service
public class UserInfoIntegServiceImpl implements IUserInfoIntegService 
{
    @Autowired
    private UserInfoIntegMapper userInfoIntegMapper;

    @Autowired
    private SysUserMapper userMapper;
    /**
     * 查询后台集成用户
     * 
     * @param guid 后台集成用户ID
     * @return 后台集成用户
     */
    @Override
    public UserInfoInteg selectUserInfoIntegByGuid(String guid)
    {
        return userInfoIntegMapper.selectUserInfoIntegByGuid(guid);
    }

    /**
     * 查询后台集成用户列表
     * 
     * @param userInfoInteg 后台集成用户
     * @return 后台集成用户
     */
    @Override
    public List<UserInfoInteg> selectUserInfoIntegList(UserInfoInteg userInfoInteg)
    {
        List<UserInfoInteg> resList = userInfoIntegMapper.selectUserInfoIntegList(userInfoInteg);
        if(resList!=null && resList.size()>0) {
            for (UserInfoInteg userInfoInteg2 : resList) {
                if (StringUtils.isNotBlank(userInfoInteg2.getUserId())) {
                    List<Long> ids = Arrays.stream(userInfoInteg2.getUserId().split(";"))
                            .map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
                    userInfoInteg2.setUserName(userMapper.selectUserNames(ids));
                }

            }
        }
            return resList;
    }

    @Override
    public List<String> selectSysNameList(String userId) {
        return userInfoIntegMapper.selectSysNameList(userId);
    }

    /**
     * 新增后台集成用户
     * 
     * @param userInfoInteg 后台集成用户
     * @return 结果
     */
    @Override
    public int insertUserInfoInteg(UserInfoInteg userInfoInteg)
    {
        userInfoInteg.setCreateTime(DateUtils.getNowDate());
        return userInfoIntegMapper.insertUserInfoInteg(userInfoInteg);
    }

    /**
     * 修改后台集成用户
     * 
     * @param userInfoInteg 后台集成用户
     * @return 结果
     */
    @Override
    public int updateUserInfoInteg(UserInfoInteg userInfoInteg)
    {
        userInfoInteg.setUpdateTime(DateUtils.getNowDate());
        return userInfoIntegMapper.updateUserInfoInteg(userInfoInteg);
    }

    /**
     * 批量删除后台集成用户
     * 
     * @param guids 需要删除的后台集成用户ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoIntegByGuids(String[] guids)
    {
        return userInfoIntegMapper.deleteUserInfoIntegByGuids(guids);
    }

    /**
     * 删除后台集成用户信息
     * 
     * @param guid 后台集成用户ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoIntegByGuid(String guid)
    {
        return userInfoIntegMapper.deleteUserInfoIntegByGuid(guid);
    }
}
