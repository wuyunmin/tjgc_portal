package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysProcessTypeMapper;
import com.ruoyi.system.domain.SysProcessType;
import com.ruoyi.system.service.ISysProcessTypeService;

/**
 * 流程类别Service业务层处理
 * 
 * @author tjgc
 * @date 2021-01-28
 */
@Service
public class SysProcessTypeServiceImpl implements ISysProcessTypeService 
{
    @Autowired
    private SysProcessTypeMapper sysProcessTypeMapper;

    /**
     * 查询流程类别
     * 
     * @param id 流程类别ID
     * @return 流程类别
     */
    @Override
    public SysProcessType selectSysProcessTypeById(Long id)
    {
        return sysProcessTypeMapper.selectSysProcessTypeById(id);
    }

    /**
     * 查询流程类别列表
     * 
     * @param sysProcessType 流程类别
     * @return 流程类别
     */
    @Override
    public List<SysProcessType> selectSysProcessTypeList(SysProcessType sysProcessType)
    {
        return sysProcessTypeMapper.selectSysProcessTypeList(sysProcessType);
    }

    /**
     * 新增流程类别
     * 
     * @param sysProcessType 流程类别
     * @return 结果
     */
    @Override
    public int insertSysProcessType(SysProcessType sysProcessType)
    {
        sysProcessType.setCreateTime(DateUtils.getNowDate());
        return sysProcessTypeMapper.insertSysProcessType(sysProcessType);
    }

    /**
     * 修改流程类别
     * 
     * @param sysProcessType 流程类别
     * @return 结果
     */
    @Override
    public int updateSysProcessType(SysProcessType sysProcessType)
    {
        sysProcessType.setUpdateTime(DateUtils.getNowDate());
        return sysProcessTypeMapper.updateSysProcessType(sysProcessType);
    }

    /**
     * 修改流程类别
     *
     * @param sysProcessType 流程类别
     * @return 结果
     */
    @Override
    public int updateSysProcessTypeByNo(SysProcessType sysProcessType)
    {
        sysProcessType.setUpdateTime(DateUtils.getNowDate());
        return sysProcessTypeMapper.updateSysProcessTypeByNo(sysProcessType);
    }

    /**
     * 批量删除流程类别
     * 
     * @param ids 需要删除的流程类别ID
     * @return 结果
     */
    @Override
    public int deleteSysProcessTypeByIds(Long[] ids)
    {
        return sysProcessTypeMapper.deleteSysProcessTypeByIds(ids);
    }

    /**
     * 删除流程类别信息
     * 
     * @param id 流程类别ID
     * @return 结果
     */
    @Override
    public int deleteSysProcessTypeById(Long id)
    {
        return sysProcessTypeMapper.deleteSysProcessTypeById(id);
    }
}
