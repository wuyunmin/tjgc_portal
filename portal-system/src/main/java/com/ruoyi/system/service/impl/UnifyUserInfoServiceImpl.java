package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SyncDataLog;
import com.ruoyi.system.mapper.SyncDataLogMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UnifyUserInfoMapper;
import com.ruoyi.system.domain.UnifyUserInfo;
import com.ruoyi.system.service.IUnifyUserInfoService;

import javax.annotation.Resource;

/**
 * 统一用户信息Service业务层处理
 *
 * @author tjgc
 * @date 2022-03-19
 */
@Service
public class UnifyUserInfoServiceImpl implements IUnifyUserInfoService
{
    @Resource
    private UnifyUserInfoMapper unifyUserInfoMapper;

    @Resource
    private SyncDataLogMapper syncDataLogMapper;


    protected final Logger logger = LoggerFactory.getLogger(UnifyUserInfoServiceImpl.class);

    /**
     * 查询统一用户信息
     *
     * @param id 统一用户信息ID
     * @return 统一用户信息
     */
    @Override
    public UnifyUserInfo selectUnifyUserInfoById(Long id)
    {
        return unifyUserInfoMapper.selectUnifyUserInfoById(id);
    }

    /**
     * 查询统一用户信息列表
     *
     * @param unifyUserInfo 统一用户信息
     * @return 统一用户信息
     */
    @Override
    public List<UnifyUserInfo> selectUnifyUserInfoList(UnifyUserInfo unifyUserInfo)
    {
        return unifyUserInfoMapper.selectUnifyUserInfoList(unifyUserInfo);
    }

    /**
     * 新增统一用户信息
     *
     * @param unifyUserInfo 统一用户信息
     * @return 结果
     */
    @Override
    public int insertUnifyUserInfo(UnifyUserInfo unifyUserInfo)
    {
        return unifyUserInfoMapper.insertUnifyUserInfo(unifyUserInfo);
    }

    /**
     * 批量插入
     * @param unifyUserInfos
     * @return
     */
    @Override
    public int insertBatchUnifyUserInfo(List<UnifyUserInfo> unifyUserInfos)
    {
        return unifyUserInfoMapper.insertBatchUnifyUserInfo(unifyUserInfos);
    }
    /**
     * 修改统一用户信息
     *
     * @param unifyUserInfo 统一用户信息
     * @return 结果
     */
    @Override
    public int updateUnifyUserInfo(UnifyUserInfo unifyUserInfo)
    {
        return unifyUserInfoMapper.updateUnifyUserInfo(unifyUserInfo);
    }

    /**
     * 批量删除统一用户信息
     *
     * @param ids 需要删除的统一用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUnifyUserInfoByIds(Long[] ids)
    {
        return unifyUserInfoMapper.deleteUnifyUserInfoByIds(ids);
    }

    /**
     * 删除统一用户信息信息
     *
     * @param id 统一用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUnifyUserInfoById(Long id)
    {
        return unifyUserInfoMapper.deleteUnifyUserInfoById(id);
    }

    /**
     * 清空统一用户信息
     */
    @Override
    public void cleanUnifyUserInfo()
    {
         unifyUserInfoMapper.cleanUnifyUserInfo();
    }

    @Override
    public void initUnifyUserInfo(String userName) {

        SyncDataLog syncDataLog = new SyncDataLog();
        syncDataLog.setType("1");
        syncDataLog.setFlag("1");
        String format = String.format("%s 在 %s 执行的初始化", userName, DateUtils.getTime());
        syncDataLog.setInfo(format);
        syncDataLogMapper.insertSyncDataLog(syncDataLog);
        try {
            unifyUserInfoMapper.cleanUnifyUserInfo();
            List<UnifyUserInfo> unifyBpmUserInfos = unifyUserInfoMapper.selectBpmUserInfo();
            batchInsertUnifyUserInfo(unifyBpmUserInfos);


            List<UnifyUserInfo> unifyECUserInfos = unifyUserInfoMapper.selectEcUserInfo();
            batchInsertUnifyUserInfo(unifyECUserInfos);


            List<UnifyUserInfo> unifyLKUserInfos = unifyUserInfoMapper.selectLkUserInfo();
            batchInsertUnifyUserInfo(unifyLKUserInfos);



            List<UnifyUserInfo> unifyTBUserInfos = unifyUserInfoMapper.selectTbUserInfo();
            batchInsertUnifyUserInfo(unifyTBUserInfos);



            List<UnifyUserInfo> unifyEmailUserInfos = unifyUserInfoMapper.selectEmailUserInfo();
            batchInsertUnifyUserInfo(unifyEmailUserInfos);



            List<UnifyUserInfo> unifySchoolUserInfos = unifyUserInfoMapper.selectSchoolUserInfo();
            batchInsertUnifyUserInfo(unifySchoolUserInfos);



            List<UnifyUserInfo> unifyProjectUserInfos = unifyUserInfoMapper.selectProjectUserInfo();
            batchInsertUnifyUserInfo(unifyProjectUserInfos);

            List<UnifyUserInfo> unifyDingUserInfos = unifyUserInfoMapper.selectDingUserInfo();
            batchInsertUnifyUserInfo(unifyDingUserInfos);


            List<UnifyUserInfo> unifyHrUserInfos = unifyUserInfoMapper.selectHrUserInfo();
            batchInsertUnifyUserInfo(unifyHrUserInfos);



            List<UnifyUserInfo> unifyFinereportUserInfos = unifyUserInfoMapper.selectFinereportUserInfo();
            batchInsertUnifyUserInfo(unifyFinereportUserInfos);


            List<UnifyUserInfo> unifyOaUserInfos = unifyUserInfoMapper.selectOaUserInfo();
            batchInsertUnifyUserInfo(unifyOaUserInfos);


            List<UnifyUserInfo> unifyJyUserInfos = unifyUserInfoMapper.selectJyUserInfo();
            batchInsertUnifyUserInfo(unifyJyUserInfos);



            List<UnifyUserInfo> unifyXyUserInfos = unifyUserInfoMapper.selectXiaoyuUserInfo();
            batchInsertUnifyUserInfo(unifyXyUserInfos);



            List<UnifyUserInfo> unifyXaUserInfos = unifyUserInfoMapper.selectXionganUserInfo();
            batchInsertUnifyUserInfo(unifyXaUserInfos);


            List<UnifyUserInfo> unifyHolderUserInfos = unifyUserInfoMapper.selectHolderUserInfo();
            batchInsertUnifyUserInfo(unifyHolderUserInfos);


            List<UnifyUserInfo> unifyIntegUserInfos = unifyUserInfoMapper.selectIntegUserInfo();
            batchInsertUnifyUserInfo(unifyIntegUserInfos);

            List<UnifyUserInfo> unifyGroupUserInfos = unifyUserInfoMapper.selectGroupUserInfo();
            batchInsertUnifyUserInfo(unifyGroupUserInfos);
        }catch (Exception e){
            e.printStackTrace();
            syncDataLog.setDetail(e.getMessage());
            syncDataLog.setFlag("3");
            syncDataLogMapper.updateSyncDataLog(syncDataLog);
        }
        syncDataLog.setFlag("2");
        String concat = format.concat(String.format("已完成,完成时间为 %s", DateUtils.getTime()));
        syncDataLog.setInfo(concat);
        syncDataLogMapper.updateSyncDataLog(syncDataLog);
    }

    private void batchInsertUnifyUserInfo(List<UnifyUserInfo> unifyUserInfos) {
        int batchNum = 1000;
        int toIndex = batchNum;
        int listSize = unifyUserInfos.size();
        for (int i = 0; i < unifyUserInfos.size(); i += batchNum) {
            if (i + batchNum > listSize) {
                toIndex = listSize - i;
            }
            List<UnifyUserInfo> batchList = unifyUserInfos.subList(i, i + toIndex);
            unifyUserInfoMapper.insertBatchUnifyUserInfo(batchList);
        }
    }

    private void oneInsertUnifyUserInfo(List<UnifyUserInfo> unifyUserInfos){
        for (int i = 0; i < unifyUserInfos.size(); i++) {
            UnifyUserInfo unifyUserInfo = unifyUserInfos.get(i);
            unifyUserInfoMapper.insertUnifyUserInfo(unifyUserInfo);
        }
    }

}
