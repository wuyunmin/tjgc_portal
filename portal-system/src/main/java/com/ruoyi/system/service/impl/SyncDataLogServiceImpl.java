package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SyncDataLogMapper;
import com.ruoyi.system.domain.SyncDataLog;
import com.ruoyi.system.service.ISyncDataLogService;

/**
 * 同步日志信息Service业务层处理
 * 
 * @author tjgc
 * @date 2022-06-06
 */
@Service
public class SyncDataLogServiceImpl implements ISyncDataLogService 
{
    @Autowired
    private SyncDataLogMapper syncDataLogMapper;

    /**
     * 查询同步日志信息
     * 
     * @param id 同步日志信息ID
     * @return 同步日志信息
     */
    @Override
    public SyncDataLog selectSyncDataLogById(Long id)
    {
        return syncDataLogMapper.selectSyncDataLogById(id);
    }

    /**
     * 查询同步日志信息列表
     * 
     * @param syncDataLog 同步日志信息
     * @return 同步日志信息
     */
    @Override
    public List<SyncDataLog> selectSyncDataLogList(SyncDataLog syncDataLog)
    {
        return syncDataLogMapper.selectSyncDataLogList(syncDataLog);
    }


    @Override
    public int selectCountSyncDataLogToday(SyncDataLog syncDataLog) {
        return syncDataLogMapper.selectCountSyncDataLogToday(syncDataLog);
    }

    /**
     * 新增同步日志信息
     * 
     * @param syncDataLog 同步日志信息
     * @return 结果
     */
    @Override
    public int insertSyncDataLog(SyncDataLog syncDataLog)
    {
        syncDataLog.setCreateTime(DateUtils.getNowDate());
        return syncDataLogMapper.insertSyncDataLog(syncDataLog);
    }

    /**
     * 修改同步日志信息
     * 
     * @param syncDataLog 同步日志信息
     * @return 结果
     */
    @Override
    public int updateSyncDataLog(SyncDataLog syncDataLog)
    {
        syncDataLog.setUpdateTime(DateUtils.getNowDate());
        return syncDataLogMapper.updateSyncDataLog(syncDataLog);
    }

    /**
     * 批量删除同步日志信息
     * 
     * @param ids 需要删除的同步日志信息ID
     * @return 结果
     */
    @Override
    public int deleteSyncDataLogByIds(Long[] ids)
    {
        return syncDataLogMapper.deleteSyncDataLogByIds(ids);
    }

    /**
     * 删除同步日志信息信息
     * 
     * @param id 同步日志信息ID
     * @return 结果
     */
    @Override
    public int deleteSyncDataLogById(Long id)
    {
        return syncDataLogMapper.deleteSyncDataLogById(id);
    }
}
