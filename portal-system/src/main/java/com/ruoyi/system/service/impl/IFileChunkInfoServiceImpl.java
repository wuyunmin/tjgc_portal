package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.utils.SnowflakeIdWorker;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.IFileChunkInfoMapper;
import com.ruoyi.system.domain.IFileChunkInfo;
import com.ruoyi.system.service.IFileChunkInfoService;

import javax.annotation.Resource;

/**
 * 分片上传单片信息Service业务层处理
 * 
 * @author tjgc
 * @date 2021-03-15
 */
@Service
public class IFileChunkInfoServiceImpl implements IFileChunkInfoService
{
    @Resource
    private IFileChunkInfoMapper iFileChunkInfoMapper;

    /**
     * 查询分片上传单片信息
     * 
     * @param id 分片上传单片信息ID
     * @return 分片上传单片信息
     */
    @Override
    public IFileChunkInfo selectAsyncChunkInfoById(String id)
    {
        return iFileChunkInfoMapper.selectAsyncChunkInfoById(id);
    }

    /**
     * 查询分片上传单片信息列表
     * 
     * @param IFileChunkInfo 分片上传单片信息
     * @return 分片上传单片信息
     */
    @Override
    public List<IFileChunkInfo> selectAsyncChunkInfoList(IFileChunkInfo IFileChunkInfo)
    {
        return iFileChunkInfoMapper.selectAsyncChunkInfoList(IFileChunkInfo);
    }


    @Override
    public List<Integer> selectAsyncChunkNumList(IFileChunkInfo IFileChunkInfo)
    {
        return iFileChunkInfoMapper.selectAsyncChunkNumList(IFileChunkInfo);
    }

    /**
     * 新增分片上传单片信息
     * 
     * @param IFileChunkInfo 分片上传单片信息
     * @return 结果
     */
    @Override
    public int insertAsyncChunkInfo(IFileChunkInfo IFileChunkInfo)
    {
        String s = SnowflakeIdWorker.getUUID() + SnowflakeIdWorker.getUUID();
        IFileChunkInfo.setId(s);
        System.out.println("生成id: "+s);
        return iFileChunkInfoMapper.insertAsyncChunkInfo(IFileChunkInfo);
    }

    /**
     * 修改分片上传单片信息
     * 
     * @param IFileChunkInfo 分片上传单片信息
     * @return 结果
     */
    @Override
    public int updateAsyncChunkInfo(IFileChunkInfo IFileChunkInfo)
    {
        return iFileChunkInfoMapper.updateAsyncChunkInfo(IFileChunkInfo);
    }

    /**
     * 批量删除分片上传单片信息
     * 
     * @param ids 需要删除的分片上传单片信息ID
     * @return 结果
     */
    @Override
    public int deleteAsyncChunkInfoByIds(String[] ids)
    {
        return iFileChunkInfoMapper.deleteAsyncChunkInfoByIds(ids);
    }

    /**
     * 删除分片上传单片信息信息
     * 
     * @param id 分片上传单片信息ID
     * @return 结果
     */
    @Override
    public int deleteAsyncChunkInfoById(String id)
    {
        return iFileChunkInfoMapper.deleteAsyncChunkInfoById(id);
    }

    @Override
    public int deleteAsyncChunkInfoByFile(IFileChunkInfo IFileChunkInfo) {
        return iFileChunkInfoMapper.deleteAsyncChunkInfoByFile(IFileChunkInfo);
    }
}
