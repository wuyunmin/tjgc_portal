package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysSsoConfigVoMapper;
import com.ruoyi.system.domain.SysSsoConfigVo;
import com.ruoyi.system.service.ISysSsoConfigVoService;

import javax.annotation.Resource;

/**
 * 用户单点登录配置Service业务层处理
 * 
 * @author tjgc
 * @date 2020-12-21
 */
@Service
public class SysSsoConfigVoServiceImpl implements ISysSsoConfigVoService
{
    @Resource
    private SysSsoConfigVoMapper sysSsoConfigVOMapper;

    /**
     * 查询用户单点登录配置
     * 
     * @param id 用户单点登录配置ID
     * @return 用户单点登录配置
     */
    @Override
    public SysSsoConfigVo selectSysSsoConfigVOById(Long id)
    {
        return sysSsoConfigVOMapper.selectSysSsoConfigVOById(id);
    }

    @Override
    public SysSsoConfigVo selectSysSsoConfigVOByName(String name)
    {
        return sysSsoConfigVOMapper.selectSysSsoConfigVOByOne(name);
    }
    /**
     * 查询用户单点登录配置列表
     * 
     * @param sysSsoConfigVO 用户单点登录配置
     * @return 用户单点登录配置
     */
    @Override
    public List<SysSsoConfigVo> selectSysSsoConfigVOList(SysSsoConfigVo sysSsoConfigVO)
    {
        return sysSsoConfigVOMapper.selectSysSsoConfigVOList(sysSsoConfigVO);
    }


    @Override
    public List<SysSsoConfigVo> selectSysSsoConfigList(SysSsoConfigVo sysSsoConfigVO)
    {
        return sysSsoConfigVOMapper.selectSysSsoConfigList(sysSsoConfigVO);
    }
    /**
     * 新增用户单点登录配置
     * 
     * @param sysSsoConfigVO 用户单点登录配置
     * @return 结果
     */
    @Override
    public int insertSysSsoConfigVO(SysSsoConfigVo sysSsoConfigVO)
    {
        sysSsoConfigVO.setCreateTime(DateUtils.getNowDate());
        return sysSsoConfigVOMapper.insertSysSsoConfigVO(sysSsoConfigVO);
    }

    /**
     * 修改用户单点登录配置
     * 
     * @param sysSsoConfigVO 用户单点登录配置
     * @return 结果
     */
    @Override
    public int updateSysSsoConfigVO(SysSsoConfigVo sysSsoConfigVO)
    {
        sysSsoConfigVO.setUpdateTime(DateUtils.getNowDate());
        return sysSsoConfigVOMapper.updateSysSsoConfigVO(sysSsoConfigVO);
    }

    /**
     * 批量删除用户单点登录配置
     * 
     * @param ids 需要删除的用户单点登录配置ID
     * @return 结果
     */
    @Override
    public int deleteSysSsoConfigVOByIds(Long[] ids)
    {
        return sysSsoConfigVOMapper.deleteSysSsoConfigVOByIds(ids);
    }

    /**
     * 删除用户单点登录配置信息
     * 
     * @param id 用户单点登录配置ID
     * @return 结果
     */
    @Override
    public int deleteSysSsoConfigVOById(Long id)
    {
        return sysSsoConfigVOMapper.deleteSysSsoConfigVOById(id);
    }


    @Override
    public int updateSysSsoConfigStatus(SysSsoConfigVo sysSsoConfigVO)
    {
        return sysSsoConfigVOMapper.updateSysSsoConfigVO(sysSsoConfigVO);
    }

    @Override
    public List<String> selectAuthSystemList(String userNo)
    {
        return sysSsoConfigVOMapper.selectAuthSystemList(userNo);
    }

    @Override
    public List<String> selectAuthSystemListByPhone(String phone)
    {
        return sysSsoConfigVOMapper.selectAuthSystemListByPhone(phone);
    }

    @Override
    public List<SysSsoConfigVo> selectAuthSysSsoConfigVOList(SysSsoConfigVo sysSsoConfigVO)
    {
        return sysSsoConfigVOMapper.selectAuthSysSsoConfigVOList(sysSsoConfigVO);
    }
}
