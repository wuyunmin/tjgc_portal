package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysIntservMapper;
import com.ruoyi.system.domain.SysIntserv;
import com.ruoyi.system.service.ISysIntservService;

import javax.annotation.Resource;

/**
 * 综合服务配合Service业务层处理
 * 
 * @author tjgc
 * @date 2020-12-29
 */
@Service
public class SysIntservServiceImpl implements ISysIntservService 
{
    @Resource
    private SysIntservMapper sysIntservMapper;

    /**
     * 查询综合服务配合
     * 
     * @param id 综合服务配合ID
     * @return 综合服务配合
     */
    @Override
    public SysIntserv selectSysIntservById(Long id)
    {
        return sysIntservMapper.selectSysIntservById(id);
    }

    /**
     * 查询综合服务配合列表
     * 
     * @param sysIntserv 综合服务配合
     * @return 综合服务配合
     */
    @Override
    public List<SysIntserv> selectSysIntservList(SysIntserv sysIntserv)
    {
        return sysIntservMapper.selectSysIntservList(sysIntserv);
    }


    @Override
    public List<SysIntserv> selectLinkList()
    {
        return sysIntservMapper.selectLinkList();
    }

    @Override
    public List<SysIntserv> selectWeChatList()
    {
        return sysIntservMapper.selectWeChatList();
    }
    /**
     * 新增综合服务配合
     * 
     * @param sysIntserv 综合服务配合
     * @return 结果
     */
    @Override
    public int insertSysIntserv(SysIntserv sysIntserv)
    {
        sysIntserv.setCreateTime(DateUtils.getNowDate());
        return sysIntservMapper.insertSysIntserv(sysIntserv);
    }

    /**
     * 修改综合服务配合
     * 
     * @param sysIntserv 综合服务配合
     * @return 结果
     */
    @Override
    public int updateSysIntserv(SysIntserv sysIntserv)
    {
        sysIntserv.setUpdateTime(DateUtils.getNowDate());
        return sysIntservMapper.updateSysIntserv(sysIntserv);
    }

    /**
     * 批量删除综合服务配合
     * 
     * @param ids 需要删除的综合服务配合ID
     * @return 结果
     */
    @Override
    public int deleteSysIntservByIds(Long[] ids)
    {
        return sysIntservMapper.deleteSysIntservByIds(ids);
    }

    /**
     * 删除综合服务配合信息
     * 
     * @param id 综合服务配合ID
     * @return 结果
     */
    @Override
    public int deleteSysIntservById(Long id)
    {
        return sysIntservMapper.deleteSysIntservById(id);
    }

    @Override
    public int updateSysIntservStatus(SysIntserv sysIntserv)
    {
        return sysIntservMapper.updateSysIntserv(sysIntserv);
    }
}
