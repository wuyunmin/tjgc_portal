package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysSsoSystemMapper;
import com.ruoyi.system.domain.SysSsoSystem;
import com.ruoyi.system.service.ISysSsoSystemService;

/**
 * SSO应用系统Service业务层处理
 * 
 * @author tjgc
 * @date 2020-12-15
 */
@Service
public class SysSsoSystemServiceImpl implements ISysSsoSystemService 
{
    @Autowired
    private SysSsoSystemMapper sysSsoSystemMapper;

    /**
     * 查询SSO应用系统
     * 
     * @param id SSO应用系统ID
     * @return SSO应用系统
     */
    @Override
    public SysSsoSystem selectSysSsoSystemById(String id)
    {
        return sysSsoSystemMapper.selectSysSsoSystemById(id);
    }

    /**
     * 查询SSO应用系统列表
     * 
     * @param sysSsoSystem SSO应用系统
     * @return SSO应用系统
     */
    @Override
    public List<SysSsoSystem> selectSysSsoSystemList(SysSsoSystem sysSsoSystem)
    {
        return sysSsoSystemMapper.selectSysSsoSystemList(sysSsoSystem);
    }

    /**
     * 新增SSO应用系统
     * 
     * @param sysSsoSystem SSO应用系统
     * @return 结果
     */
    @Override
    public int insertSysSsoSystem(SysSsoSystem sysSsoSystem)
    {
        sysSsoSystem.setId(IdUtils.fastSimpleUUID());
        return sysSsoSystemMapper.insertSysSsoSystem(sysSsoSystem);
    }

    /**
     * 修改SSO应用系统
     * 
     * @param sysSsoSystem SSO应用系统
     * @return 结果
     */
    @Override
    public int updateSysSsoSystem(SysSsoSystem sysSsoSystem)
    {
        return sysSsoSystemMapper.updateSysSsoSystem(sysSsoSystem);
    }

    /**
     * 批量删除SSO应用系统
     * 
     * @param ids 需要删除的SSO应用系统ID
     * @return 结果
     */
    @Override
    public int deleteSysSsoSystemByIds(String[] ids)
    {
        return sysSsoSystemMapper.deleteSysSsoSystemByIds(ids);
    }

    /**
     * 删除SSO应用系统信息
     * 
     * @param id SSO应用系统ID
     * @return 结果
     */
    @Override
    public int deleteSysSsoSystemById(String id)
    {
        return sysSsoSystemMapper.deleteSysSsoSystemById(id);
    }
}
