package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SysSsoSystem;

/**
 * SSO应用系统Service接口
 * 
 * @author tjgc
 * @date 2020-12-15
 */
public interface ISysSsoSystemService 
{
    /**
     * 查询SSO应用系统
     * 
     * @param id SSO应用系统ID
     * @return SSO应用系统
     */
    public SysSsoSystem selectSysSsoSystemById(String id);

    /**
     * 查询SSO应用系统列表
     * 
     * @param sysSsoSystem SSO应用系统
     * @return SSO应用系统集合
     */
    public List<SysSsoSystem> selectSysSsoSystemList(SysSsoSystem sysSsoSystem);

    /**
     * 新增SSO应用系统
     * 
     * @param sysSsoSystem SSO应用系统
     * @return 结果
     */
    public int insertSysSsoSystem(SysSsoSystem sysSsoSystem);

    /**
     * 修改SSO应用系统
     * 
     * @param sysSsoSystem SSO应用系统
     * @return 结果
     */
    public int updateSysSsoSystem(SysSsoSystem sysSsoSystem);

    /**
     * 批量删除SSO应用系统
     * 
     * @param ids 需要删除的SSO应用系统ID
     * @return 结果
     */
    public int deleteSysSsoSystemByIds(String[] ids);

    /**
     * 删除SSO应用系统信息
     * 
     * @param id SSO应用系统ID
     * @return 结果
     */
    public int deleteSysSsoSystemById(String id);
}
