package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysSsoLog;
import com.ruoyi.system.domain.SysSsoSystem;

import java.util.List;

public interface SsoService {

    /**
     * 系统加盐校验
     * @param secretkey
     * @param syscode
     * @return
     */

    SysSsoSystem findByScAndCode(String secretkey, String syscode);

    /**
     * 插入系统登陆日志
     * @param ssoLog
     * @return
     */

    int insertLog(SysSsoLog ssoLog);

    /**
     * 更新系统登出地址
     * @param ssoSystem
     * @return
     */
    int updateSsoLogOut(SysSsoSystem ssoSystem);

    /**
     * 查询所有系统
     * @return
     */
    List<SysSsoSystem> selectAll();
}
