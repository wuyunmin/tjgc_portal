package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.UserInfoJy;

/**
 * 经营系统用户信息Service接口
 * 
 * @author tjgc
 * @date 2021-02-24
 */
public interface IUserInfoJyService 
{
    /**
     * 查询经营系统用户信息
     * 
     * @param guid 经营系统用户信息ID
     * @return 经营系统用户信息
     */
    public UserInfoJy selectUserInfoJyById(Long guid);

    /**
     * 查询经营系统用户信息列表
     * 
     * @param userInfoJy 经营系统用户信息
     * @return 经营系统用户信息集合
     */
    public List<UserInfoJy> selectUserInfoJyList(UserInfoJy userInfoJy);

    /**
     * 修改经营系统用户信息
     * 
     * @param userInfoJy 经营系统用户信息
     * @return 结果
     */
    public int updateUserInfoJy(UserInfoJy userInfoJy);
    
    /**
     * 同步经营系系统用户信息
     * 
     * @return 结果
     */
    public int importUser();
}
