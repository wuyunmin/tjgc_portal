package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysSsoInfo;

import java.util.List;

/**
 *
 * 用户单点登录信息Service接口
 * 
 * @author tjgc
 * @date 2020-12-17
 */

public interface ISysSsoInfoService 
{
    /**
     * 查询用户单点登录信息
     * 
     * @param id 用户单点登录信息ID
     * @return 用户单点登录信息
     */
    public SysSsoInfo selectSysSsoInfoById(Long id);


    /***
     * 根据系统类型和员工号查询用户单点登陆信息
     * @param systemName
     * @param userNo
     * @return
     */
    public SysSsoInfo selectSysSsoInfoByOne(String systemName,String userNo);

    /**
     * 查询用户单点登录信息列表
     * 
     * @param sysSsoInfo 用户单点登录信息
     * @return 用户单点登录信息集合
     */
    public List<SysSsoInfo> selectSysSsoInfoList(SysSsoInfo sysSsoInfo);

    /**
     * 新增用户单点登录信息
     * 
     * @param sysSsoInfo 用户单点登录信息
     * @return 结果
     */
    public int insertSysSsoInfo(SysSsoInfo sysSsoInfo);

    /**
     * 修改用户单点登录信息
     * 
     * @param sysSsoInfo 用户单点登录信息
     * @return 结果
     */
    public int updateSysSsoInfo(SysSsoInfo sysSsoInfo);

    /**
     * 批量删除用户单点登录信息
     * 
     * @param ids 需要删除的用户单点登录信息ID
     * @return 结果
     */
    public int deleteSysSsoInfoByIds(Long[] ids);

    /**
     * 删除用户单点登录信息信息
     * 
     * @param id 用户单点登录信息ID
     * @return 结果
     */
    public int deleteSysSsoInfoById(Long id);
}
