package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ruoyi.system.mapper.UserInfoXiaoyuMapper;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.UserInfoXiaoyu;
import com.ruoyi.system.service.IUserInfoXiaoyuService;

/**
 * 小鱼视频用户信息Service业务层处理
 * 
 * @author tjec
 * @date 2021-01-28
 */
@Service
public class UserInfoXiaoyuServiceImpl implements IUserInfoXiaoyuService 
{
    @Autowired
    private UserInfoXiaoyuMapper userInfoXiaoyuMapper;

    /**
     * 查询小鱼视频用户信息
     * 
     * @param guid 小鱼视频用户信息ID
     * @return 小鱼视频用户信息
     */
    @Override
    public UserInfoXiaoyu selectUserInfoXiaoyuByGuid(String guid)
    {
        return userInfoXiaoyuMapper.selectUserInfoXiaoyuByGuid(guid);
    }

    /**
     * 查询小鱼视频用户信息列表
     * 
     * @param userInfoXiaoyu 小鱼视频用户信息
     * @return 小鱼视频用户信息
     */
    @Override
    public List<UserInfoXiaoyu> selectUserInfoXiaoyuList(UserInfoXiaoyu userInfoXiaoyu)
    {
        return userInfoXiaoyuMapper.selectUserInfoXiaoyuList(userInfoXiaoyu);
    }

    /**
     * 新增小鱼视频用户信息
     * 
     * @param userInfoXiaoyu 小鱼视频用户信息
     * @return 结果
     */
    @Override
    public int insertUserInfoXiaoyu(UserInfoXiaoyu userInfoXiaoyu)
    {
        return userInfoXiaoyuMapper.insertUserInfoXiaoyu(userInfoXiaoyu);
    }

    /**
     * 修改小鱼视频用户信息
     * 
     * @param userInfoXiaoyu 小鱼视频用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoXiaoyu(UserInfoXiaoyu userInfoXiaoyu)
    {
        return userInfoXiaoyuMapper.updateUserInfoXiaoyu(userInfoXiaoyu);
    }

    /**
     * 批量删除小鱼视频用户信息
     * 
     * @param guids 需要删除的小鱼视频用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoXiaoyuByGuids(String[] guids)
    {
        return userInfoXiaoyuMapper.deleteUserInfoXiaoyuByGuids(guids);
    }

    /**
     * 删除小鱼视频用户信息信息
     * 
     * @param guid 小鱼视频用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserInfoXiaoyuByGuid(String guid)
    {
        return userInfoXiaoyuMapper.deleteUserInfoXiaoyuByGuid(guid);
    }

	@Override
	public String importUser(List<UserInfoXiaoyu> userInfoXiaoyuList, String operName) {
		//1、先删除所有原表数据
		userInfoXiaoyuMapper.cleanXiaoyuUser();
		//2、导入新数据
		if (StringUtils.isNull(userInfoXiaoyuList) || userInfoXiaoyuList.size() == 0) {
			throw new CustomException("导入用户数据不能为空！");
		}
		int successNum = 0;
		int failureNum = 0;
		StringBuilder successMsg = new StringBuilder();
		StringBuilder failureMsg = new StringBuilder();
		for (UserInfoXiaoyu userInfoXiaoyu : userInfoXiaoyuList) {
			try {
				userInfoXiaoyu.setCreateBy(operName);
				userInfoXiaoyu.setCellPhone(userInfoXiaoyu.getCellPhone()==null?"":userInfoXiaoyu.getCellPhone().trim());
				this.insertUserInfoXiaoyu(userInfoXiaoyu);
				successNum++;
				//successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 导入成功");
			} catch (Exception e) {
				failureNum++;
				String msg = "<br/>" + failureNum + "、姓名 " + userInfoXiaoyu.getUserName() + " 导入失败：";
				failureMsg.append(msg + e.getMessage());
			}
		}
		if (failureNum > 0) {
			failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
			throw new CustomException(failureMsg.toString());
		} else {
			successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
		}
		return successMsg.toString();
	}
}
