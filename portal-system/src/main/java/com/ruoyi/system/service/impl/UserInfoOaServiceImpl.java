package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ruoyi.system.mapper.UserInfoOaMapper;
import com.ruoyi.system.dao.UserInfoDao;
import com.ruoyi.system.domain.UserInfoOa;
import com.ruoyi.system.service.IUserInfoOaService;

/**
 * OA系统用户信息Service业务层处理
 * 
 * @author tjgc
 * @date 2021-02-24
 */
@Service
public class UserInfoOaServiceImpl implements IUserInfoOaService 
{
    @Autowired
    private UserInfoOaMapper userInfoOaMapper;
    
    @Autowired
    private UserInfoDao userInfoDao;

    /**
     * 查询OA系统用户信息
     * 
     * @param guid OA系统用户信息ID
     * @return OA系统用户信息
     */
    @Override
    public UserInfoOa selectUserInfoOaById(Long guid)
    {
        return userInfoOaMapper.selectUserInfoOaById(guid);
    }

    /**
     * 查询OA系统用户信息列表
     * 
     * @param userInfoOa OA系统用户信息
     * @return OA系统用户信息
     */
    @Override
    public List<UserInfoOa> selectUserInfoOaList(UserInfoOa userInfoOa)
    {
        return userInfoOaMapper.selectUserInfoOaList(userInfoOa);
    }

    /**
     * 修改OA系统用户信息
     * 
     * @param userInfoOa OA系统用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfoOa(UserInfoOa userInfoOa)
    {
        return userInfoOaMapper.updateUserInfoOa(userInfoOa);
    }

    @Transactional
	@Override
	public int importUser() {
    	//清除用户
    	userInfoOaMapper.cleanOaUser();
    	//导入用户
    	List<Map<String, Object>> oaUsers = userInfoDao.getAllOAUser();
    	if(oaUsers!=null && oaUsers.size()>0) {
    		userInfoOaMapper.insertUserInfoOa(oaUsers);
    	}else {
    		return 0;
    	}
		return 1;
	}
}
