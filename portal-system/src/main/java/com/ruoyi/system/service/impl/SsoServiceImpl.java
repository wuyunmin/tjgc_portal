package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.SysSsoLog;
import com.ruoyi.system.domain.SysSsoSystem;
import com.ruoyi.system.mapper.SysSsoLogMapper;
import com.ruoyi.system.mapper.SysSsoSystemMapper;
import com.ruoyi.system.service.SsoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SsoServiceImpl implements SsoService {

    @Autowired
    private SysSsoSystemMapper ssoSystemMapper;
    @Autowired
    private SysSsoLogMapper ssoLogMapper;

    @Override
    public SysSsoSystem findByScAndCode(String secretkey, String syscode) {
        SysSsoSystem ssoSystem = new SysSsoSystem();
        ssoSystem.setSysCode(syscode);
        ssoSystem.setSysSecret(secretkey);
        List<SysSsoSystem>list = ssoSystemMapper.selectSysSsoSystemList(ssoSystem);
        if(list != null && list.size() > 0){
            return list.get(0);
        }
        return null;
    }

    @Override
    public int insertLog(SysSsoLog ssoLog) {
        return ssoLogMapper.insertSysSsoLog(ssoLog);
    }

    @Override
    public int updateSsoLogOut(SysSsoSystem ssoSystem) {
        return ssoSystemMapper.updateSysSsoSystem(ssoSystem);
    }

    @Override
    public List<SysSsoSystem> selectAll() {
        return ssoSystemMapper.selectSysSsoSystemList(null);
    }
}
