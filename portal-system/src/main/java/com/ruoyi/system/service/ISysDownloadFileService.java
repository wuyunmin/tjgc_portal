package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.SysDownload;
import com.ruoyi.system.domain.SysDownloadFile;

/**
 * 宣传资料服务配置Service接口
 * 
 * @author tjgc
 * @date 2020-12-30
 */
public interface ISysDownloadFileService 
{
    /**
     * 查询宣传资料服务配置
     * 
     * @param id 宣传资料服务配置ID
     * @return 宣传资料服务配置
     */
    public SysDownloadFile selectSysDownloadFileById(Long id);

    /**
     * 查询宣传资料服务配置列表
     * 
     * @param sysDownloadFile 宣传资料服务配置
     * @return 宣传资料服务配置集合
     */
    public List<SysDownloadFile> selectSysDownloadFileList(SysDownloadFile sysDownloadFile);

    /**
     * 新增宣传资料服务配置
     * 
     * @param sysDownloadFile 宣传资料服务配置
     * @return 结果
     */
    public int insertSysDownloadFile(SysDownloadFile sysDownloadFile);

    /**
     * 修改宣传资料服务配置
     * 
     * @param sysDownloadFile 宣传资料服务配置
     * @return 结果
     */
    public int updateSysDownloadFile(SysDownloadFile sysDownloadFile);

    /**
     * 批量删除宣传资料服务配置
     * 
     * @param ids 需要删除的宣传资料服务配置ID
     * @return 结果
     */
    public int deleteSysDownloadFileByIds(Long[] ids);

    /**
     * 删除宣传资料服务配置信息
     * 
     * @param id 宣传资料服务配置ID
     * @return 结果
     */
    public int deleteSysDownloadFileById(Long id);

    /**
     * 修改状态
     * @param sysDownloadFile
     * @return
     */
    public int updateSysDownloadFileStatus(SysDownloadFile sysDownloadFile);
}
