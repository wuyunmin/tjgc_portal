import request from '@/utils/request'

// 查询帆软用户信息列表
export function listFinereport(query) {
  return request({
    url: '/system/finereport/list',
    method: 'get',
    params: query
  })
}

// 查询帆软用户信息详细
export function getFinereport(id) {
  return request({
    url: '/system/finereport/' + id,
    method: 'get'
  })
}

// 新增帆软用户信息
export function addFinereport(data) {
  return request({
    url: '/system/finereport',
    method: 'post',
    data: data
  })
}

// 修改帆软用户信息
export function updateFinereport(data) {
  return request({
    url: '/system/finereport',
    method: 'put',
    data: data
  })
}

// 删除帆软用户信息
export function delFinereport(id) {
  return request({
    url: '/system/finereport/' + id,
    method: 'delete'
  })
}

// 导出帆软用户信息
export function exportFinereport(query) {
  return request({
    url: '/system/finereport/export',
    method: 'get',
    params: query
  })
}