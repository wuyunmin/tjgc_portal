import request from '@/utils/request'

// 查询oa用户信息列表
export function listOA(query) {
  return request({
    url: '/system/oa/list',
    method: 'get',
    params: query
  })
}

export function importUsers() {
  return request({
    url: '/system/oa/importUser',
    method: 'get'
  })
}