import request from '@/utils/request'

// 查询企业培训学校用户信息列表
export function listSchool(query) {
  return request({
    url: '/system/school/list',
    method: 'get',
    params: query
  })
}

// 查询企业培训学校用户信息详细
export function getSchool(guid) {
  return request({
    url: '/system/school/' + guid,
    method: 'get'
  })
}

// 新增企业培训学校用户信息
export function addSchool(data) {
  return request({
    url: '/system/school',
    method: 'post',
    data: data
  })
}

// 修改企业培训学校用户信息
export function updateSchool(data) {
  return request({
    url: '/system/school',
    method: 'put',
    data: data
  })
}

// 删除企业培训学校用户信息
export function delSchool(guid) {
  return request({
    url: '/system/school/' + guid,
    method: 'delete'
  })
}

// 导出企业培训学校用户信息
export function exportSchool(query) {
  return request({
    url: '/system/school/export',
    method: 'get',
    params: query
  })
}

// 导出企业培训学校导入模板信息
export function templateDown() {
  return request({
    url: '/system/school/templateDown',
    method: 'get'
  })
}