import request from '@/utils/request'

// 查询bpm用户信息列表
export function listBpm(query) {
  return request({
    url: '/system/bpm/list',
    method: 'get',
    params: query
  })
}

// 查询bpm用户信息详细
export function getBpm(id) {
  return request({
    url: '/system/bpm/' + id,
    method: 'get'
  })
}

// 新增bpm用户信息
export function addBpm(data) {
  return request({
    url: '/system/bpm',
    method: 'post',
    data: data
  })
}

// 修改bpm用户信息
export function updateBpm(data) {
  return request({
    url: '/system/bpm',
    method: 'put',
    data: data
  })
}

// 删除bpm用户信息
export function delBpm(id) {
  return request({
    url: '/system/bpm/' + id,
    method: 'delete'
  })
}

// 导出bpm用户信息
export function exportBpm(query) {
  return request({
    url: '/system/bpm/export',
    method: 'get',
    params: query
  })
}


// 同步bpm用户状态信息
export function syncBpm() {
  return request({
    url: '/system/bpm/syncBpm',
    method: 'post',
  })
}
