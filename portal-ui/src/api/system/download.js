import request from '@/utils/request'

// 查询文件服务配置列表
export function listDownload(query) {
  return request({
    url: '/system/download/list',
    method: 'get',
    params: query
  })
}

// 查询文件服务配置详细
export function getDownload(id) {
  return request({
    url: '/system/download/' + id,
    method: 'get'
  })
}

// 新增文件服务配置
export function addDownload(data) {
  return request({
    url: '/system/download',
    method: 'post',
    data: data
  })
}

// 修改文件服务配置
export function updateDownload(data) {
  return request({
    url: '/system/download',
    method: 'put',
    data: data
  })
}

// 删除文件服务配置
export function delDownload(id) {
  return request({
    url: '/system/download/' + id,
    method: 'delete'
  })
}

// 导出文件服务配置
export function exportDownload(query) {
  return request({
    url: '/system/download/export',
    method: 'get',
    params: query
  })
}

//修改状态
export function changeStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/system/download/changeStatus',
    method: 'put',
    data: data
  })
}