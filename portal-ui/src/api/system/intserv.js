import request from '@/utils/request'

// 查询综合服务配合列表
export function listIntserv(query) {
  return request({
    url: '/system/intserv/list',
    method: 'get',
    params: query
  })
}

// 查询综合服务配合详细
export function getIntserv(id) {
  return request({
    url: '/system/intserv/' + id,
    method: 'get'
  })
}

// 新增综合服务配合
export function addIntserv(data) {
  return request({
    url: '/system/intserv',
    method: 'post',
    data: data
  })
}

// 修改综合服务配合
export function updateIntserv(data) {
  return request({
    url: '/system/intserv',
    method: 'put',
    data: data
  })
}

// 删除综合服务配合
export function delIntserv(id) {
  return request({
    url: '/system/intserv/' + id,
    method: 'delete'
  })
}

// 导出综合服务配合
export function exportIntserv(query) {
  return request({
    url: '/system/intserv/export',
    method: 'get',
    params: query
  })
}

//修改状态
export function changeStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/system/intserv/changeStatus',
    method: 'put',
    data: data
  })
}