import request from '@/utils/request'

// 查询用户单点登录配置列表
export function listSsoConfig(query) {
  return request({
    url: '/system/ssoConfig/list',
    method: 'get',
    params: query
  })
}

// 查询用户单点登录配置详细
export function getSsoConfig(id) {
  return request({
    url: '/system/ssoConfig/' + id,
    method: 'get'
  })
}

// 新增用户单点登录配置
export function addSsoConfig(data) {
  return request({
    url: '/system/ssoConfig',
    method: 'post',
    data: data
  })
}
export function changeStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/system/ssoConfig/changeStatus',
    method: 'put',
    data: data
  })
}

// 修改用户单点登录配置
export function updateSsoConfig(data) {
  return request({
    url: '/system/ssoConfig',
    method: 'put',
    data: data
  })
}

// 删除用户单点登录配置
export function delSsoConfig(id) {
  return request({
    url: '/system/ssoConfig/' + id,
    method: 'delete'
  })
}

// 导出用户单点登录配置
export function exportSsoConfig(query) {
  return request({
    url: '/system/ssoConfig/export',
    method: 'get',
    params: query
  })
}
