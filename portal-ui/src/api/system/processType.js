import request from '@/utils/request'

// 查询流程类别列表
export function listType(query) {
  return request({
    url: '/system/processType/list',
    method: 'get',
    params: query
  })
}

// 查询流程类别详细
export function getType(id) {
  return request({
    url: '/system/processType/' + id,
    method: 'get'
  })
}

// 新增流程类别
export function addType(data) {
  return request({
    url: '/system/processType',
    method: 'post',
    data: data
  })
}

// 修改流程类别
export function updateType(data) {
  return request({
    url: '/system/processType',
    method: 'put',
    data: data
  })
}

// 删除流程类别
export function delType(id) {
  return request({
    url: '/system/processType/' + id,
    method: 'delete'
  })
}

// 导出流程类别
export function exportType(query) {
  return request({
    url: '/system/processType/export',
    method: 'get',
    params: query
  })
}