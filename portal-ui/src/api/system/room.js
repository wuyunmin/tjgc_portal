import request from '@/utils/request'

// 查询服务大厅列表
export function listRoom(query) {
  return request({
    url: '/system/room/list',
    method: 'get',
    params: query
  })
}

// 查询服务大厅详细
export function getRoom(guid) {
  return request({
    url: '/system/room/' + guid,
    method: 'get'
  })
}

// 新增服务大厅
export function addRoom(data) {
  return request({
    url: '/system/room',
    method: 'post',
    data: data
  })
}

// 修改服务大厅
export function updateRoom(data) {
  return request({
    url: '/system/room',
    method: 'put',
    data: data
  })
}

// 删除服务大厅
export function delRoom(guid) {
  return request({
    url: '/system/room/' + guid,
    method: 'delete'
  })
}

// 导出服务大厅
export function exportRoom(query) {
  return request({
    url: '/system/room/export',
    method: 'get',
    params: query
  })
}