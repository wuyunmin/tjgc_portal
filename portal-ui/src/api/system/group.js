import request from '@/utils/request'

// 查询域用户信息列表
export function listGroup(query) {
  return request({
    url: '/system/group/list',
    method: 'get',
    params: query
  })
}

// 查询域用户信息详细
export function getGroup(id) {
  return request({
    url: '/system/group/' + id,
    method: 'get'
  })
}

// 新增域用户信息
export function addGroup(data) {
  return request({
    url: '/system/group',
    method: 'post',
    data: data
  })
}

// 修改域用户信息
export function updateGroup(data) {
  return request({
    url: '/system/group',
    method: 'put',
    data: data
  })
}

// 删除域用户信息
export function delGroup(id) {
  return request({
    url: '/system/group/' + id,
    method: 'delete'
  })
}

// 导出域用户信息
export function exportGroup(query) {
  return request({
    url: '/system/group/export',
    method: 'get',
    params: query
  })
}

// 导出域用户导入模板信息
export function templateDown() {
  return request({
    url: '/system/group/templateDown',
    method: 'get'
  })
}