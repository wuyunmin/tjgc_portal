import request from '@/utils/request'

// 查询后台集成用户列表
export function listInteg(query) {
  return request({
    url: '/system/integ/list',
    method: 'get',
    params: query
  })
}

// 查询后台集成用户详细
export function getInteg(guid) {
  return request({
    url: '/system/integ/' + guid,
    method: 'get'
  })
}

// 新增后台集成用户
export function addInteg(data) {
  return request({
    url: '/system/integ',
    method: 'post',
    data: data
  })
}

// 修改后台集成用户
export function updateInteg(data) {
  return request({
    url: '/system/integ',
    method: 'put',
    data: data
  })
}

// 删除后台集成用户
export function delInteg(guid) {
  return request({
    url: '/system/integ/' + guid,
    method: 'delete'
  })
}

// 导出后台集成用户
export function exportInteg(query) {
  return request({
    url: '/system/integ/export',
    method: 'get',
    params: query
  })
}