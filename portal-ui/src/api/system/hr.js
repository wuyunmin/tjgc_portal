import request from '@/utils/request'

// 查询部门同步中间列表
export function listUser(query) {
  return request({
    url: '/system/hr/list',
    method: 'get',
    params: query
  })
}

// 查询部门同步中间详细
export function getUser(id) {
  return request({
    url: '/system/hr/' + id,
    method: 'get'
  })
}

// 新增部门同步中间
export function addUser(data) {
  return request({
    url: '/system/hr',
    method: 'post',
    data: data
  })
}

// 新增部门同步中间
export function selectUser(id) {
  return request({
    url: '/system/hr/selectUser?id='+id,
    method: 'get',
  })
}

export function selectDept(id) {
  return request({
    url: '/system/hr/selectDept?id='+id,
    method: 'get',
  })
}

// 修改部门同步中间
export function updateUser(data) {
  return request({
    url: '/system/hr',
    method: 'put',
    data: data
  })
}

// 删除部门同步中间
export function delUser(id) {
  return request({
    url: '/system/hr/' + id,
    method: 'delete'
  })
}

// 导出部门同步中间
export function exportUser(query) {
  return request({
    url: '/system/hr/export',
    method: 'get',
    params: query
  })
}