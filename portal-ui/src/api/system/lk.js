import request from '@/utils/request'

// 查询领款用户信息列表
export function listLk(query) {
  return request({
    url: '/system/lk/list',
    method: 'get',
    params: query
  })
}

// 查询领款用户信息详细
export function getLk(id) {
  return request({
    url: '/system/lk/' + id,
    method: 'get'
  })
}

// 新增领款用户信息
export function addLk(data) {
  return request({
    url: '/system/lk',
    method: 'post',
    data: data
  })
}

// 修改领款用户信息
export function updateLk(data) {
  return request({
    url: '/system/lk',
    method: 'put',
    data: data
  })
}

// 删除领款用户信息
export function delLk(id) {
  return request({
    url: '/system/lk/' + id,
    method: 'delete'
  })
}

// 导出领款用户信息
export function exportLk(query) {
  return request({
    url: '/system/lk/export',
    method: 'get',
    params: query
  })
}