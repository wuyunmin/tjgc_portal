import request from '@/utils/request'

// 查询企业邮箱用户信息列表
export function listEmail(query) {
  return request({
    url: '/system/email/list',
    method: 'get',
    params: query
  })
}

// 查询企业邮箱用户信息详细
export function getEmail(guid) {
  return request({
    url: '/system/email/' + guid,
    method: 'get'
  })
}

// 新增企业邮箱用户信息
export function addEmail(data) {
  return request({
    url: '/system/email',
    method: 'post',
    data: data
  })
}

// 修改企业邮箱用户信息
export function updateEmail(data) {
  return request({
    url: '/system/email',
    method: 'put',
    data: data
  })
}

// 删除企业邮箱用户信息
export function delEmail(guid) {
  return request({
    url: '/system/email/' + guid,
    method: 'delete'
  })
}

// 导出企业邮箱用户信息
export function exportEmail(query) {
  return request({
    url: '/system/email/export',
    method: 'get',
    params: query
  })
}