import request from '@/utils/request'

// 查询领款用户信息列表
export function listEc(query) {
  return request({
    url: '/system/ec/list',
    method: 'get',
    params: query
  })
}

// 查询领款用户信息详细
export function getEc(id) {
  return request({
    url: '/system/ec/' + id,
    method: 'get'
  })
}

// 新增领款用户信息
export function addEc(data) {
  return request({
    url: '/system/ec',
    method: 'post',
    data: data
  })
}

// 修改领款用户信息
export function updateEc(data) {
  return request({
    url: '/system/ec',
    method: 'put',
    data: data
  })
}

// 删除领款用户信息
export function delEc(id) {
  return request({
    url: '/system/ec/' + id,
    method: 'delete'
  })
}

// 导出领款用户信息
export function exportEc(query) {
  return request({
    url: '/system/ec/export',
    method: 'get',
    params: query
  })
}