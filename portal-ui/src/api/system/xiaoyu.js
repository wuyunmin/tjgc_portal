import request from '@/utils/request'

// 查询小鱼视频用户信息列表
export function listXiaoyu(query) {
  return request({
    url: '/system/xiaoyu/list',
    method: 'get',
    params: query
  })
}

// 查询小鱼视频用户信息详细
export function getXiaoyu(guid) {
  return request({
    url: '/system/xiaoyu/' + guid,
    method: 'get'
  })
}

// 新增小鱼视频用户信息
export function addXiaoyu(data) {
  return request({
    url: '/system/xiaoyu',
    method: 'post',
    data: data
  })
}

// 修改小鱼视频用户信息
export function updateXiaoyu(data) {
  return request({
    url: '/system/xiaoyu',
    method: 'put',
    data: data
  })
}

// 删除小鱼视频用户信息
export function delXiaoyu(guid) {
  return request({
    url: '/system/xiaoyu/' + guid,
    method: 'delete'
  })
}

// 导出小鱼视频用户信息
export function exportXiaoyu(query) {
  return request({
    url: '/system/xiaoyu/export',
    method: 'get',
    params: query
  })
}

//导入清单
export function importData(data) {
  return request({
      url: '/system/xiaoyu/importData',
      method: 'post',
      data: data
  })
}

// 导出小鱼视频导入模板信息
export function templateDown() {
  return request({
    url: '/system/xiaoyu/templateDown',
    method: 'get'
  })
}