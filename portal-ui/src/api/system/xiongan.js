import request from '@/utils/request'

// 查询雄安协同平台用户信息列表
export function listXiongan(query) {
  return request({
    url: '/system/xiongan/list',
    method: 'get',
    params: query
  })
}

// 查询雄安协同平台用户信息详细
export function getXiongan(guid) {
  return request({
    url: '/system/xiongan/' + guid,
    method: 'get'
  })
}

// 新增雄安协同平台用户信息
export function addXiongan(data) {
  return request({
    url: '/system/xiongan',
    method: 'post',
    data: data
  })
}

// 修改雄安协同平台用户信息
export function updateXiongan(data) {
  return request({
    url: '/system/xiongan',
    method: 'put',
    data: data
  })
}

// 删除雄安协同平台用户信息
export function delXiongan(guid) {
  return request({
    url: '/system/xiongan/' + guid,
    method: 'delete'
  })
}

// 导出雄安协同平台用户信息
export function exportXiongan(query) {
  return request({
    url: '/system/xiongan/export',
    method: 'get',
    params: query
  })
}

// 导出雄安协同平台用户导入模板信息
export function templateDown() {
  return request({
    url: '/system/xiongan/templateDown',
    method: 'get'
  })
}