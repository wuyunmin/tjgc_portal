import request from '@/utils/request'

// 查询联系我们列表
export function listOur(query) {
  return request({
    url: '/system/our/list',
    method: 'get',
    params: query
  })
}

// 查询联系我们详细
export function getOur(guid) {
  return request({
    url: '/system/our/' + guid,
    method: 'get'
  })
}

// 新增联系我们
export function addOur(data) {
  return request({
    url: '/system/our',
    method: 'post',
    data: data
  })
}

// 修改联系我们
export function updateOur(data) {
  return request({
    url: '/system/our',
    method: 'put',
    data: data
  })
}

// 删除联系我们
export function delOur(guid) {
  return request({
    url: '/system/our/' + guid,
    method: 'delete'
  })
}

// 导出联系我们
export function exportOur(query) {
  return request({
    url: '/system/our/export',
    method: 'get',
    params: query
  })
}