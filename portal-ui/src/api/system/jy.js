import request from '@/utils/request'

// 查询oa用户信息列表
export function listJY(query) {
  return request({
    url: '/system/jy/list',
    method: 'get',
    params: query
  })
}

export function importUsers() {
  return request({
    url: '/system/jy/importUser',
    method: 'get'
  })
}