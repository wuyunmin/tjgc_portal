import request from '@/utils/request'

// 查询钉钉用户信息列表
export function listDing(query) {
  return request({
    url: '/system/ding/list',
    method: 'get',
    params: query
  })
}

// 查询钉钉用户信息详细
export function getDing(id) {
  return request({
    url: '/system/ding/' + id,
    method: 'get'
  })
}

// 新增钉钉用户信息
export function addDing(data) {
  return request({
    url: '/system/ding',
    method: 'post',
    data: data
  })
}

// 修改钉钉用户信息
export function updateDing(data) {
  return request({
    url: '/system/ding',
    method: 'put',
    data: data
  })
}

// 删除钉钉用户信息
export function delDing(id) {
  return request({
    url: '/system/ding/' + id,
    method: 'delete'
  })
}

// 导出钉钉用户信息
export function exportDing(query) {
  return request({
    url: '/system/ding/export',
    method: 'get',
    params: query
  })
}

// 导出钉钉用户导入模板信息
export function templateDown() {
  return request({
    url: '/system/ding/templateDown',
    method: 'get'
  })
}