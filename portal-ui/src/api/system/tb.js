import request from '@/utils/request'

// 查询teambition用户信息列表
export function listTb(query) {
  return request({
    url: '/system/tb/list',
    method: 'get',
    params: query
  })
}

// 查询teambition用户信息详细
export function getTb(guid) {
  return request({
    url: '/system/tb/' + guid,
    method: 'get'
  })
}

// 新增teambition用户信息
export function addTb(data) {
  return request({
    url: '/system/tb',
    method: 'post',
    data: data
  })
}

// 修改teambition用户信息
export function updateTb(data) {
  return request({
    url: '/system/tb',
    method: 'put',
    data: data
  })
}

// 删除teambition用户信息
export function delTb(guid) {
  return request({
    url: '/system/tb/' + guid,
    method: 'delete'
  })
}

// 导出teambition用户信息
export function exportTb(query) {
  return request({
    url: '/system/tb/export',
    method: 'get',
    params: query
  })
}

// 导出企业培训学校导入模板信息
export function templateDown() {
  return request({
    url: '/system/tb/templateDown',
    method: 'get'
  })
}