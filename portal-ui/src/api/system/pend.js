import request from '@/utils/request'

// 查询待办待阅信息列表
export function listPend(query) {
  return request({
    url: '/system/pend/allList',
    method: 'get',
    params: query
  })
}

// 查询待办待阅信息详细
export function getPend(id) {
  return request({
    url: '/system/pend/' + id,
    method: 'get'
  })
}

// 新增待办待阅信息
export function addPend(data) {
  return request({
    url: '/system/pend',
    method: 'post',
    data: data
  })
}

// 修改待办待阅信息
export function updatePend(data) {
  return request({
    url: '/system/pend',
    method: 'put',
    data: data
  })
}

// 删除待办待阅信息
export function delPend(id) {
  return request({
    url: '/system/pend/' + id,
    method: 'delete'
  })
}

// 导出待办待阅信息
export function exportPend(query) {
  return request({
    url: '/system/pend/export',
    method: 'get',
    params: query
  })
}


export function syncWorkItemHr() {
  return request({
    url: '/system/pend/syncWorkItemHr',
    method: 'get'
  })
}

export function syncWorkItemOa() {
  return request({
    url: '/system/pend/syncWorkItemOa',
    method: 'get'
  })
}


export function syncWorkItemBpm() {
  return request({
    url: '/system/pend/syncWorkItemBpm',
    method: 'get'
  })
}