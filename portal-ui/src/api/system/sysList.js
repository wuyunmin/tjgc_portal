import request from '@/utils/request'

// 查询系统清单列表
export function listSysList(query) {
  return request({
    url: '/system/sysList/list',
    method: 'get',
    params: query
  })
}

// 查询系统清单详细
export function getSysList(id) {
  return request({
    url: '/system/sysList/' + id,
    method: 'get'
  })
}

// 新增系统清单
export function addSysList(data) {
  return request({
    url: '/system/sysList',
    method: 'post',
    data: data
  })
}

// 修改系统清单
export function updateSysList(data) {
  return request({
    url: '/system/sysList',
    method: 'put',
    data: data
  })
}

// 删除系统清单
export function delSysList(id) {
  return request({
    url: '/system/sysList/' + id,
    method: 'delete'
  })
}

// 导出系统清单
export function exportSysList(query) {
  return request({
    url: '/system/sysList/export',
    method: 'get',
    params: query
  })
}

// 获取统一用户管理系统字典（允许代持账号的系统）
export function getSystemDict() {
  return request({
    url: '/system/sysList/getSystemDict',
    method: 'get'
  })
}

// 获取统一用户管理系统字典（允许代持账号的系统）
export function getAllSystemDict() {
  return request({
    url: '/system/sysList/getAllSystemDict',
    method: 'get'
  })
}

// 获取统一用户管理系统字典（允许后台用户集成的系统）
export function getSystemDictInteg() {
  return request({
    url: '/system/sysList/getSystemDictInteg',
    method: 'get'
  })
}

  // 查询所有有效的系统字典
  export function getSystemListDictData() {
    return request({
      url: '/system/sysList/getSystemListDictData',
      method: 'get'
    })
  }