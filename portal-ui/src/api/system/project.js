import request from '@/utils/request'

// 查询项目协同平台用户信息列表
export function listProject(query) {
  return request({
    url: '/system/project/list',
    method: 'get',
    params: query
  })
}

// 查询项目协同平台用户信息详细
export function getProject(guid) {
  return request({
    url: '/system/project/' + guid,
    method: 'get'
  })
}

// 新增项目协同平台用户信息
export function addProject(data) {
  return request({
    url: '/system/project',
    method: 'post',
    data: data
  })
}

// 修改项目协同平台用户信息
export function updateProject(data) {
  return request({
    url: '/system/project',
    method: 'put',
    data: data
  })
}

// 删除项目协同平台用户信息
export function delProject(guid) {
  return request({
    url: '/system/project/' + guid,
    method: 'delete'
  })
}

// 导出项目协同平台用户信息
export function exportProject(query) {
  return request({
    url: '/system/project/export',
    method: 'get',
    params: query
  })
}

// 导出项目协同平台导入模板信息
export function templateDown() {
  return request({
    url: '/system/project/templateDown',
    method: 'get'
  })
}