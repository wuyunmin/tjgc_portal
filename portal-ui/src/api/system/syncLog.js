import request from '@/utils/request'

// 查询同步日志信息列表
export function listLog(query) {
  return request({
    url: '/system/syncLog/list',
    method: 'get',
    params: query
  })
}

// 查询同步日志信息详细
export function getLog(id) {
  return request({
    url: '/system/syncLog/' + id,
    method: 'get'
  })
}

// 新增同步日志信息
export function addLog(data) {
  return request({
    url: '/system/syncLog',
    method: 'post',
    data: data
  })
}

// 修改同步日志信息
export function updateLog(data) {
  return request({
    url: '/system/syncLog',
    method: 'put',
    data: data
  })
}

// 删除同步日志信息
export function delLog(id) {
  return request({
    url: '/system/syncLog/' + id,
    method: 'delete'
  })
}

// 导出同步日志信息
export function exportLog(query) {
  return request({
    url: '/system/syncLog/export',
    method: 'get',
    params: query
  })
}