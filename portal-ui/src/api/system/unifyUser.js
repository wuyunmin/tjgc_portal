import request from '@/utils/request'

// 查询用户列表
export function holderAccountList(query) {
  return request({
    url: '/system/unifyUser/holderAccountList',
    method: 'get',
    params: query
  })
}

// 删除代持账号
export function delHolderAccount(data) {
  return request({
    url: '/system/unifyUser/' + data,
    method: 'delete'
  })
}

// 新增代持账号
export function addHolderAccount(data) {
  return request({
    url: '/system/unifyUser',
    method: 'post',
    data: data
  })
}