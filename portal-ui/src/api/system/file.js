import request from '@/utils/request'

// 查询宣传资料服务配置列表
export function listFile(query) {
  return request({
    url: '/system/file/list',
    method: 'get',
    params: query
  })
}

// 查询宣传资料服务配置详细
export function getFile(id) {
  return request({
    url: '/system/file/' + id,
    method: 'get'
  })
}

// 新增宣传资料服务配置
export function addFile(data) {
  return request({
    url: '/system/file',
    method: 'post',
    data: data
  })
}

// 修改宣传资料服务配置
export function updateFile(data) {
  return request({
    url: '/system/file',
    method: 'put',
    data: data
  })
}

// 删除宣传资料服务配置
export function delFile(id) {
  return request({
    url: '/system/file/' + id,
    method: 'delete'
  })
}

// 导出宣传资料服务配置
export function exportFile(query) {
  return request({
    url: '/system/file/export',
    method: 'get',
    params: query
  })
}

//修改状态
export function changeStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/system/file/changeStatus',
    method: 'put',
    data: data
  })
}