import Axios from "axios";
import request from '@/utils/request'

export function download(fileName,url) {
    Axios({ url, responseType: "blob" }).then((res) => {
        // 创建隐藏的可下载链接
        let eleLink = document.createElement("a");
        eleLink.download = fileName;
        eleLink.style.display = "none";
        // 字符内容转变成blob地址
        const blob = new Blob([res.data]);
        eleLink.href = URL.createObjectURL(blob);
        // 触发点击
        // document.body.appendChild(eleLink);
        eleLink.click();
        eleLink = null;
        // 然后移除;
        // document.body.removeChild(eleLink);
      });
}

export function deleteFile(data) {
  return request({
    url: '/system/deleteFileByFileUrl',
    method: 'post',
    data: data
  })
}