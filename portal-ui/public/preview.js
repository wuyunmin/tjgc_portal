//文件预览地址
export function previewUrl() {
    return 'http://preview.tongji-ec.com.cn'
}
// bpm测试环境地址
export function bpmUrl() {
    // return 'http://bpmtest.tongji-ec.com.cn'
    return 'http://bpm.tongji-ec.com.cn'
}

// export function previewStoragePathUrl() {
//     return 'http://10.66.37.181'
// }

//ec操作手册
export function operationManual() {
    // return 'http://ectest.tongji-ec.com.cn/prod-api/profile/template/操作手册/'
    return 'http://ec.tongji-ec.com.cn/prod-api/profile/template/操作手册/'
}

//ec流程操作手册
export function operationWorkflowManual() {
    // return 'http://ectest.tongji-ec.com.cn/prod-api/profile/template/流程操作指南/'
    return 'http://ec.tongji-ec.com.cn/prod-api/profile/template/流程操作指南/'
}


export function lookBpmJobNo() {
    // return '99999';
    return 'http://ec.tongji-ec.com.cn/prod-api/profile/template/流程操作指南/'
}


//门户登录地址
export function portalLoginUrl() {
    if(window["context"] == undefined) {
        if(!window.location.origin) {
         window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');  
         }
         window["context"] = location.origin+"/V6.0";
    }
    const host = window.location.origin;
    return host
} 