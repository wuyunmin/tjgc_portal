if (window["context"] == undefined) {
  if (!window.location.origin) {
    window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ":" + window.location.port : "");
  }
  window["context"] = location.origin + "/V6.0";
}
const host = window.location.origin;

window.config = {
  apiHost: "http://portaltest.tongji-ec.com.cn/stage-api",
  previewUrl: "http://preview.tongji-ec.com.cn",
  systemManagementUrl: host + "/system#/tokenLogin",
  systemManagementLogoutUrl: host + "/system#/tokenLogout",
  bpmHost: "http://bpmtest.tongji-ec.com.cn"
};
