module.exports = {
  // 处理IE兼容————vuex持久化脚本语法转译
  transpileDependencies: ["ant-design-vue"],
  // publicPath: "",
  filenameHashing: true,
  productionSourceMap: true,

  configureWebpack: (config) => {
    if (process.env.NODE_ENV === "production") {
      config.devtool = "source-map";
    } else {
      config.devtool = "cheap-eval-source-map";
    }
  },
  chainWebpack: (config) => {
    config.plugin("html").tap((args) => {
      args[0].title = "同济咨询企业综合服务门户";
      return args;
    });
  },
  devServer: {
    port: 9100,
    open: true,
    proxy: {
      [`${process.env.VUE_APP_API_PREFIX}/`]: {
        target: `${process.env.VUE_APP_API}/`,
        changeOrigin: true,
        pathRewrite: {
          [`^${process.env.VUE_APP_API_PREFIX}`]: ""
        }
      }
    },
    before: (app) => {
      /**
       * 支持跨域访问
       */
      app.all("*", function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With");
        res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
        res.header("X-Powered-By", "3.2.1");
        if (req.method == "OPTIONS") {
          res.sendStatus(200);
        } else {
          next();
        }
      });
    },
    disableHostCheck: true
  },
  css: {
    loaderOptions: {
      less: {
        javascriptEnabled: true
        // prependData: `@import "~@/assets/style/main.less";`
      }
    }
  }
};
