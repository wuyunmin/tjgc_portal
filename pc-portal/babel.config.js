module.exports = {
  presets: ["@vue/cli-plugin-babel/preset"],
  plugins: [
    [
      "@babel/plugin-transform-runtime",
      {
        corejs: 3,
        helpers: true,
        regenerator: true,
        useESModules: false
      }
    ],
    ["import", { libraryName: "ant-design-vue", libraryDirectory: "es", style: true }]
  ]
};
