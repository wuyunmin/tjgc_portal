declare namespace Api {
  interface Response {
    msg: string;
    code: number | string;
    [other: string]: any;
  }

  interface CommonList {
    pageNum: number; //起始索引，必填
    pageSize: number; //每页显示记录数，必填
    status: string; //状态码（0正常1停用），入参为0，必填
  }

  interface NoticeList {
    pageNum: number; //起始索引，必填
    pageSize: number; //每页显示记录数，必填
  }

  interface NoticeNearby {
    noticeId: number; //公告id
    turnPage: number; //翻页标识，0向前，1向后
  }

  interface Login {
    username: string;
    password: string;
  }

  interface LoginByCode {
    phonenumber: string;
    code: string;
    uuid: string;
  }

  interface SendCaptchaSMS {
    phonenumber: string; // 用户手机号码，必填
    isLogin: boolean; // 是否为登录时获取验证码（登录时通过手机获取验证码为ture，否则为fasle）,必填
  }

  interface UpdatePhonenumber {
    phonenumber: string; // 手机号码
    code: string; // 验证码
    uuid: string; // 发送验证码返回的uuid
  }

  interface UpdatePwdByOld {
    oldPassword: string;
    newPassword: string;
  }

  interface UpdatePwdByCode {
    password: string;
    phonenumber: string;
    code: string;
    uuid: string;
  }

  interface ProcessCount {
    pendType: string; // 待办/待阅识别标志（1=待办,2=待阅）
    status: string; // 待办/待阅处理状态（1=处理完成,2=未处理,3=撤回），默认2，必填
    receiverUserCode: string; // 待办/待阅接收人，默认为当前登录人工号，必填
    procedureName?: string; // 流程名称,非必填
    promoter?: string; // 发起人,非必填
    systemName?: string; // 系统来源,非必填
    promoterOrg?: string; // 发起部门,非必填
    title?: string; // 流程标题,非必填
  }

  interface ProcessList {
    pageNum: number; // 分页，起始索引，必填
    pageSize: number; // 分页，每页显示记录数，必填
    sortBy: string; //排序字段，默认传receiverTime，必填
    order: string; //正序倒序，默认desc，必填
    pendType: string; //待办/待阅识别标志（1=待办,2=待阅）
    status: string; //待办/待阅处理状态（1=处理完成,2=未处理,3=撤回），默认2，必填
    receiverUserCode: string; //待办/待阅接收人，默认为当前登录人工号，必填
    procedureName?: string; //流程名称,非必填
    promoter?: string; //发起人,非必填
    systemName?: string; //系统来源,非必填
    promoterOrg?: string; //发起部门,非必填
    title?: string; //流程标题,非必填
    processType?: string; // 流程类型
  }

  interface SsoInfoList {
    pageNum: number; //起始索引，必填
    pageSize: number; //每页显示记录数，必填
  }

  interface SsoInfoAdd {
    systemName: string; // 系统名称code
    systemNameCn: string; // 系统中文名
    userName: string; // 账号值
    password: string; // 密码值
  }

  interface SsoInfoDel {
    ids: number[];
  }

  interface SearchServiceRoom {
    name?: string; // 标题，非必填
    serviceType?: string; // 服务类别，非必填
  }

  interface GetServiceRoomLink {
    type: string; // 类型
    link: string; // 链接
  }

  interface SystemFileAllList {
    pageNum?: number;
    pageSize?: number;
    status: string; // 状态码（0正常 1停用）
  }
}
