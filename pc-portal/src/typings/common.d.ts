declare namespace Common {
  interface CTabsEmit {
    activeIndex: number;
  }

  interface CBread {
    label: string;
    to?: any;
    url?: any;
  }

  /**
   * 跳转到登录页需要带过去的参数
   */
  interface GoLoginParams {
    errmsg?: string;
    [other: string]: any;
  }

  /**
   * 发送短信验证码状态
   */
  enum SendCodeStatus {
    "undo", // 未发送
    "doing", // 正在发送
    "done" // 发送完毕
  }
}
