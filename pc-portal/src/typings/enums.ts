/**
 * 发送短信验证码状态
 */
export enum SendCodeStatus {
  "undo" = "undo", // 未发送
  "doing" = "doing", // 正在发送
  "done" = "done", // 发送完毕
  "reset" = "reset" // 重新发送
}

export enum SendCodeText {
  "undo" = "获取验证码", // 未发送
  "doing" = "正在发送...", // 正在发送
  "done" = "重新获取", // 发送完毕
  "reset" = "重新获取" // 重新发送
}

/**
 * 登陆方式
 */
export enum LoginType {
  "pwd" = "pwd", // 密码登陆
  "code" = "code" // 验证码登录
}

export enum FileType {
  "doc" = "iconword",
  "dot" = "iconword",
  "docx" = "iconword",
  "dotx" = "iconword",
  "docm" = "iconword",
  "dotm" = "iconword",
  "xls" = "iconxcel",
  "xlt" = "iconxcel",
  "xla" = "iconxcel",
  "xlsx" = "iconxcel",
  "xltx" = "iconxcel",
  "xlsm" = "iconxcel",
  "xltm" = "iconxcel",
  "xlam" = "iconxcel",
  "xlsb" = "iconxcel",
  "ppt" = "iconppt1",
  "pot" = "iconppt1",
  "pps" = "iconppt1",
  "ppa" = "iconppt1",
  "pptx" = "iconppt1",
  "potx" = "iconppt1",
  "ppsx" = "iconppt1",
  "ppam" = "iconppt1",
  "pptm" = "iconppt1",
  "potm" = "iconppt1",
  "ppsm" = "iconppt1",
  "zip" = "iconzip",
  "rar" = "iconrar",
  "pdf" = "iconpdf",
  "default" = "icondefault_file"
}

/**
 * 服务大厅中服务类型
 */
export enum ServiceHallType {
  "common_link" = "common_link", // 普通链接
  "download_link" = "download_link", // 下载链接
  "bpm_workflow_link" = "bpm_workflow_link", // BPM流程
  "hr_workflow_link" = "hr_workflow_link", // hr流程
  "oa_workflow_link" = "oa_workflow_link", // oa流程
  "preview_link" = "preview_link", // 预览链接
  "ec_link" = "ec_link"
}
