/**
 * 主组件继承Base，刷新后重载相关基础接口数据，并更新到Vuex
 */
import { Vue, Component } from "vue-property-decorator";
import { namespace } from "vuex-class";
import ClipboardJS from "clipboard";

const UserModule = namespace("User");

@Component({
  name: "Base",
  components: {}
})
export default class Base extends Vue {
  @UserModule.State("currentUser") currentUser!: any;
  @UserModule.Action("setUser") setUser!: Function;
  @UserModule.Action("updateUser") updateUser!: Function;
  protected bread: Common.CBread[] = [];

  constructor() {
    super();
    if (this.$route.name !== "Login") {
      this.updateUser();
    }
  }

  public _created(bread: Common.CBread[] = []) {
    this.bread = [...bread, { label: this.$route.meta.name }];
  }

  protected _logout() {
    this.$tools.goLoginPage();
  }

  protected _confirm(optioins: any = {}) {
    const opts = {
      ...{ okText: "确认", cancelText: "取消" },
      ...optioins
    };
    this.$modal.confirm(opts);
  }

  protected async _goFlowDetail(id: string) {
    const res = await this.$api.FlowDetailLink(id);
    if (res && res.code === 200 && res.data) {
      this._goto(res.data.URL, res.data.openWay);
    } else {
      this.$message.warning("链接地址为空");
    }
  }

  // 门户跳转逻辑
  protected async _goto(url: string, openWay?: string) {
    if (openWay === "3") {
      // 提示下载客户端
      this.$gModal({
        content:
          "\
          <p>该系统，使用客户端访问，无法使用单点登陆，直接登录。 请下载客户端，本地安装后，登陆访问</p>\
          <p><a class='row' style='color:blue;' target='_blank' href='" +
          url +
          "'>获取安装包</a></p>\
        ",
        cancelObj: {
          text: false
        },
        okObj: {
          text: false
        }
      });
    } else if (openWay === "1") {
      this.$tools.cOpen({ url });
      return;
      if (this.$tools.is360()) {
        // 360浏览器直接打开
        this.$tools.cOpen({ url });
      } else {
        this.$gModal({
          content: "<p>此系统建议使用IE浏览器打开</p>",
          cancelObj: {
            text: "一键复制地址",
            type: "primary",
            id: "modal_global_cancel"
          },
          okObj: {
            text: "继续用当前浏览器打开",
            type: ""
          },
          cancel: async () => {
            // 复制url，手动打开
            const clipboard = new ClipboardJS("#modal_global_cancel", {
              text: function() {
                return url;
              }
            });

            clipboard.on("success", () => {
              this.$message.info("已复制");
            });

            // clipboard.on("error", () => {});
          },
          ok: () => {
            this.$tools.cOpen({ url: url });
          }
        });
      }
    } else {
      // 当前浏览器打开
      this.$tools.cOpen({ url: url });
    }
  }
}
