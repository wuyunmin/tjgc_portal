import template from "./template.vue";
import Vue from "vue";
const ModalConstructor: any = Vue.extend(template);

const GModal = (o: any = {}) => {
  const alertDom = new ModalConstructor({
    el: document.createElement("div") //将Alert组件挂载到新创建的div上
  });
  document.body.appendChild(alertDom.$el); //把Alert组件的dom添加到body里

  // 标题
  alertDom.title = o.title || alertDom.title;
  // 弹框内容
  alertDom.content = o.content;

  if (o && o.cancelObj) {
    alertDom.cancelObj = Object.assign({}, alertDom.cancelObj, o.cancelObj);
  }

  if (o && o.okObj) {
    alertDom.okObj = Object.assign({}, alertDom.okObj, o.okObj);
  }

  alertDom.okText = o.okText;

  alertDom.ok = typeof o.ok === "function" ? o.ok : null;
  alertDom.cancel = typeof o.cancel === "function" ? o.cancel : null;
};

// 导出该组件
export default GModal;
