import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Index",
    redirect: "/home",
    component: () => import("@/views/Index.vue"),
    children: [
      {
        path: "/home",
        name: "Home",
        component: () => import("../views/Home/index.vue"),
        meta: {
          name: "首页",
          nav: "Home"
        }
      },
      {
        path: "/serviceHall",
        name: "ServiceHall",
        component: () => import("../views/ServiceHall/index.vue"),
        meta: {
          name: "服务大厅",
          nav: "ServiceHall"
        }
      },
      {
        path: "/notice",
        name: "Notice",
        component: () => import("../views/Notice/index.vue"),
        meta: {
          name: "通知公告",
          nav: "Notice"
        }
      },
      {
        path: "/notice/detail",
        name: "NoticeDetail",
        component: () => import("../views/Notice/detail.vue"),
        meta: {
          name: "通告详情",
          nav: "Notice"
        }
      },
      {
        path: "/link",
        name: "Link",
        component: () => import("../views/Link/index.vue"),
        meta: {
          name: "常用链接",
          nav: "Link"
        }
      },
      {
        path: "/application",
        name: "Application",
        component: () => import("../views/Application/index.vue"),
        meta: {
          name: "应用系统",
          nav: "Application"
        }
      },
      {
        path: "/flow",
        name: "Flow",
        component: () => import("../views/Flow/index.vue"),
        meta: {
          name: "流程中心",
          nav: "Flow"
        }
      },
      {
        path: "/user",
        name: "User",
        component: () => import("../views/User/index.vue"),
        meta: {
          name: "个人中心"
        }
      },
      {
        path: "/singleSign",
        name: "SingleSign",
        component: () => import("../views/SingleSign/index.vue"),
        meta: {
          name: "设置单点登录"
        }
      }
    ]
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login/index.vue"),
    meta: {
      name: "登录"
    }
  }
];

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location: any) {
  return (originalPush as any).call(this, location).catch((err: any) => err);
};

const router = new VueRouter({
  routes
});

router.beforeEach((to: any, from: any, next: Function) => {
  const token = localStorage.getItem("token");
  if (token || to.name === "Login") {
    next();
  } else {
    next({ name: "Login" });
  }
});

export default router;
