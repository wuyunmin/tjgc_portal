import { Modal, message } from "ant-design-vue";

export default class CPromise {
  /**
   * 页面初始化的时候，多个接口统一处理，显示和关闭loading
   * @param {Array<Promise>} args 需要执行的方法，必须有返回值
   */
  public static all(args: any[]) {
    const loading = message.loading("正在加载...", 0);
    return Promise.all(args)
      .then((resArr: Api.Response[]) => {
        for (const item of resArr) {
          if (item && ((item.code !== void 0 && item.code != "200") || (item.state !== void 0 && item.state != "200"))) {
            Modal.warning({
              content: item.msg
            });
            break;
          }
        }
      })
      .catch((err: ErrorEvent) => {
        console.log(11111111);
        console.log(err);
        console.log(11111111);
      })
      .finally(() => {
        setTimeout(() => {
          loading();
        }, 200);
      });
  }
}
