import Axios from "@/config/axios";

const headers = {
  post_json: { "Content-Type": "application/json; charset=UTF-8" }
};

export default {
  /**
   * 通知公告列表
   */
  NoticeList(params: Api.NoticeList): Promise<Api.Response> {
    return Axios.get("/system/notice/allList", { params });
  },

  /**
   * 通知公告详情
   */
  NoticeDetail(noticeId: string): Promise<Api.Response> {
    return Axios.get("/system/notice/getNotice", { params: { noticeId } });
  },

  /**
   * 获取登陆人是否有生成的短链接
   */
  hasShortUrl(): Promise<Api.Response> {
    return Axios.get("/getShortUrl/hasShortUrl", {});
  },
  /**
   * 通知公告详情
   */
  NoticeNearby(params: Api.NoticeNearby): Promise<Api.Response> {
    return Axios.get("/system/notice/getNearbyNotice", { params });
  },

  /**
   * 工具下载
   */
  DownloadList(params: Api.CommonList): Promise<Api.Response> {
    return Axios.get("/system/download/allList", { params });
  },

  /**
   * 综合服务-二维码
   */
  IntservWeChatList(): Promise<Api.Response> {
    return Axios.get("/system/intserv/weChatList");
  },

  /**
   * 综合服务-常用链接
   */
  IntservLink(): Promise<Api.Response> {
    return Axios.get("/system/intserv/linkList");
  },

  /**
   * 密码登陆
   */
  Login(params: Api.Login): Promise<Api.Response> {
    return Axios.post("/login", params, { headers: { ...headers.post_json } });
  },

  /**
   * 验证码登陆
   */
  LoginByCode(params: Api.LoginByCode): Promise<Api.Response> {
    return Axios.post("/phonenumberLogin", params);
  },

  /**
   * 获取用户信息
   */
  GetUserInfo(): Promise<Api.Response> {
    return Axios.get("/getInfo");
  },

  /**
   * 发送验证码
   */
  SendCaptchaSMS(params: Api.SendCaptchaSMS): Promise<Api.Response> {
    return Axios.get("/captchaSMS", { params });
  },

  /**
   * 修改手机号码
   */
  UpdatePhonenumber(params: Api.UpdatePhonenumber): Promise<Api.Response> {
    return Axios.put("/system/user/updatePhonenumber", params);
  },

  /**
   * 通过旧密码修改密码
   */
  UpdatePwdByOld(params: Api.UpdatePwdByOld): Promise<Api.Response> {
    return Axios.put("/system/user/updatePwd", params);
  },

  /**
   * 通过验证码修改密码
   */
  UpdatePwdByCode(params: Api.UpdatePwdByCode): Promise<Api.Response> {
    return Axios.put("/system/user/updatePwdByPhonenumber", params);
  },

  /**
   * 根据数据数据字典类型获取数据字典信息
   */
  SystemDict(params: string): Promise<Api.Response> {
    return Axios.get("/system/dict/data/type/" + params);
  },

  /**
   * 待办/待阅统计数量
   */
  ProcessCount(params: Api.ProcessCount): Promise<Api.Response> {
    return Axios.get("/system/process/count", { params });
  },

  /**
   * 待办/待阅分页查询
   */
  ProcessList(params: Api.ProcessList): Promise<Api.Response> {
    return Axios.get("/system/process/list", { params });
  },

  /**
   * 获取待办待阅跳转链接
   */
  FlowDetailLink(id: string): Promise<Api.Response> {
    return Axios.get("/system/process/getJumpLink", { params: { id } });
  },

  /**
   * 获取系统时间戳
   */
  SystemTime(): Promise<Api.Response> {
    return Axios.get("/system/time");
  },

  /**
   * 路由信息，用来判断是否有权限显示首页的系统管理
   */
  GetRouters(): Promise<Api.Response> {
    return Axios.get("/getRouters");
  },

  /**
   * 应用列表
   */
  SsoConfigAllList(): Promise<Api.Response> {
    return Axios.get("/system/ssoConfig/allList");
  },

  /**
   * 应用跳转地址获取
   */
  GetSsoUrl(id: string): Promise<Api.Response> {
    return Axios.get("/system/ssoConfig/getSsoUrl", { params: { id } });
  },

  /**
   * 单点配置列表
   */
  SsoInfoList(params: Api.SsoInfoList): Promise<Api.Response> {
    return Axios.get("/system/ssoInfo/list", { params });
  },

  /**
   * 单点登录数据字典
   */
  SsoConfigList(): Promise<Api.Response> {
    return Axios.get("/system/ssoConfig/ssoConfigList");
  },

  /**
   * 单点登录新增
   */
  SsoInfoAdd(params: Api.SsoInfoAdd): Promise<Api.Response> {
    return Axios.post("/system/ssoInfo/add", params, { headers: { ...headers.post_json } });
  },

  /**
   * 单点登录删除
   */
  SsoInfoDel(params: Api.SsoInfoDel): Promise<Api.Response> {
    return Axios.post("/system/ssoInfo/delete", params, { headers: { ...headers.post_json } });
  },

  /**
   * 获取组织树
   */
  DeptAllTreeSelect(): Promise<Api.Response> {
    return Axios.get("/system/dept/allTreeSelect");
  },

  /**
   * 手机验证码校验
   */
  CodeVerify(params: Api.UpdatePhonenumber): Promise<Api.Response> {
    return Axios.get("/codeVerify", { params });
  },

  /**
   * 根据标题和服务类别获取服务大厅信息
   */
  SearchServiceRoom(params: Api.SearchServiceRoom): Promise<Api.Response> {
    return Axios.get("/system/room/searchServiceRoom", { params });
  },

  /**
   * 服务大厅-联系我们
   */
  ServiceGetContactOur(): Promise<Api.Response> {
    return Axios.get("/system/our/getContactOur");
  },

  /**
   * 服务大厅-根据类型和链接地址获取跳转链接
   */
  GetServiceRoomLink(params: Api.GetServiceRoomLink): Promise<Api.Response> {
    return Axios.post("/system/ssoConfig/getServiceRoomLink", params, { headers: { ...headers.post_json } });
  },

  /**
   * 首页-宣传资料
   */
  SystemFileAllList(params: Api.SystemFileAllList): Promise<Api.Response> {
    return Axios.get("/system/file/allList", { params });
  }
};
