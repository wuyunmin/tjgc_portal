import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "@/assets/style/main.less";
import { Modal, message } from "ant-design-vue";
import GModal from "@/components/CModalGlobal/index";

import CPromise from "@/utils/cPromise";
import Api from "@/api";
import * as Enums from "@/typings/enums";
import * as tools from "@/utils/tools";
import moment from "moment";
import { Input } from "ant-design-vue";

Vue.use(Modal);

Vue.config.productionTip = false;
Vue.prototype.$modal = Modal;
Vue.prototype.$cPromise = CPromise;
Vue.prototype.$api = Api;
Vue.prototype.$enums = Enums;
Vue.prototype.$tools = tools;
Vue.prototype.$message = message;
Vue.prototype.$moment = moment;
Vue.prototype.$gModal = GModal;

// 解决IE下，输入框的清除按钮无法清除
if (!!window.ActiveXObject || "ActiveXObject" in window) {
  (Input as any).methods.handleReset = function() {
    this.stateValue = "";
    this.$emit("change.value", "");
  };
}

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount("#app");
