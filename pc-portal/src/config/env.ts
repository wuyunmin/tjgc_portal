const enableProxy = process.env.NODE_ENV === "debug";

let env = (window as any).config;

if (enableProxy) {
  env = Object.assign({}, env, {
    enableProxy,
    apiHost: process.env.VUE_APP_API
  });

  (window as any).config = env;
}

export default env;
